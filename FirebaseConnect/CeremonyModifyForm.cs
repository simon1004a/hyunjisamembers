﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class CeremonyModifyForm : Form
    {
        private EditMode _editMode;
        public EditMode CurrentEditMode {
            get { return _editMode; }
            set
            {
                _editMode = value;
                switch (_editMode)
                {
                    case EditMode.INSERT_MODE: this.buttonUpdate.Text = "추가하기"; break;
                    default: this.buttonUpdate.Text = "수정하기"; break;
                }
            }
        }
        private DateTime _ceremonyDateTime;
        public DateTime CeremonyDateTime
        {
            get { return _ceremonyDateTime; }
            set
            {
                //만일 달력에서 우클릭해서 날짜를 지정한 경우는 여기에서 값을 넣어줍니다!
                _ceremonyDateTime = value;
                this.dateTimePicker1.Value = _ceremonyDateTime;
                this.dateTimePickerStartTime.Value = _ceremonyDateTime.Date + new TimeSpan(9, 0, 0);
                this.dateTimePickerEndTime.Value = dateTimePickerStartTime.Value.AddHours(1);
            }
        }

        public CeremonyModifyForm()
        {
            InitializeComponent();
            CurrentEditMode = EditMode.INSERT_MODE;

            this.comboBoxAssembly.DataSource = Enum.GetValues(typeof(AssemblyClass));
            this.comboBoxTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxTemple.SelectedIndex = 1;
        }

        public Ceremony GetCeremonyData()
        {
            var ceremony = new Ceremony();
            ceremony.UID = (int.TryParse(textBoxCeremonyID.Text, out int ceremonyID)) ? ceremonyID : 0;
            ceremony.Name = this.textBoxName.Text;
            ceremony.AssemblyCategory = (AssemblyClass)this.comboBoxAssembly.SelectedItem;
            ceremony.Date = this.dateTimePicker1.Value;
            ceremony.Temple = (LocalTemple)this.comboBoxTemple.SelectedItem;
            ceremony.StartTime = this.dateTimePickerStartTime.Value.TimeOfDay.ToString(@"hh\:mm");
            ceremony.EndTime = this.dateTimePickerEndTime.Value.TimeOfDay.ToString(@"hh\:mm");
            ceremony.Remarks = string.IsNullOrWhiteSpace(this.textBoxRemarks.Text) ? null : this.textBoxRemarks.Text;
            ceremony.Preacher = string.IsNullOrWhiteSpace(this.textBoxPreacher.Text) ? null : this.textBoxPreacher.Text;

            return ceremony;
        }
        public void SetCeremonyData(Ceremony ceremony)
        {
            this.textBoxCeremonyID.Text = ceremony.UID.ToString();
            this.textBoxName.Text = ceremony.Name;
            this.comboBoxAssembly.SelectedItem = ceremony.AssemblyCategory;
            this.dateTimePicker1.Value = ceremony.Date;
            this.comboBoxTemple.SelectedItem = ceremony.Temple;
            this.dateTimePickerStartTime.Value = ceremony.Date.Add(TimeSpan.Parse(ceremony.StartTime));
            this.dateTimePickerEndTime.Value = ceremony.Date.Add(TimeSpan.Parse(ceremony.EndTime));
            this.textBoxRemarks.Text = ceremony.Remarks;
            this.textBoxPreacher.Text = ceremony.Preacher;
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {            
            if ( string.IsNullOrWhiteSpace(textBoxName.Text) )
            {
                MessageBox.Show("행사 제목을 입력해주세요.");
                return;
            }

            var ceremony = GetCeremonyData();
            
            if (this.CurrentEditMode == EditMode.INSERT_MODE)
            {
                if (SqlManager.InsertCeremonyToSQL(ceremony))
                    this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (SqlManager.UpdateCeremonyInSQL(ceremony))
                    this.DialogResult = DialogResult.OK;
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
