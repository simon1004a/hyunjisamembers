﻿namespace HyunjisaBeliever
{
    partial class UserControlCalendar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTheDate = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonMonthView = new System.Windows.Forms.Button();
            this.buttonTotalView = new System.Windows.Forms.Button();
            this.buttonYearView = new System.Windows.Forms.Button();
            this.customCalendar1 = new HyunjisaBeliever.CustomCalendar();
            this.buttonAdminInsert = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTitle.Location = new System.Drawing.Point(25, 20);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(67, 37);
            this.labelTitle.TabIndex = 2;
            this.labelTitle.Text = "일정";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(778, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "이날의 일정";
            // 
            // labelTheDate
            // 
            this.labelTheDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTheDate.AutoSize = true;
            this.labelTheDate.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTheDate.Location = new System.Drawing.Point(778, 121);
            this.labelTheDate.Name = "labelTheDate";
            this.labelTheDate.Size = new System.Drawing.Size(88, 16);
            this.labelTheDate.TabIndex = 8;
            this.labelTheDate.Text = "2000-01-01";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(778, 150);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(275, 560);
            this.dataGridView1.TabIndex = 9;
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonInsert.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonInsert.FlatAppearance.BorderSize = 2;
            this.buttonInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonInsert.ForeColor = System.Drawing.Color.Maroon;
            this.buttonInsert.Location = new System.Drawing.Point(534, 20);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(116, 50);
            this.buttonInsert.TabIndex = 38;
            this.buttonInsert.Text = "일정 추가";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
            // 
            // buttonMonthView
            // 
            this.buttonMonthView.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonMonthView.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonMonthView.FlatAppearance.BorderSize = 2;
            this.buttonMonthView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMonthView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonMonthView.ForeColor = System.Drawing.Color.Maroon;
            this.buttonMonthView.Location = new System.Drawing.Point(656, 20);
            this.buttonMonthView.Name = "buttonMonthView";
            this.buttonMonthView.Size = new System.Drawing.Size(116, 50);
            this.buttonMonthView.TabIndex = 39;
            this.buttonMonthView.Text = "한달 일정표";
            this.buttonMonthView.UseVisualStyleBackColor = false;
            this.buttonMonthView.Click += new System.EventHandler(this.ButtonMonthView_Click);
            // 
            // buttonTotalView
            // 
            this.buttonTotalView.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonTotalView.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonTotalView.FlatAppearance.BorderSize = 2;
            this.buttonTotalView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTotalView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonTotalView.ForeColor = System.Drawing.Color.Maroon;
            this.buttonTotalView.Location = new System.Drawing.Point(914, 20);
            this.buttonTotalView.Name = "buttonTotalView";
            this.buttonTotalView.Size = new System.Drawing.Size(139, 50);
            this.buttonTotalView.TabIndex = 40;
            this.buttonTotalView.Text = "전체 일정표";
            this.buttonTotalView.UseVisualStyleBackColor = false;
            this.buttonTotalView.Click += new System.EventHandler(this.ButtonTotalView_Click);
            // 
            // buttonYearView
            // 
            this.buttonYearView.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonYearView.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonYearView.FlatAppearance.BorderSize = 2;
            this.buttonYearView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonYearView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonYearView.ForeColor = System.Drawing.Color.Maroon;
            this.buttonYearView.Location = new System.Drawing.Point(778, 20);
            this.buttonYearView.Name = "buttonYearView";
            this.buttonYearView.Size = new System.Drawing.Size(130, 50);
            this.buttonYearView.TabIndex = 41;
            this.buttonYearView.Text = "연간 일정표";
            this.buttonYearView.UseVisualStyleBackColor = false;
            this.buttonYearView.Click += new System.EventHandler(this.ButtonYearView_Click);
            // 
            // customCalendar1
            // 
            this.customCalendar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customCalendar1.Location = new System.Drawing.Point(28, 92);
            this.customCalendar1.MonthCeremonies = null;
            this.customCalendar1.Name = "customCalendar1";
            this.customCalendar1.schedulerFontSize = 0F;
            this.customCalendar1.SelectedDate = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            this.customCalendar1.Size = new System.Drawing.Size(744, 618);
            this.customCalendar1.TabIndex = 5;
            // 
            // buttonAdminInsert
            // 
            this.buttonAdminInsert.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonAdminInsert.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonAdminInsert.FlatAppearance.BorderSize = 2;
            this.buttonAdminInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdminInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonAdminInsert.ForeColor = System.Drawing.Color.Maroon;
            this.buttonAdminInsert.Location = new System.Drawing.Point(131, 20);
            this.buttonAdminInsert.Name = "buttonAdminInsert";
            this.buttonAdminInsert.Size = new System.Drawing.Size(212, 50);
            this.buttonAdminInsert.TabIndex = 42;
            this.buttonAdminInsert.Text = "자동으로 일정 추가하기(관리자)";
            this.buttonAdminInsert.UseVisualStyleBackColor = false;
            this.buttonAdminInsert.Visible = false;
            this.buttonAdminInsert.Click += new System.EventHandler(this.ButtonAdminInsert_Click);
            // 
            // UserControlCalendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.Controls.Add(this.buttonAdminInsert);
            this.Controls.Add(this.buttonYearView);
            this.Controls.Add(this.buttonTotalView);
            this.Controls.Add(this.buttonMonthView);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.labelTheDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customCalendar1);
            this.Controls.Add(this.labelTitle);
            this.Name = "UserControlCalendar";
            this.Size = new System.Drawing.Size(1060, 788);
            this.Load += new System.EventHandler(this.UserControlCalendar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private HyunjisaBeliever.CustomCalendar customCalendar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTheDate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.Button buttonMonthView;
        private System.Windows.Forms.Button buttonTotalView;
        private System.Windows.Forms.Button buttonYearView;
        public System.Windows.Forms.Button buttonAdminInsert;
    }
}
