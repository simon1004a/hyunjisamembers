﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserFamilyForm : Form
    {
        private int believerUID;
        DataTable dataTableFamily = new DataTable();

        public FamilyMember selectedFamilyMember = new FamilyMember();
        private bool _IsFamilyMemberSelectionMode;
        private bool IsFamilyMemberSelectionMode
        {
            get { return _IsFamilyMemberSelectionMode; }
            set { _IsFamilyMemberSelectionMode = value;
                this.label1.Visible = _IsFamilyMemberSelectionMode;
                this.buttonSelectConfirm.Visible = _IsFamilyMemberSelectionMode;
            }
        }

        public UserFamilyForm(int believerID, bool bFamilyMemberSelectionMode = false)
        {
            InitializeComponent();
            this.believerUID = believerID;

            IsFamilyMemberSelectionMode = bFamilyMemberSelectionMode;
        }

        private void UserFamilyForm_Load(object sender, EventArgs e)
        {
            RefreshFamilyMembers();
        }

        private void RefreshFamilyMembers()
        {
            dataTableFamily = SqlManager.SelectFamilyMembersFromSQL(this.believerUID);
            this.dataGridView1.DataSource = dataTableFamily;
        }

        private FamilyMember SelectFamilyMemberInDataTable(int rowIndex)
        {
            if (rowIndex >= dataTableFamily.Rows.Count)
                return null;

            int memberId;
            bool isMemberBeliever;
            string selectedName, selectedBirthYear, selectedRemarks, selectedRelation;

            try
            {
                memberId = Convert.ToInt32(this.dataTableFamily.Rows[rowIndex]["MemberID"]);
                isMemberBeliever = Convert.ToBoolean(Convert.ToInt32(this.dataTableFamily.Rows[rowIndex]["신도여부"]));

                //이름, 생년, 관계, 비고                    
                selectedName = this.dataTableFamily.Rows[rowIndex]["이름"].ToString();
                selectedBirthYear = this.dataTableFamily.Rows[rowIndex]["생년"].ToString();
                selectedRelation = dataTableFamily.Rows[rowIndex]["관계"].ToString();

                var remarkValue = dataTableFamily.Rows[rowIndex]["비고"];
                selectedRemarks = (remarkValue != null) ? remarkValue.ToString() : string.Empty;
            }
            catch
            {
                Console.WriteLine("Failed to convert the 이름, 생년, 관계...");
                return null;
            }

            var familyMemberInfo = new FamilyMember
            {
                MemberUID = memberId,
                IsBeliever = isMemberBeliever,
                Name = selectedName,
                BirthDate = selectedBirthYear,
                Ganzhi = MainWindow.GetGanzhi(Convert.ToDateTime(selectedBirthYear).Year),
                FamilyRelation = selectedRelation,
                Remarks = selectedRemarks,
            };

            return familyMemberInfo;
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {   
            var familyMemberInfo = SelectFamilyMemberInDataTable(e.RowIndex);
            if (familyMemberInfo == null) return;

            //With this constructor, familyUpdateForm will be set as UPDATE_MODE.
            var familyUpdateForm = new FamilyUpdateForm(believerUID, familyMemberInfo);
            if (familyUpdateForm.ShowDialog() == DialogResult.OK)
            {
                this.RefreshFamilyMembers();
            }
        }

        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow <= this.dataGridView1.RowCount - 1)
                {
                    if (dataGridView1.SelectedRows.Count == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    miniMenu.Items.Add("가족 정보 삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int iSelectedRow;
            switch (e.ClickedItem.Name)
            {
                case "Delete":
                    string deleteMessage = "";
                    var deleteIdList = new List<int>();
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        iSelectedRow = dataGridView1.SelectedRows[i].Index;
                        try
                        {
                            int memberId = Convert.ToInt32(this.dataTableFamily.Rows[iSelectedRow]["MemberID"]);
                            deleteIdList.Add(memberId);
                            deleteMessage += this.dataTableFamily.Rows[iSelectedRow]["이름"].ToString() + " ";                            
                        }
                        catch { continue; }
                    }
                    deleteMessage += " 분을 가족 구성원에서 제외하시겠습니까?";
                    var askResult = MessageBox.Show(deleteMessage, "가족원 삭제", MessageBoxButtons.YesNo);
                    if (askResult == DialogResult.Yes)
                    {
                        foreach(int id in deleteIdList)
                            SqlManager.DeleteFamilyMemberInSQL(id);
                    }
                    this.RefreshFamilyMembers();                    
                    break;
            }
        }

        private void ButtonSelectConfirm_Click(object sender, EventArgs e)
        {
            int iSelectedMemberCount = dataGridView1.SelectedRows.Count;
            if (iSelectedMemberCount <= 0)
                MessageBox.Show("선택된 사람이 아무도 없습니다.");
            else if (iSelectedMemberCount > 1)
                MessageBox.Show("한 사람만 선택해주십시오.");
            else if(iSelectedMemberCount == 1)
            {
                this.selectedFamilyMember = this.SelectFamilyMemberInDataTable(dataGridView1.SelectedRows[0].Index);
                if(selectedFamilyMember != null)
                {
                    this.DialogResult = DialogResult.OK;
                    return;
                }
            }
            this.DialogResult = DialogResult.Abort;
        }

    }
}
