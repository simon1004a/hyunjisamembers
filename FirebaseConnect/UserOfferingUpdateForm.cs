﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserOfferingUpdateForm : Form
    {
        enum OfferorType { 신도본인명의 = 0, 신도가족명의 = 1, 그외 = 2 }

        EditMode editMode;
        int savedBelieverUID = 0;
        FamilyMember registeredFamilyMember = null;

        public UserOfferingUpdateForm(Offering offering = null)
        {
            InitializeComponent();
            this.comboBoxMajesty.DataSource = Enum.GetValues(typeof(MajestyBuddha));
            this.comboBoxOfferingTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxOfferorClass.DataSource = Enum.GetValues(typeof(OfferorType));

            if (offering == null)
            {
                editMode = EditMode.INSERT_MODE;
                this.comboBoxOfferorClass.SelectedIndex = (int)OfferorType.신도본인명의;
            }
            else
            {
                editMode = EditMode.UPDATE_MODE;
                if (offering.FamilyMemberUID != 0)
                    this.comboBoxOfferorClass.SelectedIndex = (int)OfferorType.신도가족명의;
                else 
                {
                    if( offering.believerUID != 0
                     && SqlManager.SelectBelieverFromSQL(offering.believerUID).Name == offering.OfferorName)
                        this.comboBoxOfferorClass.SelectedIndex = (int)OfferorType.신도본인명의;
                    else
                        this.comboBoxOfferorClass.SelectedIndex = (int)OfferorType.그외;
                }
                this.SetOfferingInfo(offering);                
            }
        }

        private void UserOfferingUpdateForm_Load(object sender, EventArgs e)
        {
            if (editMode == EditMode.INSERT_MODE)
            {
                this.comboBoxOfferingTemple.SelectedItem = MainWindow.manageArea;
            }
        }

        private void SetOfferingInfo(Offering _offering)
        {
            this.textBoxID.Text = _offering.UID.ToString();
            this.comboBoxMajesty.SelectedItem = _offering.buddha;
            this.comboBoxOfferingTemple.SelectedItem = _offering.OfferingTemple;
            this.dateTimePickerOfferingDay.Value = _offering.DateOfOffer;

            this.SelectBelieverID(_offering.believerUID);
            this.SetFamilyMemberInfo(SqlManager.SelectFamilyMemberFromSQL(_offering.FamilyMemberUID));

            this.textBoxOfferorRelation.Text = _offering.OfferorRelation;
            this.textBoxOfferorName.Text = _offering.OfferorName;
            this.textBoxOfferorBirthYear.Text = _offering.OfferorBirthYear;
            this.textBoxOfferorAddress.Text = _offering.OfferorAddress;
            this.textBoxOfferorPhone.Text = _offering.OfferorPhone;
            this.textBoxDonator.Text = _offering.Donator;

            this.textBoxRequest.Text = _offering.Request;
            this.textBoxParticipants.Text = _offering.Participants;
            this.textBoxRemarks.Text = _offering.Remarks;
        }

        public Offering GetOfferingInfo()
        {
            var _UID = (int.TryParse(textBoxID.Text, out int prayID)) ? prayID : 0;
            var _buddha = (MajestyBuddha)Enum.Parse(typeof(MajestyBuddha), comboBoxMajesty.SelectedItem.ToString());
            var _OfferingTemple = (LocalTemple)comboBoxOfferingTemple.SelectedItem;
            var _DateOfOffer = dateTimePickerOfferingDay.Value.Date;
            var _believerUID = savedBelieverUID;
            var _FamilyMemberUID = (registeredFamilyMember != null) ? registeredFamilyMember.MemberUID : 0;
            var _OfferorRelation = textBoxOfferorRelation.Text;
            var _OfferorName = textBoxOfferorName.Text;
            var _OfferorBirthYear = textBoxOfferorBirthYear.Text;
            var _OfferorAddress = textBoxOfferorAddress.Text;
            var _OfferorPhone = textBoxOfferorPhone.Text;            
            var _Donator = textBoxDonator.Text;
            var _Request = textBoxRequest.Text;
            var _Participants = textBoxParticipants.Text;
            var _Remarks = textBoxRemarks.Text;                          
            var _CeremonyID = 0;

            var offering = new Offering
            {
                UID = _UID,
                buddha = _buddha,
                OfferingTemple = _OfferingTemple,
                DateOfOffer = _DateOfOffer,
                believerUID = _believerUID,
                FamilyMemberUID = _FamilyMemberUID,
                OfferorRelation = _OfferorRelation,
                OfferorName = _OfferorName,
                OfferorBirthYear = _OfferorBirthYear,
                OfferorAddress = _OfferorAddress,
                OfferorPhone = _OfferorPhone,
                Donator = _Donator,
                Request = _Request,
                Participants = _Participants,
                Remarks = _Remarks,
                CeremonyID = _CeremonyID,                
            };

            return offering;
        }


        private void SelectBelieverID(int believerID)
        {
            this.savedBelieverUID = believerID;
            if (savedBelieverUID == 0)
            {
                this.textBoxBelieverName.ResetText();
                this.textBoxFamilyMemberName.ResetText();
                this.textBoxOfferorRelation.ResetText();
            }
            else
            {
                try
                {
                    var believer = SqlManager.SelectBelieverFromSQL(believerID);
                    this.textBoxBelieverName.Text = believer.Name;

                    var offerorClass = (OfferorType)this.comboBoxOfferorClass.SelectedItem;
                    if (offerorClass == OfferorType.신도본인명의)
                    {
                        this.textBoxFamilyMemberName.Text = believer.Name;
                        this.textBoxOfferorRelation.Text = "본인";

                        this.textBoxOfferorName.Text = believer.Name;
                        this.textBoxOfferorBirthYear.Text = believer.Ganzhi;
                        this.textBoxOfferorAddress.Text = believer.AddressProvince.ToString() + " " + believer.AddressDetail;                        
                    }
                    else
                    {
                        //신도가 새로 지정되었으니 가족 정보는 초기화한다. 
                        this.textBoxFamilyMemberName.ResetText();
                        this.textBoxOfferorRelation.ResetText();                        
                    }
                    
                    if (offerorClass == OfferorType.신도가족명의)
                    {                        
                        //가족의 주소 정보가 따로 입력되어 있지 않다면 여기서 신도 주소로 업데이트 하자. 
                        if (string.IsNullOrWhiteSpace(this.textBoxOfferorAddress.Text))
                            this.textBoxOfferorAddress.Text = believer.AddressProvince.ToString() + " " + believer.AddressDetail;
                    }

                    //복위자의 입금자, 연락처는 신도나 가족의 경우 신도 명의로 일단 해 둔다.
                    if (offerorClass != OfferorType.그외)
                    {
                        if (string.IsNullOrWhiteSpace(textBoxDonator.Text))
                            this.textBoxDonator.Text = believer.Name;
                        if (string.IsNullOrWhiteSpace(textBoxOfferorPhone.Text))
                            this.textBoxOfferorPhone.Text = believer.Phone;

                    }

                }
                catch { MessageBox.Show("신도 정보가 올바르지 않습니다. 다시 입력해주세요."); }
            }
        }

        private void SetFamilyMemberInfo(FamilyMember memberInfo)
        {
            this.registeredFamilyMember = memberInfo;
            if (memberInfo != null)
            {
                this.textBoxFamilyMemberName.Text = memberInfo.Name;
                this.textBoxOfferorRelation.Text = memberInfo.FamilyRelation;

                this.textBoxOfferorName.Text = memberInfo.Name;
                this.textBoxOfferorBirthYear.Text = memberInfo.Ganzhi;
            }
        }

        private void ButtonBelieverSearch_Click(object sender, EventArgs e)
        {
            var believerSearchForm = new BelieverSearchForm();
            believerSearchForm.EventSelectBelieverID += this.SelectBelieverID;
            if (believerSearchForm.ShowDialog() == DialogResult.OK)
            {
                believerSearchForm.Close();
            }
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.textBoxOfferorName.Text))
            {
                MessageBox.Show("복위자가 입력되지 않았습니다.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(this.textBoxRequest.Text))
            {
                MessageBox.Show("발원이 입력되지 않았습니다.");
                return;
            }
            else if ((LocalTemple)this.comboBoxOfferingTemple.SelectedItem == LocalTemple.미지정)
            {
                MessageBox.Show("접수 분원이 정해지지 않았습니다.");
            }

            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.공양담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //----------accessedMonk auth check part----------//            
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)comboBoxOfferingTemple.SelectedItem) )
                {
                    MessageBox.Show("다른 분원의 공양 정보는 입력할 수 없습니다.");
                    return;
                }
            }

            if (this.editMode == EditMode.INSERT_MODE)
            {
                if (SqlManager.InsertOfferingToSQL(GetOfferingInfo()))
                    this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (SqlManager.UpdateOfferingInSQL(GetOfferingInfo()))
                    this.DialogResult = DialogResult.OK;
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboBoxOfferingTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainWindow.accessedMonk != null
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)comboBoxOfferingTemple.SelectedItem) )
                {
                    if ((LocalTemple)comboBoxOfferingTemple.SelectedItem != LocalTemple.미지정)
                        MessageBox.Show("다른 분원의 공양은 입력할 수 없습니다.");

                    comboBoxOfferingTemple.SelectedItem = MainWindow.manageArea;
                }
            }
        }

        private void buttonFamilySearch_Click(object sender, EventArgs e)
        {
            if (this.savedBelieverUID == 0)
            {
                MessageBox.Show("먼저 신도 가족부터 입력해주십시오.");
                this.textBoxFamilyMemberName.ResetText();
                this.textBoxOfferorRelation.ResetText();
                return;
            }

            var userFamilyInfo = new UserFamilyForm(savedBelieverUID, bFamilyMemberSelectionMode: true);
            if (userFamilyInfo.ShowDialog() == DialogResult.OK)
            {
                this.SetFamilyMemberInfo(userFamilyInfo.selectedFamilyMember);
                userFamilyInfo.Close();
            }

        }
                

        private void comboBoxOfferorClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxOfferorClass.SelectedItem)
            {
                case OfferorType.신도본인명의:
                    //신도 본인 명의로 입재하는 경우!
                    EnableBelieverInputArea(true);
                    EnableFamilyInputArea(false);
                    this.textBoxOfferorRelation.Text = "본인";
                    this.textBoxOfferorName.ReadOnly = true;
                    this.textBoxOfferorBirthYear.ReadOnly = true;
                    this.textBoxOfferorAddress.ReadOnly = true;
                    break;
                case OfferorType.신도가족명의:
                    //신도 가족 명의로 입재하는 경우!
                    EnableBelieverInputArea(true);
                    EnableFamilyInputArea(true);
                    this.textBoxOfferorName.ReadOnly = true;
                    this.textBoxOfferorBirthYear.ReadOnly = true;
                    this.textBoxOfferorAddress.ReadOnly = false;
                    break;
                case OfferorType.그외:
                    //그외 경우
                    EnableBelieverInputArea(true);  //신도 지인일 경우 신도를 입력해야 하므로 풀어준다.
                    EnableFamilyInputArea(false);
                    this.textBoxOfferorName.ReadOnly = false;
                    this.textBoxOfferorBirthYear.ReadOnly = false;
                    this.textBoxOfferorAddress.ReadOnly = false;
                    break;
            }
        }

        private void EnableBelieverInputArea(bool bActive)
        {
            this.buttonBelieverSearch.Enabled = bActive;
            this.textBoxBelieverName.Enabled = bActive;
            if (!bActive)
            {
                this.savedBelieverUID = 0;
                this.textBoxBelieverName.ResetText();
            }
        }

        private void EnableFamilyInputArea(bool bActive)
        {
            this.buttonFamilySearch.Enabled = bActive;
            this.textBoxFamilyMemberName.ResetText();
            this.textBoxFamilyMemberName.Enabled = bActive;
            this.textBoxOfferorRelation.ResetText();
        }

        
    }
}
