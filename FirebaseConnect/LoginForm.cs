﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{    
    public partial class LoginForm : Form
    {
        public delegate void LoginEventHandler(Monk monkData);
        public event LoginEventHandler EventLoginMonkID;
                
        public LoginForm()
        {            
            InitializeComponent();
        }

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxID.Text) || string.IsNullOrWhiteSpace(textBoxPassword.Text))
            {
                MessageBox.Show("ID와 비밀번호를 모두 다 입력해주십시오.");
                return;
            }

            string username = textBoxID.Text;
            string password = textBoxPassword.Text;
            Monk monkSignedIn = SqlManager.SearchMonkInSQL(username, password);
            if (monkSignedIn != null)
            {
                if (monkSignedIn.LevelOfAccess != AccessLevel.미허가자)
                {
                    this.DialogResult = DialogResult.OK;
                    if (EventLoginMonkID != null)
                        EventLoginMonkID(monkSignedIn);
                }
                else
                {
                    MessageBox.Show("아직 총 관리자로부터 사용 권한을 부여받지 못했습니다. 총관리자에게 연락을 취해주십시오.");
                }

            }
            else
            {                
                this.textBoxPassword.Clear();
            }
        }

        private void LabelJoin_Click(object sender, EventArgs e)
        {
            var adminJoin = new AdminInfoForm();
            if (adminJoin.ShowDialog() == DialogResult.OK)
            {
                adminJoin.Close();                
                MessageBox.Show("관리자 가입이 신청되었습니다. 총괄 관리자의 연락을 기다려주십시오. ");
            }
        }

        private void TextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.ButtonLogin_Click(sender,e);
            }
        }
    }
}
