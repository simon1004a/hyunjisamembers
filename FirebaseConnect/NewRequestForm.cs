﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class NewRequestForm : Form
    {
        public DateTime DateOfRequest { get { return this.dateTimePickerRequest.Value; } }
        public string NewRequest { get { return this.textBoxNewRequest.Text; } }
        public string Remarks { get { return this.textBoxRemarks.Text; } }
        public NewRequestForm()
        {
            InitializeComponent();
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(NewRequest))
                this.DialogResult = DialogResult.OK;
            else
                MessageBox.Show("발원이 입력되지 않았습니다.");

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
