﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class CeremonyViewForm : Form
    {
        public delegate void DateTimeEventHandler(DateTime? dateTime = null);        
        public event DateTimeEventHandler EventRefreshCeremonyList;
        DateTime ceremonyStartDate, ceremonyEndDate;
        
        public CeremonyViewForm(DateTime theDate)
        {
            InitializeComponent();
            RefreshCeremonyDataInGridView(theDate, theDate);
        }

        public CeremonyViewForm(DateTime beginDate, DateTime endDate)
        {
            InitializeComponent();
            RefreshCeremonyDataInGridView(beginDate, endDate);
        }

        private void RefreshCeremonyDataInGridView(DateTime beginDate, DateTime endDate)
        {
            this.ceremonyStartDate = beginDate;
            this.ceremonyEndDate = endDate;

            var dataTable = SqlManager.SelectCeremonyDataTableFromSQL(ceremonyStartDate, ceremonyEndDate);
            this.dataGridView1.DataSource = dataTable;

            if (this.EventRefreshCeremonyList != null)
                EventRefreshCeremonyList(null);
        }

        private void RefreshCeremonyDataInGridView()
        {
            RefreshCeremonyDataInGridView(this.ceremonyStartDate, this.ceremonyEndDate);
        }

        private Ceremony GetCeremonyDataFromSqlByDataGridViewRow(int iRow)
        {
            DataGridViewRow row = this.dataGridView1.Rows[iRow];
            if (row.IsNewRow) return null;

            int ceremonyID = Convert.ToInt32(row.Cells[0].Value);
            var selectedCeremony = SqlManager.SelectCeremonyFromSQL(ceremonyID);

            return selectedCeremony;
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < this.dataGridView1.RowCount)
            {
                var ceremonyData = this.GetCeremonyDataFromSqlByDataGridViewRow(e.RowIndex);
                if (ceremonyData != null)
                {
                    var ceremonyModifyForm = new CeremonyModifyForm { CurrentEditMode = EditMode.UPDATE_MODE };
                    ceremonyModifyForm.SetCeremonyData(ceremonyData);
                    if (ceremonyModifyForm.ShowDialog() == DialogResult.OK)
                    {
                        RefreshCeremonyDataInGridView();
                        ceremonyModifyForm.Close();
                    }
                }

            }            
        }

        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridView1.RowCount)
                {
                    if (dataGridView1.SelectedRows.Count == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    //if user selected multiple rows, only the deletion is available.
                    if (dataGridView1.SelectedRows.Count <= 1)
                        miniMenu.Items.Add("수정하기").Name = "Modify";
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "Modify":
                    int iSelectedRow = dataGridView1.SelectedRows[0].Index;
                    var ceremonyData = this.GetCeremonyDataFromSqlByDataGridViewRow(iSelectedRow);
                    if (ceremonyData != null)
                    {
                        var ceremonyModifyForm = new CeremonyModifyForm { CurrentEditMode = EditMode.UPDATE_MODE };
                        ceremonyModifyForm.SetCeremonyData(ceremonyData);
                        if (ceremonyModifyForm.ShowDialog() == DialogResult.OK)
                        {
                            RefreshCeremonyDataInGridView();
                            ceremonyModifyForm.Close();
                        }
                    }
                    break;
                case "Delete":
                    var deleteIdList = new List<int>();
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        int iSelectedRowIndex = dataGridView1.SelectedRows[i].Index;
                        int believerID = Convert.ToInt32(dataGridView1.Rows[iSelectedRowIndex].Cells[0].Value);
                        deleteIdList.Add(believerID);
                    }

                    deleteIdList.Sort();
                    string deleteMessage = (deleteIdList.Count > 1) ? "다음 ID의 일정들을 삭제하시겠습니까?" : "다음 ID의 일정을 삭제하시겠습니까?";
                    string deleteList = string.Empty;
                    for (int j = 0; j < deleteIdList.Count; j++)
                    {
                        string divider = (deleteIdList.Count > 1 && j != 0) ? ", " : "";
                        deleteList += (divider + deleteIdList[j].ToString());
                    }

                    DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 일정 ID : " + deleteList, "ID 삭제", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        for (int k = 0; k < deleteIdList.Count; k++)
                        {
                            SqlManager.DeleteCeremonyInSQL(deleteIdList[k]);
                        }
                        RefreshCeremonyDataInGridView();
                    }
                    break;
            }
        }
    }
}
