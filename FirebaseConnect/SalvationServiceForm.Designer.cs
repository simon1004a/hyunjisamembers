﻿namespace HyunjisaBeliever
{
    partial class SalvationServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxRequest = new System.Windows.Forms.TextBox();
            this.buttonBelieverSearch = new System.Windows.Forms.Button();
            this.dateTimePickerSalvationService = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBelieverPhone = new System.Windows.Forms.TextBox();
            this.textBoxBelieverName = new System.Windows.Forms.TextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.comboBoxServiceTemple = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonCancel.FlatAppearance.BorderSize = 2;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCancel.Location = new System.Drawing.Point(339, 559);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(116, 50);
            this.buttonCancel.TabIndex = 162;
            this.buttonCancel.Text = "취소";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.FlatAppearance.BorderSize = 2;
            this.buttonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonUpdate.ForeColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.Location = new System.Drawing.Point(199, 559);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(116, 50);
            this.buttonUpdate.TabIndex = 161;
            this.buttonUpdate.Text = "확인";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label9.Location = new System.Drawing.Point(19, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 31);
            this.label9.TabIndex = 160;
            this.label9.Text = "발원";
            // 
            // textBoxRequest
            // 
            this.textBoxRequest.Location = new System.Drawing.Point(25, 141);
            this.textBoxRequest.Multiline = true;
            this.textBoxRequest.Name = "textBoxRequest";
            this.textBoxRequest.Size = new System.Drawing.Size(606, 257);
            this.textBoxRequest.TabIndex = 159;
            // 
            // buttonBelieverSearch
            // 
            this.buttonBelieverSearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonBelieverSearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.FlatAppearance.BorderSize = 2;
            this.buttonBelieverSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBelieverSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonBelieverSearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.Location = new System.Drawing.Point(115, 46);
            this.buttonBelieverSearch.Name = "buttonBelieverSearch";
            this.buttonBelieverSearch.Size = new System.Drawing.Size(82, 61);
            this.buttonBelieverSearch.TabIndex = 157;
            this.buttonBelieverSearch.Text = "신도검색";
            this.buttonBelieverSearch.UseVisualStyleBackColor = false;
            this.buttonBelieverSearch.Click += new System.EventHandler(this.ButtonBelieverSearch_Click);
            // 
            // dateTimePickerSalvationService
            // 
            this.dateTimePickerSalvationService.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePickerSalvationService.Location = new System.Drawing.Point(429, 82);
            this.dateTimePickerSalvationService.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerSalvationService.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerSalvationService.Name = "dateTimePickerSalvationService";
            this.dateTimePickerSalvationService.Size = new System.Drawing.Size(202, 24);
            this.dateTimePickerSalvationService.TabIndex = 155;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(336, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 153;
            this.label5.Text = "천도일";
            // 
            // textBoxBelieverPhone
            // 
            this.textBoxBelieverPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxBelieverPhone.Location = new System.Drawing.Point(203, 83);
            this.textBoxBelieverPhone.Name = "textBoxBelieverPhone";
            this.textBoxBelieverPhone.ReadOnly = true;
            this.textBoxBelieverPhone.Size = new System.Drawing.Size(114, 24);
            this.textBoxBelieverPhone.TabIndex = 151;
            // 
            // textBoxBelieverName
            // 
            this.textBoxBelieverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxBelieverName.Location = new System.Drawing.Point(203, 46);
            this.textBoxBelieverName.Name = "textBoxBelieverName";
            this.textBoxBelieverName.ReadOnly = true;
            this.textBoxBelieverName.Size = new System.Drawing.Size(114, 24);
            this.textBoxBelieverName.TabIndex = 152;
            // 
            // textBoxID
            // 
            this.textBoxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxID.Location = new System.Drawing.Point(115, 12);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(202, 24);
            this.textBoxID.TabIndex = 150;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(22, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 149;
            this.label3.Text = "신도 이름";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(22, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 148;
            this.label4.Text = "신도 전화";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(22, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 18);
            this.label1.TabIndex = 146;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 401);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 18);
            this.label2.TabIndex = 164;
            this.label2.Text = "비고";
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Location = new System.Drawing.Point(25, 422);
            this.textBoxRemarks.Multiline = true;
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(606, 131);
            this.textBoxRemarks.TabIndex = 163;
            // 
            // comboBoxServiceTemple
            // 
            this.comboBoxServiceTemple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxServiceTemple.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxServiceTemple.FormattingEnabled = true;
            this.comboBoxServiceTemple.ItemHeight = 18;
            this.comboBoxServiceTemple.Location = new System.Drawing.Point(429, 46);
            this.comboBoxServiceTemple.Name = "comboBoxServiceTemple";
            this.comboBoxServiceTemple.Size = new System.Drawing.Size(202, 26);
            this.comboBoxServiceTemple.TabIndex = 166;
            this.comboBoxServiceTemple.SelectedIndexChanged += new System.EventHandler(this.ComboBoxServiceTemple_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(336, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 18);
            this.label7.TabIndex = 165;
            this.label7.Text = "분원";
            // 
            // SalvationServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(652, 621);
            this.Controls.Add(this.comboBoxServiceTemple);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxRemarks);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxRequest);
            this.Controls.Add(this.buttonBelieverSearch);
            this.Controls.Add(this.dateTimePickerSalvationService);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxBelieverPhone);
            this.Controls.Add(this.textBoxBelieverName);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Name = "SalvationServiceForm";
            this.Text = "천도재 입력하기";
            this.Load += new System.EventHandler(this.SalvationServiceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxRequest;
        private System.Windows.Forms.Button buttonBelieverSearch;
        public System.Windows.Forms.DateTimePicker dateTimePickerSalvationService;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBoxBelieverPhone;
        public System.Windows.Forms.TextBox textBoxBelieverName;
        public System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRemarks;
        public System.Windows.Forms.ComboBox comboBoxServiceTemple;
        private System.Windows.Forms.Label label7;
    }
}