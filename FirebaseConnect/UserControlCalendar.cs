﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserControlCalendar : UserControl
    {
        public static List<Ceremony> monthCeremonyList;
        public static DataTable dayScheduleTable;
        public DateTime referenceDate;

        public UserControlCalendar()
        {
            InitializeComponent();
            if (monthCeremonyList == null) monthCeremonyList = new List<Ceremony>();
            InitScheduleTable();

            this.customCalendar1.EventDateChanged += ChangeDateInCalendar;
            this.customCalendar1.EventUpdateSchedule += RefreshCeremonyListInCalendar;
        }


        private void UserControlCalendar_Load(object sender, EventArgs e)
        {
            referenceDate = DateTime.Now.Date;
            RefreshCeremonyListInCalendar(referenceDate);
        }

        private void InitScheduleTable()
        {
            if (dayScheduleTable == null)
                dayScheduleTable = new DataTable();

            dayScheduleTable.Clear();
            dayScheduleTable.Rows.Clear();
            dayScheduleTable.Columns.Clear();
            dayScheduleTable.Columns.Add("Time");            
            dayScheduleTable.Columns.Add("Schedule");
            var scheduleTime = new TimeSpan(0, 00, 00);
            for (int i = 1; i <= 24; i++)
            {
                var strTime = scheduleTime.ToString(@"hh\:mm");
                dayScheduleTable.Rows.Add(strTime);
                scheduleTime = scheduleTime.Add(new TimeSpan(1, 00, 00));
            }
            dayScheduleTable.Rows.Add("24:00", string.Empty);
        }

        private bool UpdateMonthCeremonyList(DateTime dateTime)
        {
            bool bUpdated = false;
            if ( referenceDate == null || referenceDate.Month != dateTime.Month )
            {                
                this.UpdateMonthCeremonyListFromSQL(dateTime);
                bUpdated = true;
            }
            referenceDate = dateTime.Date;
            customCalendar1.SelectedDate = dateTime.Date;

            return bUpdated;
        }

        private void UpdateMonthCeremonyListFromSQL(DateTime dateTime)
        {            
            monthCeremonyList.Clear();
            monthCeremonyList = SqlManager.SelectCeremonyListFromSQL(dateTime);
            customCalendar1.MonthCeremonies = monthCeremonyList;            
        }

        private void UpdateScheduleTable(DateTime dateTime)
        {            
            labelTheDate.Text = dateTime.ToShortDateString();

            dayScheduleTable.Clear();            
            var ceremonyInDay = Ceremony.GetCeremonyOfTheDay(monthCeremonyList, dateTime.Date);
            if (ceremonyInDay != null)
            {
                InitScheduleTable();
                for (int i = 0; i < ceremonyInDay.Count; i++)
                {
                    string scheduleName = ceremonyInDay[i].Name;                    
                    if (!string.IsNullOrWhiteSpace(scheduleName) )
                    {
                        TimeSpan startTime, endTime;
                        try { startTime = TimeSpan.Parse(ceremonyInDay[i].StartTime); }
                        catch { startTime = new TimeSpan(9, 0, 0); Console.WriteLine("Failed to get startTime so init with 9:00"); }
                        try { endTime = TimeSpan.Parse(ceremonyInDay[i].EndTime); }
                        catch { endTime = startTime.Add(new TimeSpan(1, 0, 0)); Console.WriteLine("Failed to get endTime so init with 10:00"); }
                        for (int j = 0; j <= 24; j++)
                        {
                            TimeSpan tableTime;
                            if (j != 24)
                            {
                                try { tableTime = TimeSpan.Parse(dataGridView1.Rows[j].Cells[0].Value.ToString()); }
                                catch { Console.WriteLine("Failed to get a tableTime"); continue; }
                            }
                            else
                                tableTime = new TimeSpan(23, 59, 59);
                                                        
                            if ( startTime <= tableTime && tableTime < endTime )
                            {
                                var cellContent = dataGridView1.Rows[j].Cells[1].Value.ToString();
                                dataGridView1.Rows[j].Cells[1].Value = string.IsNullOrWhiteSpace(cellContent)
                                    ? scheduleName : cellContent + "/" + scheduleName;
                            }
                        }
                    }
                }
            }
                        
            this.dataGridView1.DataSource = dayScheduleTable;
            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        
        private void ChangeDateInCalendar(DateTime? dateTime)
        {
            UpdateMonthCeremonyList((DateTime)dateTime);
            UpdateScheduleTable((DateTime)dateTime);
        }
        
        public void RefreshCeremonyListInCalendar(DateTime? _dateTime = null)
        {
            DateTime dateTime;
            if (_dateTime == null)
                dateTime = referenceDate;                            
            else
                dateTime = (DateTime)_dateTime;                
            
            UpdateMonthCeremonyListFromSQL(dateTime);
            customCalendar1.SelectDate(dateTime);
            UpdateScheduleTable(dateTime);
        }

        private void ButtonInsert_Click(object sender, EventArgs e)
        {
            var dateTime = customCalendar1.SelectedDate;
            var ceremonyInsertForm = new CeremonyModifyForm { CeremonyDateTime = dateTime };
            if (ceremonyInsertForm.ShowDialog() == DialogResult.OK)
            {
                RefreshCeremonyListInCalendar(dateTime);
                ceremonyInsertForm.Close();
            }
        }
        
        private void ButtonMonthView_Click(object sender, EventArgs e)
        {
            DateTime dateBegin = new DateTime(referenceDate.Year, referenceDate.Month, 1);
            int endDay = DateTime.DaysInMonth(referenceDate.Year, referenceDate.Month);
            DateTime dateEnd = new DateTime(referenceDate.Year, referenceDate.Month, endDay);

            var monthCeremonyView = new CeremonyViewForm(dateBegin, dateEnd);
            monthCeremonyView.EventRefreshCeremonyList += RefreshCeremonyListInCalendar;
            monthCeremonyView.ShowDialog();
        }

        private void ButtonYearView_Click(object sender, EventArgs e)
        {
            var dateBegin = new DateTime(referenceDate.Year, 1, 1);
            var dateEnd = new DateTime(referenceDate.Year, 12, 31);
            var yearCeremonyView = new CeremonyViewForm(dateBegin, dateEnd);
            yearCeremonyView.EventRefreshCeremonyList += RefreshCeremonyListInCalendar;
            yearCeremonyView.ShowDialog();
        }

        private void ButtonTotalView_Click(object sender, EventArgs e)
        {
            var everyCeremonyView = new CeremonyViewForm(DateTime.MinValue, DateTime.MaxValue);
            everyCeremonyView.EventRefreshCeremonyList += RefreshCeremonyListInCalendar;
            everyCeremonyView.ShowDialog();
        }

        private void ButtonAdminInsert_Click(object sender, EventArgs e)
        {
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                MessageBox.Show("총괄 관리자만 접근할 수 있는 기능입니다. ");
                return;
            }

            var ceremonyAdminInsertForm = new CeremonyAdminInsertForm();
            if (ceremonyAdminInsertForm.ShowDialog() == DialogResult.OK)
            {
                RefreshCeremonyListInCalendar();
                ceremonyAdminInsertForm.Close();
            }

        }
    }
}
