﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserControlSalvation : UserControl
    {
        public UserControlSalvation()
        {
            InitializeComponent();
            this.comboBoxServiceTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
        }

        private void UserControlSalvation_Load(object sender, EventArgs e)
        {
            RefreshSalvationServiceListInDataGridView(true);
        }
        private void ResetServiceListSearchCondition()
        {
            if (MainWindow.accessedMonk != null)
                this.comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;

            this.textBoxBeliever.ResetText();            
            this.dateTimePickerServiceDay.Value = dateTimePickerServiceDay.MinDate;
        }

        public void RefreshSalvationServiceListInDataGridView(bool bResetSearchConditions = false)
        {
            if (MainWindow.accessedMonk == null) return;

            var searchConditions = new List<string>();
            if (bResetSearchConditions)
            {
                ResetServiceListSearchCondition();
            }

            string serviceTemple = (string.IsNullOrWhiteSpace(comboBoxServiceTemple.Text)) ? LocalTemple.전체.ToString() : comboBoxServiceTemple.Text;

            if ( serviceTemple != LocalTemple.전체.ToString()
              && MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)Enum.Parse(typeof(LocalTemple), serviceTemple)) )
                searchConditions.Add(" ServiceTempleID = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), serviceTemple))));
            else
            {
                //----------accessedMonk auth check part----------//
                if ( MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자
                  && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님)
                {
                    comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;
                    MessageBox.Show("소속 분원에 등록된 천도재만 검색할 수 있습니다.");
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(textBoxBeliever.Text))
                searchConditions.Add(" believer IN (SELECT BelieverID FROM Believer WHERE Name LIKE '%" + textBoxBeliever.Text + "%') ");

            if (dateTimePickerServiceDay.Value != dateTimePickerServiceDay.MinDate)
            {
                DateTime pickedDate = dateTimePickerServiceDay.Value;
                DateTime firstDay = new DateTime(pickedDate.Year, pickedDate.Month, 1);
                int endDay = DateTime.DaysInMonth(pickedDate.Year, pickedDate.Month);
                DateTime lastDay = new DateTime(pickedDate.Year, pickedDate.Month, endDay);
                searchConditions.Add(" DateOfService BETWEEN '" + firstDay.ToShortDateString() + "' AND '" + lastDay.ToShortDateString() + "' ");
            }

            int iSelectedIndex = (dataGridView1.SelectedRows.Count > 0) ? dataGridView1.SelectedRows[0].Index : 0;

            if (searchConditions.Count != 0)
                dataGridView1.DataSource = SqlManager.SelectSalvationServiceListFromSQL(searchConditions.ToArray());
            else
                dataGridView1.DataSource = SqlManager.SelectSalvationServiceListFromSQL();

            this.labelRowCount.Text = string.Format("총 {0}개의 데이터가 검색되었습니다. ", dataGridView1.Rows.Count - 1);

            //현재 selected row로 돌아오자.
            if (iSelectedIndex != 0 && iSelectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = iSelectedIndex;
                dataGridView1.CurrentCell = dataGridView1.Rows[iSelectedIndex].Cells[0];
            }
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshSalvationServiceListInDataGridView(true);
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            RefreshSalvationServiceListInDataGridView(false);
        }

        private void ButtonAddNew_Click(object sender, EventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.천도재담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            var salvationServiceUpdateForm = new SalvationServiceForm{ editMode = EditMode.INSERT_MODE };
            if (salvationServiceUpdateForm.ShowDialog() == DialogResult.OK)
            {
                RefreshSalvationServiceListInDataGridView();
                salvationServiceUpdateForm.Close();
            }
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < this.dataGridView1.RowCount - 1)
            {
                SalvationService serviceData = this.GetServiceDataFromSqlByDataGridViewRow(e.RowIndex);
                if (serviceData == null) return;
                var salvationServiceUpdateForm = new SalvationServiceForm { serviceInfo = serviceData };

                if (salvationServiceUpdateForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshSalvationServiceListInDataGridView();
                    salvationServiceUpdateForm.Close();
                }
            }
            else if (e.RowIndex == this.dataGridView1.RowCount - 1)
            {
                var salvationServiceInsertForm = new SalvationServiceForm { editMode = EditMode.INSERT_MODE};
                if (salvationServiceInsertForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshSalvationServiceListInDataGridView();
                    salvationServiceInsertForm.Close();
                }
            }
        }

        private SalvationService GetServiceDataFromSqlByDataGridViewRow(int rowIndex)
        {
            SalvationService service = null;
            if (rowIndex >= 0 && rowIndex < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
                if (row.IsNewRow) return null;

                int serviceID = Convert.ToInt32(row.Cells[0].Value);
                service = SqlManager.SelectSalvationServiceFromSQL(serviceID);
            }
            return service;
        }

        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridView1.RowCount - 1)
                {
                    if (dataGridView1.SelectedRows.Count == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    //if user selected multiple rows, only the deletion is available.
                    if (dataGridView1.SelectedRows.Count <= 1)
                    {
                        miniMenu.Items.Add("수정하기").Name = "Modify";
                        miniMenu.Items.Add("문자보내기").Name = "SendSMS";
                    }
                        
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.천도재담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            int iSelectedRow;
            SalvationService serviceData;

            switch (e.ClickedItem.Name)
            {
                case "SendSMS":
                    iSelectedRow = dataGridView1.SelectedRows[0].Index;
                    serviceData = this.GetServiceDataFromSqlByDataGridViewRow(iSelectedRow);
                    if (serviceData == null) return;
                    var believer = SqlManager.SelectBelieverFromSQL(serviceData.believerUID);
                    if (believer != null)
                    {
                        var smsSendForm = new SMSSendingForm(believer);
                        if (smsSendForm.ShowDialog() == DialogResult.OK)
                            smsSendForm.Close();
                    }
                    break;
                case "Modify":
                    iSelectedRow = dataGridView1.SelectedRows[0].Index;
                    serviceData = this.GetServiceDataFromSqlByDataGridViewRow(iSelectedRow);
                    if (serviceData == null) return;
                    var salvationServiceUpdateForm = new SalvationServiceForm { serviceInfo = serviceData };

                    if (salvationServiceUpdateForm.ShowDialog() == DialogResult.OK)
                    {
                        RefreshSalvationServiceListInDataGridView();
                        salvationServiceUpdateForm.Close();
                    }
                    break;
                case "Delete":
                    var deleteIdList = new List<int>();
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        int iSelectedRowIndex = dataGridView1.SelectedRows[i].Index;
                        int serviceID = Convert.ToInt32(dataGridView1.Rows[iSelectedRowIndex].Cells[0].Value);
                        deleteIdList.Add(serviceID);
                    }

                    deleteIdList.Sort();
                    string deleteMessage = "다음 ID의 천도재 정보를 삭제하시겠습니까?";
                    string deleteList = string.Empty;
                    for (int j = 0; j < deleteIdList.Count; j++)
                    {
                        string divider = (deleteIdList.Count > 1 && j != 0) ? ", " : "";
                        deleteList += (divider + deleteIdList[j].ToString());
                    }

                    DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 ID : " + deleteList, "ID 삭제", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        for (int k = 0; k < deleteIdList.Count; k++)
                        {
                            SqlManager.DeleteSalvationServiceInSQL(deleteIdList[k]);
                        }
                        RefreshSalvationServiceListInDataGridView();
                    }
                    break;
            }
        }

        private void ComboBoxServiceTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            //----------accessedMonk auth check part----------//
            if ( MainWindow.accessedMonk != null
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple( (LocalTemple)comboBoxServiceTemple.SelectedItem ))
                {
                    comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;
                    MessageBox.Show("소속 분원 외의 천도재는 검색할 수 없습니다.");
                }
            }
        }

        private void ComboBoxServiceTemple_KeyUp(object sender, KeyEventArgs e)
        {
            if (MainWindow.accessedMonk.LevelOfAccess == AccessLevel.총괄관리자
              || MainWindow.accessedMonk.LevelOfAccess == AccessLevel.주지스님)
                return;

            MessageBox.Show("리스트 항목 중에서 선택해주십시오.");
            comboBoxServiceTemple.ResetText();
            comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;
        }

        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.MinimumHeight = MainWindow.iDatagridviewRowHeight;
            }
            this.dataGridView1.DefaultCellStyle.Font = new Font("Gulim", MainWindow.iDatagridviewFontSize);
        }

        private void ButtonExportExcel_Click(object sender, EventArgs e)
        {
            MainWindow.ExportExcelFile(this.dataGridView1);
        }

        private void ComboBoxServiceTemple_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strTemple = this.comboBoxServiceTemple.Text;
                if (!string.IsNullOrWhiteSpace(strTemple)
                 && !Enum.TryParse<LocalTemple>(strTemple, out LocalTemple selectedTemple))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshSalvationServiceListInDataGridView();
            }
        }

        private void TextBoxBeliever_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshSalvationServiceListInDataGridView();
        }

        private void DateTimePickerServiceDay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshSalvationServiceListInDataGridView();
        }
    }
}
