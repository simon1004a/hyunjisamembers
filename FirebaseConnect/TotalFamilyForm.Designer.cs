﻿namespace HyunjisaBeliever
{
    partial class TotalFamilyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonExcelExport = new System.Windows.Forms.Button();
            this.buttonExcelImport = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(13, 75);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(980, 470);
            this.dataGridView1.TabIndex = 0;
            // 
            // buttonExcelExport
            // 
            this.buttonExcelExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExcelExport.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonExcelExport.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonExcelExport.FlatAppearance.BorderSize = 2;
            this.buttonExcelExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExcelExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonExcelExport.ForeColor = System.Drawing.Color.Maroon;
            this.buttonExcelExport.Location = new System.Drawing.Point(878, 12);
            this.buttonExcelExport.Name = "buttonExcelExport";
            this.buttonExcelExport.Size = new System.Drawing.Size(115, 50);
            this.buttonExcelExport.TabIndex = 129;
            this.buttonExcelExport.Text = "Excel 출력";
            this.buttonExcelExport.UseVisualStyleBackColor = false;
            this.buttonExcelExport.Click += new System.EventHandler(this.ButtonExcelExport_Click);
            // 
            // buttonExcelImport
            // 
            this.buttonExcelImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExcelImport.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonExcelImport.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonExcelImport.FlatAppearance.BorderSize = 2;
            this.buttonExcelImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExcelImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonExcelImport.ForeColor = System.Drawing.Color.Maroon;
            this.buttonExcelImport.Location = new System.Drawing.Point(757, 12);
            this.buttonExcelImport.Name = "buttonExcelImport";
            this.buttonExcelImport.Size = new System.Drawing.Size(115, 50);
            this.buttonExcelImport.TabIndex = 130;
            this.buttonExcelImport.Text = "Excel 입력";
            this.buttonExcelImport.UseVisualStyleBackColor = false;
            this.buttonExcelImport.Visible = false;
            this.buttonExcelImport.Click += new System.EventHandler(this.ButtonExcelImport_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(505, 12);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(246, 23);
            this.progressBar1.TabIndex = 131;
            this.progressBar1.Visible = false;
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(505, 41);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(246, 21);
            this.txtFilePath.TabIndex = 132;
            this.txtFilePath.Visible = false;
            // 
            // TotalFamilyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(1005, 557);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonExcelImport);
            this.Controls.Add(this.buttonExcelExport);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TotalFamilyForm";
            this.Text = "TotalFamilyForm";
            this.Load += new System.EventHandler(this.TotalFamilyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonExcelExport;
        private System.Windows.Forms.Button buttonExcelImport;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtFilePath;
    }
}