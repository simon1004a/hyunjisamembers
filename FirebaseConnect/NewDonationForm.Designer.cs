﻿namespace HyunjisaBeliever
{
    partial class NewDonationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDate = new System.Windows.Forms.Label();
            this.dateTimePickerDonation = new System.Windows.Forms.DateTimePicker();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.textBoxNewDonation = new System.Windows.Forms.TextBox();
            this.labelDonation = new System.Windows.Forms.Label();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelDate.Location = new System.Drawing.Point(12, 17);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(34, 18);
            this.labelDate.TabIndex = 93;
            this.labelDate.Text = "날짜";
            // 
            // dateTimePickerDonation
            // 
            this.dateTimePickerDonation.Location = new System.Drawing.Point(85, 17);
            this.dateTimePickerDonation.Name = "dateTimePickerDonation";
            this.dateTimePickerDonation.Size = new System.Drawing.Size(177, 21);
            this.dateTimePickerDonation.TabIndex = 92;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonCancel.FlatAppearance.BorderSize = 2;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCancel.Location = new System.Drawing.Point(139, 194);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(123, 44);
            this.buttonCancel.TabIndex = 91;
            this.buttonCancel.Text = "취소";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.FlatAppearance.BorderSize = 2;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.Location = new System.Drawing.Point(15, 194);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(118, 44);
            this.buttonConfirm.TabIndex = 90;
            this.buttonConfirm.Text = "확인";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // textBoxNewDonation
            // 
            this.textBoxNewDonation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxNewDonation.Location = new System.Drawing.Point(85, 44);
            this.textBoxNewDonation.Name = "textBoxNewDonation";
            this.textBoxNewDonation.Size = new System.Drawing.Size(177, 24);
            this.textBoxNewDonation.TabIndex = 89;
            this.textBoxNewDonation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxNewDonation_KeyPress);
            // 
            // labelDonation
            // 
            this.labelDonation.AutoSize = true;
            this.labelDonation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelDonation.Location = new System.Drawing.Point(12, 46);
            this.labelDonation.Name = "labelDonation";
            this.labelDonation.Size = new System.Drawing.Size(70, 18);
            this.labelDonation.TabIndex = 88;
            this.labelDonation.Text = "기도비(원)";
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxRemarks.Location = new System.Drawing.Point(85, 71);
            this.textBoxRemarks.Multiline = true;
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(177, 117);
            this.textBoxRemarks.TabIndex = 95;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 18);
            this.label1.TabIndex = 94;
            this.label1.Text = "비고";
            // 
            // NewDonationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(274, 250);
            this.Controls.Add(this.textBoxRemarks);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.dateTimePickerDonation);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.textBoxNewDonation);
            this.Controls.Add(this.labelDonation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NewDonationForm";
            this.Text = "기도비 입력하기";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDonation;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonConfirm;
        public System.Windows.Forms.TextBox textBoxNewDonation;
        private System.Windows.Forms.Label labelDonation;
        public System.Windows.Forms.TextBox textBoxRemarks;
        private System.Windows.Forms.Label label1;
    }
}