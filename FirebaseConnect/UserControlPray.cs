﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace HyunjisaBeliever
{
    public partial class UserControlPray : UserControl
    {
        enum PrayState { 진행중, 회향됨, 모든기도 }

        public UserControlPray()
        {
            InitializeComponent();
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;

            this.comboBoxPrayTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxPrayState.DataSource = Enum.GetValues(typeof(PrayState));            
        }

        private void UserControlPray_Load(object sender, EventArgs e)
        {   
            RefreshPrayListInDataGridView(true);            
        }

                
        private void ButtonInsert_Click(object sender, EventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            var userPrayUpdateForm = new UserPrayUpdateForm();
            if (userPrayUpdateForm.ShowDialog() == DialogResult.OK)
            {
                RefreshPrayListInDataGridView();
                userPrayUpdateForm.Close();
            }
        }        

        void ResetPrayListSearchCondition()
        {
            this.comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;

            this.textBoxPaperNumber.ResetText();
            this.textBoxBeliever.ResetText();
            this.textBoxPrayerName.ResetText();
            this.textBoxSearch.ResetText();
            this.comboBoxBuddha.ResetText();
            Pray.SetBuddhaComboBoxByArea(this.comboBoxBuddha, (LocalTemple)comboBoxPrayTemple.SelectedItem);

            this.comboBoxPrayState.SelectedItem = PrayState.진행중;
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < this.dataGridView1.RowCount - 1)
            {                
                var prayData = this.GetPrayDataFromSqlByDataGridViewRow(e.RowIndex);
                if (prayData == null) return;
                var userPrayUpdateForm = new UserPrayUpdateForm(prayData);
                
                if (userPrayUpdateForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshPrayListInDataGridView();
                    userPrayUpdateForm.Close();
                }
            }
            else if (e.RowIndex == this.dataGridView1.RowCount - 1)
            {
                var userPrayUpdateForm = new UserPrayUpdateForm();

                if (userPrayUpdateForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshPrayListInDataGridView();
                    userPrayUpdateForm.Close();
                }
            }
        }

        private Pray GetPrayDataFromSqlByDataGridViewRow(int rowIndex)
        {
            Pray prayData = null;
            if (rowIndex >= 0 && rowIndex < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
                if (row.IsNewRow) return null;

                int prayID = Convert.ToInt32(row.Cells[0].Value);
                prayData = SqlManager.SelectPrayFromSQL(prayID);                
            }
            return prayData;
        }

        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridView1.RowCount - 1)
                {
                    if (dataGridView1.SelectedRows.Count == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    //if user selected multiple rows, only the deletion is available.
                    if (dataGridView1.SelectedRows.Count <= 1)
                    {
                        miniMenu.Items.Add("수정하기").Name = "Modify";
                        miniMenu.Items.Add("문자보내기").Name = "SendSMS";
                    }
                        
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            int iSelectedRow;
            Pray prayData;

            switch (e.ClickedItem.Name)
            {
                case "SendSMS":
                    iSelectedRow = dataGridView1.SelectedRows[0].Index;
                    prayData = this.GetPrayDataFromSqlByDataGridViewRow(iSelectedRow);
                    if (prayData == null) return;
                    var believer = SqlManager.SelectBelieverFromSQL(prayData.believerUID);
                    if (believer != null)
                    {
                        if(prayData.PrayerPhone == believer.Phone)
                        {
                            var smsSendForm = new SMSSendingForm(believer);
                            if (smsSendForm.ShowDialog() == DialogResult.OK)
                                smsSendForm.Close();
                        }
                        else
                        {
                            var askSmsSend = MessageBox.Show("기도 연락처와 실제 신도 입재자의 연락처가 다릅니다. 실제 신도 입재자 분께 문자를 보내시겠습니까? 그렇다면 yes를 눌러주시고, " +
                                "No를 클릭하시면 기도의 연락처 번호로 문자를 보내겠습니다.", "문자 수신 번호 정하기", MessageBoxButtons.YesNoCancel);
                            if (askSmsSend == DialogResult.Yes)
                            {
                                var smsSendForm = new SMSSendingForm(believer);
                                if (smsSendForm.ShowDialog() == DialogResult.OK)
                                    smsSendForm.Close();
                            }
                            else if(askSmsSend == DialogResult.No)
                            {
                                if (string.IsNullOrWhiteSpace(prayData.PrayerPhone))
                                    MessageBox.Show("연락처 정보가 없으므로 문자를 보낼 수 없습니다. ");
                                else
                                {
                                    var smsSendForm = new SMSSendingForm(prayData.PrayerPhone, prayData.PrayerName, prayData.PrayerBirthYear);
                                    if (smsSendForm.ShowDialog() == DialogResult.OK)
                                        smsSendForm.Close();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(prayData.PrayerPhone))
                            MessageBox.Show("연락처 정보가 없으므로 문자를 보낼 수 없습니다. ");
                        else
                        {
                            var smsSendForm = new SMSSendingForm(prayData.PrayerPhone, prayData.PrayerName, prayData.PrayerBirthYear);
                            if (smsSendForm.ShowDialog() == DialogResult.OK)
                                smsSendForm.Close();
                        }
                    }
                    break;
                case "Modify":
                    iSelectedRow = dataGridView1.SelectedRows[0].Index;
                    prayData = this.GetPrayDataFromSqlByDataGridViewRow(iSelectedRow);
                    if (prayData == null) return;
                    var userPrayUpdateForm = new UserPrayUpdateForm(prayData);

                    if (userPrayUpdateForm.ShowDialog() == DialogResult.OK)
                    {
                        RefreshPrayListInDataGridView();
                        userPrayUpdateForm.Close();
                    }
                    break;
                case "Delete":
                    var deleteIdList = new List<int>();
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        int iSelectedRowIndex = dataGridView1.SelectedRows[i].Index;
                        int prayID = Convert.ToInt32(dataGridView1.Rows[iSelectedRowIndex].Cells[0].Value);
                        deleteIdList.Add(prayID);
                    }

                    deleteIdList.Sort();
                    string deleteMessage = "다음 ID의 기도 정보를 삭제하시겠습니까?";
                    string deleteList = string.Empty;
                    for (int j = 0; j < deleteIdList.Count; j++)
                    {
                        string divider = (deleteIdList.Count > 1 && j != 0) ? ", " : "";
                        deleteList += (divider + deleteIdList[j].ToString());
                    }

                    DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 ID : " + deleteList, "ID 삭제", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        for (int k = 0; k < deleteIdList.Count; k++)
                        {
                            SqlManager.DeletePrayInSQL(deleteIdList[k]);
                        }
                        RefreshPrayListInDataGridView();
                    }
                    break;
            }
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            RefreshPrayListInDataGridView(false);
        }
        private void ButtonReset_Click(object sender, EventArgs e)
        {
            RefreshPrayListInDataGridView(true);
        }
        public void RefreshPrayListInDataGridView(bool bResetSearchConditions = false)
        {
            if (MainWindow.accessedMonk == null) return;

            //Refresh하기 전에 마지막으로 선택한 행의 번호는 기억해둬야 한다!
            int iSelectedIndex = (dataGridView1.SelectedRows.Count > 0) ? dataGridView1.SelectedRows[0].Index : 0;

            dataGridView1.DataSource = null;
            var searchConditions = new List<string>();
            if (bResetSearchConditions)
            {
                ResetPrayListSearchCondition();
            }

            string prayTemple =(string.IsNullOrWhiteSpace(comboBoxPrayTemple.Text))? LocalTemple.전체.ToString(): comboBoxPrayTemple.Text;

            if ( prayTemple != LocalTemple.전체.ToString()
              && MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)Enum.Parse(typeof(LocalTemple), prayTemple)) )
                searchConditions.Add(" PrayTempleID = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), prayTemple))));
            else
            {
                //----------accessedMonk auth check part----------//
                if ( MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자
                  && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님)
                {
                    comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;
                    MessageBox.Show("소속 분원에 등록된 기도만 검색할 수 있습니다.");
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(comboBoxBuddha.Text))
                searchConditions.Add(" BuddhaID = " + Convert.ToString((int)(Enum.Parse(typeof(MajestyBuddha), comboBoxBuddha.Text))));

            if (!string.IsNullOrWhiteSpace(textBoxPaperNumber.Text)
              && int.TryParse(textBoxPaperNumber.Text, out int iPaperNumber))
                searchConditions.Add(" P.PrayPaperNumber = " + iPaperNumber);

            if (!string.IsNullOrWhiteSpace(textBoxBeliever.Text))
                searchConditions.Add(" A.Name LIKE '%"+ textBoxBeliever.Text + "%' ");

            if (!string.IsNullOrWhiteSpace(textBoxPrayerName.Text))
                searchConditions.Add(" P.PrayerName LIKE '%" + textBoxPrayerName.Text + "%' ");

            if (!string.IsNullOrWhiteSpace(textBoxSearch.Text))
            {
                var searchKeyword = textBoxSearch.Text;
                searchConditions.Add(" (P.donator LIKE '%" + searchKeyword + "%' "
                    + " OR " + " A.Name LIKE '%" + searchKeyword + "%' "
                    + " OR " + " P.PrayerName LIKE '%" + searchKeyword + "%' )");
            }
                

            if (!string.IsNullOrWhiteSpace(comboBoxPrayState.Text))
            {
                var searchPrayState = (PrayState)comboBoxPrayState.SelectedItem;
                switch (searchPrayState)
                {
                    case PrayState.진행중:
                        searchConditions.Add(" P.IsPrayFinished = 0 ");
                        break;
                    case PrayState.회향됨:
                        searchConditions.Add(" P.IsPrayFinished = 1 ");
                        break;
                    case PrayState.모든기도:
                        break;
                }
            }
                        
            if (searchConditions.Count != 0)
                dataGridView1.DataSource = SqlManager.SelectPrayListFromSQL(searchConditions.ToArray());
            else
                dataGridView1.DataSource = SqlManager.SelectPrayListFromSQL();

            this.labelRowCount.Text = string.Format("총 {0}개의 데이터가 검색되었습니다. ", dataGridView1.Rows.Count - 1);

            //dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //발원이 길 경우 multiline으로 만들기 위한 코드...
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                string columnName = dataGridView1.Columns[i].Name;
                if ( columnName == "발원" || columnName == "비고" || columnName == "주소")
                    dataGridView1.Columns[i].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }

            //현재 selected row로 돌아오자.
            if (iSelectedIndex != 0 && iSelectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = iSelectedIndex;
                dataGridView1.CurrentCell = dataGridView1.Rows[iSelectedIndex].Cells[0];
            }
        }

        private void ComboBoxPrayTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nowSelectedTemple = (LocalTemple)comboBoxPrayTemple.SelectedItem;

            //----------accessedMonk auth check part----------//
            if ( MainWindow.accessedMonk != null
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자 
              && !MainWindow.accessedMonk.IsBelongedTemple(nowSelectedTemple) )
            {                
                comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;
                MessageBox.Show("소속 분원 외의 기도는 검색할 수 없습니다.");
                return;                
            }

            this.comboBoxBuddha.ResetText();
            Pray.SetBuddhaComboBoxByArea(this.comboBoxBuddha, nowSelectedTemple);
        }

        private void ComboBoxPrayTemple_KeyUp(object sender, KeyEventArgs e)
        {
            if (MainWindow.accessedMonk.LevelOfAccess == AccessLevel.총괄관리자
              || MainWindow.accessedMonk.LevelOfAccess == AccessLevel.주지스님)
                return;

            MessageBox.Show("리스트 항목 중에서 선택해주십시오.");
            comboBoxPrayTemple.ResetText();
            comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;
        }

        private void ComboBoxPrayTemple_KeyPress(object sender, KeyPressEventArgs e)
        {                        
            if (e.KeyChar == '\r')
            {
                string strTemple = this.comboBoxPrayTemple.Text;
                if (!string.IsNullOrWhiteSpace(strTemple)
                 && !Enum.TryParse<LocalTemple>(strTemple, out LocalTemple selectedTemple))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshPrayListInDataGridView();
            }
        }
        private void ComboBoxBuddha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strBuddha = this.comboBoxBuddha.Text;
                if (!string.IsNullOrWhiteSpace(strBuddha) && !Enum.TryParse<MajestyBuddha>(strBuddha, out MajestyBuddha selectdBuddha))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshPrayListInDataGridView();
            }

        }

        private void TextBoxBeliever_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshPrayListInDataGridView();
        }

        private void TextBoxPrayerName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshPrayListInDataGridView();
        }

        private void TextBoxDonator_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshPrayListInDataGridView();
        }

        private void ComboBoxPrayState_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshPrayListInDataGridView();
        }

        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.MinimumHeight = MainWindow.iDatagridviewRowHeight;
            }
            this.dataGridView1.DefaultCellStyle.Font = new Font("Gulim", MainWindow.iDatagridviewFontSize);
        }

        private void ButtonExportExcel_Click(object sender, EventArgs e)
        {
            MainWindow.ExportExcelFile(dataGridView1, progressBar1);
        }

        private void ButtonImportExcel_Click(object sender, EventArgs e)
        {
            //여기에서 기도 정보를 통째로 읽어와야 합니다...
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                progressBar1.Visible = true;
                progressBar1.Value = 5;

                string filename = openFileDialog1.FileName;
                txtFilePath.Text = filename;
                txtFilePath.Visible = true;

                var excelApp = new Excel.Application();

                Excel.Workbook excelBook = excelApp.Workbooks.Open(filename, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel.Worksheet excelSheet = (Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                int rowCnt = 0;
                int colCnt = 0;
                var redRowIndexes = new List<int>();

                DataTable totalPrayDataTable = new DataTable();                
                dataGridView1.DataSource = null;

                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumnName = (string)(excelRange.Cells[1, colCnt] as Excel.Range).Value2;
                    totalPrayDataTable.Columns.Add(strColumnName, typeof(string));
                }

                int iTotalRowCnt = excelRange.Rows.Count;
                //먼저 excel data를 읽어서 DataTable을 채웁니다. 
                //첫행은 컬럼제목이므로 읽을 필요가 없다.
                for (rowCnt = 2; rowCnt <= iTotalRowCnt; rowCnt++)
                {
                    string strRowData = "";
                    bool isRedRow = false;
                    bool isAllCellsEmpty = true;
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = excelRange.Cells[rowCnt, colCnt].Value.ToString();
                            isAllCellsEmpty = false;

                            string columnName = totalPrayDataTable.Columns[colCnt - 1].ColumnName;
                            if (columnName == "기도분원")
                            {
                                if (!Enum.TryParse<LocalTemple>(strCellData, out LocalTemple temple))
                                    isRedRow = true;
                            }
                            else if (columnName == "부처님")
                            {
                                if (!Enum.TryParse<MajestyBuddha>(strCellData, out MajestyBuddha buddha))
                                    isRedRow = true;
                            }
                            else if (columnName == "관계")
                            {
                                //일단 올릴 때는 데이터베이스에서 관계를 찾아 입력할 테니 excel 파일의 data는 쓰지 않는다.
                                //if (!Enum.TryParse<FamilyRelation>(strCellData, out FamilyRelation relation))
                                //    isRedRow = true;
                            }
                            else if (columnName == "입재일" || columnName == "회향일" || columnName == "완납일")
                            {
                                if (!DateTime.TryParse(strCellData, out DateTime date))
                                    isRedRow = true;
                            }
                        }
                        catch
                        {
                            strCellData = "";
                        }
                        strRowData += strCellData + "|";
                    }
                    if (isAllCellsEmpty)
                        continue;

                    strRowData = strRowData.Remove(strRowData.Length - 1, 1);
                    totalPrayDataTable.Rows.Add(strRowData.Split('|'));
                    if (isRedRow)
                        redRowIndexes.Add(rowCnt - 2);
                    progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt);
                }

                dataGridView1.DataSource = totalPrayDataTable;
                if (redRowIndexes.Count > 0)
                {
                    string redRowList = "";
                    for (int i = 0; i < redRowIndexes.Count; i++)
                    {
                        var redRow = dataGridView1.Rows[redRowIndexes[i]];
                        redRow.DefaultCellStyle.BackColor = Color.Red;
                        redRowList += ", " + redRowIndexes[i].ToString();
                    }

                    MessageBox.Show("불완전한 데이터가 있어 서버에 업로드할 수 없습니다. " +
                        "빨간색으로 표시된 데이터들을 수정해주세요. 불완전한 행 번호들은 다음과 같습니다. :" +
                        redRowList);
                }
                else   //dataGridView1에 옮겨진 DataTable의 내용을 이제 실제로 database에 업로드합니다!
                {
                    var warningResult = MessageBox.Show("경고메시지", "이 기능을 실행하면 Pray 테이블에 엑셀 데이터가 다 들어갑니다. " +
                        "반드시 개발자와 논의하고 또 백업을 해 두신 후에 이 기능을 실행하셔야 합니다. 그래도 진행하시겠습니까?", MessageBoxButtons.YesNoCancel);
                    if(warningResult == DialogResult.Yes)
                    {
                        //이 로직으로 기도 정보를 올리는 건 Pray 데이터가 완전히 비어 있는 상태에서 
                        //실행된 것이니, 추후 만일 다시 이 기능을 사용하려 한다면 반드시 심사숙고하여 로직을 검토하고 사용하거나
                        //새로 짜야 할 것입니다. - 2019.10.24 은명 권희재                    
                        iTotalRowCnt = this.dataGridView1.RowCount - 1;
                        for (rowCnt = 0; rowCnt < iTotalRowCnt; rowCnt++)
                        {
                            //먼저 엑셀 행의 정보를 읽어들이고,
                            Pray prayData = this.GetPrayDataFromDatagridViewRow(rowCnt);

                            //prayData에 들어가지 않았지만 꼭 필요한 데이터를 읽어온다.
                            DataGridViewRow row = this.dataGridView1.Rows[rowCnt];
                            string realPrayRegister = row.Cells["신청자"].Value.ToString();
                            string prayRequest = (row.Cells["발원"].Value != null) ? row.Cells["발원"].Value.ToString() : "";

                            //실제입재자가 신도인지 먼저 체크하고, 
                            var believerData = SqlManager.SelectBelieverFromSQL(realPrayRegister, prayData.PrayerPhone);
                            if (believerData != null)
                            {
                                //실제 신도로 검색되면 believerUID부터 매긴다.
                                prayData.believerUID = believerData.UID;
                                //본인명의라면 바로 insert를 수행하고, 
                                if (realPrayRegister == prayData.PrayerName)
                                    this.InsertNewPray(prayData, prayRequest);
                                else //아니라면 입재자가 가족인지 검사하고, 가족이라면 familyMemberID를 매긴다.
                                {
                                    if (IsPrayerFamilyMember(prayData.believerUID, prayData.PrayerName, out int memberId))
                                        prayData.FamilyMemberUID = memberId;

                                    //가족 또는 그외의 경우를 insert한다!
                                    this.InsertNewPray(prayData, prayRequest);
                                }
                            }
                            else //believer가 없으면 그외의 경우이니, 그냥 넣는다.
                            {
                                this.InsertNewPray(prayData, prayRequest);
                            }

                            progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt) + 50;
                        }

                        progressBar1.Value = 100;
                        MessageBox.Show(filename + " 엑셀 파일을 로드하였습니다.");
                        MessageBox.Show("엑셀의 전체 데이터를 서버에 업로드하였습니다.");
                    }
                }

                excelBook.Close(true, null, null);
                Marshal.FinalReleaseComObject(excelBook);
                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
                progressBar1.Visible = false;
            }
        }

        private void InsertNewPray(Pray prayData, string prayRequest)
        {
            if (SqlManager.InsertPrayToSQL(prayData, out int insertedPrayID))
            {
                //발원 입력하기
                string insertQuery = "INSERT INTO PrayRequestHistory (PrayID, DateOfRequest, Request) " +
                            "VALUES( " + insertedPrayID + ", '" + prayData.DateOfStart.ToShortDateString() + "', '" + prayRequest + "')";
                SqlManager.ExecuteQueryOnSQL(insertQuery);

                //완납일과 회향일을 통해서 총납부했던 금액을 계산하여 오늘 날짜로 넣읍시다!
                //완납일 = 입재일 + (총납입금/10만원)th Month - 1 day
                //10만원만 낸 경우, 입재일이 1월1일이면 완납일은 1월31일입니다. 
                //근데 2월 28일이면 3월 27일로 나오는데 이 경우는 사실 3월 30일이어야 합니다. 달력 때문에 생기는 오류입니다.                 
                DateTime lastPaid = prayData.DateOfFullPay.AddDays(1);
                int paidMonths = (lastPaid.Year - prayData.DateOfStart.Year) * 12
                    + lastPaid.Month - prayData.DateOfStart.Month;
                string paidTotal = (paidMonths * 100000).ToString();
                insertQuery = "INSERT INTO PrayDonationHistory (PrayID, DateOfDonation, donation) " +
                    "VALUES( " + insertedPrayID + ", '" + prayData.DateOfFullPay.ToShortDateString() + "', " + paidTotal + ")";
                SqlManager.ExecuteQueryOnSQL(insertQuery);
            }

        }
        private bool IsPrayerFamilyMember(int believerUID, string prayerName, out int memberId)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(SqlManager.connectionString))
            {
                string query = "SELECT MemberID FROM BelieverFamily " +
                    "WHERE FamilyID = ( SELECT FamilyID FROM Believer WHERE BelieverID = @believerId ) " +
                    "AND Name = @memberName ";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@believerId", believerUID);
                    command.Parameters.AddWithValue("@memberName", prayerName);
                    
                    connection.Open();
                    var result = command.ExecuteScalar();                    
                    memberId = (result != null) ? Convert.ToInt32(result):0;
                    bResult = (memberId > 0);
                }
            }
            return bResult;
        }

        private Pray GetPrayDataFromDatagridViewRow(int iRow)
        {   
            var prayData = new Pray();
            if (iRow >= 0 && iRow < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[iRow];
                if (row.IsNewRow) return prayData;

                try
                {
                    //prayData.UID = Convert.ToInt32(row.Cells["PrayID"].Value);                    
                    prayData.PrayPaperNumber = Convert.ToInt32(row.Cells["원장번호"].Value);
                    prayData.PrayTemple = (LocalTemple)Enum.Parse(typeof(LocalTemple), row.Cells["기도분원"].Value.ToString());
                    prayData.buddha = (MajestyBuddha)Enum.Parse( typeof(MajestyBuddha), row.Cells["부처님"].Value.ToString() );
                    //실제입재자와 가족 관계는 나중에 처리하기로 하고 여기서는 일단 null값으로 해둔다.
                    prayData.believerUID = 0;
                    prayData.FamilyMemberUID = 0;
                    //입재자, 생년, 입재일, 회향일은 반드시 있어야 합니다. 
                    prayData.PrayerName = row.Cells["입재자"].Value.ToString();
                    prayData.PrayerBirthYear = row.Cells["생년"].Value.ToString();
                    
                    prayData.PrayerAddress = (row.Cells["주소"].Value != null)? row.Cells["주소"].Value.ToString() : string.Empty;
                    prayData.PrayerPhone = (row.Cells["휴대폰"].Value != null)? row.Cells["휴대폰"].Value.ToString() : string.Empty;
                    prayData.PrayerPhone = prayData.PrayerPhone.Replace("-", "");
                    prayData.Donator = (row.Cells["입금자"].Value != null)? row.Cells["입금자"].Value.ToString(): string.Empty;

                    prayData.DateOfStart = DateTime.Parse(row.Cells["입재일"].Value.ToString());
                    prayData.DateOfFinish = DateTime.Parse(row.Cells["회향일"].Value.ToString());

                    var paidDay = row.Cells["완납일"].Value;
                    if (paidDay == null || string.IsNullOrWhiteSpace(paidDay.ToString()))
                        prayData.DateOfFullPay = prayData.DateOfStart;
                    else
                        prayData.DateOfFullPay = DateTime.Parse(paidDay.ToString());

                    var finishConfirm = row.Cells["회향확정"].Value;                    
                    prayData.IsFinishConfirmed = (finishConfirm != null && !string.IsNullOrWhiteSpace(finishConfirm.ToString())) ? true : false;
                    prayData.Remarks = (row.Cells["비고"].Value!= null)? row.Cells["비고"].Value.ToString(): string.Empty;

                    var prayFinished = row.Cells["회향됨"].Value;
                    prayData.IsPrayFinished = (prayFinished != null && !string.IsNullOrWhiteSpace(prayFinished.ToString())) ? true : false;
                }
                catch { MessageBox.Show("데이터 값이 잘못되어 있습니다:" + row + "행"); }
            }
            return prayData;
            
        }

        private void textBoxPaperNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            //엔터 입력을 받으면 전체를 refresh하되, 그 외의 경우는 숫자만 입력받게 합시다. 
            if (e.KeyChar == '\r')
            {
                RefreshPrayListInDataGridView();
            }
            else if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F)
            {
                MessageBox.Show("Ctrl + F 키 기능을 준비하는 중에 있습니다!");
            }
        }
    }
}
