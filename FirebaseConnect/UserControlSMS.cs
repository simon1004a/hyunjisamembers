﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using static HyunjisaBeliever.MainWindow;
using System.Text.RegularExpressions;

public enum MESSAGE_SERVICE
{
    SHORT_MESSAGE = 0,
    MULTI_MESSAGE = 1,
}

namespace HyunjisaBeliever
{
    public partial class UserControlSMS : UserControl
    {
        //C++ dll과 서로 통신하기 위한 struct 선언!!
        public struct OBSIResult
        {
            [MarshalAs(UnmanagedType.I4)]
            public Int32 lResult;
            
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public String szMessage;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public String szNoticeUrl;
        };

        public struct OBSILoginResult
        {
            public OBSIResult Result;
            public OBSIResult ResultOaasys;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulMaxReceiverCount;
        };

        public struct OBSISendResult
        {
            public OBSIResult Result;
            public IntPtr pContext;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulSent;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulFailed;
        };


        public struct OBSIReceiver
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 51)]
            public String szName;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 51)]
            public String szPhoneNumber;
        };

        public struct OBSIAttachment
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 261)]
            public String szFilePath;
        };

        public struct OBSISMSContent
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 91)]
            public String szMessage;
        };

        public struct OBSIMMSContent
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2001)]
            public String szMessage;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulImageFileCount;
            
            //OBSIAttachment*            
            public IntPtr pImageFiles;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulSoundFileCount;
            
            //OBSIAttachment*             
            public IntPtr pSoundFiles;
        };

        public struct OBSIFMSContent
        {
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulFileCount;

            //OBSIAttachment*             
            public IntPtr pFiles;
        };

        public struct OBSIVMSTextContent
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1001)]
            public String szMessage;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 33)]
            public String szSpeaker;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulChoiceCount;
        };

        public struct OBSIVMSFileContent
        {
            public OBSIAttachment File;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulChoiceCount;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct OBSIMessage
        {
            //[FieldOffset(0)]
            [MarshalAs(UnmanagedType.I4)]
            public Int32 lMsgType;

            //[FieldOffset(4)]
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 65)]
            public String szSubject;

            //*******union start*******//
            //union OBSIMessageContent 
            //{
            //[FieldOffset(69)]
            //OBSISMSContent*		
            public IntPtr pMessageContent;

            ////[FieldOffset(69)]
            ////OBSIMMSContent*		
            //public IntPtr pMMSContent;

            ////[FieldOffset(69)]
            ////OBSIFMSContent*		
            //public IntPtr pFMSContent;

            ////[FieldOffset(69)]
            ////OBSIVMSTextContent*	
            //public IntPtr pVMSTextContent;

            ////[FieldOffset(69)]
            ////OBSIVMSFileContent*	
            //public IntPtr pVMSFileContent;
            //} Content;
            //*******union end*******//
            //[FieldOffset(73)]
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 ulReceiverCount;

            //[FieldOffset(77)]
            //OBSIReceiver* 
            public IntPtr pReceivers;

            //[FieldOffset(81)]
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 31)]
            public String szCallbackNumber;

            //[FieldOffset(112)]
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public String szReserveTime;
        };

        public struct OBSIReceiveMessage
        {
            [MarshalAs(UnmanagedType.I4)]
            public Int32 lMsgType;

            [MarshalAs(UnmanagedType.I4)]
            public Int32 lSeqno;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 31)]
            public String szSendNumber;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 31)]
            public String szReceiveNumber;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public String szCreateTime;
        };


        public const int WM_USER = 0x0400;
        public const int WM_APP = 0x8000;

        public const int OBSI_SMS = 1;
        public const int OBSI_MMS = 2;
        public const int OBSI_VMS_TEXT = 3;
        public const int OBSI_VMS_FILE = 4;
        public const int OBSI_FMS = 5;
        public const int OBSI_CTC = 6;
        public const int OBSI_CALL = 7;

        public const int SMS_LIMIT = 45;
        public const int MMS_LIMIT = 1000;


        [DllImport("OBSISDK.dll")]
        public static extern void OBSISetCallbackWindow(IntPtr ptrWnd, [param: MarshalAs(UnmanagedType.U4)]UInt32 uiMsg);
        [DllImport("OBSISDK.dll")]
        public static extern void OBSISetReceiveCallbackWindow(IntPtr ptrWnd, [param: MarshalAs(UnmanagedType.U4)]UInt32 uiMsg);

        //long __stdcall OBSILogin(const char* szUserID, const char* szPassword, char* szISVAPIKey, OBSILoginResult* pResult);
        [DllImport("OBSISDK.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 OBSILogin(string szUserID, string szPassword, string szISVAPIKey, ref OBSILoginResult pResult);
        [DllImport("OBSISDK.dll")]
        public static extern void OBSILogout();
        
        //long __stdcall OBSISend(OBSIMessage* pMsg, void* pContext, OBSISendResult* pResult);
        [DllImport("OBSISDK.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 OBSISend(ref OBSIMessage pMsg, IntPtr pContext, ref OBSISendResult pResult);

        public static MESSAGE_SERVICE currentMessageService = MESSAGE_SERVICE.SHORT_MESSAGE;
                
        private static bool _isLoggedIn;
        public bool IsLoggedIn
        {
            get { return _isLoggedIn; }
            set
            {
                _isLoggedIn = value;
                if (_isLoggedIn)
                {
                    this.panelSignIn.Visible = false;
                    this.panelSMS.Visible = true;
                    this.panelSearch.Visible = true;
                    
                    RefreshReceiverListInDatagridview(true);
                }
                else
                {
                    this.panelSignIn.Visible = true;
                    this.panelSMS.Visible = false;
                    this.panelSearch.Visible = false;
                }
            }
        }

        public DataTable membersDataTable = null;
                
        public UserControlSMS()
        {
            InitializeComponent();
            IsLoggedIn = false;
            membersDataTable = new DataTable();
        }
                
        private void UserControl2_Load(object sender, EventArgs e)
        {
            comboBoxSender.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxTemple1.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxTemple2.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxProvince1.DataSource = Enum.GetValues(typeof(Province));
            comboBoxProvince2.DataSource = Enum.GetValues(typeof(Province));

            comboBoxSender.SelectedItem = LocalTemple.춘천본사;
        }

        public void ResetReceiverListSearchCondition()
        {
            if(MainWindow.accessedMonk != null)
                this.comboBoxTemple1.SelectedItem = MainWindow.manageArea;

            this.comboBoxSender.SelectedItem = LocalTemple.춘천본사;
            
            this.comboBoxTemple2.ResetText();
            this.comboBoxProvince1.ResetText();
            this.comboBoxProvince2.ResetText();            
        }

        public void RefreshReceiverListInDatagridview(bool bResetSearchConditions = false)
        {
            var searchConditions = new List<string>();
            if( bResetSearchConditions )
            {
                ResetReceiverListSearchCondition();
            }

            //문자 보낼 대상은 소속분원과 지역별로 찾는데 여러 조건의 합집합이 되어야 합니다. OR condition!
            if (!string.IsNullOrWhiteSpace(comboBoxTemple1.Text))
                searchConditions.Add(" BelongedTemple = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), comboBoxTemple1.Text))));

            if (!string.IsNullOrWhiteSpace(comboBoxTemple2.Text))
                searchConditions.Add(" BelongedTemple = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), comboBoxTemple2.Text))));

            if (!string.IsNullOrWhiteSpace(comboBoxProvince1.Text))
                searchConditions.Add(" AddressProvince = " + Convert.ToString((int)(Enum.Parse(typeof(Province), comboBoxProvince1.Text))));

            if (!string.IsNullOrWhiteSpace(comboBoxProvince2.Text))
                searchConditions.Add(" AddressProvince = " + Convert.ToString((int)(Enum.Parse(typeof(Province), comboBoxProvince2.Text))));

            int iSelectedIndex = (dataGridView1.CurrentCell != null) ? dataGridView1.CurrentCell.RowIndex : 0;

            this.membersDataTable = SqlManager.SelectSMSReceiverListFromSQL(searchConditions.ToArray());
            //꼭 필요한 데이터 필드만 남겨야 한다.
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.UID));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.MEMBER_NUMBER));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.ADDRESS));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.DHARMA_NAME));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.GANZHI));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.BIRTH));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.REGISTER));
            this.membersDataTable.Columns.Remove(GetColumn(DATA_FIELD.EMAIL));

            this.dataGridView1.DataSource = this.membersDataTable;
            this.labelRowCount.Text = string.Format("총 {0}명의 신도가 검색되었습니다. ", dataGridView1.Rows.Count - 1);

            this.checkBoxReceiver.Checked = false;
            AddCheckBoxColumn();

            //현재 selected row로 돌아오자.
            if (iSelectedIndex != 0 && iSelectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = iSelectedIndex;
                dataGridView1.CurrentCell = dataGridView1.Rows[iSelectedIndex].Cells[0];
            }
                
        }

        private void AddCheckBoxColumn()
        {
            if (this.dataGridView1.Columns.Contains("선택하기")) return;

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.Name = "선택하기";
            checkBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            this.dataGridView1.Columns.Add(checkBoxColumn);

            //방금 추가한 checkbox Column하나만 제외하고는 전부 readonly로 바꿉니다!!!
            int indexCheckBoxColumn = this.dataGridView1.Columns["선택하기"].Index;

            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                if(i != indexCheckBoxColumn)
                    this.dataGridView1.Columns[i].ReadOnly = true;
            }
            
        }

        private void buttonLogin_Click_1(object sender, EventArgs e)
        {
            string strID = textBoxLoginID.Text;
            string strPWD = textBoxLoginPassword.Text;

            if (string.IsNullOrWhiteSpace(strID) || string.IsNullOrWhiteSpace(strPWD))
            {
                MessageBox.Show("아이디와 패스워드를 입력해주세요. ");
            }
            else
            {
                OBSILoginResult loginResult = new OBSILoginResult();
                string strISVKey = "";

                try
                {
                    OBSISetReceiveCallbackWindow(this.Handle, WM_USER); //전화 착신 알림받을 callback Window 설정                    
                    if (OBSILogin(strID, strPWD, strISVKey, ref loginResult) != 0)
                    {
                        IsLoggedIn = false;
                        MessageBox.Show("올레 비즈 세이 시스템에 로그인하지 못했습니다.\n" + loginResult.Result.szMessage);
                        return;
                    }
                    this.IsLoggedIn = true;
                    string strMsg = "올레 비즈 세이 시스템에 성공적으로 로그인하였습니다!!!\n";
                    // Oaasys 인증 결과
                    if (strISVKey.Length > 0)
                        strMsg += loginResult.ResultOaasys.szMessage;

                    MessageBox.Show(strMsg);
                    OBSISetCallbackWindow(this.Handle, WM_APP);         // 메시지 발송 결과 콜백 윈도 설정
                }
                catch
                {
                    MessageBox.Show("올레 비즈 모듈 함수를 불러오는 데에 실패하였습니다.");
                }

            }

        }

        private void buttonSendMsg_Click_1(object sender, EventArgs e)
        {   
            if (string.IsNullOrWhiteSpace(textBoxMsg.Text))
            {
                MessageBox.Show("문자 내용을 입력해주세요.");
                return;
            }

            var receiversList = new List<string>();
            for (int i = 0; i < this.dataGridView1.RowCount; i++)
            {                
                bool bChecked = Convert.ToBoolean(this.dataGridView1.Rows[i].Cells["선택하기"].Value);
                if (bChecked)
                {
                    var phoneNumber = this.dataGridView1.Rows[i].Cells[GetColumn(DATA_FIELD.PHONE)].Value;
                    if(phoneNumber != null && !string.IsNullOrWhiteSpace(phoneNumber.ToString()))
                        receiversList.Add(phoneNumber.ToString());
                }
            }

            if (receiversList.Count == 0)
            {
                MessageBox.Show("수신자가 선택되지 않았습니다.");
                return;
            }

            DialogResult result = MessageBox.Show("총 " + receiversList.Count + "명의 신도들에게 메시지를 발송하시겠습니까?", "발송하기", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                string strTotalSendResult = string.Empty;
                for (int a = 0; a < receiversList.Count; a++)
                {
                    if ( SendTextMessageByOBSI(receiversList[a], textBoxMsg.Text, out string strSendResult, GetSenderNumber((LocalTemple)comboBoxSender.SelectedItem)) )
                        strTotalSendResult += (receiversList[a] + " : 전송에 성공하였습니다." + strSendResult + "\n");
                    else
                        strTotalSendResult += (receiversList[a] + " : 전송에 실패하였습니다." + strSendResult + "\n");
                }
                MessageBox.Show(strTotalSendResult);
            }
            
        }

        public static string GetSenderNumber(LocalTemple temple)
        {
            //TODO : This part that hard coded can be moved to database, in Azure.
            string senderNumber;            
            switch (temple)
            {
                case LocalTemple.춘천본사: senderNumber = "0332431787";    break;
                case LocalTemple.부산분원: senderNumber = "0515545150";    break;
                case LocalTemple.대전분원: senderNumber = "0425255325";    break;                                
                case LocalTemple.고성분원: senderNumber = "0336815515";    break;                
                case LocalTemple.대구분원: senderNumber = "01026177350"; break;
                //다음 번호들은 올레 비즈 세이 사이트에서 회신 번호로 등록을 해야 할 것으로 보입니다 - 2019.4.29
                //case LocalTemple.제주제2본산:   senderNumber = "0647835355";    break;
                //case LocalTemple.일본분원: senderNumber = "001-81-827-31-1611"; break;
                //case LocalTemple.서울: break;
                //case LocalTemple.김해분원: break;                
                default:
                    senderNumber = "0332431787";
                    MessageBox.Show(temple.ToString() + "은 올레 비즈 사이트에 회신 번호가 아직 등록되지 않았기에 " +
                        "춘천 본사 번호로 회신 번호를 지정합니다.");
                    break;
            }
            return senderNumber;
        }

        //Olleh Biz Say Inside 이용하여 실제로 문자 보내기!
        public static bool SendTextMessageByOBSI(string phoneNumber, string strContentText, out string ResultMessage,
            string callbackNumber, string receiverName = "현지사 신도분들", string strTextTitle = "공지")
        {
            if (!_isLoggedIn)
            {
                MessageBox.Show("문자 보내기 탭에서 먼저 올레 비즈 세이에 로그인해주십시오.");
                ResultMessage = string.Empty;
                return false;
            }

            OBSIMessage msg = new OBSIMessage();
            OBSIReceiver receivers = new OBSIReceiver();
            OBSISendResult result = new OBSISendResult();

            //LoadOBSIMessage(msg, receivers);
            msg.szSubject = strTextTitle;           //제목
            msg.szCallbackNumber = callbackNumber;  //회신 번호
            receivers.szName = receiverName;        //받을 분의 이름
            receivers.szPhoneNumber = phoneNumber;  //받을 분의 번호

            //한 번에 한 사람에게만 보냅시다.
            msg.ulReceiverCount = 1;
            IntPtr pMsgReceivers = Marshal.AllocHGlobal(Marshal.SizeOf(receivers));
            Marshal.StructureToPtr(receivers, pMsgReceivers, false);
            msg.pReceivers = pMsgReceivers;


            //보낼 메시지의 내용을 정합시다. 
            IntPtr pContent = IntPtr.Zero;
            if (strContentText.Length <= SMS_LIMIT)
            {
                currentMessageService = MESSAGE_SERVICE.SHORT_MESSAGE;
                msg.lMsgType = OBSI_SMS;
                OBSISMSContent content = new OBSISMSContent();
                content.szMessage = strContentText;

                pContent = Marshal.AllocHGlobal(Marshal.SizeOf(content));
                Marshal.StructureToPtr(content, pContent, false);
                msg.pMessageContent = pContent;
            }
            else if (strContentText.Length <= MMS_LIMIT)
            {
                currentMessageService = MESSAGE_SERVICE.MULTI_MESSAGE;
                msg.lMsgType = OBSI_MMS;
                OBSIMMSContent content = new OBSIMMSContent();
                content.szMessage = strContentText;
                pContent = Marshal.AllocHGlobal(Marshal.SizeOf(content));
                Marshal.StructureToPtr(content, pContent, false);
                msg.pMessageContent = pContent;
            }
            else
            {
                ResultMessage = "글자 길이가 " + MMS_LIMIT + "를 초과하여 전송할 수 없습니다.";
                //MessageBox.Show(ResultMessage);
                return false;
            }

            //실제로 보내고 그 결과를 기록합니다!
            bool bResult = (OBSISend(ref msg, IntPtr.Zero, ref result) == 0);
            ResultMessage = (bResult) ? currentMessageService.ToString() : result.Result.szMessage;

            //Free the memory in Pointer
            Marshal.FreeHGlobal(pMsgReceivers);
            Marshal.FreeHGlobal(pContent);

            return bResult;
        }

        //CheckBox 체크할 때 
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= this.dataGridView1.RowCount)
                return; 

            //int iLastCheckBoxColumnIndex = this.dataGridView1.ColumnCount - 1;
            if ( dataGridView1.Columns[e.ColumnIndex].Name == "선택하기")
            {
                string columnName = GetColumn(DATA_FIELD.PHONE);                
                string phoneNumber = this.dataGridView1.Rows[e.RowIndex].Cells[columnName].Value.ToString();

                //Console.WriteLine("체크된 행의 Cell Phone 번호입니다!" + phoneNumber);                
            }
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {            
            this.RefreshReceiverListInDatagridview();
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {            
            this.RefreshReceiverListInDatagridview(true);
        }

        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.MinimumHeight = MainWindow.iDatagridviewRowHeight;
            }
            this.dataGridView1.DefaultCellStyle.Font = new Font("Gulim", MainWindow.iDatagridviewFontSize);
        }

        private void CheckBoxReceiver_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.dataGridView1.Columns.Contains("선택하기")) return;

            int indexCheckBoxColumn = this.dataGridView1.Columns["선택하기"].Index;
            for (int j = 0; j < this.dataGridView1.RowCount; j++)
            {
                this.dataGridView1[indexCheckBoxColumn, j].Value = this.checkBoxReceiver.Checked;
            }
            this.dataGridView1.EndEdit();
        }
    }
}
