﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HyunjisaBeliever.MainWindow;

namespace HyunjisaBeliever
{
    public partial class BelieverSearchForm : Form
    {
        public delegate void SelectIDEventHandler(int id);
        public event SelectIDEventHandler EventSelectBelieverID;

        private int _selectedBelieverID;
        public int SelectedBelieverID
        {
            get { return _selectedBelieverID; }
            set
            {
                _selectedBelieverID = value;
                EventSelectBelieverID?.Invoke(_selectedBelieverID);
            }
        }

        public BelieverSearchForm()
        {
            InitializeComponent();

            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputBoxSearchCondition_KeyPress);
            this.textBoxDharmaName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputBoxSearchCondition_KeyPress);
            this.textBoxGanzhi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputBoxSearchCondition_KeyPress);
            this.textBoxPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputBoxSearchCondition_KeyPress);
            this.textBoxAddressDetail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputBoxSearchCondition_KeyPress);

        }

        private void BelieverSearchForm_Load(object sender, EventArgs e)
        {
            ResetBelieverDatagridview();

            comboBoxGender.DataSource = Enum.GetValues(typeof(Gender));
            comboBoxBelongedTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxProvince.DataSource = Enum.GetValues(typeof(Province));
                        
            comboBoxGender.ResetText();
            comboBoxProvince.ResetText();
            comboBoxBelongedTemple.ResetText();
        }

        public void ResetBelieverDatagridview(params string[] searchConditions)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();
            this.dataGridView1.Columns.Clear();
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn { Name = "선택하기" };
            checkBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.Columns.Add(checkBoxColumn);

            //이제 신도 정보로 테이블을 채웁니다. 
            var believerDataTable = SqlManager.SelectBelieverListFromSQL( /*false,*/ searchConditions );            
            believerDataTable.Columns.Remove(GetColumn(DATA_FIELD.MEMBER_NUMBER));            
            believerDataTable.Columns.Remove(GetColumn(DATA_FIELD.BIRTH));
            believerDataTable.Columns.Remove(GetColumn(DATA_FIELD.REGISTER));
            believerDataTable.Columns.Remove(GetColumn(DATA_FIELD.EMAIL));
            this.dataGridView1.DataSource = believerDataTable;

            //먼저 추가한 checkbox Column하나만 제외하고는 전부 readonly로 바꿉니다!!!
            int indexCheckBoxColumn = this.dataGridView1.Columns["선택하기"].Index;
            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                if (i != indexCheckBoxColumn)
                    this.dataGridView1.Columns[i].ReadOnly = true;
            }
        }

        public void RefreshBelieverListInDataGridView()
        {
            var searchConditions = new List<string>();
            //여기서 datagridview 테이블을 검색된 것만 보여주면 될 것 같습니다...
            if (!string.IsNullOrWhiteSpace(textBoxName.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.NAME) + " LIKE '%" + textBoxName.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxDharmaName.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.DHARMA_NAME) + " LIKE '%" + textBoxDharmaName.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxGanzhi.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.GANZHI) + " LIKE '%" + textBoxGanzhi.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxPhoneNumber.Text))
            {
                string newString = Regex.Replace(textBoxPhoneNumber.Text, "[^.0-9]", "");
                if (!string.IsNullOrWhiteSpace(newString))
                    searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.PHONE) + " LIKE '%" + textBoxPhoneNumber.Text + "%'");
            }

            if (!string.IsNullOrWhiteSpace(comboBoxGender.Text))
                searchConditions.Add(" B.Gender = " + Convert.ToString((int)(Enum.Parse(typeof(Gender), comboBoxGender.Text))));

            //검색은 열람만 하는 것이고, 또 기도 등록같은 경우는 다른 분원에 할 수도 있으니 이 때는 검색 가능토록 해야 합니다...
            if (!string.IsNullOrWhiteSpace(comboBoxBelongedTemple.Text))
                searchConditions.Add(" B.BelongedTemple = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), comboBoxBelongedTemple.Text))));

            if (!string.IsNullOrWhiteSpace(comboBoxProvince.Text))
                searchConditions.Add(" B.AddressProvince = " + Convert.ToString((int)(Enum.Parse(typeof(Province), comboBoxProvince.Text))));

            if (!string.IsNullOrWhiteSpace(textBoxAddressDetail.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.ADDRESS) + " LIKE '%" + textBoxAddressDetail.Text + "%'");

            this.ResetBelieverDatagridview(searchConditions.ToArray());
        }

        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            RefreshBelieverListInDataGridView();
        }

        private void ButtonSelect_Click(object sender, EventArgs e)
        {
            if (SelectedBelieverID == 0)
                MessageBox.Show("신도가 선택되지 않았습니다. 신도를 선택해주세요. ");
            else
            {
                this.DialogResult = DialogResult.OK;                
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (this.DialogResult != DialogResult.OK)
            {
                this.SelectedBelieverID = 0;

            }

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {            
            int indexCheckBoxColumn = this.dataGridView1.Columns["선택하기"].Index;
            int iTotalRowCount = this.dataGridView1.RowCount;

            if ( e.ColumnIndex == indexCheckBoxColumn
              && e.RowIndex >= 0 && e.RowIndex < iTotalRowCount)
            {   
                //Just one click event the modification is over, so commit is necessary
                this.dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);

                var isChecked = (bool)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                if (isChecked)
                {
                    //체크된 신자의 ID를 넘겨서 신도 지정을 먼저 합니다. 
                    SelectedBelieverID = (int)dataGridView1.Rows[e.RowIndex].Cells[GetColumn(DATA_FIELD.UID)].Value;
                    
                    //그 외의 신자는 체크를 해제합니다! (한 신자만 선택되어야 하기 때문에)
                    for (int i = 0; i < iTotalRowCount; i++)
                    {
                        if (i != e.RowIndex)
                            this.dataGridView1.Rows[i].Cells[indexCheckBoxColumn].Value = false;
                    }
                }
                else
                {
                    SelectedBelieverID = 0;                    
                }
            }
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            this.textBoxName.ResetText();
            this.textBoxDharmaName.ResetText();
            this.textBoxGanzhi.ResetText();
            this.textBoxPhoneNumber.ResetText();
            this.textBoxAddressDetail.ResetText();

            this.comboBoxGender.ResetText();
            this.comboBoxProvince.ResetText();
            this.comboBoxBelongedTemple.ResetText();
            this.ResetBelieverDatagridview();
        }

        private void InputBoxSearchCondition_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshBelieverListInDataGridView();
        }

        private void ComboBoxBelongedTemple_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strTemple = this.comboBoxBelongedTemple.Text;
                if (!string.IsNullOrWhiteSpace(strTemple)
                 && !Enum.TryParse<LocalTemple>(strTemple, out LocalTemple selectedTemple))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    RefreshBelieverListInDataGridView();
            }
        }

        private void ComboBoxProvince_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strProvince = this.comboBoxProvince.Text;
                if (!string.IsNullOrWhiteSpace(strProvince)
                 && !Enum.TryParse<Province>(strProvince, out Province selectedProvince))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    RefreshBelieverListInDataGridView();
            }
        }

        private void ComboBoxGender_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshBelieverListInDataGridView();
            else
                e.Handled = true;

        }
    }
}
