﻿namespace HyunjisaBeliever
{
    partial class UserControlPray
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPrayerName = new System.Windows.Forms.TextBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.comboBoxPrayState = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxPrayTemple = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxBuddha = new System.Windows.Forms.ComboBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.labelBeliever = new System.Windows.Forms.Label();
            this.labelBuddha = new System.Windows.Forms.Label();
            this.textBoxBeliever = new System.Windows.Forms.TextBox();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelRowCount = new System.Windows.Forms.Label();
            this.buttonImportExcel = new System.Windows.Forms.Button();
            this.buttonExportExcel = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPaperNumber = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.PapayaWhip;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxPaperNumber);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxSearch);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxPrayerName);
            this.panel1.Controls.Add(this.buttonReset);
            this.panel1.Controls.Add(this.comboBoxPrayState);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comboBoxPrayTemple);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBoxBuddha);
            this.panel1.Controls.Add(this.buttonConfirm);
            this.panel1.Controls.Add(this.labelBeliever);
            this.panel1.Controls.Add(this.labelBuddha);
            this.panel1.Controls.Add(this.textBoxBeliever);
            this.panel1.Location = new System.Drawing.Point(18, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1021, 85);
            this.panel1.TabIndex = 96;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(254, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 104;
            this.label3.Text = "통합 검색";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(341, 58);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(157, 21);
            this.textBoxSearch.TabIndex = 103;
            this.textBoxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxDonator_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(270, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 102;
            this.label2.Text = "입재자";
            // 
            // textBoxPrayerName
            // 
            this.textBoxPrayerName.Location = new System.Drawing.Point(341, 31);
            this.textBoxPrayerName.Name = "textBoxPrayerName";
            this.textBoxPrayerName.Size = new System.Drawing.Size(157, 21);
            this.textBoxPrayerName.TabIndex = 101;
            this.textBoxPrayerName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxPrayerName_KeyPress);
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonReset.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonReset.FlatAppearance.BorderSize = 2;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonReset.ForeColor = System.Drawing.Color.Maroon;
            this.buttonReset.Location = new System.Drawing.Point(895, 6);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(116, 50);
            this.buttonReset.TabIndex = 100;
            this.buttonReset.Text = "초기화";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // comboBoxPrayState
            // 
            this.comboBoxPrayState.FormattingEnabled = true;
            this.comboBoxPrayState.Location = new System.Drawing.Point(600, 6);
            this.comboBoxPrayState.Name = "comboBoxPrayState";
            this.comboBoxPrayState.Size = new System.Drawing.Size(157, 20);
            this.comboBoxPrayState.TabIndex = 99;
            this.comboBoxPrayState.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxPrayState_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(529, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 98;
            this.label4.Text = "진행 상황";
            // 
            // comboBoxPrayTemple
            // 
            this.comboBoxPrayTemple.FormattingEnabled = true;
            this.comboBoxPrayTemple.Location = new System.Drawing.Point(83, 6);
            this.comboBoxPrayTemple.Name = "comboBoxPrayTemple";
            this.comboBoxPrayTemple.Size = new System.Drawing.Size(157, 20);
            this.comboBoxPrayTemple.TabIndex = 97;
            this.comboBoxPrayTemple.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPrayTemple_SelectedIndexChanged);
            this.comboBoxPrayTemple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxPrayTemple_KeyPress);
            this.comboBoxPrayTemple.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBoxPrayTemple_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 96;
            this.label1.Text = "접수 분원";
            // 
            // comboBoxBuddha
            // 
            this.comboBoxBuddha.FormattingEnabled = true;
            this.comboBoxBuddha.Location = new System.Drawing.Point(83, 32);
            this.comboBoxBuddha.Name = "comboBoxBuddha";
            this.comboBoxBuddha.Size = new System.Drawing.Size(157, 20);
            this.comboBoxBuddha.TabIndex = 91;
            this.comboBoxBuddha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxBuddha_KeyPress);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.FlatAppearance.BorderSize = 2;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.Location = new System.Drawing.Point(773, 6);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(116, 50);
            this.buttonConfirm.TabIndex = 89;
            this.buttonConfirm.Text = "기도 검색";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // labelBeliever
            // 
            this.labelBeliever.AutoSize = true;
            this.labelBeliever.Location = new System.Drawing.Point(270, 9);
            this.labelBeliever.Name = "labelBeliever";
            this.labelBeliever.Size = new System.Drawing.Size(41, 12);
            this.labelBeliever.TabIndex = 79;
            this.labelBeliever.Text = "신청자";
            // 
            // labelBuddha
            // 
            this.labelBuddha.AutoSize = true;
            this.labelBuddha.Location = new System.Drawing.Point(12, 35);
            this.labelBuddha.Name = "labelBuddha";
            this.labelBuddha.Size = new System.Drawing.Size(41, 12);
            this.labelBuddha.TabIndex = 78;
            this.labelBuddha.Text = "부처님";
            // 
            // textBoxBeliever
            // 
            this.textBoxBeliever.Location = new System.Drawing.Point(341, 5);
            this.textBoxBeliever.Name = "textBoxBeliever";
            this.textBoxBeliever.Size = new System.Drawing.Size(157, 21);
            this.textBoxBeliever.TabIndex = 76;
            this.textBoxBeliever.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxBeliever_KeyPress);
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonInsert.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonInsert.FlatAppearance.BorderSize = 2;
            this.buttonInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonInsert.ForeColor = System.Drawing.Color.Maroon;
            this.buttonInsert.Location = new System.Drawing.Point(660, 50);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(116, 50);
            this.buttonInsert.TabIndex = 94;
            this.buttonInsert.Text = "새로 입력하기";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Gulim", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(18, 197);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 50;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1021, 315);
            this.dataGridView1.TabIndex = 93;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellDoubleClick);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DataGridView1_CellPainting);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridView1_MouseClick);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTitle.Location = new System.Drawing.Point(27, 26);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(67, 37);
            this.labelTitle.TabIndex = 92;
            this.labelTitle.Text = "기도";
            // 
            // labelRowCount
            // 
            this.labelRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRowCount.AutoSize = true;
            this.labelRowCount.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelRowCount.Location = new System.Drawing.Point(14, 515);
            this.labelRowCount.Name = "labelRowCount";
            this.labelRowCount.Size = new System.Drawing.Size(265, 21);
            this.labelRowCount.TabIndex = 97;
            this.labelRowCount.Text = "총 n개의 데이터가 검색되었습니다.";
            // 
            // buttonImportExcel
            // 
            this.buttonImportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonImportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.FlatAppearance.BorderSize = 2;
            this.buttonImportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonImportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.Location = new System.Drawing.Point(791, 50);
            this.buttonImportExcel.Name = "buttonImportExcel";
            this.buttonImportExcel.Size = new System.Drawing.Size(116, 50);
            this.buttonImportExcel.TabIndex = 98;
            this.buttonImportExcel.Text = "Excel 읽기";
            this.buttonImportExcel.UseVisualStyleBackColor = false;
            this.buttonImportExcel.Visible = false;
            this.buttonImportExcel.Click += new System.EventHandler(this.ButtonImportExcel_Click);
            // 
            // buttonExportExcel
            // 
            this.buttonExportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonExportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.FlatAppearance.BorderSize = 2;
            this.buttonExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonExportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.Location = new System.Drawing.Point(913, 50);
            this.buttonExportExcel.Name = "buttonExportExcel";
            this.buttonExportExcel.Size = new System.Drawing.Size(116, 50);
            this.buttonExportExcel.TabIndex = 98;
            this.buttonExportExcel.Text = "Excel 출력";
            this.buttonExportExcel.UseVisualStyleBackColor = false;
            this.buttonExportExcel.Click += new System.EventHandler(this.ButtonExportExcel_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(791, 21);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(238, 23);
            this.progressBar1.TabIndex = 99;
            this.progressBar1.Visible = false;
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(530, 21);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(246, 21);
            this.txtFilePath.TabIndex = 134;
            this.txtFilePath.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 106;
            this.label5.Text = "원장 번호";
            // 
            // textBoxPaperNumber
            // 
            this.textBoxPaperNumber.Location = new System.Drawing.Point(83, 58);
            this.textBoxPaperNumber.Name = "textBoxPaperNumber";
            this.textBoxPaperNumber.Size = new System.Drawing.Size(157, 21);
            this.textBoxPaperNumber.TabIndex = 105;
            this.textBoxPaperNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPaperNumber_KeyPress);
            // 
            // UserControlPray
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonExportExcel);
            this.Controls.Add(this.buttonImportExcel);
            this.Controls.Add(this.labelRowCount);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.labelTitle);
            this.Name = "UserControlPray";
            this.Size = new System.Drawing.Size(1056, 612);
            this.Load += new System.EventHandler(this.UserControlPray_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxBuddha;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Label labelBeliever;
        private System.Windows.Forms.Label labelBuddha;
        private System.Windows.Forms.TextBox textBoxBeliever;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.ComboBox comboBoxPrayState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxPrayTemple;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label labelRowCount;
        private System.Windows.Forms.Button buttonExportExcel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtFilePath;
        public System.Windows.Forms.Button buttonImportExcel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPrayerName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPaperNumber;
    }
}
