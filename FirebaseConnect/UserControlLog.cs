﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserControlLog : UserControl
    {
        public bool isAwaken = false;
        readonly string queryForLog = "SELECT LogID, Time as 시간, MonkName as 스님, LogQuery, T.TempleName as 분원 " +
                "FROM TaskLog as L " +
                "INNER JOIN Temples AS T ON L.manageArea = T.TempleID ";

        public UserControlLog()
        {
            InitializeComponent();
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;

            this.comboBoxTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
        }

        private void UserControlLog_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now.Date;
            RefreshLogListInDataGridView(true);
            
        }

        void ResetLogListSearchCondition()
        {   
            this.comboBoxTemple.ResetText();
            this.textBoxMonk.ResetText();

            this.dateTimePicker1.Value = DateTime.Now.Date;            
        }

        public void RefreshLogListInDataGridView(bool bResetSearchConditions = false)
        {
            if (!isAwaken) return;
            if (MainWindow.accessedMonk == null) return;

            dataGridView1.DataSource = null;
            var searchConditions = new List<string>();
            if (bResetSearchConditions)
            {
                ResetLogListSearchCondition();
            }

            if (!string.IsNullOrWhiteSpace(comboBoxTemple.Text))
                searchConditions.Add(" manageArea = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), comboBoxTemple.Text))));
            
            if (!string.IsNullOrWhiteSpace(textBoxMonk.Text))
                searchConditions.Add(" MonkName = '" + textBoxMonk.Text + "' ");

            if (dateTimePicker1.Value != dateTimePicker1.MinDate)
                searchConditions.Add(" cast (L.Time as date) = '" + dateTimePicker1.Value.ToShortDateString() + "' ");

            string query = queryForLog;
            if (searchConditions.Count != 0)
            {
                int conditionNumber = searchConditions.Count;
                if (conditionNumber > 0)
                {
                    string searchCondition = " WHERE ";
                    for (int i = 0; i < conditionNumber; i++)
                    {
                        string seperator = (i == 0) ? "" : " AND ";
                        searchCondition += (seperator + searchConditions[i]);
                    }
                    query += searchCondition;
                }
            }

            dataGridView1.DataSource = SqlManager.GetDataTableFromSQL(query);

            this.labelRowCount.Text = string.Format("총 {0}개의 데이터가 검색되었습니다. ", dataGridView1.Rows.Count - 1);

            //발원이 길 경우 multiline으로 만들기 위한 코드...
            //dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;            
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                string columnName = dataGridView1.Columns[i].Name;
                if (columnName == "LogQuery")
                    dataGridView1.Columns[i].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }            
        }

        private void ButtonExportExcel_Click(object sender, EventArgs e)
        {
            MainWindow.ExportExcelFile(dataGridView1, progressBar1);
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            RefreshLogListInDataGridView(true);
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            RefreshLogListInDataGridView();
        }

        private void ComboBoxTemple_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strTemple = this.comboBoxTemple.Text;
                if (!string.IsNullOrWhiteSpace(strTemple)
                 && !Enum.TryParse<LocalTemple>(strTemple, out LocalTemple selectedTemple))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshLogListInDataGridView();
            }
        }

        private void DateTimePicker1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshLogListInDataGridView();
        }

        private void TextBoxMonk_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.RefreshLogListInDataGridView();
        }
    }
}
