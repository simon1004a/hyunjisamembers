﻿namespace HyunjisaBeliever
{
    partial class UserControlBeliever
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelTitle = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.buttonImportExcel = new System.Windows.Forms.Button();
            this.buttonExportExcel = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panelInput = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelGanzhi = new System.Windows.Forms.Label();
            this.textBoxGanzhi = new System.Windows.Forms.TextBox();
            this.labelDharmaname = new System.Windows.Forms.Label();
            this.textBoxDharmaname = new System.Windows.Forms.TextBox();
            this.comboBoxProvince = new System.Windows.Forms.ComboBox();
            this.labelProvince = new System.Windows.Forms.Label();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.comboBoxBelongedTemple = new System.Windows.Forms.ComboBox();
            this.labelArea = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelRowCount = new System.Windows.Forms.Label();
            this.buttonFamilyList = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelInput.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTitle.Location = new System.Drawing.Point(25, 20);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(126, 37);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "신도 관리";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(499, 18);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(246, 21);
            this.txtFilePath.TabIndex = 53;
            this.txtFilePath.Visible = false;
            // 
            // buttonImportExcel
            // 
            this.buttonImportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonImportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.FlatAppearance.BorderSize = 2;
            this.buttonImportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonImportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.Location = new System.Drawing.Point(781, 50);
            this.buttonImportExcel.Name = "buttonImportExcel";
            this.buttonImportExcel.Size = new System.Drawing.Size(116, 49);
            this.buttonImportExcel.TabIndex = 52;
            this.buttonImportExcel.Text = "Excel 읽기";
            this.buttonImportExcel.UseVisualStyleBackColor = false;
            this.buttonImportExcel.Visible = false;
            this.buttonImportExcel.Click += new System.EventHandler(this.buttonImportExcel_Click);
            // 
            // buttonExportExcel
            // 
            this.buttonExportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonExportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.FlatAppearance.BorderSize = 2;
            this.buttonExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonExportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.Location = new System.Drawing.Point(903, 50);
            this.buttonExportExcel.Name = "buttonExportExcel";
            this.buttonExportExcel.Size = new System.Drawing.Size(123, 49);
            this.buttonExportExcel.TabIndex = 51;
            this.buttonExportExcel.Text = "Excel 출력";
            this.buttonExportExcel.UseVisualStyleBackColor = false;
            this.buttonExportExcel.Click += new System.EventHandler(this.buttonExportExcel_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Gulim", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(19, 214);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1008, 300);
            this.dataGridView1.TabIndex = 42;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DataGridView1_CellPainting);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonInsert.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonInsert.FlatAppearance.BorderSize = 2;
            this.buttonInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonInsert.ForeColor = System.Drawing.Color.Maroon;
            this.buttonInsert.Location = new System.Drawing.Point(629, 50);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(116, 50);
            this.buttonInsert.TabIndex = 37;
            this.buttonInsert.Text = "신도 추가";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // panelInput
            // 
            this.panelInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelInput.BackColor = System.Drawing.Color.PapayaWhip;
            this.panelInput.Controls.Add(this.buttonRefresh);
            this.panelInput.Controls.Add(this.labelGanzhi);
            this.panelInput.Controls.Add(this.textBoxGanzhi);
            this.panelInput.Controls.Add(this.labelDharmaname);
            this.panelInput.Controls.Add(this.textBoxDharmaname);
            this.panelInput.Controls.Add(this.comboBoxProvince);
            this.panelInput.Controls.Add(this.labelProvince);
            this.panelInput.Controls.Add(this.buttonConfirm);
            this.panelInput.Controls.Add(this.comboBoxGender);
            this.panelInput.Controls.Add(this.comboBoxBelongedTemple);
            this.panelInput.Controls.Add(this.labelArea);
            this.panelInput.Controls.Add(this.labelGender);
            this.panelInput.Controls.Add(this.labelPhone);
            this.panelInput.Controls.Add(this.labelName);
            this.panelInput.Controls.Add(this.textBoxPhoneNumber);
            this.panelInput.Controls.Add(this.textBoxName);
            this.panelInput.Location = new System.Drawing.Point(19, 105);
            this.panelInput.Name = "panelInput";
            this.panelInput.Size = new System.Drawing.Size(1007, 103);
            this.panelInput.TabIndex = 56;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonRefresh.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonRefresh.FlatAppearance.BorderSize = 2;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonRefresh.ForeColor = System.Drawing.Color.Maroon;
            this.buttonRefresh.Location = new System.Drawing.Point(884, 16);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(118, 50);
            this.buttonRefresh.TabIndex = 81;
            this.buttonRefresh.Text = "초기화";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // labelGanzhi
            // 
            this.labelGanzhi.AutoSize = true;
            this.labelGanzhi.Location = new System.Drawing.Point(497, 20);
            this.labelGanzhi.Name = "labelGanzhi";
            this.labelGanzhi.Size = new System.Drawing.Size(63, 12);
            this.labelGanzhi.TabIndex = 80;
            this.labelGanzhi.Text = "생년(간지)";
            // 
            // textBoxGanzhi
            // 
            this.textBoxGanzhi.Location = new System.Drawing.Point(568, 16);
            this.textBoxGanzhi.Name = "textBoxGanzhi";
            this.textBoxGanzhi.Size = new System.Drawing.Size(158, 21);
            this.textBoxGanzhi.TabIndex = 79;
            this.textBoxGanzhi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxGanzhi_KeyPress);
            // 
            // labelDharmaname
            // 
            this.labelDharmaname.AutoSize = true;
            this.labelDharmaname.Location = new System.Drawing.Point(264, 47);
            this.labelDharmaname.Name = "labelDharmaname";
            this.labelDharmaname.Size = new System.Drawing.Size(29, 12);
            this.labelDharmaname.TabIndex = 78;
            this.labelDharmaname.Text = "법명";
            // 
            // textBoxDharmaname
            // 
            this.textBoxDharmaname.Location = new System.Drawing.Point(312, 43);
            this.textBoxDharmaname.Name = "textBoxDharmaname";
            this.textBoxDharmaname.Size = new System.Drawing.Size(168, 21);
            this.textBoxDharmaname.TabIndex = 77;
            this.textBoxDharmaname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxDharmaname_KeyPress);
            // 
            // comboBoxProvince
            // 
            this.comboBoxProvince.FormattingEnabled = true;
            this.comboBoxProvince.Location = new System.Drawing.Point(82, 42);
            this.comboBoxProvince.Name = "comboBoxProvince";
            this.comboBoxProvince.Size = new System.Drawing.Size(167, 20);
            this.comboBoxProvince.TabIndex = 76;
            this.comboBoxProvince.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxProvince_KeyPress);
            this.comboBoxProvince.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBoxProvince_KeyUp);
            // 
            // labelProvince
            // 
            this.labelProvince.AutoSize = true;
            this.labelProvince.Location = new System.Drawing.Point(11, 45);
            this.labelProvince.Name = "labelProvince";
            this.labelProvince.Size = new System.Drawing.Size(29, 12);
            this.labelProvince.TabIndex = 75;
            this.labelProvince.Text = "지역";
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.FlatAppearance.BorderSize = 2;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.Location = new System.Drawing.Point(762, 16);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(116, 50);
            this.buttonConfirm.TabIndex = 74;
            this.buttonConfirm.Text = "검색하기";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Location = new System.Drawing.Point(82, 68);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(167, 20);
            this.comboBoxGender.TabIndex = 67;
            this.comboBoxGender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxGender_KeyPress);
            // 
            // comboBoxBelongedTemple
            // 
            this.comboBoxBelongedTemple.FormattingEnabled = true;
            this.comboBoxBelongedTemple.Location = new System.Drawing.Point(82, 16);
            this.comboBoxBelongedTemple.Name = "comboBoxBelongedTemple";
            this.comboBoxBelongedTemple.Size = new System.Drawing.Size(167, 20);
            this.comboBoxBelongedTemple.TabIndex = 66;
            this.comboBoxBelongedTemple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxBelongedTemple_KeyPress);
            this.comboBoxBelongedTemple.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBoxBelongedTemple_KeyUp);
            // 
            // labelArea
            // 
            this.labelArea.AutoSize = true;
            this.labelArea.Location = new System.Drawing.Point(11, 19);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(53, 12);
            this.labelArea.TabIndex = 64;
            this.labelArea.Text = "소속분원";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.Location = new System.Drawing.Point(11, 72);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(29, 12);
            this.labelGender.TabIndex = 62;
            this.labelGender.Text = "성별";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(497, 47);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(65, 12);
            this.labelPhone.TabIndex = 61;
            this.labelPhone.Text = "휴대폰번호";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(264, 20);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(29, 12);
            this.labelName.TabIndex = 59;
            this.labelName.Text = "이름";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(568, 43);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(158, 21);
            this.textBoxPhoneNumber.TabIndex = 58;
            this.textBoxPhoneNumber.Text = "-는 빼고 입력해주세요";
            this.textBoxPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumberOnly_KeyPress);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(312, 16);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(168, 21);
            this.textBoxName.TabIndex = 56;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxName_KeyPress);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(780, 18);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(246, 23);
            this.progressBar1.TabIndex = 58;
            this.progressBar1.Visible = false;
            // 
            // labelRowCount
            // 
            this.labelRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRowCount.AutoSize = true;
            this.labelRowCount.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelRowCount.Location = new System.Drawing.Point(19, 521);
            this.labelRowCount.Name = "labelRowCount";
            this.labelRowCount.Size = new System.Drawing.Size(265, 21);
            this.labelRowCount.TabIndex = 59;
            this.labelRowCount.Text = "총 n개의 데이터가 검색되었습니다.";
            // 
            // buttonFamilyList
            // 
            this.buttonFamilyList.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonFamilyList.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonFamilyList.FlatAppearance.BorderSize = 2;
            this.buttonFamilyList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFamilyList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonFamilyList.ForeColor = System.Drawing.Color.Maroon;
            this.buttonFamilyList.Location = new System.Drawing.Point(169, 50);
            this.buttonFamilyList.Name = "buttonFamilyList";
            this.buttonFamilyList.Size = new System.Drawing.Size(161, 50);
            this.buttonFamilyList.TabIndex = 60;
            this.buttonFamilyList.Text = "가족 정보 보기";
            this.buttonFamilyList.UseVisualStyleBackColor = false;
            this.buttonFamilyList.Visible = false;
            this.buttonFamilyList.Click += new System.EventHandler(this.ButtonFamilyList_Click);
            // 
            // UserControlBeliever
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.Controls.Add(this.buttonFamilyList);
            this.Controls.Add(this.labelRowCount);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.buttonImportExcel);
            this.Controls.Add(this.buttonExportExcel);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.panelInput);
            this.Controls.Add(this.dataGridView1);
            this.Name = "UserControlBeliever";
            this.Size = new System.Drawing.Size(1056, 612);
            this.Load += new System.EventHandler(this.UserControl1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelInput.ResumeLayout(false);
            this.panelInput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button buttonExportExcel;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panelInput;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.ComboBox comboBoxBelongedTemple;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxProvince;
        private System.Windows.Forms.Label labelProvince;
        private System.Windows.Forms.Label labelDharmaname;
        private System.Windows.Forms.TextBox textBoxDharmaname;
        private System.Windows.Forms.Label labelGanzhi;
        private System.Windows.Forms.TextBox textBoxGanzhi;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Label labelRowCount;
        public System.Windows.Forms.Button buttonImportExcel;
        public System.Windows.Forms.Button buttonFamilyList;
    }
}
