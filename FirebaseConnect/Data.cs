﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    //The order of this enum declaration shall not be changed, since the int value is fixed in database!
    public enum MajestyBuddha
    {
        석가모니불 = 1,
        문수보살 = 2,
        보현보살 = 3,
        다보여래불 = 4,
        준제보살 = 5,
        약사여래불 = 6,
        아미타불 = 7,
        관세음보살 = 8,
        대세지보살 = 9,
        보명여래불 = 10,
        혜위등왕불 = 11,
        지장보살 = 12,
        수미산정_산왕불 = 13,
        치성광여래불 = 14,
        광명불 = 15,
        무현보살 = 16,
        모현보살 = 17,        
        자재통왕불 = 18,
        상행왕_정행왕보살 = 19,
        현지보살 = 20,
        //혜영스님 요청으로 추가 - 아직 붓다는 아니시지만 미래의 붓다들이시니! 2020-3-10
        화엄성중 = 21,
        오백나한 = 22,
        지신재 = 23,
    }


    public enum Gender { 남성 = 1, 여성 = 0 }
    public enum Province { 미지정 = 0, 서울특별시, 부산광역시, 대전광역시, 대구광역시, 울산광역시, 인천광역시, 광주광역시, 세종특별자치시,
        경기도, 강원도, 충청북도, 충청남도, 경상북도, 경상남도, 전라북도, 전라남도, 제주특별자치도, 일본, 미국, 그외 }
    public enum LocalTemple { 전체 = -1, 미지정 = 0, 서울 = 1, 춘천본사 = 2, 부산분원 = 3, /*영산불교대학 = 4,*/ 대전분원 = 5, 고성분원 = 6, 제주2본산 = 7, 일본분원 = 8, 대구분원 = 9 }
    public enum AssemblyClass{ 법회 = 1,재일,특별공양,대재,공부모임,불교대학,명절,그외 }
    public enum AccessLevel { 미허가자 = 0, 총괄관리자 = 1, 주지스님, 분원스님,  }
    public enum ManagePartFlag { 기도담당 = 1, 공양담당 = 2, 천도재담당 = 4 }

    [Serializable]
    public class Monk
    {
        public int UID { get; set; }
        public string DharmaName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Gender GenderType { get; set; }        
        public AccessLevel LevelOfAccess { get; set; }
        public LocalTemple BelongedTemple { get; set; }
        public LocalTemple BelongedTemple2 { get; set; }    //Nullable        
        public Int16 PartOfManage{ get; set; }              //Nullable - Flag로 ManagePartFlag형 여럿이 결합될 수 있다. 
        public string Remarks { get; set; }                 //Nullable

        public bool IsBelongedManagePart(ManagePartFlag managePart)
        {
            if (this.LevelOfAccess == AccessLevel.총괄관리자 || this.LevelOfAccess == AccessLevel.주지스님)
                return true;

            return (this.PartOfManage & (Int16)managePart) != 0;
        }

        public bool IsBelongedTemple(LocalTemple temple)
        {            
            if (this.LevelOfAccess == AccessLevel.총괄관리자 || this.LevelOfAccess == AccessLevel.주지스님)
                return true;

            return this.BelongedTemple == temple || this.BelongedTemple2 == temple;
        }

    }
    
    [Serializable]
    public class Believer
    {        
        //UID(Unique ID)
        public int UID { get; set; }
        //신도번호
        public string BelieverNumber { get; set; }
        //가족번호
        public int FamilyID { get; set; }
        //가족멤버ID
        public int FamilyMemberID { get; set; }
        //법명
        public string DharmaName { get; set; }
        //이름(실명)
        public string Name { get; set; }
        //집주소 Address
        public Province AddressProvince { get; set; }
        public string AddressDetail { get; set; }
        //전화번호(핸드폰)
        public string Phone { get; set; }

        //성별 - 남자는 true, 여자는 false로 하기로 합니다. 
        public bool bGender { get; set; }
        //간지
        public string Ganzhi { get; set; }
        //생년월일 - 이걸 기준으로 나이까지 추정합니다. 
        public string BirthDate { get; set; }
        //소속 분원(지역)
        public LocalTemple BelongedTemple { get; set; }
        //등록날짜
        public string RegisteredDate { get; set; }
        //이메일
        public string Email { get; set; }
        //비고
        public string Remarks { get; set; }

        public static LocalTemple GetLocalTempleByProvince(Province province)
        {
            LocalTemple belongedTemple = LocalTemple.미지정;            
            switch (province)
            {
                case Province.서울특별시:
                case Province.인천광역시:
                case Province.경기도:
                    belongedTemple = LocalTemple.서울;
                    break;
                case Province.강원도:
                case Province.미국:
                case Province.그외:
                    belongedTemple = LocalTemple.춘천본사;
                    break;
                case Province.대전광역시:
                case Province.세종특별자치시:
                case Province.충청북도:
                case Province.충청남도:                
                case Province.광주광역시:
                case Province.전라북도:
                case Province.전라남도:                                
                    belongedTemple = LocalTemple.대전분원;
                    break;
                case Province.대구광역시:
                case Province.경상북도:
                    belongedTemple = LocalTemple.대구분원;
                    break;
                case Province.부산광역시:
                case Province.울산광역시:
                case Province.경상남도:
                    belongedTemple = LocalTemple.부산분원;
                    break;
                case Province.제주특별자치도:
                    belongedTemple = LocalTemple.제주2본산;
                    break;
                case Province.일본:
                    belongedTemple = LocalTemple.일본분원;
                    break;
            }

            return belongedTemple;
        }
        
    }
    
    //public enum FamilyRelation{ 본인 = 1, 남편, 아내, 아버지, 어머니, 아들, 딸, 형, 동생, 누이, 언니, 오라버니,
    //    손자, 손녀, 외손자, 외손녀, 조부, 조모, 외조부, 외조모, 사위, 며느리, 장인, 장모, 시아버지, 시어머니, 삼촌, 사촌, 그외 }
    [Serializable]
    public class FamilyMember
    {
        public int MemberUID { get; set; }
        public int FamilyID { get; set; }
        
        public string FamilyRelation { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }
        public string Ganzhi{get; set;}
        public string Remarks { get; set; }

        //This is the field that is not exist in SQL Database
        public bool IsBeliever { get; set; }
    }
    
    [Serializable]
    public class Ceremony
    {
        public int UID { get; set; }
        public string Name { get; set; }
        public AssemblyClass AssemblyCategory { get; set; }
        public DateTime Date { get; set; }        
        public LocalTemple Temple { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }        
        public string Remarks { get; set; }
        public string Preacher { get; set; }
        public static List<Ceremony> GetCeremonyOfTheDay(List<Ceremony> ceremonyList, DateTime date)
        {
            if (ceremonyList == null) return null;

            date = date.Date;

            var ceremonies = new List<Ceremony>();            
            for (int i = 0; i < ceremonyList.Count; i++)
            {
                if (date == ceremonyList[i].Date)
                    ceremonies.Add(ceremonyList[i]);
            }

            if (ceremonies.Count > 0) return ceremonies;
            else return null;
        }
    }

    [Serializable]
    public class Pray
    {
        public int UID { get; set; }
        public LocalTemple PrayTemple { get; set; }
        public MajestyBuddha buddha { get; set; }
        public int believerUID { get; set; }
        //가족명의 기도인 경우 사용할 field
        public int FamilyMemberUID { get; set; }
        
        //신도와 혈연이 아닌 사람도 입력할 수 있도록 table field들 대폭 확장합니다 - 2019.10.22
        public string PrayerName { get; set; }
        public string PrayerBirthYear { get; set; }
        public string PrayerAddress { get; set; }
        public string PrayerPhone { get; set; }
        public DateTime DateOfStart { get; set; }
        public DateTime DateOfFinish { get; set; }
        public DateTime DateOfFullPay { get; set; }
        public bool IsFinishConfirmed { get; set; }
        //입금자명 Depositor
        public string Donator { get; set; }        
        public string Remarks { get; set; }
        public bool IsPrayFinished { get; set; }
        //원장 번호
        public int PrayPaperNumber { get; set; }
        //관계 field 설정할 수 있도록 함! 
        public string PrayerRelation { get; set; }


        //분원에 따라서 combobox에 부처님을 다르게 배정하기 위한 함수
        public static void SetBuddhaComboBoxByArea(ComboBox comboBox, LocalTemple temple)
        {
            comboBox.DataSource = null;
            comboBox.Items.Clear();

            switch(temple)
            {
                case LocalTemple.춘천본사:
                    comboBox.Items.Add(MajestyBuddha.석가모니불.ToString());
                    comboBox.Items.Add(MajestyBuddha.문수보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.보현보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.다보여래불.ToString());
                    break;
                case LocalTemple.부산분원:
                    comboBox.Items.Add(MajestyBuddha.관세음보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.수미산정_산왕불.ToString());
                    comboBox.Items.Add(MajestyBuddha.치성광여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.화엄성중.ToString());
                    break;
                case LocalTemple.대전분원:
                    comboBox.Items.Add(MajestyBuddha.아미타불.ToString());
                    comboBox.Items.Add(MajestyBuddha.오백나한.ToString());
                    break;
                case LocalTemple.고성분원:
                    comboBox.Items.Add(MajestyBuddha.약사여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.보명여래불.ToString());
                    break;
                case LocalTemple.대구분원:
                    comboBox.Items.Add(MajestyBuddha.지장보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.혜위등왕불.ToString());
                    break;
                case LocalTemple.제주2본산:
                    comboBox.Items.Add(MajestyBuddha.석가모니불.ToString());
                    comboBox.Items.Add(MajestyBuddha.약사여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.지장보살.ToString());                    
                    break;
                default:
                    comboBox.Items.Add(MajestyBuddha.석가모니불.ToString());
                    comboBox.Items.Add(MajestyBuddha.다보여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.보명여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.약사여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.아미타불.ToString());
                    comboBox.Items.Add(MajestyBuddha.문수보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.보현보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.관세음보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.지장보살.ToString());
                    comboBox.Items.Add(MajestyBuddha.혜위등왕불.ToString());
                    comboBox.Items.Add(MajestyBuddha.수미산정_산왕불.ToString());
                    comboBox.Items.Add(MajestyBuddha.치성광여래불.ToString());
                    comboBox.Items.Add(MajestyBuddha.화엄성중.ToString());                    
                    comboBox.Items.Add(MajestyBuddha.오백나한.ToString());
                    break;
            }


        }

    }

    [Serializable]
    public class Offering
    {        
        public int UID { get; set; }
        public MajestyBuddha buddha { get; set; }
        public LocalTemple OfferingTemple { get; set; }
        public DateTime DateOfOffer { get; set; }

        public int believerUID { get; set; }
        //가족명의인 경우 사용할 field
        public int FamilyMemberUID { get; set; }
        public string OfferorRelation { get; set; }

        //신도와 혈연이 아닌 사람도 입력할 수 있도록 table field들 확장합니다 - 2020.4.20
        public string OfferorName { get; set; }
        public string OfferorBirthYear { get; set; }
        public string OfferorAddress { get; set; }
        public string OfferorPhone { get; set; }
        //입금자명 Depositor
        public string Donator { get; set; }
        public string Request { get; set; }
        public string Participants { get; set; }
        public string Remarks { get; set; }
        public int CeremonyID { get; set; }

    }

    [Serializable]
    public class SalvationService
    {
        public int UID { get; set; }        
        public int believerUID { get; set; }
        public LocalTemple ServiceTemple { get; set; }
        public DateTime DateOfService { get; set; }
        public string Request { get; set; }        
        public string Remarks { get; set; }
    }




}