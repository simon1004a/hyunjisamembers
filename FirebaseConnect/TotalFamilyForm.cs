﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace HyunjisaBeliever
{
    public partial class TotalFamilyForm : Form
    {
        DataTable totalFamilyDataTable = new DataTable();
        public TotalFamilyForm()
        {
            InitializeComponent();
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
        }

        private void TotalFamilyForm_Load(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            string query = "SELECT F.MemberID, F.FamilyID as 가족번호, " +
                "cast (case when B.FamilyMemberID IS NULL then 0 else 1 end as bit) as 신도여부, " +
                "F.Name as 이름, F.Relation as 관계, BirthDate as 생년, F.Ganzhi as 간지, F.Remarks as 비고 " +
                "FROM BelieverFamily as F " +
                "LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID " +
                "ORDER BY F.FamilyID, BirthDate";
                        
            this.dataGridView1.DataSource = SqlManager.GetDataTableFromSQL(query);
        }

        private void ButtonExcelExport_Click(object sender, EventArgs e)
        {
            MainWindow.ExportExcelFile(this.dataGridView1);
        }

        private void ButtonExcelImport_Click(object sender, EventArgs e)
        {
            //여기에서 가족 정보를 통째로 읽어와야 합니다...
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {   
                progressBar1.Visible = true;
                progressBar1.Value = 5;

                string filename = openFileDialog1.FileName;
                txtFilePath.Text = filename;
                txtFilePath.Visible = true;

                var excelApp = new Excel.Application();

                Excel.Workbook excelBook = excelApp.Workbooks.Open(filename, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel.Worksheet excelSheet = (Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                int rowCnt = 0;
                int colCnt = 0;
                var redRowIndexes = new List<int>();

                totalFamilyDataTable.Rows.Clear();
                totalFamilyDataTable.Columns.Clear();
                dataGridView1.DataSource = null;

                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumnName = (string)(excelRange.Cells[1, colCnt] as Excel.Range).Value2;
                    totalFamilyDataTable.Columns.Add(strColumnName, typeof(string));
                }

                int iTotalRowCnt = excelRange.Rows.Count;
                //먼저 excel data를 읽어서 DataTable을 채웁니다. 
                //첫행은 컬럼제목이므로 읽을 필요가 없다.
                for (rowCnt = 2; rowCnt <= iTotalRowCnt; rowCnt++)
                {
                    string strRowData = "";
                    bool isRedRow = false;
                    bool isAllCellsEmpty = true;
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = excelRange.Cells[rowCnt, colCnt].Value.ToString();
                            isAllCellsEmpty = false;

                            string columnName = totalFamilyDataTable.Columns[colCnt - 1].ColumnName;
                            if (columnName == "지역")
                            {
                                if (!Enum.TryParse<Province>(strCellData, out Province province))
                                    isRedRow = true;
                            }
                            else if (columnName == "생일")
                            {
                                if(!DateTime.TryParse(strCellData, out DateTime birthDate))
                                    isRedRow = true;
                            }
                        }
                        catch
                        {
                            strCellData = "";
                        }
                        strRowData += strCellData + "|";
                    }
                    if (isAllCellsEmpty)
                        continue;

                    strRowData = strRowData.Remove(strRowData.Length - 1, 1);
                    totalFamilyDataTable.Rows.Add(strRowData.Split('|'));
                    if (isRedRow)
                        redRowIndexes.Add(rowCnt-2);
                    progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt);
                }

                dataGridView1.DataSource = totalFamilyDataTable;
                if (redRowIndexes.Count > 0)
                {
                    string redRowList = "";
                    for (int i = 0; i < redRowIndexes.Count; i++)
                    {
                        var redRow = dataGridView1.Rows[redRowIndexes[i]];
                        redRow.DefaultCellStyle.BackColor = Color.Red;
                        redRowList += ", " + redRowIndexes[i].ToString();
                    }

                    MessageBox.Show("불완전한 데이터가 있어 서버에 업로드할 수 없습니다. " +
                        "빨간색으로 표시된 데이터들을 수정해주세요. 불완전한 행 번호들은 다음과 같습니다. :" +
                        redRowList);
                }
                else   //dataGridView1에 옮겨진 DataTable의 내용을 이제 실제로 database에 업로드합니다!
                {
                    //이 로직으로 가족 정보를 올리는 건 BelieverFamily가 완전히 비어 있는 상태에서 
                    //실행된 것이니, 추후 만일 다시 이 기능을 사용하려 한다면 반드시 심사숙고하여 로직을 검토하고 사용하거나
                    //새로 짜야 할 것입니다. - 2019.9.26 은명 권희재                    
                    int familyNumber = 1;
                    Believer lastInsertedBeliever = null;
                    Believer tempMainMembersBelieverData = null;
                    for (rowCnt = 2; rowCnt <= iTotalRowCnt; rowCnt++)
                    {
                        //먼저 엑셀 행의 정보를 읽어들이고, 
                        FamilyMember memberData = this.GetFamilyMemberDataFromDataGridViewRow(rowCnt - 2, 
                            out Believer believerData, 
                            out bool isMainMember);
                        
                        //새로운 가족 번호가 나왔다면, 먼저 그 전의 가족 전체에서 신도가 한 명도 나오지 않았는지 점검해보고
                        //나오지 않았다면 대주를 신도로 취급하여 그 정보를 업데이트 한다. (이 때 대주는 핸드폰 번호도 없는 경우임)
                        if (familyNumber < memberData.FamilyID || rowCnt == iTotalRowCnt)
                        {
                            familyNumber = memberData.FamilyID;
                            
                            if (lastInsertedBeliever == null)
                                lastInsertedBeliever = tempMainMembersBelieverData;
                            if (lastInsertedBeliever.FamilyID < familyNumber - 1)
                            {
                                SqlManager.InsertBelieverToSQL(tempMainMembersBelieverData);
                                lastInsertedBeliever = tempMainMembersBelieverData;
                            }
                        }

                        //행의 정보를 통해서 familyMember 변수를 채우되, 
                        //신도인지 검사부터 한다. - 휴대폰 번호를 소유했으면 신도로 취급한다. 
                        //만일 가족 중에 신도가 아무도 없다면, MainBeliever(대주)를 신도로 취급한다. 
                        if (SqlManager.InsertFamilyMemberInSQL(memberData.FamilyID, memberData, out int newMemberID))
                        {                            
                            if (memberData.IsBeliever && believerData != null)
                            {
                                believerData.FamilyMemberID = newMemberID;
                                //Believer 테이블에서 이름/번호로 검색하고 있으면 지역/주소/생일을 업데이트하고, 비고는 추가한다. 
                                if (TryUpdateBelieverInfo(ref believerData))
                                {
                                    lastInsertedBeliever = believerData;
                                    Console.WriteLine("다음 신도의 정보가 Believer 테이블에서 업데이트되었습니다. : "
                                       + believerData.Name + " : " + believerData.Phone);
                                }
                                else
                                {
                                    if (!SqlManager.InsertBelieverToSQL(believerData))
                                        MessageBox.Show("다음 신도의 정보가 Believer 테이블에 Insert되지 못했었습니다... : "
                                        + believerData.Name + " : " + believerData.Phone);
                                    else
                                        lastInsertedBeliever = believerData;
                                }
                                    
                                //바로 위 두 함수 안에서 familyID,familyMemberId까지 업데이트하므로 다음은 주석처리한다. 
                                //SqlManager.SetNewFamilyMemberIDInSQL(chosenBelieverID, familyID, newMemberID);
                            }

                            if (isMainMember)
                            {
                                believerData.FamilyMemberID = newMemberID;
                                tempMainMembersBelieverData = believerData;
                            }
                                
                        }

                        progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt) + 50;                        
                    }

                    progressBar1.Value = 100;
                    MessageBox.Show(filename + " 엑셀 파일을 로드하였습니다.");
                    MessageBox.Show("엑셀의 전체 데이터를 서버에 업로드하였습니다.");
                }

                excelBook.Close(true, null, null);
                Marshal.FinalReleaseComObject(excelBook);
                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
                progressBar1.Visible = false;
            }
        }

        private FamilyMember GetFamilyMemberDataFromDataGridViewRow(int iRow, out Believer believerData, out bool isMainBeliever)
        {            
            isMainBeliever = false;
            believerData = null;
            var memberData = new FamilyMember();
            if (iRow >= 0 && iRow < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[iRow];
                if (row.IsNewRow) return memberData;

                try
                {
                    memberData.FamilyID = Convert.ToInt32(row.Cells["가족번호"].Value);
                    memberData.Name = row.Cells["이름"].Value.ToString();
                    memberData.FamilyRelation = row.Cells["관계"].Value.ToString();
                    
                    var memberBirth = row.Cells["생일"].Value;
                    if (memberBirth != null && DateTime.TryParse(memberBirth.ToString(), out DateTime birthDate))
                    {
                        memberData.BirthDate = birthDate.ToShortDateString();
                        memberData.Ganzhi = MainWindow.GetGanzhi(birthDate.Year);
                    }
                    else
                    {
                        memberData.BirthDate = "";
                        memberData.Ganzhi = "";
                    }

                    var memberRemark = row.Cells["비고"].Value;
                    if (memberRemark != null)
                        memberData.Remarks = memberRemark.ToString();
                    else
                        memberData.Remarks = "";

                    //먼저 신도가 아닌 걸로 가정하고, 만일 휴대폰 번호가 있다면 신도로 가정한다. 
                    memberData.IsBeliever = false;
                    object phoneInfo = row.Cells["휴대전화"].Value;
                    string phoneNumber = "";
                    if (phoneInfo != null)
                    {
                        phoneNumber = phoneInfo.ToString();
                        if (phoneNumber.StartsWith("1")) phoneNumber = "0" + phoneNumber;
                        if (phoneNumber.Contains("-")) phoneNumber = phoneNumber.Replace("-", "");
                        if (phoneNumber.Length > 9)
                            memberData.IsBeliever = true;
                    }

                    isMainBeliever = row.Cells["MainBeliever"].Value != null
                            && row.Cells["MainBeliever"].Value.ToString() == "O";

                    //신도라면 Believer class를 만들어서 BelieverInfo를 넣을 수 있도록 합시다!
                    //대주도 일단 만일의 경우를 대비해서 신도 정보를 준비합니다. 
                    if (memberData.IsBeliever || isMainBeliever )
                    {
                        believerData = new Believer
                        {
                            Phone = phoneNumber,
                            Name = memberData.Name,
                            BirthDate = memberData.BirthDate,
                            Ganzhi = memberData.Ganzhi,
                            Remarks = memberData.Remarks,
                            FamilyID = memberData.FamilyID,
                        };
                        var province = row.Cells["지역"].Value;
                        if (province != null && Enum.TryParse<Province>(province.ToString(), out Province addressProvince))
                            believerData.AddressProvince = addressProvince;
                        else
                            believerData.AddressProvince = Province.미지정;

                        believerData.BelongedTemple = Believer.GetLocalTempleByProvince(believerData.AddressProvince);

                        var addressDetail = row.Cells["상세주소"].Value;
                        believerData.AddressDetail = (addressDetail != null) ? addressDetail.ToString() : "";

                    }

                }
                catch { MessageBox.Show("데이터 그리드뷰에 필요한 값이 전부 다 들어있지 않습니다."); }
            }
            return memberData;
        }


        private bool TryUpdateBelieverInfo(ref Believer believer)
        {   
            var searchedBeliever = SqlManager.SelectBelieverFromSQL(believer.Name, believer.Phone);
            if (searchedBeliever == null)
                return false;
            else
            {
                //DB에 저장된 값이 유효하면 그냥 그걸 유지합시다. 
                if (searchedBeliever.BelongedTemple != LocalTemple.미지정)
                    believer.BelongedTemple = searchedBeliever.BelongedTemple;                
                if (!string.IsNullOrWhiteSpace(searchedBeliever.AddressDetail))
                {
                    believer.AddressProvince = searchedBeliever.AddressProvince;
                    believer.AddressDetail = searchedBeliever.AddressDetail;                    
                }                
                if (DateTime.TryParse(searchedBeliever.BirthDate, out DateTime birthInDB))
                {                    
                    if (birthInDB.Month != 1 || birthInDB.Day != 1)
                    {
                        believer.BirthDate = searchedBeliever.BirthDate;
                        believer.Ganzhi = searchedBeliever.Ganzhi;
                    }
                }
                if (searchedBeliever.Remarks != null && !string.IsNullOrWhiteSpace(searchedBeliever.Remarks))
                    believer.Remarks = searchedBeliever.Remarks + "   " + believer.Remarks;
            }

            bool bResult = false;
            using (var connection = new SqlConnection(SqlManager.connectionString))
            {
                string query = "UPDATE Believer SET AddressProvince = @province, AddressDetail = @address, " +
                    "BelongedTemple = @belongedtemple, DateOfBirth = @dateofbirth, Ganzhi = @ganzhi, " +
                    "FamilyID = @familyId, FamilyMemberID = @memberId, Remarks = @remarks " +
                    "WHERE Name = @name AND Phone = @phone";

                using (var command = new SqlCommand(query, connection))
                {                    
                    command.Parameters.AddWithValue("@province", believer.AddressProvince);
                    command.Parameters.AddWithValue("@address", believer.AddressDetail);                    
                    command.Parameters.AddWithValue("@belongedtemple", (int)believer.BelongedTemple);
                    if (string.IsNullOrWhiteSpace(believer.BirthDate))
                        command.Parameters.AddWithValue("@dateofbirth", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofbirth", believer.BirthDate);
                    command.Parameters.AddWithValue("@ganzhi", believer.Ganzhi);
                    command.Parameters.AddWithValue("@familyId", believer.FamilyID);
                    command.Parameters.AddWithValue("@memberId", believer.FamilyMemberID);
                    command.Parameters.AddWithValue("@remarks", believer.Remarks);
                    command.Parameters.AddWithValue("@name", believer.Name);
                    command.Parameters.AddWithValue("@phone", believer.Phone);

                    connection.Open();
                    int result = command.ExecuteNonQuery();
                    bResult = (result > 0);
                    if (!bResult)
                        MessageBox.Show("업데이트하는 도중에 문제가 발생했습니다: "
                            + believer.Name + " : " + believer.Phone);
                }
            }
            return bResult;
        }


    }
}
