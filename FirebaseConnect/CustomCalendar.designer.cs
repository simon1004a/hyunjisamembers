﻿namespace HyunjisaBeliever
{
    partial class CustomCalendar
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomCalendar));
            this.splitContainer1 = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.labelCaption = new System.Windows.Forms.Label();
            this.panelDay = new System.Windows.Forms.Panel();
            this.labelPre = new System.Windows.Forms.Label();
            this.labelNext = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1.Panel1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1.Panel2)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.labelNext);
            this.splitContainer1.Panel1.Controls.Add(this.labelPre);
            this.splitContainer1.Panel1.Controls.Add(this.labelCaption);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelDay);
            // 
            // labelCaption
            // 
            this.labelCaption.BackColor = System.Drawing.Color.White;
            this.labelCaption.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.labelCaption, "labelCaption");
            this.labelCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Click += new System.EventHandler(this.label_caption_Click);
            // 
            // panelDay
            // 
            this.panelDay.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.panelDay, "panelDay");
            this.panelDay.Name = "panelDay";
            // 
            // labelPre
            // 
            this.labelPre.BackColor = System.Drawing.Color.White;
            this.labelPre.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.labelPre, "labelPre");
            this.labelPre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.labelPre.Name = "labelPre";
            this.labelPre.Click += new System.EventHandler(this.label_pre_Click);
            this.labelPre.DoubleClick += new System.EventHandler(this.label_pre_Click);
            // 
            // labelNext
            // 
            this.labelNext.BackColor = System.Drawing.Color.White;
            this.labelNext.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.labelNext, "labelNext");
            this.labelNext.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.labelNext.Name = "labelNext";
            this.labelNext.Click += new System.EventHandler(this.label_next_Click);
            this.labelNext.DoubleClick += new System.EventHandler(this.label_next_Click);
            // 
            // CustomCalendar
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.splitContainer1);
            this.Name = "CustomCalendar";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1.Panel1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1.Panel2)).EndInit();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer  splitContainer1;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Panel panelDay;
        private System.Windows.Forms.Label labelNext;
        private System.Windows.Forms.Label labelPre;
    }
}
