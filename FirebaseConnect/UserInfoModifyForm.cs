﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserInfoModifyForm : Form
    {
        private EditMode _editMode = EditMode.UPDATE_MODE;
        public EditMode CurrentEditMode
        {
            set
            {
                _editMode = value;
                if (_editMode == EditMode.INSERT_MODE)
                {                    
                    this.comboBoxBelongedTemple.SelectedItem = MainWindow.manageArea;
                    this.textBoxID.Text = (SqlManager.GetLastBelieverIDFromSQL() + 1).ToString();                    
                }
            }
            get { return _editMode; }
        }
        private Believer _believerInfo = new Believer();
        public Believer BelieverInfo
        {
            set
            {
                _believerInfo = value;
                textBoxID.Text = _believerInfo.UID.ToString();
                textBoxBelieverNumber.Text = _believerInfo.BelieverNumber;
                textBoxDharmaName.Text = _believerInfo.DharmaName;
                textBoxGanzhi.Text = _believerInfo.Ganzhi;
                textBoxName.Text = _believerInfo.Name;
                textBoxPhone.Text = _believerInfo.Phone;
                comboBoxProvince.SelectedItem = _believerInfo.AddressProvince;
                textBoxAddress.Text = _believerInfo.AddressDetail;
                comboBoxGender.Text = ((Gender)Convert.ToInt32(_believerInfo.bGender)).ToString();
                comboBoxBelongedTemple.Text = _believerInfo.BelongedTemple.ToString();
                dateTimePickerBirthDay.Text = _believerInfo.BirthDate;
                if (DateTime.TryParse(_believerInfo.RegisteredDate, out DateTime registerDate))
                    dateTimePickerRegisterDay.Value = registerDate;
                else
                    dateTimePickerRegisterDay.Value = new DateTime(2500, 1, 1);
                textBoxEmail.Text = _believerInfo.Email;
                textBoxRemarks.Text = _believerInfo.Remarks;
            }
            get
            {
                _believerInfo.UID = Convert.ToInt32(textBoxID.Text);
                _believerInfo.BelieverNumber = textBoxBelieverNumber.Text;
                _believerInfo.DharmaName = textBoxDharmaName.Text;
                _believerInfo.Ganzhi = textBoxGanzhi.Text;

                _believerInfo.Name = textBoxName.Text;
                _believerInfo.AddressProvince = (Province)comboBoxProvince.SelectedItem;
                _believerInfo.AddressDetail = textBoxAddress.Text;
                _believerInfo.Phone = textBoxPhone.Text;

                _believerInfo.bGender = Convert.ToBoolean((int)(Enum.Parse(typeof(Gender), comboBoxGender.Text)));
                _believerInfo.BelongedTemple = (LocalTemple)Enum.Parse(typeof(LocalTemple), comboBoxBelongedTemple.Text);

                _believerInfo.BirthDate = dateTimePickerBirthDay.Value.ToShortDateString();
                _believerInfo.RegisteredDate = (dateTimePickerRegisterDay.Value.Year != 2500)
                    ? dateTimePickerRegisterDay.Value.ToShortDateString(): "";
                _believerInfo.Remarks = textBoxRemarks.Text;
                _believerInfo.Email = textBoxEmail.Text;
                return _believerInfo;
            }
        }

        DataTable dataTableFamily = new DataTable();
        public UserInfoModifyForm()
        {
            InitializeComponent();
            comboBoxGender.DataSource = Enum.GetValues(typeof(Gender));
            comboBoxBelongedTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxProvince.DataSource = Enum.GetValues(typeof(Province));
        }

        private void UserInfoModifyForm_Load(object sender, EventArgs e)
        {
            RefreshFamilyMembers();
        }

        private void RefreshFamilyMembers()
        {
            dataTableFamily = SqlManager.SelectFamilyMembersFromSQL(this.BelieverInfo.UID);            
            this.dataGridView1.DataSource = dataTableFamily;            
        }


        private void ButtonUpdate_ClickAsync(object sender, EventArgs e)
        {
            string strErrMsg = "";
            if (string.IsNullOrWhiteSpace(textBoxName.Text)) strErrMsg += "이름 ";
            if (string.IsNullOrWhiteSpace(textBoxAddress.Text)) strErrMsg += "주소 ";
            if (string.IsNullOrWhiteSpace(textBoxPhone.Text)) strErrMsg += "폰번호 ";
            if (string.IsNullOrWhiteSpace(textBoxGanzhi.Text)) strErrMsg += "생년 ";
            if (!string.IsNullOrWhiteSpace(strErrMsg))
            {
                strErrMsg += "정보가 없어서 진행할 수 없습니다.";
                MessageBox.Show(strErrMsg);
                return;
            }

            if (string.IsNullOrWhiteSpace(textBoxBelieverNumber.Text))
            {
                var result = MessageBox.Show("신도 번호가 비어있습니다. 자동으로 부여할까요? 부여된 번호는 이후 수정가능합니다.", "신도번호", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    var belongedTemple = (LocalTemple)comboBoxBelongedTemple.SelectedItem;
                    string registerYear = dateTimePickerRegisterDay.Value.Year.ToString();
                    int iNewBelieverNumber = SqlManager.GetTempleRegisteredBelieverCountInYearFromSQL(belongedTemple, registerYear) + 1;
                    string strNewBelieverNumber = (iNewBelieverNumber < 1000) ? String.Format("{0:000}", iNewBelieverNumber) : iNewBelieverNumber.ToString();
                    string believerNumber = ((int)belongedTemple).ToString() + "-" + registerYear.Substring(2, 2) + "-" + strNewBelieverNumber;
                    textBoxBelieverNumber.Text = believerNumber;
                }
            }

            if (this.CurrentEditMode == EditMode.INSERT_MODE)
            {
                if (SqlManager.InsertBelieverToSQL(BelieverInfo))
                    this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (SqlManager.UpdateBelieverInSQL(BelieverInfo))
                    this.DialogResult = DialogResult.OK;
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonAddFamilyMember_Click(object sender, EventArgs e)
        {
            if (this.CurrentEditMode != EditMode.UPDATE_MODE)
            {
                MessageBox.Show("죄송합니다. 신도 정보가 생성되기 전에는 가족 정보를 입력할 수 없습니다. ");
                return;
            }
            //With this constructor, familyUpdateForm will be set as INSERT_MODE.
            var familyInsertForm = new FamilyUpdateForm(this.BelieverInfo.UID);
            if (familyInsertForm.ShowDialog() == DialogResult.OK)
            {
                this.RefreshFamilyMembers();
            }

        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int memberId;
            bool isMemberBeliever;
            string selectedName, selectedBirthYear, selectedRemarks, selectedRelation;            

            if (rowIndex >= dataTableFamily.Rows.Count)
                return;
            else
            {
                try
                {
                    memberId = Convert.ToInt32(this.dataTableFamily.Rows[rowIndex]["MemberID"]);
                    isMemberBeliever = Convert.ToBoolean(Convert.ToInt32(this.dataTableFamily.Rows[rowIndex]["신도여부"]));

                    //이름, 생년, 관계, 비고                    
                    selectedName = this.dataTableFamily.Rows[rowIndex]["이름"].ToString();
                    selectedBirthYear = this.dataTableFamily.Rows[rowIndex]["생년"].ToString();
                    selectedRelation = dataTableFamily.Rows[rowIndex]["관계"].ToString();

                    var remarkValue = dataTableFamily.Rows[rowIndex]["비고"];
                    selectedRemarks = (remarkValue != null) ? remarkValue.ToString() : string.Empty;                    
                }
                catch
                {
                    Console.WriteLine("Failed to convert the 이름, 생년, 관계...");
                    return;
                }
            }

            var familyMemberInfo = new FamilyMember
            {
                MemberUID = memberId,
                IsBeliever = isMemberBeliever,
                Name = selectedName,
                BirthDate = selectedBirthYear,
                Ganzhi = MainWindow.GetGanzhi(Convert.ToDateTime(selectedBirthYear).Year),
                FamilyRelation = selectedRelation,
                Remarks = selectedRemarks,                
            };

            //With this constructor, familyUpdateForm will be set as UPDATE_MODE.
            var familyUpdateForm = new FamilyUpdateForm(BelieverInfo.UID, familyMemberInfo);
            if (familyUpdateForm.ShowDialog() == DialogResult.OK)
            {
                this.RefreshFamilyMembers();
            }

        }

        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow <= this.dataGridView1.RowCount - 1)
                {
                    if (dataGridView1.SelectedRows.Count == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    ContextMenuStrip miniMenu = new ContextMenuStrip();                    
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int iSelectedRow;            
            switch (e.ClickedItem.Name)
            {
                case "Delete":
                    string deleteMessage = "";
                    var deleteIdList = new List<int>();
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        iSelectedRow = dataGridView1.SelectedRows[i].Index;
                        try
                        {
                            int memberId = Convert.ToInt32(this.dataTableFamily.Rows[iSelectedRow]["MemberID"]);
                            deleteIdList.Add(memberId);
                            deleteMessage += this.dataTableFamily.Rows[iSelectedRow]["이름"].ToString() + " ";
                        }
                        catch { continue; }
                    }
                    deleteMessage += " 분을 가족 구성원에서 제외하시겠습니까?";
                    var askResult = MessageBox.Show(deleteMessage, "가족원 삭제", MessageBoxButtons.YesNo);
                    if (askResult == DialogResult.Yes)
                    {
                        foreach (int id in deleteIdList)
                            SqlManager.DeleteFamilyMemberInSQL(id);
                    }
                    this.RefreshFamilyMembers();                    
                    break;                
            }
        }
        private void TextBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void TextBoxGanzhi_Enter(object sender, EventArgs e)
        {
            MessageBox.Show("생년월일을 정해주시면 간지가 자동으로 입력됩니다.");

        }

        private void TextBoxEmail_Leave(object sender, EventArgs e)
        {
            var strEmail = textBoxEmail.Text;
            if (string.IsNullOrWhiteSpace(strEmail))
                return;

            //Object initialization for Regex
            Regex reg = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$", RegexOptions.IgnoreCase);
            if (!reg.IsMatch(strEmail))
            {
                MessageBox.Show("이메일 주소의 형식이 틀립니다. 다시 입력해주십시오.");
                textBoxEmail.Clear();
            }
        }

        private void DateTimePickerBirthDay_ValueChanged(object sender, EventArgs e)
        {
            var dateBirth = dateTimePickerBirthDay.Value;            
            textBoxGanzhi.Text = MainWindow.GetGanzhi(dateBirth.Year);
            CustomCalendar.LunarDateConvertFromSolarDate(dateBirth, out int lunarYear, out int lunarMonth, out int lunarDay);
            labelLunarBirth.Text = lunarYear.ToString() + "-" + lunarMonth.ToString() + "-" + lunarDay.ToString(); 
        }

        //private void DateTimePickerRegisterDay_ValueChanged(object sender, EventArgs e)
        //{
        //    SetBelieverNumber((LocalTemple)comboBoxBelongedArea.SelectedItem);
        //}

        private void SetBelieverNumber( LocalTemple belongedTemple )
        {
            //이 함수가 소속 분원 바꿀 때, 등록일자 바꿀 때 두 번 모두 불리니 textBoxBelieverNumber.Text가 아니라
            //원래 데이터가 비었는지를 체크해야 합니다...
            //textBoxBelieverNumber.Text를 추가한 건 - 억지로 신도번호를 지웠다면 다시 assign하기 위함입니다!
            if (string.IsNullOrWhiteSpace(_believerInfo.BelieverNumber) || string.IsNullOrWhiteSpace(textBoxBelieverNumber.Text))
            {
                string registerYear = dateTimePickerRegisterDay.Value.Year.ToString();
                //온 순서는 그 해에 온 신도 중에서 데이터베이스에 등록된 순서를 말합니다!
                int iNewBelieverNumber = SqlManager.GetTempleRegisteredBelieverCountInYearFromSQL(belongedTemple, registerYear) + 1;
                string strNewBelieverNumber = (iNewBelieverNumber < 1000) ? String.Format("{0:000}", iNewBelieverNumber) : iNewBelieverNumber.ToString();
                string believerNumber = ((int)belongedTemple).ToString() + "-" + registerYear.Substring(2, 2) + "-" + strNewBelieverNumber;
                textBoxBelieverNumber.Text = believerNumber;
            }
        }

        private void TextBoxBelieverNumber_Enter(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxBelieverNumber.Text))
            {
                MessageBox.Show("신도 번호는 '분원번호-온년도-온순서'의 포맷으로 기록됩니다. \n " +
                    "ex) 2-17-013 \n" +
                    "미지정 상태에서 등록연도를 지정하면 자동으로 부여됩니다. ");
            }
        }

        private void comboBoxBelongedTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((LocalTemple)comboBoxBelongedTemple.SelectedItem == LocalTemple.전체)
            {
                comboBoxBelongedTemple.SelectedItem = MainWindow.manageArea;
                MessageBox.Show("소속 분원이 전체일 수는 없습니다. 정해지지 않은 경우 미지정으로 해 주세요.");
            }
        }
    }
}
