﻿namespace HyunjisaBeliever
{
    partial class UserPrayUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.dateTimePickerStartDay = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBelieverName = new System.Windows.Forms.TextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerFinishDay = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonBelieverSearch = new System.Windows.Forms.Button();
            this.dataGridViewDonation = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxMajesty = new System.Windows.Forms.ComboBox();
            this.comboBoxPrayTemple = new System.Windows.Forms.ComboBox();
            this.dataGridViewRequest = new System.Windows.Forms.DataGridView();
            this.buttonNewRequest = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxDonatedTotal = new System.Windows.Forms.TextBox();
            this.buttonNewDonation = new System.Windows.Forms.Button();
            this.textBoxDonator = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxFixFinishDay = new System.Windows.Forms.CheckBox();
            this.textBoxFamilyMemberName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxPrayerRelation = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxPrayClass = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePickerFullPay = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPrayerName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxPrayerBirthYear = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxPrayerAddress = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxPrayerPhone = new System.Windows.Forms.TextBox();
            this.buttonFamilySearch = new System.Windows.Forms.Button();
            this.checkBoxPrayFinish = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPrayPaperNumber = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDonation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRequest)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonCancel.FlatAppearance.BorderSize = 2;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCancel.Location = new System.Drawing.Point(458, 704);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(116, 50);
            this.buttonCancel.TabIndex = 115;
            this.buttonCancel.Text = "취소";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.FlatAppearance.BorderSize = 2;
            this.buttonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonUpdate.ForeColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.Location = new System.Drawing.Point(330, 704);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(116, 50);
            this.buttonUpdate.TabIndex = 114;
            this.buttonUpdate.Text = "확인";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // dateTimePickerStartDay
            // 
            this.dateTimePickerStartDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePickerStartDay.Location = new System.Drawing.Point(65, 449);
            this.dateTimePickerStartDay.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerStartDay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerStartDay.Name = "dateTimePickerStartDay";
            this.dateTimePickerStartDay.Size = new System.Drawing.Size(254, 24);
            this.dateTimePickerStartDay.TabIndex = 112;
            this.dateTimePickerStartDay.ValueChanged += new System.EventHandler(this.DateTimePickerStartDay_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(12, 490);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 18);
            this.label8.TabIndex = 109;
            this.label8.Text = "회향일";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(12, 453);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 18);
            this.label7.TabIndex = 108;
            this.label7.Text = "입재일";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(11, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 106;
            this.label5.Text = "기도 분원";
            // 
            // textBoxBelieverName
            // 
            this.textBoxBelieverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBelieverName.Location = new System.Drawing.Point(176, 159);
            this.textBoxBelieverName.Name = "textBoxBelieverName";
            this.textBoxBelieverName.ReadOnly = true;
            this.textBoxBelieverName.Size = new System.Drawing.Size(143, 26);
            this.textBoxBelieverName.TabIndex = 105;
            // 
            // textBoxID
            // 
            this.textBoxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxID.Location = new System.Drawing.Point(88, 12);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(56, 24);
            this.textBoxID.TabIndex = 102;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(12, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 101;
            this.label3.Text = "신도 이름";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(12, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 99;
            this.label2.Text = "부처님";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 18);
            this.label1.TabIndex = 98;
            this.label1.Text = "ID";
            // 
            // dateTimePickerFinishDay
            // 
            this.dateTimePickerFinishDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePickerFinishDay.Location = new System.Drawing.Point(65, 487);
            this.dateTimePickerFinishDay.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerFinishDay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerFinishDay.Name = "dateTimePickerFinishDay";
            this.dateTimePickerFinishDay.Size = new System.Drawing.Size(254, 24);
            this.dateTimePickerFinishDay.TabIndex = 116;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(325, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 31);
            this.label9.TabIndex = 119;
            this.label9.Text = "발원 일지";
            // 
            // buttonBelieverSearch
            // 
            this.buttonBelieverSearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonBelieverSearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.FlatAppearance.BorderSize = 2;
            this.buttonBelieverSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBelieverSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBelieverSearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.Location = new System.Drawing.Point(88, 159);
            this.buttonBelieverSearch.Margin = new System.Windows.Forms.Padding(1);
            this.buttonBelieverSearch.Name = "buttonBelieverSearch";
            this.buttonBelieverSearch.Size = new System.Drawing.Size(82, 26);
            this.buttonBelieverSearch.TabIndex = 120;
            this.buttonBelieverSearch.Text = "신도검색";
            this.buttonBelieverSearch.UseVisualStyleBackColor = false;
            this.buttonBelieverSearch.Click += new System.EventHandler(this.ButtonBelieverSearch_Click);
            // 
            // dataGridViewDonation
            // 
            this.dataGridViewDonation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDonation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDonation.Location = new System.Drawing.Point(331, 375);
            this.dataGridViewDonation.Name = "dataGridViewDonation";
            this.dataGridViewDonation.RowHeadersVisible = false;
            this.dataGridViewDonation.RowTemplate.Height = 23;
            this.dataGridViewDonation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewDonation.Size = new System.Drawing.Size(638, 317);
            this.dataGridViewDonation.TabIndex = 122;
            this.dataGridViewDonation.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDonation_CellEndEdit);
            this.dataGridViewDonation.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridViewDonation_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(326, 332);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 31);
            this.label10.TabIndex = 123;
            this.label10.Text = "입금 기록";
            // 
            // comboBoxMajesty
            // 
            this.comboBoxMajesty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMajesty.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxMajesty.FormattingEnabled = true;
            this.comboBoxMajesty.ItemHeight = 18;
            this.comboBoxMajesty.Location = new System.Drawing.Point(88, 84);
            this.comboBoxMajesty.Name = "comboBoxMajesty";
            this.comboBoxMajesty.Size = new System.Drawing.Size(231, 26);
            this.comboBoxMajesty.TabIndex = 124;
            // 
            // comboBoxPrayTemple
            // 
            this.comboBoxPrayTemple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrayTemple.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxPrayTemple.FormattingEnabled = true;
            this.comboBoxPrayTemple.ItemHeight = 18;
            this.comboBoxPrayTemple.Location = new System.Drawing.Point(88, 49);
            this.comboBoxPrayTemple.Name = "comboBoxPrayTemple";
            this.comboBoxPrayTemple.Size = new System.Drawing.Size(231, 26);
            this.comboBoxPrayTemple.TabIndex = 125;
            this.comboBoxPrayTemple.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPrayTemple_SelectedIndexChanged);
            // 
            // dataGridViewRequest
            // 
            this.dataGridViewRequest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewRequest.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRequest.Location = new System.Drawing.Point(331, 49);
            this.dataGridViewRequest.Name = "dataGridViewRequest";
            this.dataGridViewRequest.RowHeadersVisible = false;
            this.dataGridViewRequest.RowTemplate.Height = 23;
            this.dataGridViewRequest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewRequest.Size = new System.Drawing.Size(637, 274);
            this.dataGridViewRequest.TabIndex = 126;
            this.dataGridViewRequest.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRequest_CellEndEdit);
            this.dataGridViewRequest.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridViewRequest_MouseClick);
            // 
            // buttonNewRequest
            // 
            this.buttonNewRequest.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonNewRequest.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonNewRequest.FlatAppearance.BorderSize = 2;
            this.buttonNewRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNewRequest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonNewRequest.ForeColor = System.Drawing.Color.Maroon;
            this.buttonNewRequest.Location = new System.Drawing.Point(842, 12);
            this.buttonNewRequest.Name = "buttonNewRequest";
            this.buttonNewRequest.Size = new System.Drawing.Size(126, 31);
            this.buttonNewRequest.TabIndex = 127;
            this.buttonNewRequest.Text = "발원 입력하기";
            this.buttonNewRequest.UseVisualStyleBackColor = false;
            this.buttonNewRequest.Click += new System.EventHandler(this.ButtonNewRequest_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(789, 706);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 128;
            this.label6.Text = "현재 총납부액";
            // 
            // textBoxDonatedTotal
            // 
            this.textBoxDonatedTotal.BackColor = System.Drawing.Color.SandyBrown;
            this.textBoxDonatedTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDonatedTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDonatedTotal.Location = new System.Drawing.Point(877, 704);
            this.textBoxDonatedTotal.Name = "textBoxDonatedTotal";
            this.textBoxDonatedTotal.ReadOnly = true;
            this.textBoxDonatedTotal.Size = new System.Drawing.Size(92, 21);
            this.textBoxDonatedTotal.TabIndex = 130;
            // 
            // buttonNewDonation
            // 
            this.buttonNewDonation.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonNewDonation.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonNewDonation.FlatAppearance.BorderSize = 2;
            this.buttonNewDonation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNewDonation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonNewDonation.ForeColor = System.Drawing.Color.Maroon;
            this.buttonNewDonation.Location = new System.Drawing.Point(843, 330);
            this.buttonNewDonation.Name = "buttonNewDonation";
            this.buttonNewDonation.Size = new System.Drawing.Size(126, 31);
            this.buttonNewDonation.TabIndex = 132;
            this.buttonNewDonation.Text = "기도비 입력하기";
            this.buttonNewDonation.UseVisualStyleBackColor = false;
            this.buttonNewDonation.Click += new System.EventHandler(this.ButtonNewDonation_Click);
            // 
            // textBoxDonator
            // 
            this.textBoxDonator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxDonator.Location = new System.Drawing.Point(65, 409);
            this.textBoxDonator.Name = "textBoxDonator";
            this.textBoxDonator.Size = new System.Drawing.Size(254, 24);
            this.textBoxDonator.TabIndex = 134;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(12, 412);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 18);
            this.label12.TabIndex = 133;
            this.label12.Text = "입금자";
            // 
            // checkBoxFixFinishDay
            // 
            this.checkBoxFixFinishDay.AutoSize = true;
            this.checkBoxFixFinishDay.Location = new System.Drawing.Point(203, 517);
            this.checkBoxFixFinishDay.Name = "checkBoxFixFinishDay";
            this.checkBoxFixFinishDay.Size = new System.Drawing.Size(112, 16);
            this.checkBoxFixFinishDay.TabIndex = 135;
            this.checkBoxFixFinishDay.Text = "회향일 확정하기";
            this.checkBoxFixFinishDay.UseVisualStyleBackColor = true;
            this.checkBoxFixFinishDay.CheckedChanged += new System.EventHandler(this.CheckBoxFixFinishDay_CheckedChanged);
            // 
            // textBoxFamilyMemberName
            // 
            this.textBoxFamilyMemberName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxFamilyMemberName.Location = new System.Drawing.Point(176, 193);
            this.textBoxFamilyMemberName.Name = "textBoxFamilyMemberName";
            this.textBoxFamilyMemberName.ReadOnly = true;
            this.textBoxFamilyMemberName.Size = new System.Drawing.Size(142, 24);
            this.textBoxFamilyMemberName.TabIndex = 138;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(12, 196);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 18);
            this.label13.TabIndex = 137;
            this.label13.Text = "가족 이름";
            // 
            // textBoxPrayerRelation
            // 
            this.textBoxPrayerRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayerRelation.Location = new System.Drawing.Point(88, 223);
            this.textBoxPrayerRelation.Name = "textBoxPrayerRelation";
            this.textBoxPrayerRelation.Size = new System.Drawing.Size(230, 24);
            this.textBoxPrayerRelation.TabIndex = 140;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(12, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 18);
            this.label14.TabIndex = 139;
            this.label14.Text = "관계";
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxRemarks.Location = new System.Drawing.Point(65, 594);
            this.textBoxRemarks.Multiline = true;
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(254, 98);
            this.textBoxRemarks.TabIndex = 142;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(12, 597);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 18);
            this.label15.TabIndex = 141;
            this.label15.Text = "비고";
            // 
            // comboBoxPrayClass
            // 
            this.comboBoxPrayClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrayClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxPrayClass.FormattingEnabled = true;
            this.comboBoxPrayClass.ItemHeight = 18;
            this.comboBoxPrayClass.Location = new System.Drawing.Point(88, 121);
            this.comboBoxPrayClass.Name = "comboBoxPrayClass";
            this.comboBoxPrayClass.Size = new System.Drawing.Size(231, 26);
            this.comboBoxPrayClass.TabIndex = 143;
            this.comboBoxPrayClass.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPrayClass_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(12, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 18);
            this.label16.TabIndex = 144;
            this.label16.Text = "입재 종류";
            // 
            // dateTimePickerFullPay
            // 
            this.dateTimePickerFullPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePickerFullPay.Location = new System.Drawing.Point(65, 542);
            this.dateTimePickerFullPay.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerFullPay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerFullPay.Name = "dateTimePickerFullPay";
            this.dateTimePickerFullPay.Size = new System.Drawing.Size(254, 24);
            this.dateTimePickerFullPay.TabIndex = 146;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(12, 545);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 18);
            this.label11.TabIndex = 145;
            this.label11.Text = "완납일";
            // 
            // textBoxPrayerName
            // 
            this.textBoxPrayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayerName.Location = new System.Drawing.Point(65, 289);
            this.textBoxPrayerName.Name = "textBoxPrayerName";
            this.textBoxPrayerName.Size = new System.Drawing.Size(99, 24);
            this.textBoxPrayerName.TabIndex = 148;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 258);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 24);
            this.label17.TabIndex = 147;
            this.label17.Text = "입재자";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(12, 292);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 18);
            this.label18.TabIndex = 149;
            this.label18.Text = "이름";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(184, 292);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 18);
            this.label19.TabIndex = 151;
            this.label19.Text = "생년";
            // 
            // textBoxPrayerBirthYear
            // 
            this.textBoxPrayerBirthYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayerBirthYear.Location = new System.Drawing.Point(224, 289);
            this.textBoxPrayerBirthYear.Name = "textBoxPrayerBirthYear";
            this.textBoxPrayerBirthYear.Size = new System.Drawing.Size(95, 24);
            this.textBoxPrayerBirthYear.TabIndex = 150;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(12, 322);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 18);
            this.label20.TabIndex = 153;
            this.label20.Text = "주소";
            // 
            // textBoxPrayerAddress
            // 
            this.textBoxPrayerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayerAddress.Location = new System.Drawing.Point(65, 319);
            this.textBoxPrayerAddress.Multiline = true;
            this.textBoxPrayerAddress.Name = "textBoxPrayerAddress";
            this.textBoxPrayerAddress.Size = new System.Drawing.Size(254, 42);
            this.textBoxPrayerAddress.TabIndex = 152;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(12, 378);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 18);
            this.label21.TabIndex = 155;
            this.label21.Text = "연락처";
            // 
            // textBoxPrayerPhone
            // 
            this.textBoxPrayerPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayerPhone.Location = new System.Drawing.Point(65, 375);
            this.textBoxPrayerPhone.Name = "textBoxPrayerPhone";
            this.textBoxPrayerPhone.Size = new System.Drawing.Size(254, 24);
            this.textBoxPrayerPhone.TabIndex = 154;
            // 
            // buttonFamilySearch
            // 
            this.buttonFamilySearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonFamilySearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonFamilySearch.FlatAppearance.BorderSize = 2;
            this.buttonFamilySearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFamilySearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFamilySearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonFamilySearch.Location = new System.Drawing.Point(88, 191);
            this.buttonFamilySearch.Margin = new System.Windows.Forms.Padding(1);
            this.buttonFamilySearch.Name = "buttonFamilySearch";
            this.buttonFamilySearch.Size = new System.Drawing.Size(82, 26);
            this.buttonFamilySearch.TabIndex = 156;
            this.buttonFamilySearch.Text = "가족검색";
            this.buttonFamilySearch.UseVisualStyleBackColor = false;
            this.buttonFamilySearch.Click += new System.EventHandler(this.ButtonFamilySearch_Click);
            // 
            // checkBoxPrayFinish
            // 
            this.checkBoxPrayFinish.AutoSize = true;
            this.checkBoxPrayFinish.Location = new System.Drawing.Point(215, 572);
            this.checkBoxPrayFinish.Name = "checkBoxPrayFinish";
            this.checkBoxPrayFinish.Size = new System.Drawing.Size(100, 16);
            this.checkBoxPrayFinish.TabIndex = 157;
            this.checkBoxPrayFinish.Text = "회향 처리하기";
            this.checkBoxPrayFinish.UseVisualStyleBackColor = true;
            this.checkBoxPrayFinish.Click += new System.EventHandler(this.CheckBoxPrayFinish_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(150, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 18);
            this.label4.TabIndex = 159;
            this.label4.Text = "원장번호";
            // 
            // textBoxPrayPaperNumber
            // 
            this.textBoxPrayPaperNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxPrayPaperNumber.Location = new System.Drawing.Point(216, 12);
            this.textBoxPrayPaperNumber.Name = "textBoxPrayPaperNumber";
            this.textBoxPrayPaperNumber.Size = new System.Drawing.Size(103, 24);
            this.textBoxPrayPaperNumber.TabIndex = 158;
            this.textBoxPrayPaperNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxPrayPaperNumber_KeyPress);
            // 
            // UserPrayUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(981, 766);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxPrayPaperNumber);
            this.Controls.Add(this.checkBoxPrayFinish);
            this.Controls.Add(this.buttonFamilySearch);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.textBoxPrayerPhone);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBoxPrayerAddress);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBoxPrayerBirthYear);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBoxPrayerName);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.dateTimePickerFullPay);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboBoxPrayClass);
            this.Controls.Add(this.textBoxRemarks);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxPrayerRelation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxFamilyMemberName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.checkBoxFixFinishDay);
            this.Controls.Add(this.textBoxDonator);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.buttonNewDonation);
            this.Controls.Add(this.textBoxDonatedTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonNewRequest);
            this.Controls.Add(this.dataGridViewRequest);
            this.Controls.Add(this.comboBoxPrayTemple);
            this.Controls.Add(this.comboBoxMajesty);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dataGridViewDonation);
            this.Controls.Add(this.buttonBelieverSearch);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateTimePickerFinishDay);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.dateTimePickerStartDay);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxBelieverName);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "UserPrayUpdateForm";
            this.Text = "기도 등록하기";
            this.Load += new System.EventHandler(this.UserPrayUpdateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDonation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRequest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonUpdate;
        public System.Windows.Forms.DateTimePicker dateTimePickerStartDay;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBoxBelieverName;
        public System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DateTimePicker dateTimePickerFinishDay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonBelieverSearch;
        private System.Windows.Forms.DataGridView dataGridViewDonation;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox comboBoxMajesty;
        public System.Windows.Forms.ComboBox comboBoxPrayTemple;
        private System.Windows.Forms.DataGridView dataGridViewRequest;
        private System.Windows.Forms.Button buttonNewRequest;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox textBoxDonatedTotal;
        private System.Windows.Forms.Button buttonNewDonation;
        public System.Windows.Forms.TextBox textBoxDonator;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxFixFinishDay;
        public System.Windows.Forms.TextBox textBoxFamilyMemberName;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox textBoxPrayerRelation;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox textBoxRemarks;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox comboBoxPrayClass;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.DateTimePicker dateTimePickerFullPay;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox textBoxPrayerName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox textBoxPrayerBirthYear;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox textBoxPrayerAddress;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox textBoxPrayerPhone;
        private System.Windows.Forms.Button buttonFamilySearch;
        private System.Windows.Forms.CheckBox checkBoxPrayFinish;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textBoxPrayPaperNumber;
    }
}