﻿namespace HyunjisaBeliever
{
    partial class UserControlOffering
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.comboBoxBuddha = new System.Windows.Forms.ComboBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.dateTimePickerOfferingDay = new System.Windows.Forms.DateTimePicker();
            this.labelRegisterDay = new System.Windows.Forms.Label();
            this.labelBeliever = new System.Windows.Forms.Label();
            this.textBoxBeliever = new System.Windows.Forms.TextBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonAddNew = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelBuddha = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOfferor = new System.Windows.Forms.TextBox();
            this.comboBoxOfferingTemple = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelRowCount = new System.Windows.Forms.Label();
            this.buttonExportExcel = new System.Windows.Forms.Button();
            this.buttonImportExcel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxBuddha
            // 
            this.comboBoxBuddha.FormattingEnabled = true;
            this.comboBoxBuddha.Location = new System.Drawing.Point(87, 39);
            this.comboBoxBuddha.Name = "comboBoxBuddha";
            this.comboBoxBuddha.Size = new System.Drawing.Size(157, 20);
            this.comboBoxBuddha.TabIndex = 91;
            this.comboBoxBuddha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxBuddha_KeyPress);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.FlatAppearance.BorderSize = 2;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.Location = new System.Drawing.Point(773, 13);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(116, 50);
            this.buttonConfirm.TabIndex = 89;
            this.buttonConfirm.Text = "공양 검색";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // dateTimePickerOfferingDay
            // 
            this.dateTimePickerOfferingDay.CustomFormat = "yyyy / MM";
            this.dateTimePickerOfferingDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerOfferingDay.Location = new System.Drawing.Point(87, 65);
            this.dateTimePickerOfferingDay.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerOfferingDay.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerOfferingDay.Name = "dateTimePickerOfferingDay";
            this.dateTimePickerOfferingDay.Size = new System.Drawing.Size(157, 21);
            this.dateTimePickerOfferingDay.TabIndex = 88;
            this.dateTimePickerOfferingDay.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerOfferingDay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateTimePickerOfferingDay_KeyPress);
            // 
            // labelRegisterDay
            // 
            this.labelRegisterDay.AutoSize = true;
            this.labelRegisterDay.Location = new System.Drawing.Point(16, 71);
            this.labelRegisterDay.Name = "labelRegisterDay";
            this.labelRegisterDay.Size = new System.Drawing.Size(41, 12);
            this.labelRegisterDay.TabIndex = 84;
            this.labelRegisterDay.Text = "공양일";
            // 
            // labelBeliever
            // 
            this.labelBeliever.AutoSize = true;
            this.labelBeliever.Location = new System.Drawing.Point(285, 14);
            this.labelBeliever.Name = "labelBeliever";
            this.labelBeliever.Size = new System.Drawing.Size(41, 12);
            this.labelBeliever.TabIndex = 79;
            this.labelBeliever.Text = "신청자";
            // 
            // textBoxBeliever
            // 
            this.textBoxBeliever.Location = new System.Drawing.Point(332, 11);
            this.textBoxBeliever.Name = "textBoxBeliever";
            this.textBoxBeliever.Size = new System.Drawing.Size(157, 21);
            this.textBoxBeliever.TabIndex = 76;
            this.textBoxBeliever.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxBeliever_KeyPress);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonRefresh.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonRefresh.FlatAppearance.BorderSize = 2;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonRefresh.ForeColor = System.Drawing.Color.Maroon;
            this.buttonRefresh.Location = new System.Drawing.Point(895, 13);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(116, 50);
            this.buttonRefresh.TabIndex = 95;
            this.buttonRefresh.Text = "초기화";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // buttonAddNew
            // 
            this.buttonAddNew.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonAddNew.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonAddNew.FlatAppearance.BorderSize = 2;
            this.buttonAddNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonAddNew.ForeColor = System.Drawing.Color.Maroon;
            this.buttonAddNew.Location = new System.Drawing.Point(669, 26);
            this.buttonAddNew.Name = "buttonAddNew";
            this.buttonAddNew.Size = new System.Drawing.Size(116, 50);
            this.buttonAddNew.TabIndex = 94;
            this.buttonAddNew.Text = "새로 입력하기";
            this.buttonAddNew.UseVisualStyleBackColor = false;
            this.buttonAddNew.Click += new System.EventHandler(this.ButtonAddNew_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Gulim", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(18, 183);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1011, 329);
            this.dataGridView1.TabIndex = 93;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellDoubleClick);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DataGridView1_CellPainting);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridView1_MouseClick);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTitle.Location = new System.Drawing.Point(27, 26);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(67, 37);
            this.labelTitle.TabIndex = 92;
            this.labelTitle.Text = "공양";
            // 
            // labelBuddha
            // 
            this.labelBuddha.AutoSize = true;
            this.labelBuddha.Location = new System.Drawing.Point(16, 42);
            this.labelBuddha.Name = "labelBuddha";
            this.labelBuddha.Size = new System.Drawing.Size(41, 12);
            this.labelBuddha.TabIndex = 78;
            this.labelBuddha.Text = "부처님";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.PapayaWhip;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxSearch);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxOfferor);
            this.panel1.Controls.Add(this.comboBoxOfferingTemple);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonRefresh);
            this.panel1.Controls.Add(this.comboBoxBuddha);
            this.panel1.Controls.Add(this.buttonConfirm);
            this.panel1.Controls.Add(this.dateTimePickerOfferingDay);
            this.panel1.Controls.Add(this.labelRegisterDay);
            this.panel1.Controls.Add(this.labelBeliever);
            this.panel1.Controls.Add(this.labelBuddha);
            this.panel1.Controls.Add(this.textBoxBeliever);
            this.panel1.Location = new System.Drawing.Point(18, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1011, 95);
            this.panel1.TabIndex = 96;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(269, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 103;
            this.label3.Text = "통합 검색";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(332, 66);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(157, 21);
            this.textBoxSearch.TabIndex = 102;
            this.textBoxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSearch_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(285, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 101;
            this.label2.Text = "복위자";
            // 
            // textBoxOfferor
            // 
            this.textBoxOfferor.Location = new System.Drawing.Point(332, 39);
            this.textBoxOfferor.Name = "textBoxOfferor";
            this.textBoxOfferor.Size = new System.Drawing.Size(157, 21);
            this.textBoxOfferor.TabIndex = 100;
            this.textBoxOfferor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxOfferor_KeyPress);
            // 
            // comboBoxOfferingTemple
            // 
            this.comboBoxOfferingTemple.FormattingEnabled = true;
            this.comboBoxOfferingTemple.Location = new System.Drawing.Point(87, 12);
            this.comboBoxOfferingTemple.Name = "comboBoxOfferingTemple";
            this.comboBoxOfferingTemple.Size = new System.Drawing.Size(157, 20);
            this.comboBoxOfferingTemple.TabIndex = 99;
            this.comboBoxOfferingTemple.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOfferingTemple_SelectedIndexChanged);
            this.comboBoxOfferingTemple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxOfferingTemple_KeyPress);
            this.comboBoxOfferingTemple.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBoxOfferingTemple_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 98;
            this.label1.Text = "접수 분원";
            // 
            // labelRowCount
            // 
            this.labelRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRowCount.AutoSize = true;
            this.labelRowCount.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelRowCount.Location = new System.Drawing.Point(14, 515);
            this.labelRowCount.Name = "labelRowCount";
            this.labelRowCount.Size = new System.Drawing.Size(265, 21);
            this.labelRowCount.TabIndex = 97;
            this.labelRowCount.Text = "총 n개의 데이터가 검색되었습니다.";
            // 
            // buttonExportExcel
            // 
            this.buttonExportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonExportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.FlatAppearance.BorderSize = 2;
            this.buttonExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonExportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonExportExcel.Location = new System.Drawing.Point(913, 26);
            this.buttonExportExcel.Name = "buttonExportExcel";
            this.buttonExportExcel.Size = new System.Drawing.Size(116, 50);
            this.buttonExportExcel.TabIndex = 99;
            this.buttonExportExcel.Text = "Excel 출력";
            this.buttonExportExcel.UseVisualStyleBackColor = false;
            this.buttonExportExcel.Click += new System.EventHandler(this.ButtonExportExcel_Click);
            // 
            // buttonImportExcel
            // 
            this.buttonImportExcel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonImportExcel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.FlatAppearance.BorderSize = 2;
            this.buttonImportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonImportExcel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonImportExcel.Location = new System.Drawing.Point(791, 26);
            this.buttonImportExcel.Name = "buttonImportExcel";
            this.buttonImportExcel.Size = new System.Drawing.Size(116, 50);
            this.buttonImportExcel.TabIndex = 100;
            this.buttonImportExcel.Text = "Excel 읽기";
            this.buttonImportExcel.UseVisualStyleBackColor = false;
            this.buttonImportExcel.Visible = false;
            // 
            // UserControlOffering
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.Controls.Add(this.buttonExportExcel);
            this.Controls.Add(this.buttonImportExcel);
            this.Controls.Add(this.labelRowCount);
            this.Controls.Add(this.buttonAddNew);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.panel1);
            this.Name = "UserControlOffering";
            this.Size = new System.Drawing.Size(1056, 612);
            this.Load += new System.EventHandler(this.UserControlOffering_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxBuddha;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.DateTimePicker dateTimePickerOfferingDay;
        private System.Windows.Forms.Label labelRegisterDay;
        private System.Windows.Forms.Label labelBeliever;
        private System.Windows.Forms.TextBox textBoxBeliever;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonAddNew;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelBuddha;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxOfferingTemple;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelRowCount;
        private System.Windows.Forms.Button buttonExportExcel;
        private System.Windows.Forms.Button buttonImportExcel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOfferor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSearch;
    }
}
