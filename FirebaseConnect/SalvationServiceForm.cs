﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class SalvationServiceForm : Form
    {
        public EditMode editMode;
        int savedBelieverUID;
        private SalvationService _salvationServiceInfo = new SalvationService();
        public SalvationService serviceInfo
        {
            set
            {
                _salvationServiceInfo = value;
                this.textBoxID.Text = _salvationServiceInfo.UID.ToString();                
                this.SelectBelieverID(_salvationServiceInfo.believerUID);
                this.dateTimePickerSalvationService.Value = _salvationServiceInfo.DateOfService;
                this.comboBoxServiceTemple.SelectedItem = (LocalTemple)_salvationServiceInfo.ServiceTemple;
                this.textBoxRequest.Text = _salvationServiceInfo.Request;
                this.textBoxRemarks.Text = _salvationServiceInfo.Remarks;                
            }
            get
            {
                _salvationServiceInfo.UID = (int.TryParse(textBoxID.Text, out int serviceID)) ? serviceID : 0;
                _salvationServiceInfo.believerUID = this.savedBelieverUID;
                _salvationServiceInfo.DateOfService = dateTimePickerSalvationService.Value.Date;
                _salvationServiceInfo.ServiceTemple = (LocalTemple)this.comboBoxServiceTemple.SelectedItem;
                _salvationServiceInfo.Request = textBoxRequest.Text;
                _salvationServiceInfo.Remarks = textBoxRemarks.Text;
                return _salvationServiceInfo;
            }
        }

        private void SelectBelieverID(int believerUID)
        {
            this.savedBelieverUID = believerUID;
            if (savedBelieverUID == 0)
            {
                this.textBoxBelieverName.Text = string.Empty;
                this.textBoxBelieverPhone.Text = string.Empty;
            }
            else
            {
                try
                {
                    var believer = SqlManager.SelectBelieverFromSQL(savedBelieverUID);
                    this.textBoxBelieverName.Text = believer.Name;
                    this.textBoxBelieverPhone.Text = believer.Phone;
                }
                catch { MessageBox.Show("신도 정보가 올바르지 않습니다. 다시 입력해주세요."); }
            }
        }

        public SalvationServiceForm()
        {
            InitializeComponent();
            comboBoxServiceTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
        }

        private void SalvationServiceForm_Load(object sender, EventArgs e)
        {
            if(this.editMode == EditMode.INSERT_MODE)
                this.comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;

        }

        private void ButtonBelieverSearch_Click(object sender, EventArgs e)
        {
            var believerSearchForm = new BelieverSearchForm();
            believerSearchForm.EventSelectBelieverID += this.SelectBelieverID;
            if (believerSearchForm.ShowDialog() == DialogResult.OK)
            {
                believerSearchForm.Close();
            }
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (this.savedBelieverUID == 0)
            {
                MessageBox.Show("신도가 누구인지 입력되지 않았습니다. ");
                return;
            }
            else if ((LocalTemple)this.comboBoxServiceTemple.SelectedItem == LocalTemple.미지정)
            {
                MessageBox.Show("접수 분원이 정해지지 않았습니다.");
            }

            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.천도재담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //----------accessedMonk auth check part----------//            
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple( (LocalTemple)comboBoxServiceTemple.SelectedItem ) )
                {
                    MessageBox.Show("다른 분원의 천도재 정보는 입력할 수 없습니다.");
                    return;
                }
            }

            if (this.editMode == EditMode.INSERT_MODE)
            {
                if (SqlManager.InsertSalvationServiceToSQL(serviceInfo))
                    this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (SqlManager.UpdateSalvationServiceInSQL(serviceInfo))
                    this.DialogResult = DialogResult.OK;
            }

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboBoxServiceTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainWindow.accessedMonk != null
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)comboBoxServiceTemple.SelectedItem) )
                {
                    if ((LocalTemple)comboBoxServiceTemple.SelectedItem != LocalTemple.미지정)
                        MessageBox.Show("다른 분원의 천도재는 입력할 수 없습니다.");

                    comboBoxServiceTemple.SelectedItem = MainWindow.manageArea;
                }
            }
        }
    }
}
