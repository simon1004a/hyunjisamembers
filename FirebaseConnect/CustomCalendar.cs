﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;

namespace HyunjisaBeliever
{
    public partial class CustomCalendar : UserControl
    {        
        //원래 void 가 아니라 DateTime형을 return했으나 2019-2-29 같은 날짜를 처리하지 못하여 이런 식으로 바꾸었다...
        public static void LunarDateConvertFromSolarDate(DateTime solarDate, out int year, out int month, out int day)
        {
            var lunarCalendar = new KoreanLunisolarCalendar();
            year = lunarCalendar.GetYear(solarDate);
            month = lunarCalendar.GetMonth(solarDate);
            day = lunarCalendar.GetDayOfMonth(solarDate);
            
            //윤달이 끼어 있으면 1을 빼주어야 한다. 
            if (lunarCalendar.GetMonthsInYear(year) > 12)
            {
                int leapMonth = lunarCalendar.GetLeapMonth(year);
                if (month >= leapMonth)
                    month--;                
            }            
            return;
        }


        public float schedulerFontSize
        {
            get{ return calendarTable.schedulerFontSize; }
            set{ calendarTable.schedulerFontSize = value; }
        }
        
        public delegate void DateTimeEventHandler(DateTime? dateTime = null);
        public event DateTimeEventHandler EventDateChanged;
        public event DateTimeEventHandler EventUpdateSchedule;
        CalendarTablePanel calendarTable;
        public CustomCalendar()
        {
            InitializeComponent();
            calendarTable = new CalendarTablePanel();
            calendarTable.EventDateChangeInTable += new CalendarTablePanel.DateTimeEventHandler(CalendarDateChange);
            calendarTable.EventUpdateScheduleInTable += new CalendarTablePanel.DateTimeEventHandler(CalendarScheduleUpdate);
            calendarTable.Dock = DockStyle.Fill;
                        
            //calendarTable.SetRedDay(new int[] { 3, 12 }, new string[] { "빨간날", "RedDay" });            
            calendarTable.SelectDate(DateTime.Now);
            
            panelDay.Controls.Add(calendarTable);
            panelDay.BackColor = Color.FromArgb(41, 128, 185);
            labelCaption.Text = DateTime.Now.Year + "년 " + DateTime.Now.Month + "월";
        }

        public DateTime SelectedDate
        {
            get { return calendarTable.dateTimeValue; }
            set { calendarTable.dateTimeValue = value; }
        }

        public List<Ceremony> MonthCeremonies
        {
            get { return calendarTable.monthScheduleList; }
            set { calendarTable.monthScheduleList = value; }
        }
        
        private void CalendarDateChange(DateTime? _dateTime)
        {
            var dateTime = (_dateTime != null) ? (DateTime)_dateTime : DateTime.Now.Date;
            labelCaption.Text = dateTime.Year + "년 " + dateTime.Month + "월";
            if (EventDateChanged != null)
                EventDateChanged(_dateTime);
        }

        private void CalendarScheduleUpdate(DateTime? _dateTime)
        {            
            if (EventUpdateSchedule != null)
                EventUpdateSchedule(_dateTime);
        }

        private void label_next_Click(object sender, EventArgs e)
        {
            calendarTable.NextMonth();
        }

        private void label_pre_Click(object sender, EventArgs e)
        {
            calendarTable.PreMonth();
        }

        private void label_caption_Click(object sender, EventArgs e)
        {
            calendarTable.MoveToToday();
        }

        public void SetHoliday(int[] dayNumber, string[] redDayName, DateTime dt)
        {
            calendarTable.SetRedDay(dayNumber, redDayName);
            calendarTable.SelectDate(dt);
            labelCaption.Text = dt.Year + "년 " + dt.Month + "월";
        }

        public void SelectDate(DateTime dateTime)
        {
            calendarTable.SelectDate(dateTime);
        }
        public void GoToTheDay(DateTime date)
        {
            calendarTable.MoveToTheDay(date);
        }
        public void GoToToday()
        {
            calendarTable.MoveToToday();
        }
        public void GoToPreDay()
        {
            calendarTable.PreDay();
        }
        public void GoToNextDay()
        {
            calendarTable.NextDay();
        }
        public void GoToPreWeek()
        {
            calendarTable.PreWeek();
        }
        public void GoToNextWeek()
        {
            calendarTable.NextWeek();
        }
        public void GoToPreMonth()
        {
            calendarTable.PreMonth();
        }
        public void GoToNextMonth()
        {
            calendarTable.NextMonth();
        }
    }

    class DateLabel : Label
    {
        public DateLabel()
        {
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw, true);
            this.UpdateStyles();
        }
    }

    class CalendarTablePanel : TableLayoutPanel
    {        
        public delegate void DateTimeEventHandler(DateTime? dateTime = null);
        public event DateTimeEventHandler EventDateChangeInTable;
        public event DateTimeEventHandler EventUpdateScheduleInTable;

        //public string UserName { get; set; }                
        public float schedulerFontSize { get; set; }
                
        public DateTime dateTimeValue = DateTime.Now;
        public DateLabel[] dateLabelTable = new DateLabel[42];
        int[] redDayList = null;
        string[] redDayName = null;
        public List<Ceremony> monthScheduleList;
        private readonly ToolTip toolTip;
        
        public CalendarTablePanel()
        {
            DoubleBuffered = true;          
            SetRowAndColumn(7, 7);
            toolTip = new ToolTip();
        }

        public void SetRedDay(int[] redDayList, string[] redDayName)
        {
            this.redDayList = redDayList;
            this.redDayName = redDayName;
        }

        //달력 그리기
        public void SetRowAndColumn(int _rowCount, int _columnCount)
        {
            RowCount = _rowCount;
            ColumnCount = _columnCount;
            
            for (int i = 0; i < RowCount; i++)
            {
                if( i == 0)
                    RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
                else
                    RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            }   

            for (int i = 0; i < ColumnCount; i++) 
                ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                        
            DateLabel dateLabel = null;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    dateLabel = new DateLabel();
                    dateLabel.AutoSize = false;
                    if (i == 0)             //첫 줄은 요일이다.
                    {
                        dateLabel.Text = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[j][0].ToString() + "요일";
                        //토요일 일요일 타이틀 색 변경
                        if (j == 0 || j == 6)
                            dateLabel.ForeColor = System.Drawing.Color.FromArgb(255, 250, 54, 156);
                        else
                            dateLabel.ForeColor = System.Drawing.Color.White;

                        dateLabel.BackColor = System.Drawing.Color.FromArgb(255, 148, 214, 240);
                        
                        dateLabel.TextAlign = ContentAlignment.MiddleCenter;
                    }
                    else
                    {
                        dateLabel.ForeColor = System.Drawing.Color.Black;
                        dateLabel.BackColor = System.Drawing.Color.FromArgb(255, 246, 229);
                        dateLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                        dateLabel.MouseClick += new MouseEventHandler(CalendarTable_MouseClick);                        
                        dateLabel.MouseHover += new EventHandler(CalendarTable_MouseOver);
                        dateLabelTable[j + ((i-1) * 7)] = dateLabel;    
                    }

                    dateLabel.Font = new System.Drawing.Font("나눔바른고딕", 12, FontStyle.Bold );
                    //dateLabel.BorderStyle = BorderStyle.None;
                    dateLabel.Margin = new Padding(1);
                    dateLabel.Padding = new Padding(0);
                    dateLabel.Dock = DockStyle.Fill;
                    this.BackColor = Color.FromArgb(255, 230, 234, 243);
                    Controls.Add(dateLabel, j, i);
                }
            }
        }

                
        public void SelectDate(DateTime dateTime)
        {            
            dateTimeValue = dateTime;

            ClearDay();
            DateTime firstDay = new DateTime(dateTime.Year, dateTime.Month, 1);
            int endDay = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
            int week = (int)firstDay.DayOfWeek;

            for (int iDay = 1; iDay <= endDay; iDay++)
            {   
                int index = iDay + week -1;

                dateLabelTable[index].Cursor = Cursors.Hand;
                dateLabelTable[index].Text = string.Format("{0:0}", iDay);
                dateLabelTable[index].BackColor = Color.White;

                DateTime dateDay = firstDay.AddDays(iDay - 1);
                dateLabelTable[index].Tag = dateDay;

                //Show also the Lunar date!
                CustomCalendar.LunarDateConvertFromSolarDate(dateDay, out int lunarYear, out int lunarMonth, out int lunarDay );
                if( lunarDay == 1 || lunarDay == 15 )
                    dateLabelTable[index].Text += string.Format("/음{0}.{1}", lunarMonth, lunarDay);
                else
                    dateLabelTable[index].Text += string.Format("/음{0}", lunarDay);

                //Highlight the Date if it is holiday or weekend
                int specialDayIndex;                
                if (redDayList != null && (specialDayIndex = Array.IndexOf(redDayList, iDay)) >= 0)
                {
                    dateLabelTable[index].ForeColor = Color.FromArgb(255, 250, 54, 156);

                    try { dateLabelTable[index].Text += "\n" + redDayName[specialDayIndex]; }
                    catch { Console.WriteLine("redDayName may not exist at index : " + specialDayIndex); }
                }
                else if (dateDay.DayOfWeek == DayOfWeek.Sunday || dateDay.DayOfWeek == DayOfWeek.Saturday)
                {
                    dateLabelTable[index].ForeColor = Color.FromArgb(255, 250, 54, 156);
                }                
                else
                {
                    dateLabelTable[index].ForeColor = Color.FromArgb(255, 2, 109, 191);
                }
                
                //Specify Today
                if (dateDay.Date == DateTime.Now.Date)
                {
                    dateLabelTable[index].BorderStyle = BorderStyle.Fixed3D;
                }
                                
                //Highlight the Selected Date.
                if (dateDay.Date == dateTimeValue.Date)
                {
                    dateLabelTable[index].ForeColor = Color.FromArgb(255, 255, 215, 0);
                    dateLabelTable[index].BorderStyle = BorderStyle.FixedSingle;
                }

                //Update schedule!
                var ceremonyInDay = Ceremony.GetCeremonyOfTheDay(monthScheduleList, dateDay.Date);
                if (ceremonyInDay != null)
                {
                    for (int i = 0; i < ceremonyInDay.Count; i++)
                    {
                        string scheduleName = ceremonyInDay[i].Name;
                        if (!string.IsNullOrWhiteSpace(scheduleName))
                            dateLabelTable[index].Text += "\n" + scheduleName;
                    }
                }
                
            }

        }


        public void NextMonth()
        {
            DateTime dateTime = dateTimeValue.AddMonths(1);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void PreMonth()
        {
            DateTime dateTime = dateTimeValue.AddMonths(-1);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void NextWeek()
        {
            DateTime dateTime = dateTimeValue.AddDays(7);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void PreWeek()
        {
            DateTime dateTime = dateTimeValue.AddDays(-7);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void NextDay()
        {
            DateTime dateTime = dateTimeValue.AddDays(1);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void PreDay()
        {
            DateTime dateTime = dateTimeValue.AddDays(-1);            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }
        public void MoveToToday()
        {
            DateTime dateTime = DateTime.Now;            
            if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
            SelectDate(dateTime);
        }

        public void MoveToTheDay(DateTime date)
        {
            if (EventDateChangeInTable != null) EventDateChangeInTable(date);
            SelectDate(date);
        }

        public void ClearDay()
        {
            for(int i = 0; i < 42; i++)
            {
                dateLabelTable[i].BorderStyle = System.Windows.Forms.BorderStyle.None;
                dateLabelTable[i].BackColor = Color.White;   //날짜 없는 날 색 변경
                dateLabelTable[i].Cursor = Cursors.Default;
                dateLabelTable[i].Text = string.Empty;
            }
        }


        private void CalendarTable_MouseOver(object sender, EventArgs e)
        {            
            toolTip.SetToolTip(((Control)sender), ((Control)sender).Text);
        }
        
        //To detect the Double Click event - mouse_doubleClick event does not fire from dateLabel control.
        static int clickCount;
        static DateTime clickedDate = new DateTime();
        static Stopwatch stopWatch = new Stopwatch();

        private void CalendarTable_MouseClick(object sender, MouseEventArgs e)
        {
            var senderControl = (Control)sender;
            if (!string.IsNullOrEmpty(senderControl.Text) && senderControl.Tag != null)
            {
                DateTime dateTime = (DateTime)senderControl.Tag;
                
                #region Check_MouseDoubleClickEvent                
                if (clickedDate != dateTime)
                {
                    clickedDate = dateTime;
                    clickCount = 1;
                }
                else clickCount++;

                if (clickCount == 1)
                    stopWatch.Restart();
                else if (clickCount > 1)
                {                    
                    stopWatch.Stop();
                    var elapsed = stopWatch.ElapsedMilliseconds;
                    stopWatch.Reset();
                    if (elapsed < 200)
                    {
                        clickCount = 0;
                        this.CalendarTable_MouseDoubleClick(sender, e);
                        return;
                    }
                    else
                    {
                        stopWatch.Restart();
                        clickCount = 1;
                    }
                }
                #endregion

                if (EventDateChangeInTable != null) EventDateChangeInTable(dateTime);
                SelectDate(dateTime);

                if (e.Button == MouseButtons.Right)
                {
                    var menuStrip = new ContextMenuStrip();
                    menuStrip.Items.Clear();
                    //menuStrip.Items.Add("공휴일로 등록").Name = "SetRedDay";
                    menuStrip.Items.Add("일정 등록").Name = "AddSchedule";
                    var ceremonyInDay = Ceremony.GetCeremonyOfTheDay(monthScheduleList, dateTime.Date);
                    if (ceremonyInDay != null)
                    {
                        if (ceremonyInDay.Count > 0)
                            menuStrip.Items.Add("일정 수정").Name = "ModifyScheduleTable";                        
                    }
                    menuStrip.Items.Add("등록 취소").Name = "Cancel";
                    menuStrip.Tag = dateTime;

                    menuStrip.Show(senderControl, new Point(e.X, e.Y));
                    menuStrip.ItemClicked += menuStrip_ItemClicked;
                }
            }
            else
            {
                stopWatch.Stop();
                stopWatch.Reset();
            }
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var dateTime = (DateTime)((ContextMenuStrip)sender).Tag;
            switch (e.ClickedItem.Name)
            {
                case "SetRedDay":
                    break;
                case "AddSchedule":
                    var ceremonyInsertForm = new CeremonyModifyForm { CeremonyDateTime = dateTime };
                    if (ceremonyInsertForm.ShowDialog() == DialogResult.OK)
                    {
                        CalendarScheduleUpdateInTable(dateTime);
                        ceremonyInsertForm.Close();
                    }
                    break;
                case "ModifyScheduleTable":
                    var ceremonyViewForm = new CeremonyViewForm(dateTime);
                    ceremonyViewForm.EventRefreshCeremonyList += CalendarScheduleUpdateInTable;
                    ceremonyViewForm.ShowDialog();
                    break;                
                case "Cancel":
                    break;
            }
        }

        private void CalendarTable_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderControl = (Control)sender;
            if (!string.IsNullOrEmpty(senderControl.Text) && senderControl.Tag != null)
            {
                DateTime dateTime = (DateTime)senderControl.Tag;
                var ceremonyInDay = Ceremony.GetCeremonyOfTheDay(monthScheduleList, dateTime.Date);

                if (ceremonyInDay != null)
                {
                    if (ceremonyInDay.Count > 0)
                    {
                        var ceremonyViewForm = new CeremonyViewForm(dateTime);
                        ceremonyViewForm.EventRefreshCeremonyList += CalendarScheduleUpdateInTable;
                        ceremonyViewForm.ShowDialog();
                    }                        
                }
            }
        }

        void CalendarScheduleUpdateInTable(DateTime? dateTime = null)
        {
            Console.WriteLine("CalendarScheduleUpdateInTable called!");
            if (EventUpdateScheduleInTable != null)
                EventUpdateScheduleInTable(dateTime);
        }

    }
}
