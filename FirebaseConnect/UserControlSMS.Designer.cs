﻿namespace HyunjisaBeliever
{
    partial class UserControlSMS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelSMS = new System.Windows.Forms.Panel();
            this.checkBoxReceiver = new System.Windows.Forms.CheckBox();
            this.labelRowCount = new System.Windows.Forms.Label();
            this.labelSender = new System.Windows.Forms.Label();
            this.comboBoxSender = new System.Windows.Forms.ComboBox();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.buttonReset = new System.Windows.Forms.Button();
            this.comboBoxProvince2 = new System.Windows.Forms.ComboBox();
            this.labelProvince2 = new System.Windows.Forms.Label();
            this.comboBoxProvince1 = new System.Windows.Forms.ComboBox();
            this.labelProvince1 = new System.Windows.Forms.Label();
            this.comboBoxTemple2 = new System.Windows.Forms.ComboBox();
            this.labelTemple2 = new System.Windows.Forms.Label();
            this.labelPanelTitle = new System.Windows.Forms.Label();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.comboBoxTemple1 = new System.Windows.Forms.ComboBox();
            this.labelTemple1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSendMsg = new System.Windows.Forms.Button();
            this.textBoxMsg = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.textBoxLoginPassword = new System.Windows.Forms.TextBox();
            this.textBoxLoginID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelSignIn = new System.Windows.Forms.Panel();
            this.panelSMS.SuspendLayout();
            this.panelSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelSignIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTitle.Location = new System.Drawing.Point(25, 20);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(151, 37);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "문자 보내기";
            // 
            // panelSMS
            // 
            this.panelSMS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSMS.Controls.Add(this.checkBoxReceiver);
            this.panelSMS.Controls.Add(this.labelRowCount);
            this.panelSMS.Controls.Add(this.labelSender);
            this.panelSMS.Controls.Add(this.comboBoxSender);
            this.panelSMS.Controls.Add(this.panelSearch);
            this.panelSMS.Controls.Add(this.dataGridView1);
            this.panelSMS.Controls.Add(this.buttonSendMsg);
            this.panelSMS.Controls.Add(this.textBoxMsg);
            this.panelSMS.Location = new System.Drawing.Point(31, 82);
            this.panelSMS.Name = "panelSMS";
            this.panelSMS.Size = new System.Drawing.Size(998, 564);
            this.panelSMS.TabIndex = 10;
            // 
            // checkBoxReceiver
            // 
            this.checkBoxReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxReceiver.AutoSize = true;
            this.checkBoxReceiver.Location = new System.Drawing.Point(895, 2);
            this.checkBoxReceiver.Name = "checkBoxReceiver";
            this.checkBoxReceiver.Size = new System.Drawing.Size(100, 16);
            this.checkBoxReceiver.TabIndex = 94;
            this.checkBoxReceiver.Text = "모두 선택하기";
            this.checkBoxReceiver.UseVisualStyleBackColor = true;
            this.checkBoxReceiver.CheckedChanged += new System.EventHandler(this.CheckBoxReceiver_CheckedChanged);
            // 
            // labelRowCount
            // 
            this.labelRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRowCount.AutoSize = true;
            this.labelRowCount.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelRowCount.Location = new System.Drawing.Point(427, 467);
            this.labelRowCount.Name = "labelRowCount";
            this.labelRowCount.Size = new System.Drawing.Size(249, 21);
            this.labelRowCount.TabIndex = 93;
            this.labelRowCount.Text = "총 n명의 신도가 검색되었습니다.";
            // 
            // labelSender
            // 
            this.labelSender.AutoSize = true;
            this.labelSender.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold);
            this.labelSender.Location = new System.Drawing.Point(31, 178);
            this.labelSender.Name = "labelSender";
            this.labelSender.Size = new System.Drawing.Size(99, 16);
            this.labelSender.TabIndex = 92;
            this.labelSender.Text = "문자 송신처";
            this.labelSender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSender
            // 
            this.comboBoxSender.FormattingEnabled = true;
            this.comboBoxSender.ItemHeight = 12;
            this.comboBoxSender.Location = new System.Drawing.Point(243, 174);
            this.comboBoxSender.Name = "comboBoxSender";
            this.comboBoxSender.Size = new System.Drawing.Size(168, 20);
            this.comboBoxSender.TabIndex = 91;
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panelSearch.Controls.Add(this.buttonReset);
            this.panelSearch.Controls.Add(this.comboBoxProvince2);
            this.panelSearch.Controls.Add(this.labelProvince2);
            this.panelSearch.Controls.Add(this.comboBoxProvince1);
            this.panelSearch.Controls.Add(this.labelProvince1);
            this.panelSearch.Controls.Add(this.comboBoxTemple2);
            this.panelSearch.Controls.Add(this.labelTemple2);
            this.panelSearch.Controls.Add(this.labelPanelTitle);
            this.panelSearch.Controls.Add(this.buttonConfirm);
            this.panelSearch.Controls.Add(this.comboBoxTemple1);
            this.panelSearch.Controls.Add(this.labelTemple1);
            this.panelSearch.Location = new System.Drawing.Point(36, 249);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(337, 270);
            this.panelSearch.TabIndex = 90;
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonReset.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonReset.FlatAppearance.BorderSize = 2;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonReset.ForeColor = System.Drawing.Color.Maroon;
            this.buttonReset.Location = new System.Drawing.Point(37, 189);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(120, 50);
            this.buttonReset.TabIndex = 112;
            this.buttonReset.Text = "초기화";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // comboBoxProvince2
            // 
            this.comboBoxProvince2.FormattingEnabled = true;
            this.comboBoxProvince2.Location = new System.Drawing.Point(106, 151);
            this.comboBoxProvince2.Name = "comboBoxProvince2";
            this.comboBoxProvince2.Size = new System.Drawing.Size(177, 20);
            this.comboBoxProvince2.TabIndex = 111;
            // 
            // labelProvince2
            // 
            this.labelProvince2.AutoSize = true;
            this.labelProvince2.Location = new System.Drawing.Point(35, 154);
            this.labelProvince2.Name = "labelProvince2";
            this.labelProvince2.Size = new System.Drawing.Size(35, 12);
            this.labelProvince2.TabIndex = 110;
            this.labelProvince2.Text = "지역2";
            // 
            // comboBoxProvince1
            // 
            this.comboBoxProvince1.FormattingEnabled = true;
            this.comboBoxProvince1.Location = new System.Drawing.Point(106, 125);
            this.comboBoxProvince1.Name = "comboBoxProvince1";
            this.comboBoxProvince1.Size = new System.Drawing.Size(177, 20);
            this.comboBoxProvince1.TabIndex = 109;
            // 
            // labelProvince1
            // 
            this.labelProvince1.AutoSize = true;
            this.labelProvince1.Location = new System.Drawing.Point(35, 128);
            this.labelProvince1.Name = "labelProvince1";
            this.labelProvince1.Size = new System.Drawing.Size(35, 12);
            this.labelProvince1.TabIndex = 108;
            this.labelProvince1.Text = "지역1";
            // 
            // comboBoxTemple2
            // 
            this.comboBoxTemple2.FormattingEnabled = true;
            this.comboBoxTemple2.Location = new System.Drawing.Point(106, 83);
            this.comboBoxTemple2.Name = "comboBoxTemple2";
            this.comboBoxTemple2.Size = new System.Drawing.Size(177, 20);
            this.comboBoxTemple2.TabIndex = 107;
            // 
            // labelTemple2
            // 
            this.labelTemple2.AutoSize = true;
            this.labelTemple2.Location = new System.Drawing.Point(35, 86);
            this.labelTemple2.Name = "labelTemple2";
            this.labelTemple2.Size = new System.Drawing.Size(59, 12);
            this.labelTemple2.TabIndex = 106;
            this.labelTemple2.Text = "소속분원2";
            // 
            // labelPanelTitle
            // 
            this.labelPanelTitle.AutoSize = true;
            this.labelPanelTitle.Font = new System.Drawing.Font("GulimChe", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelPanelTitle.Location = new System.Drawing.Point(79, 18);
            this.labelPanelTitle.Name = "labelPanelTitle";
            this.labelPanelTitle.Size = new System.Drawing.Size(195, 22);
            this.labelPanelTitle.TabIndex = 105;
            this.labelPanelTitle.Text = "수신자 목록 검색";
            this.labelPanelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonConfirm.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.FlatAppearance.BorderSize = 2;
            this.buttonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonConfirm.ForeColor = System.Drawing.Color.Maroon;
            this.buttonConfirm.Location = new System.Drawing.Point(163, 189);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(120, 50);
            this.buttonConfirm.TabIndex = 104;
            this.buttonConfirm.Text = "검색하기";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // comboBoxTemple1
            // 
            this.comboBoxTemple1.FormattingEnabled = true;
            this.comboBoxTemple1.Location = new System.Drawing.Point(106, 57);
            this.comboBoxTemple1.Name = "comboBoxTemple1";
            this.comboBoxTemple1.Size = new System.Drawing.Size(177, 20);
            this.comboBoxTemple1.TabIndex = 100;
            // 
            // labelTemple1
            // 
            this.labelTemple1.AutoSize = true;
            this.labelTemple1.Location = new System.Drawing.Point(35, 60);
            this.labelTemple1.Name = "labelTemple1";
            this.labelTemple1.Size = new System.Drawing.Size(59, 12);
            this.labelTemple1.TabIndex = 98;
            this.labelTemple1.Text = "소속분원1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.AntiqueWhite;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Gulim", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(431, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(564, 439);
            this.dataGridView1.TabIndex = 18;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DataGridView1_CellPainting);
            // 
            // buttonSendMsg
            // 
            this.buttonSendMsg.BackColor = System.Drawing.Color.Peru;
            this.buttonSendMsg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSendMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonSendMsg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSendMsg.Location = new System.Drawing.Point(0, 204);
            this.buttonSendMsg.Name = "buttonSendMsg";
            this.buttonSendMsg.Size = new System.Drawing.Size(411, 32);
            this.buttonSendMsg.TabIndex = 17;
            this.buttonSendMsg.Text = "문자 보내기";
            this.buttonSendMsg.UseVisualStyleBackColor = false;
            this.buttonSendMsg.Click += new System.EventHandler(this.buttonSendMsg_Click_1);
            // 
            // textBoxMsg
            // 
            this.textBoxMsg.Location = new System.Drawing.Point(0, 0);
            this.textBoxMsg.Multiline = true;
            this.textBoxMsg.Name = "textBoxMsg";
            this.textBoxMsg.Size = new System.Drawing.Size(411, 168);
            this.textBoxMsg.TabIndex = 16;
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Peru;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonLogin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.buttonLogin.Location = new System.Drawing.Point(431, 272);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(125, 40);
            this.buttonLogin.TabIndex = 14;
            this.buttonLogin.Text = "로그인하기";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click_1);
            // 
            // textBoxLoginPassword
            // 
            this.textBoxLoginPassword.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxLoginPassword.Location = new System.Drawing.Point(431, 221);
            this.textBoxLoginPassword.Name = "textBoxLoginPassword";
            this.textBoxLoginPassword.PasswordChar = '*';
            this.textBoxLoginPassword.Size = new System.Drawing.Size(206, 26);
            this.textBoxLoginPassword.TabIndex = 13;
            // 
            // textBoxLoginID
            // 
            this.textBoxLoginID.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxLoginID.Location = new System.Drawing.Point(431, 186);
            this.textBoxLoginID.Name = "textBoxLoginID";
            this.textBoxLoginID.Size = new System.Drawing.Size(206, 26);
            this.textBoxLoginID.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F);
            this.label4.Location = new System.Drawing.Point(340, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(277, 32);
            this.label4.TabIndex = 11;
            this.label4.Text = "Olleh Biz Say 접속하기";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.Location = new System.Drawing.Point(341, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 24);
            this.label3.TabIndex = 10;
            this.label3.Text = "비밀번호";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.Location = new System.Drawing.Point(341, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "아이디";
            // 
            // panelSignIn
            // 
            this.panelSignIn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSignIn.Controls.Add(this.buttonLogin);
            this.panelSignIn.Controls.Add(this.textBoxLoginPassword);
            this.panelSignIn.Controls.Add(this.textBoxLoginID);
            this.panelSignIn.Controls.Add(this.label4);
            this.panelSignIn.Controls.Add(this.label3);
            this.panelSignIn.Controls.Add(this.label2);
            this.panelSignIn.Location = new System.Drawing.Point(31, 82);
            this.panelSignIn.Name = "panelSignIn";
            this.panelSignIn.Size = new System.Drawing.Size(998, 564);
            this.panelSignIn.TabIndex = 15;
            // 
            // UserControlSMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.panelSMS);
            this.Controls.Add(this.panelSignIn);
            this.Name = "UserControlSMS";
            this.Size = new System.Drawing.Size(1056, 680);
            this.Load += new System.EventHandler(this.UserControl2_Load);
            this.panelSMS.ResumeLayout(false);
            this.panelSMS.PerformLayout();
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelSignIn.ResumeLayout(false);
            this.panelSignIn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelSMS;
        private System.Windows.Forms.Button buttonSendMsg;
        private System.Windows.Forms.TextBox textBoxMsg;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.TextBox textBoxLoginPassword;
        private System.Windows.Forms.TextBox textBoxLoginID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelSignIn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.Label labelPanelTitle;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.ComboBox comboBoxTemple1;
        private System.Windows.Forms.Label labelTemple1;
        private System.Windows.Forms.Label labelSender;
        private System.Windows.Forms.ComboBox comboBoxSender;
        private System.Windows.Forms.ComboBox comboBoxTemple2;
        private System.Windows.Forms.Label labelTemple2;
        private System.Windows.Forms.ComboBox comboBoxProvince2;
        private System.Windows.Forms.Label labelProvince2;
        private System.Windows.Forms.ComboBox comboBoxProvince1;
        private System.Windows.Forms.Label labelProvince1;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label labelRowCount;
        private System.Windows.Forms.CheckBox checkBoxReceiver;
    }
}
