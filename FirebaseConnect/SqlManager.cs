﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public static class SqlManager
    {
        public static readonly string connectionString = ConfigurationManager.ConnectionStrings["HyunjisaConnection"].ConnectionString;
        
        public static DataTable GetDataTableFromSQL(string query)
        {
            DataTable dataTable = new DataTable();            
            //if you use using statement, connection will be lost after this function's scope
            using (var connection = new SqlConnection(connectionString))
            {
                using (var cmd = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        dataTable.Load(reader);
                    }
                    catch
                    {
                        MessageBox.Show("데이터베이스에 연결하는데 실패하였습니다.");
                        Application.Exit();
                    }
                    
                }
            }
            return dataTable;
        }
        public static bool ExecuteQueryOnSQL(string query)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {                    
                    connection.Open();
                    int result = command.ExecuteNonQuery();
                    bResult = (result >= 0);
                    if(bResult)
                        WriteLogOnSQL(command);                    
                }
            }
            return bResult;
        }

        public static void WriteLogOnSQL(SqlCommand sqlCommand)
        {
            Monk currentMonk = MainWindow.accessedMonk;
            if (currentMonk == null)
                return;

            string log = sqlCommand.CommandText;
            foreach (SqlParameter p in sqlCommand.Parameters)
            {
                string parameterValue;
                if (p.Value == null || string.IsNullOrWhiteSpace(p.Value.ToString()))
                    parameterValue = "NULL";
                else if (p.SqlDbType == SqlDbType.NVarChar || p.SqlDbType == SqlDbType.DateTime)
                    parameterValue = "'" + p.Value.ToString() + "'";
                else if (p.SqlDbType == SqlDbType.Bit)
                    parameterValue = (p.Value.ToString() == "True") ? "1" : "0";
                else
                    parameterValue = p.Value.ToString();
                
                log = log.Replace(p.ParameterName, parameterValue);
            }

            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO TaskLog (Time, MonkID, MonkName, LogQuery, manageArea) " +
                    "VALUES(@time, @monkId, @monkName, @logQuery, @manageArea)";                               

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@time", DateTime.Now);
                    command.Parameters.AddWithValue("@monkId", currentMonk.UID);
                    command.Parameters.AddWithValue("@monkName", currentMonk.DharmaName);
                    command.Parameters.AddWithValue("@logQuery", log);
                    command.Parameters.AddWithValue("@manageArea", (Int16)MainWindow.manageArea);                                        
                    connection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0)
                        MessageBox.Show("Log를 남기는데 실패하였습니다. 프로그램 작동에 문제는 없어도 " +
                            "관리자에게 알려주시는 게 좋습니다.");
                }
            }
        }

        #region Monk

        public static bool IsMonkExistInSQL(string monkname)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT MonkID, DharmaName FROM Monk WHERE DharmaName = @monkname";
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@monkname", monkname);                    
                    command.CommandType = CommandType.Text;
                    using (var reader = command.ExecuteReader())
                    {
                        bResult = reader.HasRows;           
                    }
                }
            }
            return bResult;
        }
        public static DataTable SelectMonkListFromSQL()
        {
            string query = "SELECT MonkID, DharmaName, Email, Phone, GenderName, TempleName, LevelName " +
                "FROM Monk AS M " +
                "INNER JOIN Gender AS G ON M.Gender = G.GenderID " +
                "INNER JOIN Temples AS T ON M.BelongedTemple = T.TempleID " +
                "INNER JOIN AccessLevel AS L ON M.LevelOfAccess = L.LevelID";
            return GetDataTableFromSQL(query);
        }
        public static Monk SelectMonkFromSQL(int iMonkID)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT M.MonkID, M.DharmaName, M.Password, M.Email, M.Phone, " +
                    "M.Gender, M.BelongedTemple, M.BelongedTemple2, M.LevelOfAccess, M.PartOfManage, M.Remarks " +
                    "FROM Monk AS M " +
                    "INNER JOIN Gender AS G ON M.Gender = G.GenderID " +
                    "INNER JOIN Temples AS T ON M.BelongedTemple = T.TempleID " +
                    "WHERE M.MonkID = @monkId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@monkId", iMonkID);                    
                    command.CommandType = CommandType.Text;

                    var monk = new Monk();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            monk.UID = reader.GetInt32(0);
                            monk.DharmaName = reader.GetString(1);
                            monk.Password = reader.GetString(2);
                            monk.Email = reader.GetString(3);
                            monk.Phone = reader.GetString(4);
                            monk.GenderType = (reader.GetBoolean(5)) ? Gender.남성 : Gender.여성;
                            monk.BelongedTemple = (LocalTemple)reader.GetInt16(6);
                            monk.BelongedTemple2 = (reader.IsDBNull(7)) ? LocalTemple.미지정 : (LocalTemple)reader.GetInt16(7);
                            monk.LevelOfAccess = (AccessLevel)reader.GetInt16(8);
                            monk.PartOfManage = (reader.IsDBNull(9)) ? (short)0 : reader.GetInt16(9);
                            if (!reader.IsDBNull(10))
                                monk.Remarks = reader.GetString(10);
                            return monk;
                        }
                        else
                        {
                            MessageBox.Show("해당 ID의 관리자 스님이 존재하지 않습니다.");
                        }
                    }
                }
            }
            return null;
        }
        public static Monk SearchMonkInSQL(string monkname, string password)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT M.MonkID, M.DharmaName, M.Password, M.Email, M.Phone, " +
                    "M.Gender, M.BelongedTemple, M.BelongedTemple2, M.LevelOfAccess, M.PartOfManage, M.Remarks " +
                    "FROM Monk AS M " +
                    "INNER JOIN Gender AS G ON M.Gender = G.GenderID " +
                    "INNER JOIN Temples AS T ON M.BelongedTemple = T.TempleID " +
                    "WHERE DharmaName = @monkname AND Password = @password";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@monkname", monkname);
                    command.Parameters.AddWithValue("@password", password);
                    command.CommandType = CommandType.Text;

                    var monk = new Monk();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            monk.UID = reader.GetInt32(0);
                            monk.DharmaName = reader.GetString(1);
                            monk.Password = reader.GetString(2);
                            monk.Email = reader.GetString(3);
                            monk.Phone = reader.GetString(4);
                            monk.GenderType = (reader.GetBoolean(5))? Gender.남성:Gender.여성;
                            monk.BelongedTemple = (LocalTemple)reader.GetInt16(6);
                            monk.BelongedTemple2 = (reader.IsDBNull(7)) ? LocalTemple.미지정: (LocalTemple)reader.GetInt16(7);
                            monk.LevelOfAccess = (AccessLevel)reader.GetInt16(8);
                            monk.PartOfManage = (reader.IsDBNull(9)) ? (short)0: reader.GetInt16(9);
                            if (!reader.IsDBNull(10))
                                monk.Remarks = reader.GetString(10);
                            return monk;
                        }
                        else
                        {
                            MessageBox.Show("ID 또는 비밀번호가 맞지 않습니다. ");
                        }
                    }
                }
            }
            return null;
        }        
        public static bool InsertMonkToSQL(Monk monk)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {                
                string query = "INSERT INTO Monk (DharmaName, Password, Email, Phone, Gender, BelongedTemple, LevelOfAccess , Remarks) " +
                    "VALUES(@dharmaname, @password, @email, @phone, @gender, @belongedtemple, @accesslevel, @remarks)";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@dharmaname", monk.DharmaName);
                    command.Parameters.AddWithValue("@password", monk.Password);
                    command.Parameters.AddWithValue("@email", monk.Email);
                    command.Parameters.AddWithValue("@phone", monk.Phone);
                    command.Parameters.AddWithValue("@gender", Convert.ToBoolean((int)monk.GenderType));
                    command.Parameters.AddWithValue("@belongedtemple", (Int16)monk.BelongedTemple);
                    command.Parameters.AddWithValue("@accesslevel", (Int16)monk.LevelOfAccess);
                    command.Parameters.AddWithValue("@remarks", monk.Remarks);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("새로운 관리자 정보를 입력하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                        
                }
            }
            return bResult;
        }

        public static bool UpdateMonkInSQL(Monk monk)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Monk SET DharmaName = @dharmaname, Password = @password, Email = @email, Phone = @phone, " +
                    "Gender = @gender, BelongedTemple = @belongedtemple, Remarks = @remarks, " +
                    "LevelOfAccess = @accesslevel, BelongedTemple2 = @belongedTemple2, PartOfManage = @managepart " +
                    "WHERE MonkID = @monkID";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@monkID", monk.UID);
                    command.Parameters.AddWithValue("@dharmaname", monk.DharmaName);
                    command.Parameters.AddWithValue("@password", monk.Password);
                    command.Parameters.AddWithValue("@email", monk.Email);
                    command.Parameters.AddWithValue("@phone", monk.Phone);
                    command.Parameters.AddWithValue("@gender", Convert.ToBoolean((int)monk.GenderType));
                    command.Parameters.AddWithValue("@belongedtemple", (Int16)monk.BelongedTemple);                    
                    command.Parameters.AddWithValue("@accesslevel", (Int16)monk.LevelOfAccess);
                    if(monk.PartOfManage == 0)
                        command.Parameters.AddWithValue("@managepart", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@managepart", monk.PartOfManage);
                    if (monk.BelongedTemple2 == LocalTemple.미지정)
                        command.Parameters.AddWithValue("@belongedtemple2", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@belongedtemple2", (Int16)monk.BelongedTemple2);
                    command.Parameters.AddWithValue("@remarks", monk.Remarks);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(monk.DharmaName + "스님의 정보를 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }

        #endregion

        #region Believer
        public static DataTable SelectBelieverListFromSQL(params string[] searchCriteria)
        {
            string query = "SELECT BelieverId as 신도아이디, BelieverNumber as 신도번호, FamilyID as 가족번호, Name as 이름, DharmaName as 법명, " +
                "P.ProvinceName as 지역, AddressDetail as 상세주소, Phone as 휴대폰, G.GenderName as 성별, T.TempleName as 소속분원, " +
                "Ganzhi as 간지, DateOfBirth as 생일, DateOfRegister as 등록일, Email as 이메일, Remarks as 비고 " +
                "FROM Believer AS B " +
                "INNER JOIN Gender AS G ON B.Gender = G.GenderID " +
                "INNER JOIN Temples AS T ON B.BelongedTemple = T.TempleID " +
                "INNER JOIN Provinces AS P ON B.AddressProvince = P.ProvinceID ";

            int conditionNumber = searchCriteria.Length;
            if (conditionNumber > 0)
            {
                string searchCondition = " WHERE ";                
                for (int i = 0; i < conditionNumber; i++)
                {
                    string seperator = (i == 0) ? "" : " AND ";
                    searchCondition += (seperator + searchCriteria[i]);
                }
                query += searchCondition;
            }
            
            return GetDataTableFromSQL(query);
        }
        public static DataTable SelectSMSReceiverListFromSQL(params string[] searchCriteria)
        {
            string query = "SELECT B.BelieverId as 신도아이디, B.BelieverNumber as 신도번호, B.DharmaName as 법명, B.Name as 이름, " +
                "P.ProvinceName as 지역, B.AddressDetail as 상세주소, B.Phone as 휴대폰, G.GenderName as 성별, T.TempleName as 소속분원, " +
                "B.Ganzhi as 간지, B.DateOfBirth as 생일, B.DateOfRegister as 등록일 , B.Email as 이메일 " +
                "FROM Believer AS B " +
                "INNER JOIN Gender AS G ON B.Gender = G.GenderID " +
                "INNER JOIN Temples AS T ON B.BelongedTemple = T.TempleID " +
                "INNER JOIN Provinces AS P ON B.AddressProvince = P.ProvinceID ";
            //문자는 자기 분원이 아니더라도 자기 분원에 기도를 올린 분들께 보내야 하니 일단은 모든 사람의 번호는 알 수 있어야 합니다...
            int conditionNumber = searchCriteria.Length;
            if (conditionNumber > 0)
            {
                string searchCondition = " WHERE ";
                for (int i = 0; i < conditionNumber; i++)
                {
                    string seperator = (i == 0) ? "" : " OR ";
                    searchCondition += (seperator + searchCriteria[i]);
                }
                query += searchCondition;
            }
            return GetDataTableFromSQL(query);
        }
        public static int GetLastBelieverIDFromSQL()
        {
            int lastID = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT IDENT_CURRENT('Believer')";
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    lastID = Convert.ToInt32(command.ExecuteScalar()) ;
                }
            }
            return lastID;
        }
        //그 해에, 그 소속 분원에 등록된 총 신도수를 세는 쿼리입니다. 
        public static int GetTempleRegisteredBelieverCountInYearFromSQL(LocalTemple temple, string year)
        {
            int believerCount = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT COUNT(*) FROM Believer WHERE BelongedTemple = " + (int)temple
                    + " AND DateOfRegister BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'"
                    + " AND BelieverNumber IS NOT NULL AND BelieverNumber != '' ";
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    believerCount = Convert.ToInt32(command.ExecuteScalar());
                }
            }
            return believerCount;
        }
        public static Believer SelectBelieverFromSQL(int believerID)
        {
            using (var connection = new SqlConnection(connectionString))
            {                
                string query = "SELECT BelieverID, BelieverNumber, DharmaName, Name, AddressProvince, AddressDetail, Phone, " +
                    "Gender, BelongedTemple, Ganzhi, DateOfBirth, DateOfRegister, Remarks, Email, FamilyID, FamilyMemberID " +
                    "FROM Believer WHERE BelieverID = @believerId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@believerId", believerID);
                    command.CommandType = CommandType.Text;

                    var believer = new Believer();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            believer.UID = reader.GetInt32(0);
                            believer.BelieverNumber = (!reader.IsDBNull(1)) ? reader.GetString(1) : "";
                            believer.DharmaName = (!reader.IsDBNull(2)) ? reader.GetString(2) : "";
                            believer.Name = (!reader.IsDBNull(3)) ? reader.GetString(3) : "";
                            believer.AddressProvince = (!reader.IsDBNull(4)) ? (Province)reader.GetInt16(4) : Province.미지정;
                            believer.AddressDetail = (!reader.IsDBNull(5)) ? reader.GetString(5) : "";
                            believer.Phone = (!reader.IsDBNull(6)) ? reader.GetString(6) : "";
                            believer.bGender = (!reader.IsDBNull(7)) ? reader.GetBoolean(7) : false;
                            believer.BelongedTemple = (!reader.IsDBNull(8)) ? (LocalTemple)reader.GetInt16(8) : LocalTemple.미지정;
                            believer.Ganzhi = (!reader.IsDBNull(9)) ? reader.GetString(9) : "";
                            believer.BirthDate = (!reader.IsDBNull(10)) ? reader.GetDateTime(10).ToShortDateString() : "";
                            believer.RegisteredDate = (!reader.IsDBNull(11)) ? reader.GetDateTime(11).ToShortDateString() : "";
                            believer.Remarks = (!reader.IsDBNull(12)) ? reader.GetString(12) : "";
                            believer.Email = (!reader.IsDBNull(13)) ? reader.GetString(13) : "";
                            believer.FamilyID = (!reader.IsDBNull(14)) ? reader.GetInt32(14) : 0;
                            believer.FamilyMemberID = (!reader.IsDBNull(15)) ? reader.GetInt32(15) : 0;
                            return believer;
                        }
                        else
                        {
                            MessageBox.Show("신도 ID가 존재하지 않습니다.");
                        }
                    }
                }
            }
            return null;
        }

        public static Believer SelectBelieverFromSQL(string name, string phone)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT BelieverID, BelieverNumber, DharmaName, Name, AddressProvince, AddressDetail, Phone, " +
                    "Gender, BelongedTemple, Ganzhi, DateOfBirth, DateOfRegister, Remarks, Email, FamilyID, FamilyMemberID " +
                    "FROM Believer WHERE Name = @name AND Phone = @phone ";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@name", name);
                    command.Parameters.AddWithValue("@phone", phone);
                    command.CommandType = CommandType.Text;

                    var believer = new Believer();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            believer.UID = reader.GetInt32(0);
                            believer.BelieverNumber = (!reader.IsDBNull(1)) ? reader.GetString(1) : "";
                            believer.DharmaName = (!reader.IsDBNull(2)) ? reader.GetString(2) : "";
                            believer.Name = (!reader.IsDBNull(3)) ? reader.GetString(3) : "";
                            believer.AddressProvince = (!reader.IsDBNull(4)) ? (Province)reader.GetInt16(4) : Province.미지정;
                            believer.AddressDetail = (!reader.IsDBNull(5)) ? reader.GetString(5) : "";
                            believer.Phone = (!reader.IsDBNull(6)) ? reader.GetString(6) : "";
                            believer.bGender = (!reader.IsDBNull(7)) ? reader.GetBoolean(7) : false;
                            believer.BelongedTemple = (!reader.IsDBNull(8)) ? (LocalTemple)reader.GetInt16(8) : LocalTemple.미지정;
                            believer.Ganzhi = (!reader.IsDBNull(9)) ? reader.GetString(9) : "";
                            believer.BirthDate = (!reader.IsDBNull(10)) ? reader.GetDateTime(10).ToShortDateString() : "";
                            believer.RegisteredDate = (!reader.IsDBNull(11)) ? reader.GetDateTime(11).ToShortDateString() : "";
                            believer.Remarks = (!reader.IsDBNull(12)) ? reader.GetString(12) : "";
                            believer.Email = (!reader.IsDBNull(13)) ? reader.GetString(13) : "";
                            believer.FamilyID = (!reader.IsDBNull(14)) ? reader.GetInt32(14) : 0;
                            believer.FamilyMemberID = (!reader.IsDBNull(15)) ? reader.GetInt32(15) : 0;
                            return believer;
                        }                        
                    }
                }
            }
            return null;
        }

        public static bool InsertBelieverToSQL(Believer believer)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {                
                string query = "INSERT INTO Believer (BelieverNumber, DharmaName, Name, AddressProvince, AddressDetail, Phone, " +
                    "Gender, BelongedTemple, Ganzhi, DateOfBirth, DateOfRegister, Remarks, Email, FamilyID, FamilyMemberID) " +
                    "VALUES (@believernumber, @dharmaname, @name, @province, @address, @phone, @gender, @belongedtemple, " +
                    "@ganzhi, @dateofbirth, @dateofregister, @remarks, @email, @familyId, @familyMemberId)";

                using (var command = new SqlCommand(query, connection))
                {
                    if (string.IsNullOrWhiteSpace(believer.BelieverNumber))
                        command.Parameters.AddWithValue("@believernumber", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@believernumber", believer.BelieverNumber);
                    if (string.IsNullOrWhiteSpace(believer.DharmaName))
                        command.Parameters.AddWithValue("@dharmaname", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dharmaname", believer.DharmaName);
                    command.Parameters.AddWithValue("@name", believer.Name);
                    command.Parameters.AddWithValue("@province", (int)believer.AddressProvince);
                    command.Parameters.AddWithValue("@address", believer.AddressDetail);
                    command.Parameters.AddWithValue("@phone", believer.Phone);
                    command.Parameters.AddWithValue("@gender", believer.bGender);
                    command.Parameters.AddWithValue("@belongedtemple", (int)believer.BelongedTemple);
                    command.Parameters.AddWithValue("@ganzhi", believer.Ganzhi);
                    if(string.IsNullOrWhiteSpace(believer.BirthDate))
                        command.Parameters.AddWithValue("@dateofbirth", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofbirth", believer.BirthDate);
                    if (string.IsNullOrWhiteSpace(believer.RegisteredDate))
                        command.Parameters.AddWithValue("@dateofregister", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofregister", believer.RegisteredDate);
                    command.Parameters.AddWithValue("@remarks", believer.Remarks);
                    if (string.IsNullOrWhiteSpace(believer.Email))
                        command.Parameters.AddWithValue("@email", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@email", believer.Email);
                    if (believer.FamilyID == 0)
                    {
                        command.Parameters.AddWithValue("@familyId", DBNull.Value);
                        command.Parameters.AddWithValue("@familyMemberId", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@familyId", believer.FamilyID);
                        command.Parameters.AddWithValue("@familyMemberId", believer.FamilyMemberID);
                    }                        

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("새로운 신도 정보를 입력하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool UpdateBelieverInSQL(Believer believer)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Believer SET Name = @name, AddressProvince = @province, AddressDetail = @address, Phone = @phone, " +
                    "Gender = @gender, BelongedTemple = @belongedtemple, DateOfBirth = @dateofbirth, DateOfRegister = @dateofregister, Remarks = @remarks, " +
                    "BelieverNumber = @believernumber, Ganzhi = @ganzhi, DharmaName = @dharmaname, Email = @email " +
                    "WHERE BelieverID = @believerId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@name", believer.Name);
                    command.Parameters.AddWithValue("@province", (int)believer.AddressProvince);
                    command.Parameters.AddWithValue("@address", believer.AddressDetail);
                    command.Parameters.AddWithValue("@phone", believer.Phone);
                    command.Parameters.AddWithValue("@gender", believer.bGender);
                    command.Parameters.AddWithValue("@belongedtemple", (int)believer.BelongedTemple);
                    if (string.IsNullOrWhiteSpace(believer.BirthDate))
                        command.Parameters.AddWithValue("@dateofbirth", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofbirth", believer.BirthDate);
                    if (string.IsNullOrWhiteSpace(believer.RegisteredDate))
                        command.Parameters.AddWithValue("@dateofregister", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofregister", believer.RegisteredDate);
                    command.Parameters.AddWithValue("@remarks", believer.Remarks);

                    command.Parameters.AddWithValue("@believernumber", believer.BelieverNumber);
                    command.Parameters.AddWithValue("@dharmaname", believer.DharmaName);
                    command.Parameters.AddWithValue("@ganzhi", believer.Ganzhi);
                    command.Parameters.AddWithValue("@email", believer.Email);

                    command.Parameters.AddWithValue("@believerId", believer.UID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(believer.Name + "님의 신도 정보를 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;                        
                        WriteLogOnSQL(command);
                    }
                        
                }
            }
            return bResult;
        }

        //해당 ID에 해당하는 record가 있으면 Update를 하고, 그렇지 않다면 Insert를 한다.
        public static bool UpdateOrInsertBelieverInSQL(Believer believer)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Believer SET Name = @name, AddressProvince = @province, AddressDetail = @address, Phone = @phone, " +
                    "Gender = @gender, BelongedTemple = @belongedtemple, DateOfBirth = @dateofbirth, DateOfRegister = @dateofregister, Remarks = @remarks, " +
                    "BelieverNumber = @believernumber, Ganzhi = @ganzhi, DharmaName = @dharmaname, Email = @email " +
                    "WHERE BelieverID = @believerId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@name", believer.Name);
                    command.Parameters.AddWithValue("@province", (int)believer.AddressProvince);
                    command.Parameters.AddWithValue("@address", believer.AddressDetail);
                    command.Parameters.AddWithValue("@phone", believer.Phone);
                    command.Parameters.AddWithValue("@gender", believer.bGender);
                    command.Parameters.AddWithValue("@belongedtemple", (int)believer.BelongedTemple);
                    if (string.IsNullOrWhiteSpace(believer.BirthDate))
                        command.Parameters.AddWithValue("@dateofbirth", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofbirth", believer.BirthDate);
                    if (string.IsNullOrWhiteSpace(believer.RegisteredDate))
                        command.Parameters.AddWithValue("@dateofregister", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@dateofregister", believer.RegisteredDate);
                    command.Parameters.AddWithValue("@remarks", believer.Remarks);

                    command.Parameters.AddWithValue("@believernumber", believer.BelieverNumber);
                    command.Parameters.AddWithValue("@dharmaname", believer.DharmaName);
                    command.Parameters.AddWithValue("@ganzhi", believer.Ganzhi);
                    command.Parameters.AddWithValue("@email", believer.Email);

                    command.Parameters.AddWithValue("@believerId", believer.UID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result <= 0)
                        bResult = InsertBelieverToSQL(believer);
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool DeleteBelieverInSQL(int believerID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "DELETE FROM Believer WHERE BelieverID = @believerId";
                using (var command = new SqlCommand(query, connection))
                {                    
                    command.Parameters.AddWithValue("@believerId", believerID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(believerID + " id의 신도 정보를 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        //이 함수를 사용하면 모든 Believer 데이터가 삭제됩니다. 
        //그런데 그럼 연결된 데이터 처리가 곤란하니, 당분간 사용하지 않는 걸로 합니다 - 2019.9.5 목요일
        public static bool ResetBelieverTableInSQL()
        {
            //At here, Believer Table and Seed Reset
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                // Start a local transaction.
                SqlTransaction sqlTran = connection.BeginTransaction();

                // Enlist a command in the current transaction.
                SqlCommand command = connection.CreateCommand();
                command.Transaction = sqlTran;

                try
                {
                    //TODO : Related Tables must be reset, and need to be backed up to preserve the data.
                    // Execute two separate commands.
                    command.CommandText = "DELETE FROM Believer";
                    command.ExecuteNonQuery();
                    command.CommandText = "DBCC CHECKIDENT ('Believer', RESEED, 0)";
                    command.ExecuteNonQuery();
                    
                    // Commit the transaction.
                    sqlTran.Commit();
                    bResult = true;
                }
                catch (Exception ex)
                {
                    // Handle the exception if the transaction fails to commit.                    
                    bResult = false;
                    string failMessage = "Believer 테이블을 Reset하는데 실패하였습니다. " + ex.Message;
                    try
                    {   
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        // Throws an InvalidOperationException if the connection is closed or the transaction has already been rolled back on the server.
                        failMessage += exRollback.Message;                        
                    }
                    MessageBox.Show(failMessage);
                }
            }
            return bResult;
        }
        #endregion

        #region Ceremony
        public static Ceremony SelectCeremonyFromSQL(int ceremonyID)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT C.CeremonyID, C.CeremonyName, A.AssemblyName, C.Date, T.TempleName, Preacher, StartTime, EndTime, C.Remarks " +
                    "FROM Ceremony AS C " +
                    "INNER JOIN Assembly AS A ON C.AssemblyID = A.AssemblyID " +
                    "INNER JOIN Temples AS T ON C.Temple = T.TempleID " +
                    "WHERE C.CeremonyID = @ceremonyId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@ceremonyId", ceremonyID);
                    command.CommandType = CommandType.Text;

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row
                            var ceremony = new Ceremony();
                            ceremony.UID = reader.GetInt32(0);
                            ceremony.Name = reader.GetString(1);
                            ceremony.AssemblyCategory = (AssemblyClass)Enum.Parse(typeof(AssemblyClass), reader.GetString(2));
                            ceremony.Date = reader.GetDateTime(3);
                            ceremony.Temple = (LocalTemple)Enum.Parse(typeof(LocalTemple), reader.GetString(4));
                            ceremony.Preacher = (!reader.IsDBNull(5)) ? reader.GetString(5) : string.Empty;
                            ceremony.StartTime = reader.GetTimeSpan(6).ToString();
                            ceremony.EndTime = reader.GetTimeSpan(7).ToString();
                            ceremony.Remarks = (!reader.IsDBNull(8)) ? reader.GetString(8) : string.Empty;
                            return ceremony;
                        }
                        else
                        {
                            MessageBox.Show(string.Format("ID {0} 번에 해당하는 일정이 존재하지 않습니다.", ceremonyID));
                        }
                    }
                }
            }
            return null;
        }
        public static DataTable SelectCeremonyDataTableFromSQL(DateTime dateBegin, DateTime dateEnd)
        {   
            string query = "SELECT C.CeremonyID ,C.CeremonyName, A.AssemblyName, C.Date, T.TempleName, Preacher, " +
                    "StartTime, EndTime, C.Remarks " +
                    "FROM Ceremony AS C " +
                    "INNER JOIN Assembly AS A ON C.AssemblyID = A.AssemblyID " +
                    "INNER JOIN Temples AS T ON C.Temple = T.TempleID " +
                    "WHERE C.Date BETWEEN '" + dateBegin.ToShortDateString() + "' AND '" + dateEnd.ToShortDateString() + "'";
            return GetDataTableFromSQL(query);
        }
        public static List<Ceremony> SelectCeremonyListFromSQL(DateTime dateTime)
        {            
            var ceremonyList = new List<Ceremony>();
            using (var connection = new SqlConnection(connectionString))
            {
                DateTime dateBegin = new DateTime(dateTime.Year, dateTime.Month, 1);
                int endDay = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
                DateTime dateEnd = new DateTime(dateTime.Year, dateTime.Month, endDay);
                
                string query = "SELECT C.CeremonyID, C.CeremonyName, A.AssemblyName, C.Date, T.TempleName, Preacher, " +
                    "StartTime, EndTime, C.Remarks " +
                    "FROM Ceremony AS C " +
                    "INNER JOIN Assembly AS A ON C.AssemblyID = A.AssemblyID " +
                    "INNER JOIN Temples AS T ON C.Temple = T.TempleID " +
                    "WHERE C.Date BETWEEN @dateBegin AND @dateEnd ";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@dateBegin", dateBegin.ToShortDateString());
                    command.Parameters.AddWithValue("@dateEnd", dateEnd.ToShortDateString());
                    command.CommandType = CommandType.Text;
                    
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var ceremony = new Ceremony();
                                ceremony.UID = reader.GetInt32(0);
                                ceremony.Name = reader.GetString(1);
                                ceremony.AssemblyCategory = (AssemblyClass)Enum.Parse(typeof(AssemblyClass), reader.GetString(2));
                                ceremony.Date = reader.GetDateTime(3);
                                ceremony.Temple = (LocalTemple)Enum.Parse(typeof(LocalTemple), reader.GetString(4));
                                ceremony.Preacher = (!reader.IsDBNull(5))? reader.GetString(5): string.Empty;                                 
                                ceremony.StartTime = reader.GetTimeSpan(6).ToString();
                                ceremony.EndTime = reader.GetTimeSpan(7).ToString();
                                ceremony.Remarks = (!reader.IsDBNull(8)) ? reader.GetString(8) : string.Empty;

                                //Console.WriteLine("Added ceremony is {0} and Date is {1}!! ", ceremony.Name, ceremony.Date);
                                ceremonyList.Add(ceremony);
                            }                            
                        }                        
                    }
                }
            }
            return ceremonyList;
        }
        public static bool InsertCeremonyToSQL(Ceremony ceremony)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO Ceremony(CeremonyName, AssemblyID, Date, Temple, StartTime, EndTime, Remarks,Preacher ) " +
                    "VALUES(@ceremonyName, @assemblyId, @date, @templeId, @startTime, @endTime, @remarks,@preacher)";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ceremonyName", ceremony.Name);
                    command.Parameters.AddWithValue("@assemblyId", (Byte)ceremony.AssemblyCategory);
                    command.Parameters.AddWithValue("@date", ceremony.Date);
                    command.Parameters.AddWithValue("@templeId", (int)ceremony.Temple);
                    command.Parameters.AddWithValue("@startTime", ceremony.StartTime);
                    command.Parameters.AddWithValue("@endTime", ceremony.EndTime);                    
                     
                    if(string.IsNullOrWhiteSpace(ceremony.Preacher))
                        command.Parameters.AddWithValue("@preacher", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@preacher", ceremony.Preacher);

                    if (string.IsNullOrWhiteSpace(ceremony.Remarks))
                        command.Parameters.AddWithValue("@remarks", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@remarks", ceremony.Remarks);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("새로운 일정을 입력하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool UpdateCeremonyInSQL(Ceremony ceremony)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {                
                string query = "UPDATE Ceremony SET CeremonyName = @ceremonyName, AssemblyID = @assemblyId, Date = @date, " +
                    "Temple = @templeId, StartTime = @startTime, EndTime = @endTime, Remarks = @remarks, Preacher = @preacher " +
                    "WHERE CeremonyID = @ceremonyId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ceremonyId", ceremony.UID);
                    command.Parameters.AddWithValue("@ceremonyName", ceremony.Name);
                    command.Parameters.AddWithValue("@assemblyId", (Byte)ceremony.AssemblyCategory);
                    command.Parameters.AddWithValue("@date", ceremony.Date);
                    command.Parameters.AddWithValue("@templeId", (int)ceremony.Temple);
                    command.Parameters.AddWithValue("@startTime", ceremony.StartTime);
                    command.Parameters.AddWithValue("@endTime", ceremony.EndTime);
                    if (ceremony.Remarks == null)
                        ceremony.Remarks = string.Empty;
                    command.Parameters.AddWithValue("@remarks", ceremony.Remarks);
                    if (ceremony.Preacher == null)
                        ceremony.Preacher = string.Empty;
                    command.Parameters.AddWithValue("@preacher", ceremony.Preacher);
                    
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(ceremony.Name + " 일정을 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool DeleteCeremonyInSQL(int ceremonyID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "DELETE FROM Ceremony WHERE CeremonyID = @ceremonyId";
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@ceremonyId", ceremonyID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(ceremonyID + " id의 일정을 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        #endregion

        #region Pray
        public static DataTable SelectPrayListFromSQL(params string[] searchCriteria)
        {
            string query = "SELECT PrayID, P.PrayPaperNumber as 원장번호, T.TempleName as 기도분원, B.buddha as 부처님, " +
                "A.FamilyID as 가족번호, P.PrayerRelation as 관계, P.PrayerBirthYear as 생년, " +
                "P.PrayerName as 입재자, P.donator as 입금자, P.PrayerPhone as 연락처, " +
                "P.DateOfStart as 입재일, P.DateOfFinish as 회향일, P.DateOfFullPay as 완납일, P.IsFinishConfirmed as 회향확정, " +
                "P.Remarks as 비고, P.PrayerAddress as 주소, " +
                "(SELECT TOP 1 Request FROM PrayRequestHistory as R WHERE P.PrayID = R.PrayID ORDER BY R.DateOfRequest DESC) as 발원, " +
                "P.IsPrayFinished as 회향여부 " +
                "FROM Pray as P " +
                "LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID " +
                "INNER JOIN Buddhas as B ON P.buddha = B.BuddhaID " +
                "INNER JOIN Temples as T ON P.PrayTempleID = TempleID " +
                "LEFT OUTER JOIN BelieverFamily as F ON P.FamilyMemberID = F.MemberID ";
                
            int conditionNumber = searchCriteria.Length;
            if (conditionNumber > 0)
            {
                string searchCondition = " WHERE ";
                for (int i = 0; i < conditionNumber; i++)
                {
                    string seperator = (i == 0) ? "" : " AND ";
                    searchCondition += (seperator + searchCriteria[i]);
                }
                query += searchCondition;
            }
            string orderCriteria = " ORDER BY PrayPaperNumber, 가족번호";
            query += orderCriteria;
            return GetDataTableFromSQL(query);
        }
        public static Pray SelectPrayFromSQL(int prayID)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT PrayID, PrayTempleID, buddha, believer, FamilyMemberID, " +
                    "PrayerName, PrayerBirthYear, PrayerAddress, PrayerPhone, donator, " +
                    "DateOfStart, DateOfFinish, DateOfFullPay, IsFinishConfirmed, Remarks, IsPrayFinished, " +
                    "PrayPaperNumber, PrayerRelation " +
                    "FROM Pray WHERE PrayID = @prayId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@prayId", prayID);
                    command.CommandType = CommandType.Text;

                    var pray = new Pray();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row
                            pray.UID = reader.GetInt32(0);
                            pray.PrayTemple = (LocalTemple)reader.GetInt16(1);
                            pray.buddha = (MajestyBuddha)reader.GetByte(2);
                            pray.believerUID = (!reader.IsDBNull(3)) ? reader.GetInt32(3): 0;
                            pray.FamilyMemberUID = (!reader.IsDBNull(4)) ? reader.GetInt32(4) : 0;

                            pray.PrayerName = (!reader.IsDBNull(5)) ? reader.GetString(5) : string.Empty;
                            pray.PrayerBirthYear = (!reader.IsDBNull(6)) ? reader.GetString(6) : string.Empty;
                            pray.PrayerAddress = (!reader.IsDBNull(7)) ? reader.GetString(7) : string.Empty;
                            pray.PrayerPhone = (!reader.IsDBNull(8)) ? reader.GetString(8) : string.Empty;
                            pray.Donator = (!reader.IsDBNull(9)) ? reader.GetString(9) : string.Empty;

                            pray.DateOfStart = reader.GetDateTime(10);
                            pray.DateOfFinish = (!reader.IsDBNull(11)) ? reader.GetDateTime(11): new DateTime(2500,12,31);
                            pray.DateOfFullPay = (!reader.IsDBNull(12)) ? reader.GetDateTime(12) : pray.DateOfStart;
                            pray.IsFinishConfirmed = reader.GetBoolean(13);
                            pray.Remarks = (!reader.IsDBNull(14)) ? reader.GetString(14):string.Empty;
                            pray.IsPrayFinished = reader.GetBoolean(15);
                            pray.PrayPaperNumber = reader.GetInt32(16);
                            pray.PrayerRelation = (!reader.IsDBNull(17)) ? reader.GetString(17) : string.Empty;
                            return pray;
                        }
                        else
                        {
                            MessageBox.Show("다음 ID에 해당하는 기도 정보가 존재하지 않습니다 : ID " + prayID);
                        }
                    }
                }
            }
            return null;
        }
        public static bool InsertPrayToSQL(Pray pray, out int insertedID)
        {
            bool bResult = false;
            insertedID = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO Pray (buddha, PrayTempleID, believer, FamilyMemberID, " +
                    "PrayerName, PrayerBirthYear, PrayerAddress, PrayerPhone, donator, " +
                    "DateOfStart, DateOfFinish, DateOfFullPay, IsFinishConfirmed, Remarks, IsPrayFinished, PrayPaperNumber, PrayerRelation) " +
                    "OUTPUT INSERTED.PrayID " +
                    "VALUES(@buddha, @prayTemple, @believerId, @familyMemberId," +
                    "@prayerName, @prayerBirthYear, @prayerAddress, @prayerPhone, @donator," +
                    "@startDate, @finishDate, @fullpayDate, @isFinishConfirmed, @remarks, @isPrayFinished, @prayPaperNumber, @prayerRelation) ";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@buddha", (Byte)pray.buddha);
                    command.Parameters.AddWithValue("@prayTemple", (Int16)pray.PrayTemple);
                    if (pray.believerUID == 0)
                        command.Parameters.AddWithValue("@believerId", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@believerId", pray.believerUID);
                    if (pray.FamilyMemberUID == 0)
                        command.Parameters.AddWithValue("@familyMemberId", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@familyMemberId", pray.FamilyMemberUID);

                    command.Parameters.AddWithValue("@prayerName", pray.PrayerName);
                    command.Parameters.AddWithValue("@prayerBirthYear", pray.PrayerBirthYear);
                    command.Parameters.AddWithValue("@prayerAddress", pray.PrayerAddress);
                    command.Parameters.AddWithValue("@prayerPhone", pray.PrayerPhone);
                    command.Parameters.AddWithValue("@donator", pray.Donator);
                    
                    command.Parameters.AddWithValue("@startDate", pray.DateOfStart.ToShortDateString());
                    if( pray.DateOfFinish.Year == 2500 )
                        command.Parameters.AddWithValue("@finishDate", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@finishDate", pray.DateOfFinish.ToShortDateString());
                    command.Parameters.AddWithValue("@fullpayDate", pray.DateOfFullPay.ToShortDateString());
                    command.Parameters.AddWithValue("@isFinishConfirmed", pray.IsFinishConfirmed);

                    command.Parameters.AddWithValue("@remarks", pray.Remarks);
                    command.Parameters.AddWithValue("@isPrayFinished", pray.IsPrayFinished);
                    command.Parameters.AddWithValue("@prayPaperNumber", pray.PrayPaperNumber);                    
                    command.Parameters.AddWithValue("@prayerRelation", pray.PrayerRelation);

                    connection.Open();
                    try
                    {
                        insertedID = (int)command.ExecuteScalar();
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                    catch { MessageBox.Show("새로운 기도 정보를 입력하는데 실패했습니다!"); }                        
                }
            }
            return bResult;
        }
        public static bool UpdatePrayInSQL(Pray pray)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Pray SET buddha = @buddha, PrayTempleID = @prayTemple, " +
                    "believer = @believerId, FamilyMemberID = @familyMemberId, " +
                    "PrayerName = @prayerName, PrayerBirthYear = @prayerBirthYear, PrayerAddress = @prayerAddress, " +
                    "PrayerPhone = @prayerPhone, donator = @donator, " +
                    "DateOfStart = @startDate, DateOfFinish = @finishDate, DateOfFullPay = @fullpayDate," +
                    "IsFinishConfirmed = @isFinishConfirmed, Remarks = @remarks, IsPrayFinished = @isPrayFinished, " +
                    "PrayPaperNumber = @prayPaperNumber, PrayerRelation = @prayerRelation " +
                    "WHERE PrayID = @prayId ";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@prayId", pray.UID);
                    command.Parameters.AddWithValue("@buddha", (Byte)pray.buddha);
                    command.Parameters.AddWithValue("@prayTemple", (Int16)pray.PrayTemple);
                    if (pray.believerUID == 0)
                        command.Parameters.AddWithValue("@believerId", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@believerId", pray.believerUID);
                    if (pray.FamilyMemberUID == 0)
                        command.Parameters.AddWithValue("@familyMemberId", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@familyMemberId", pray.FamilyMemberUID);

                    command.Parameters.AddWithValue("@prayerName", pray.PrayerName);
                    command.Parameters.AddWithValue("@prayerBirthYear", pray.PrayerBirthYear);
                    command.Parameters.AddWithValue("@prayerAddress", pray.PrayerAddress);
                    command.Parameters.AddWithValue("@prayerPhone", pray.PrayerPhone);
                    command.Parameters.AddWithValue("@donator", pray.Donator);

                    command.Parameters.AddWithValue("@startDate", pray.DateOfStart.ToShortDateString());
                    if (pray.DateOfFinish == new DateTime(2500, 12, 31))
                        command.Parameters.AddWithValue("@finishDate", DBNull.Value);
                    else
                        command.Parameters.AddWithValue("@finishDate", pray.DateOfFinish.ToShortDateString());
                    command.Parameters.AddWithValue("@fullpayDate", pray.DateOfFullPay.ToShortDateString());
                    command.Parameters.AddWithValue("@isFinishConfirmed", pray.IsFinishConfirmed);

                    command.Parameters.AddWithValue("@remarks", pray.Remarks);
                    command.Parameters.AddWithValue("@isPrayFinished", pray.IsPrayFinished);
                    command.Parameters.AddWithValue("@prayPaperNumber", pray.PrayPaperNumber);
                    command.Parameters.AddWithValue("@prayerRelation", pray.PrayerRelation);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(pray.UID + " ID의 기도 정보를 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool DeletePrayInSQL(int prayID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "DELETE FROM PrayDonationHistory WHERE PrayID = @prayId " +
                    " DELETE FROM PrayRequestHistory WHERE PrayID = @prayId" +
                    " DELETE FROM Pray WHERE PrayID = @prayId";
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@prayId", prayID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(prayID + " id의 기도 정보를 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        #endregion

        #region Offering
        public static DataTable SelectOfferingListFromSQL(params string[] searchCriteria)
        {
            string query = "SELECT O.OfferingID, T.TempleName as 공양분원, B.buddha as 부처님, " +
                "L.FamilyID as 가족번호, L.Name as 신청자, OfferorRelation as 관계, " +
                "OfferorName as 복위자, OfferorBirthYear as 생년, OfferorAddress as 주소, OfferorPhone as 연락처, donator as 입금자, " +
                "DateOfOffer as 공양일, Request as 발원 " + //C.CeremonyName as 해당일정,                 
                "FROM Offering AS O " +
                "INNER JOIN Buddhas AS B ON O.buddha = B.BuddhaID " +                
                "INNER JOIN Temples as T ON O.OfferingTempleID = T.TempleID " + 
                "LEFT OUTER JOIN Believer AS L ON O.believer = L.BelieverID ";
                //+ "LEFT OUTER JOIN Ceremony AS C ON C.CeremonyID = O.CeremonyID";

            int conditionNumber = searchCriteria.Length;
            if (conditionNumber > 0)
            {
                string searchCondition = " WHERE ";
                for (int i = 0; i < conditionNumber; i++)
                {
                    string seperator = (i == 0) ? "" : " AND ";
                    searchCondition += (seperator + searchCriteria[i]);
                }
                query += searchCondition;
            }
            string orderCriteria = " ORDER BY DateOfOffer DESC";
            query += orderCriteria;
            return GetDataTableFromSQL(query);
        }
        public static Offering SelectOfferingFromSQL(int offeringID)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT OfferingID, buddha, OfferingTempleID, DateOfOffer, " +
                    "believer, OfferorRelation, FamilyMemberID, " +
                    "OfferorName, OfferorBirthYear, OfferorAddress, OfferorPhone, donator, " +
                    "Request, Participants, Remarks " + //CeremonyID " +
                    "FROM Offering WHERE OfferingID = @offeringId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@offeringId", offeringID);
                    command.CommandType = CommandType.Text;

                    var offering = new Offering();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            offering.UID = reader.GetInt32(0);
                            offering.buddha = (MajestyBuddha)reader.GetByte(1);
                            offering.OfferingTemple = (LocalTemple)reader.GetInt16(2);
                            offering.DateOfOffer = reader.GetDateTime(3);
                            offering.believerUID = (!reader.IsDBNull(4)) ? reader.GetInt32(4) : 0;
                            offering.OfferorRelation = (!reader.IsDBNull(5)) ? reader.GetString(5) : string.Empty;
                            offering.FamilyMemberUID = (!reader.IsDBNull(6)) ? reader.GetInt32(6) : 0;
                            offering.OfferorName = (!reader.IsDBNull(7)) ? reader.GetString(7) : string.Empty;
                            offering.OfferorBirthYear = (!reader.IsDBNull(8)) ? reader.GetString(8) : string.Empty;
                            offering.OfferorAddress = (!reader.IsDBNull(9)) ? reader.GetString(9) : string.Empty;
                            offering.OfferorPhone = (!reader.IsDBNull(10)) ? reader.GetString(10) : string.Empty;
                            offering.Donator = (!reader.IsDBNull(11)) ? reader.GetString(11) : string.Empty;
                            offering.Request = reader.GetString(12);
                            offering.Participants = (!reader.IsDBNull(13)) ? reader.GetString(13): string.Empty;
                            offering.Remarks = (!reader.IsDBNull(14)) ? reader.GetString(14) : string.Empty;

                            //offering.CeremonyID = (!reader.IsDBNull(6))? reader.GetInt32(6) : 0;                            
                            return offering;
                        }
                        else
                        {
                            MessageBox.Show("다음 ID에 해당하는 공양 정보가 존재하지 않습니다 : ID " + offeringID);
                        }
                    }
                }
            }
            return null;
        }
        public static bool InsertOfferingToSQL(Offering offering)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO Offering (buddha, OfferingTempleID, DateOfOffer, " +
                    "believer, FamilyMemberID, OfferorRelation, " +
                    "OfferorName, OfferorBirthYear, OfferorAddress, OfferorPhone, donator, " +
                    "Request, Participants, Remarks) " /* CeremonyID) "*/ +
                    "VALUES(@buddha, @offeringTemple, @offeringDate, " +
                    "@believerId, @familyMemberId, @offerorRelation, " +
                    "@offerorName, @offerorBirthYear, @offerorAddress, @offerorPhone, @donator, " +
                    "@request, @participants, @remarks)";//@ceremonyID)";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@buddha", (Byte)offering.buddha);
                    command.Parameters.AddWithValue("@offeringTemple", (Int16)offering.OfferingTemple);
                    command.Parameters.AddWithValue("@offeringDate", offering.DateOfOffer);
                    if (offering.believerUID != 0)
                        command.Parameters.AddWithValue("@believerId", offering.believerUID);
                    else
                        command.Parameters.AddWithValue("@believerId", DBNull.Value);

                    if (offering.FamilyMemberUID != 0)
                        command.Parameters.AddWithValue("@familyMemberId", offering.FamilyMemberUID);
                    else
                        command.Parameters.AddWithValue("@familyMemberId", DBNull.Value);
                    command.Parameters.AddWithValue("@offerorRelation", offering.OfferorRelation);

                    command.Parameters.AddWithValue("@offerorName", offering.OfferorName);
                    command.Parameters.AddWithValue("@offerorBirthYear", offering.OfferorBirthYear);
                    command.Parameters.AddWithValue("@offerorAddress", offering.OfferorAddress);
                    command.Parameters.AddWithValue("@offerorPhone", offering.OfferorPhone);
                    command.Parameters.AddWithValue("@donator", offering.Donator);

                    command.Parameters.AddWithValue("@request", offering.Request);
                    command.Parameters.AddWithValue("@participants", offering.Participants);
                    command.Parameters.AddWithValue("@remarks", offering.Remarks);
                    //if(offering.CeremonyID == 0)
                    //    command.Parameters.AddWithValue("@ceremonyID", DBNull.Value);
                    //else
                    //    command.Parameters.AddWithValue("@ceremonyID", offering.CeremonyID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("새로운 기도 정보를 입력하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool UpdateOfferingInSQL(Offering offering)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Offering SET buddha = @buddha, OfferingTempleID = @offeringTemple, DateOfOffer = @offeringDate, " +
                    "believer = @believerId, FamilyMemberID = @familyMemberId, OfferorRelation = @offerorRelation, " +
                    "OfferorName = @offerorName, OfferorBirthYear = @offerorBirthYear, OfferorAddress = @offerorAddress, " +
                    "OfferorPhone = @offerorPhone, Donator = @donator, " +
                    "Request = @request, Participants = @participants, Remarks = @remarks " + //CeremonyID = @ceremonyID " +
                    "WHERE OfferingID = @offeringId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@buddha", (Byte)offering.buddha);
                    command.Parameters.AddWithValue("@offeringTemple", (Int16)offering.OfferingTemple);
                    command.Parameters.AddWithValue("@offeringDate", offering.DateOfOffer);
                    if(offering.believerUID != 0)
                        command.Parameters.AddWithValue("@believerId", offering.believerUID);
                    else
                        command.Parameters.AddWithValue("@believerId", DBNull.Value);

                    if (offering.FamilyMemberUID != 0)
                        command.Parameters.AddWithValue("@familyMemberId", offering.FamilyMemberUID);
                    else
                        command.Parameters.AddWithValue("@familyMemberId", DBNull.Value);
                    command.Parameters.AddWithValue("@offerorRelation", offering.OfferorRelation);

                    command.Parameters.AddWithValue("@offerorName", offering.OfferorName);
                    command.Parameters.AddWithValue("@offerorBirthYear", offering.OfferorBirthYear);
                    command.Parameters.AddWithValue("@offerorAddress", offering.OfferorAddress);
                    command.Parameters.AddWithValue("@offerorPhone", offering.OfferorPhone);
                    command.Parameters.AddWithValue("@donator", offering.Donator);

                    command.Parameters.AddWithValue("@request", offering.Request);
                    command.Parameters.AddWithValue("@participants", offering.Participants);
                    command.Parameters.AddWithValue("@remarks", offering.Remarks);
                    //if(offering.CeremonyID != 0)
                    //    command.Parameters.AddWithValue("@ceremonyID", offering.CeremonyID);                    
                    //else
                    //    command.Parameters.AddWithValue("@ceremonyID", DBNull.Value);
                    command.Parameters.AddWithValue("@offeringId", offering.UID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(offering.UID + " ID의 기도 정보를 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool DeleteOfferingInSQL(int offeringID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "DELETE FROM Offering WHERE OfferingID = @offeringId";
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@offeringId", offeringID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(offeringID + " id의 공양 정보를 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        #endregion

        #region SalvationService
        public static DataTable SelectSalvationServiceListFromSQL(params string[] searchCriteria)
        {
            string query = "SELECT S.ServiceID, T.TempleName as 천도재분원, " +
                "B.FamilyID as 가족번호, B.Name AS 복위자, " +
                "B.Ganzhi as 생년, V.ProvinceName as 지역, B.AddressDetail as 주소, B.Phone as 휴대폰," +
                "S.DateOfService AS 천도일, S.Request AS 발원, S.Remarks AS 비고 " +
                "FROM SalvationService AS S " +
                "INNER JOIN Believer AS B ON S.believer = B.BelieverID " +
                "INNER JOIN Provinces as V ON B.AddressProvince = V.ProvinceID " +
                "INNER JOIN Temples as T ON S.ServiceTempleID = T.TempleID ";

            int conditionNumber = searchCriteria.Length;
            if (conditionNumber > 0)
            {
                string searchCondition = " WHERE ";
                for (int i = 0; i < conditionNumber; i++)
                {
                    string seperator = (i == 0) ? "" : " AND ";
                    searchCondition += (seperator + searchCriteria[i]);
                }
                query += searchCondition;
            }
            string orderCriteria = " ORDER BY DateOfService DESC";
            query += orderCriteria;            
            return GetDataTableFromSQL(query);
        }
        public static SalvationService SelectSalvationServiceFromSQL(int serviceID)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT serviceID, believer, ServiceTempleID, DateOfService, Request, Remarks " +
                    "FROM SalvationService WHERE ServiceID = @serviceId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@serviceId", serviceID);
                    command.CommandType = CommandType.Text;

                    var service = new SalvationService();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            service.UID = reader.GetInt32(0);                            
                            service.believerUID = reader.GetInt32(1);
                            service.ServiceTemple = (LocalTemple)reader.GetInt16(2);
                            service.DateOfService = reader.GetDateTime(3);
                            service.Request = (!reader.IsDBNull(4))? reader.GetString(4): string.Empty;
                            service.Remarks = (!reader.IsDBNull(5)) ? reader.GetString(5) : string.Empty;                            
                            return service;
                        }
                        else
                        {
                            MessageBox.Show("다음 ID에 해당하는 천도재 정보가 존재하지 않습니다 : ID " + serviceID);
                        }
                    }
                }
            }
            return null;
        }
        public static bool InsertSalvationServiceToSQL(SalvationService serviceInfo)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO SalvationService (believer, ServiceTempleID, DateOfService, Request, Remarks) " +
                    "VALUES(@believerId, @serviceTemple, @serviceDate, @request, @remarks)";

                using (var command = new SqlCommand(query, connection))
                {                    
                    command.Parameters.AddWithValue("@believerId", serviceInfo.believerUID);
                    command.Parameters.AddWithValue("@serviceTemple", (Int16)serviceInfo.ServiceTemple);
                    command.Parameters.AddWithValue("@serviceDate", serviceInfo.DateOfService);                                       
                    command.Parameters.AddWithValue("@request", serviceInfo.Request);
                    command.Parameters.AddWithValue("@remarks", serviceInfo.Remarks);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("새로운 천도재 정보를 입력하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool UpdateSalvationServiceInSQL(SalvationService serviceInfo)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE SalvationService SET believer = @believerId, ServiceTempleID = @serviceTemple, " +
                    "DateOfService = @serviceDate, Request = @request, Remarks = @remarks " +
                    "WHERE ServiceID = @serviceId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@believerId", serviceInfo.believerUID);
                    command.Parameters.AddWithValue("@serviceTemple", (Int16)serviceInfo.ServiceTemple);
                    command.Parameters.AddWithValue("@serviceDate", serviceInfo.DateOfService);                    
                    command.Parameters.AddWithValue("@request", serviceInfo.Request);
                    command.Parameters.AddWithValue("@remarks", serviceInfo.Remarks);
                    command.Parameters.AddWithValue("@serviceId", serviceInfo.UID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(serviceInfo.UID + " ID의 천도재 정보를 수정하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool DeleteSalvationServiceInSQL(int serviceID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "DELETE FROM SalvationService WHERE ServiceID = @serviceId";
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@serviceId", serviceID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(serviceID + " id의 천도재 정보를 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        #endregion

        #region Family
        
        public static int GetNewFamilyIDFromSQL()
        {
            int newFamilyID;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT MAX(FamilyID)+1 FROM BelieverFamily";
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    var newestID = command.ExecuteScalar();                    
                    newFamilyID = (newestID != DBNull.Value)? Convert.ToInt32(newestID) : 1;
                }
            }
            return newFamilyID;
        }
        public static int GetFamilyIDAmongBelieversInSQL(params int[] believersIdList)
        {
            int unionFamilyID;
            string idLists = "";
            for (int i = 0; i < believersIdList.Length; i++)
            {
                if (i > 0) idLists += ",";
                idLists += believersIdList[i].ToString();
            }
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT MAX(FamilyID) FROM Believer " +                    
                    "WHERE BelieverID IN(" + idLists + ")";
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    var maxFamilyID = command.ExecuteScalar();
                    if (maxFamilyID == null || maxFamilyID == DBNull.Value)
                        unionFamilyID = 0;
                    else
                        unionFamilyID = Convert.ToInt32(maxFamilyID);                    
                }
            }
            return (unionFamilyID == 0)? GetNewFamilyIDFromSQL(): unionFamilyID;
        }
        public static int GetFamilyIDFromSQL(int believerID)
        {
            int iFamilyId = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT FamilyID FROM Believer WHERE BelieverID = " + believerID.ToString();
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    try { iFamilyId = Convert.ToInt32(command.ExecuteScalar()); }
                    catch { iFamilyId = 0; }
                }
            }
            return iFamilyId;
        }
        public static bool SetNewFamilyMemberIDInSQL(int believerID, int newFamilyID, int newFamilyMemberID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Believer SET FamilyID = @familyId, FamilyMemberID = @memberId " +
                    "WHERE BelieverID = @believerId";

                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@familyId", newFamilyID);
                    command.Parameters.AddWithValue("@memberId", newFamilyMemberID);
                    command.Parameters.AddWithValue("@believerId", believerID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("BelieverID " + believerID + "의 FamilyMemberID를 Update하는데 실패하였습니다. ");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }

        public static DataTable SelectFamilyMembersFromSQL(int believerID)
        {
            string query = "SELECT F.MemberID, " +
                "cast (case when B.FamilyMemberID IS NULL then 0 else 1 end as bit) as 신도여부," +
                "F.Name as 이름, F.Relation as 관계, BirthDate as 생년, F.Ganzhi as 간지, F.Remarks as 비고 " +
                "FROM BelieverFamily as F " +
                "LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID " +
                "WHERE F.FamilyID = " +
                "(SELECT FamilyID FROM Believer WHERE BelieverID = "+ believerID.ToString() +") " +
                "ORDER BY 생년";

            return GetDataTableFromSQL(query);
        }

        public static FamilyMember SelectFamilyMemberFromSQL(int familyMemberID)
        {
            if (familyMemberID == 0) return null;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "SELECT F.MemberID, F.FamilyID, F.Relation, F.Name, F.BirthDate, F.Ganzhi, F.Remarks, " +
                    "cast (case when B.FamilyMemberID IS NULL then 0 else 1 end as bit) as 신도여부 " +  
                    "From BelieverFamily as F " +
                    "LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID " +
                    "WHERE MemberID = @memberId";

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@memberId", familyMemberID);
                    command.CommandType = CommandType.Text;

                    var memberInfo = new FamilyMember();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read(); // get the first row

                            memberInfo.MemberUID = reader.GetInt32(0);
                            memberInfo.FamilyID = reader.GetInt32(1);
                            memberInfo.FamilyRelation = reader.GetString(2);
                            memberInfo.Name = reader.GetString(3);
                            memberInfo.BirthDate = (!reader.IsDBNull(4))? reader.GetDateTime(4).ToShortDateString():string.Empty;
                            memberInfo.Ganzhi = reader.GetString(5);
                            memberInfo.Remarks = reader.GetString(6);
                            memberInfo.IsBeliever = reader.GetBoolean(7);
                            return memberInfo;
                        }
                        else
                        {
                            MessageBox.Show("가족멤버 ID가 존재하지 않습니다.");
                        }
                    }
                }
            }
            return null;
        }


        public static bool UpdateFamilyMemberInSQL(FamilyMember newMemberInfo)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE BelieverFamily " +
                        "SET Name = @name, Relation = @relation, BirthDate = @birthDate, Ganzhi = @ganzhi, Remarks = @remarks " +
                        "WHERE MemberID = @memberID";

                using (var command = new SqlCommand(query, connection))
                {                    
                    command.Parameters.AddWithValue("@name", newMemberInfo.Name);
                    command.Parameters.AddWithValue("@relation", newMemberInfo.FamilyRelation);                    
                    command.Parameters.AddWithValue("@birthDate", newMemberInfo.BirthDate);
                    command.Parameters.AddWithValue("@ganzhi", MainWindow.GetGanzhi(Convert.ToDateTime(newMemberInfo.BirthDate).Year));
                    command.Parameters.AddWithValue("@remarks", newMemberInfo.Remarks);
                    
                    command.Parameters.AddWithValue("@memberID", newMemberInfo.MemberUID);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show("Family Member " + newMemberInfo.Name + "의 정보를 Update하는데 실패하였습니다. ");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        public static bool InsertFamilyMemberInSQL(int familyId, FamilyMember newMemberInfo, out int insertedMemberID)
        {
            bool bResult = false;                        
            insertedMemberID = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO BelieverFamily (FamilyID, Relation, Name, BirthDate, Ganzhi, Remarks) "
                    + "OUTPUT INSERTED.MemberID "
                    + "VALUES(@familyId, @relation, @name, @birthDate, @ganzhi, @remarks)";

                //신도의 경우라면 Believer 테이블에서 FamilyMemberID field를 업데이트 해야 합니다!!!
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@familyId", familyId);
                    command.Parameters.AddWithValue("@relation", newMemberInfo.FamilyRelation);                    
                    command.Parameters.AddWithValue("@name", newMemberInfo.Name);                    
                    command.Parameters.AddWithValue("@birthDate", newMemberInfo.BirthDate);
                    command.Parameters.AddWithValue("@ganzhi", newMemberInfo.Ganzhi);                    
                    command.Parameters.AddWithValue("@remarks", newMemberInfo.Remarks);                    

                    connection.Open();
                    try
                    {   
                        insertedMemberID = Convert.ToInt32(command.ExecuteScalar());                        
                        bResult = true;
                        WriteLogOnSQL(command);                        
                    }
                    catch { MessageBox.Show("새로운 가족 구성원을 추가하는데 실패했습니다!"); }
                }
            }
            return bResult;
        }
        public static bool DeleteFamilyMemberInSQL(int memberID)
        {
            bool bResult = false;
            using (var connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Believer SET FamilyMemberId = NULL, FamilyID = NULL WHERE FamilyMemberId = @memberId ;" +
                    "DELETE FROM BelieverFamily WHERE MemberID = @memberId ";
                    
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@memberId", memberID);
                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    if (result < 0)
                        MessageBox.Show(memberID + " id의 가족원 정보를 삭제하는데 실패했습니다!");
                    else
                    {
                        bResult = true;
                        WriteLogOnSQL(command);
                    }
                }
            }
            return bResult;
        }
        #endregion
    }
}
