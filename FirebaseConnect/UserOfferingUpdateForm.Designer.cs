﻿namespace HyunjisaBeliever
{
    partial class UserOfferingUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxMajesty = new System.Windows.Forms.ComboBox();
            this.buttonBelieverSearch = new System.Windows.Forms.Button();
            this.dateTimePickerOfferingDay = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBelieverName = new System.Windows.Forms.TextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxRequest = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.comboBoxOfferingTemple = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxParticipants = new System.Windows.Forms.TextBox();
            this.comboBoxOfferorClass = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxOfferorPhone = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxOfferorAddress = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxOfferorBirthYear = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxOfferorName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxDonator = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonFamilySearch = new System.Windows.Forms.Button();
            this.textBoxOfferorRelation = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxFamilyMemberName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBoxMajesty
            // 
            this.comboBoxMajesty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMajesty.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxMajesty.FormattingEnabled = true;
            this.comboBoxMajesty.ItemHeight = 18;
            this.comboBoxMajesty.Location = new System.Drawing.Point(96, 56);
            this.comboBoxMajesty.Name = "comboBoxMajesty";
            this.comboBoxMajesty.Size = new System.Drawing.Size(219, 26);
            this.comboBoxMajesty.TabIndex = 141;
            // 
            // buttonBelieverSearch
            // 
            this.buttonBelieverSearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonBelieverSearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.FlatAppearance.BorderSize = 2;
            this.buttonBelieverSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBelieverSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonBelieverSearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonBelieverSearch.Location = new System.Drawing.Point(406, 53);
            this.buttonBelieverSearch.Name = "buttonBelieverSearch";
            this.buttonBelieverSearch.Size = new System.Drawing.Size(82, 26);
            this.buttonBelieverSearch.TabIndex = 140;
            this.buttonBelieverSearch.Text = "신도검색";
            this.buttonBelieverSearch.UseVisualStyleBackColor = false;
            this.buttonBelieverSearch.Click += new System.EventHandler(this.ButtonBelieverSearch_Click);
            // 
            // dateTimePickerOfferingDay
            // 
            this.dateTimePickerOfferingDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePickerOfferingDay.Location = new System.Drawing.Point(96, 127);
            this.dateTimePickerOfferingDay.MaxDate = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerOfferingDay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerOfferingDay.Name = "dateTimePickerOfferingDay";
            this.dateTimePickerOfferingDay.Size = new System.Drawing.Size(219, 24);
            this.dateTimePickerOfferingDay.TabIndex = 137;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(12, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 132;
            this.label5.Text = "공양일";
            // 
            // textBoxBelieverName
            // 
            this.textBoxBelieverName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxBelieverName.Location = new System.Drawing.Point(494, 53);
            this.textBoxBelieverName.Name = "textBoxBelieverName";
            this.textBoxBelieverName.ReadOnly = true;
            this.textBoxBelieverName.Size = new System.Drawing.Size(135, 24);
            this.textBoxBelieverName.TabIndex = 131;
            // 
            // textBoxID
            // 
            this.textBoxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxID.Location = new System.Drawing.Point(96, 22);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(219, 24);
            this.textBoxID.TabIndex = 129;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(323, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 128;
            this.label3.Text = "신도 이름";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 126;
            this.label2.Text = "부처님";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 18);
            this.label1.TabIndex = 125;
            this.label1.Text = "ID";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 331);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 31);
            this.label9.TabIndex = 143;
            this.label9.Text = "발원";
            // 
            // textBoxRequest
            // 
            this.textBoxRequest.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxRequest.Location = new System.Drawing.Point(12, 365);
            this.textBoxRequest.Multiline = true;
            this.textBoxRequest.Name = "textBoxRequest";
            this.textBoxRequest.Size = new System.Drawing.Size(619, 278);
            this.textBoxRequest.TabIndex = 142;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonCancel.FlatAppearance.BorderSize = 2;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCancel.Location = new System.Drawing.Point(326, 661);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(116, 50);
            this.buttonCancel.TabIndex = 145;
            this.buttonCancel.Text = "취소";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.FlatAppearance.BorderSize = 2;
            this.buttonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonUpdate.ForeColor = System.Drawing.Color.Maroon;
            this.buttonUpdate.Location = new System.Drawing.Point(204, 661);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(116, 50);
            this.buttonUpdate.TabIndex = 144;
            this.buttonUpdate.Text = "확인";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // comboBoxOfferingTemple
            // 
            this.comboBoxOfferingTemple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOfferingTemple.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxOfferingTemple.FormattingEnabled = true;
            this.comboBoxOfferingTemple.ItemHeight = 18;
            this.comboBoxOfferingTemple.Location = new System.Drawing.Point(96, 92);
            this.comboBoxOfferingTemple.Name = "comboBoxOfferingTemple";
            this.comboBoxOfferingTemple.Size = new System.Drawing.Size(219, 26);
            this.comboBoxOfferingTemple.TabIndex = 147;
            this.comboBoxOfferingTemple.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOfferingTemple_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(12, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 18);
            this.label6.TabIndex = 146;
            this.label6.Text = "공양분원";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 18);
            this.label7.TabIndex = 149;
            this.label7.Text = "동참자 명단";
            // 
            // textBoxParticipants
            // 
            this.textBoxParticipants.Location = new System.Drawing.Point(96, 163);
            this.textBoxParticipants.Multiline = true;
            this.textBoxParticipants.Name = "textBoxParticipants";
            this.textBoxParticipants.Size = new System.Drawing.Size(219, 71);
            this.textBoxParticipants.TabIndex = 148;
            // 
            // comboBoxOfferorClass
            // 
            this.comboBoxOfferorClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOfferorClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxOfferorClass.FormattingEnabled = true;
            this.comboBoxOfferorClass.ItemHeight = 18;
            this.comboBoxOfferorClass.Location = new System.Drawing.Point(406, 20);
            this.comboBoxOfferorClass.Name = "comboBoxOfferorClass";
            this.comboBoxOfferorClass.Size = new System.Drawing.Size(223, 26);
            this.comboBoxOfferorClass.TabIndex = 151;
            this.comboBoxOfferorClass.SelectedIndexChanged += new System.EventHandler(this.comboBoxOfferorClass_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(323, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 18);
            this.label8.TabIndex = 150;
            this.label8.Text = "복위자 분류";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(323, 277);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 18);
            this.label21.TabIndex = 166;
            this.label21.Text = "연락처";
            // 
            // textBoxOfferorPhone
            // 
            this.textBoxOfferorPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxOfferorPhone.Location = new System.Drawing.Point(377, 274);
            this.textBoxOfferorPhone.Name = "textBoxOfferorPhone";
            this.textBoxOfferorPhone.Size = new System.Drawing.Size(254, 24);
            this.textBoxOfferorPhone.TabIndex = 165;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(323, 221);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 18);
            this.label20.TabIndex = 164;
            this.label20.Text = "주소";
            // 
            // textBoxOfferorAddress
            // 
            this.textBoxOfferorAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxOfferorAddress.Location = new System.Drawing.Point(377, 218);
            this.textBoxOfferorAddress.Multiline = true;
            this.textBoxOfferorAddress.Name = "textBoxOfferorAddress";
            this.textBoxOfferorAddress.Size = new System.Drawing.Size(254, 42);
            this.textBoxOfferorAddress.TabIndex = 163;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(496, 191);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 18);
            this.label19.TabIndex = 162;
            this.label19.Text = "생년";
            // 
            // textBoxOfferorBirthYear
            // 
            this.textBoxOfferorBirthYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxOfferorBirthYear.Location = new System.Drawing.Point(536, 188);
            this.textBoxOfferorBirthYear.Name = "textBoxOfferorBirthYear";
            this.textBoxOfferorBirthYear.Size = new System.Drawing.Size(95, 24);
            this.textBoxOfferorBirthYear.TabIndex = 161;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(323, 191);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 18);
            this.label18.TabIndex = 160;
            this.label18.Text = "이름";
            // 
            // textBoxOfferorName
            // 
            this.textBoxOfferorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxOfferorName.Location = new System.Drawing.Point(377, 188);
            this.textBoxOfferorName.Name = "textBoxOfferorName";
            this.textBoxOfferorName.Size = new System.Drawing.Size(99, 24);
            this.textBoxOfferorName.TabIndex = 159;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(321, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 24);
            this.label17.TabIndex = 158;
            this.label17.Text = "복위자";
            // 
            // textBoxDonator
            // 
            this.textBoxDonator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxDonator.Location = new System.Drawing.Point(377, 308);
            this.textBoxDonator.Name = "textBoxDonator";
            this.textBoxDonator.Size = new System.Drawing.Size(254, 24);
            this.textBoxDonator.TabIndex = 157;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(323, 311);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 18);
            this.label12.TabIndex = 156;
            this.label12.Text = "입금자";
            // 
            // buttonFamilySearch
            // 
            this.buttonFamilySearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonFamilySearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonFamilySearch.FlatAppearance.BorderSize = 2;
            this.buttonFamilySearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFamilySearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFamilySearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonFamilySearch.Location = new System.Drawing.Point(406, 87);
            this.buttonFamilySearch.Margin = new System.Windows.Forms.Padding(1);
            this.buttonFamilySearch.Name = "buttonFamilySearch";
            this.buttonFamilySearch.Size = new System.Drawing.Size(82, 26);
            this.buttonFamilySearch.TabIndex = 171;
            this.buttonFamilySearch.Text = "가족검색";
            this.buttonFamilySearch.UseVisualStyleBackColor = false;
            this.buttonFamilySearch.Click += new System.EventHandler(this.buttonFamilySearch_Click);
            // 
            // textBoxOfferorRelation
            // 
            this.textBoxOfferorRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxOfferorRelation.Location = new System.Drawing.Point(406, 119);
            this.textBoxOfferorRelation.Name = "textBoxOfferorRelation";
            this.textBoxOfferorRelation.Size = new System.Drawing.Size(223, 24);
            this.textBoxOfferorRelation.TabIndex = 170;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(322, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 18);
            this.label14.TabIndex = 169;
            this.label14.Text = "관계";
            // 
            // textBoxFamilyMemberName
            // 
            this.textBoxFamilyMemberName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxFamilyMemberName.Location = new System.Drawing.Point(494, 89);
            this.textBoxFamilyMemberName.Name = "textBoxFamilyMemberName";
            this.textBoxFamilyMemberName.ReadOnly = true;
            this.textBoxFamilyMemberName.Size = new System.Drawing.Size(135, 24);
            this.textBoxFamilyMemberName.TabIndex = 168;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(322, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 18);
            this.label13.TabIndex = 167;
            this.label13.Text = "가족 이름";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 18);
            this.label4.TabIndex = 173;
            this.label4.Text = "비고";
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Location = new System.Drawing.Point(96, 240);
            this.textBoxRemarks.Multiline = true;
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(219, 89);
            this.textBoxRemarks.TabIndex = 172;
            // 
            // UserOfferingUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(643, 723);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxRemarks);
            this.Controls.Add(this.buttonFamilySearch);
            this.Controls.Add(this.textBoxOfferorRelation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxFamilyMemberName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.textBoxOfferorPhone);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBoxOfferorAddress);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBoxOfferorBirthYear);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBoxOfferorName);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBoxDonator);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBoxOfferorClass);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxParticipants);
            this.Controls.Add(this.comboBoxOfferingTemple);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxRequest);
            this.Controls.Add(this.comboBoxMajesty);
            this.Controls.Add(this.buttonBelieverSearch);
            this.Controls.Add(this.dateTimePickerOfferingDay);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxBelieverName);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UserOfferingUpdateForm";
            this.Text = "공양 등록하기";
            this.Load += new System.EventHandler(this.UserOfferingUpdateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox comboBoxMajesty;
        private System.Windows.Forms.Button buttonBelieverSearch;
        public System.Windows.Forms.DateTimePicker dateTimePickerOfferingDay;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBoxBelieverName;
        public System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxRequest;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonUpdate;
        public System.Windows.Forms.ComboBox comboBoxOfferingTemple;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxParticipants;
        public System.Windows.Forms.ComboBox comboBoxOfferorClass;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox textBoxOfferorPhone;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox textBoxOfferorAddress;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox textBoxOfferorBirthYear;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox textBoxOfferorName;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox textBoxDonator;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonFamilySearch;
        public System.Windows.Forms.TextBox textBoxOfferorRelation;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox textBoxFamilyMemberName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRemarks;
    }
}