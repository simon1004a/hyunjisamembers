﻿namespace HyunjisaBeliever
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelManagePart = new System.Windows.Forms.Label();
            this.labelAdminInfo = new System.Windows.Forms.Label();
            this.buttonAdminManage = new System.Windows.Forms.Button();
            this.labelTemple = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.SidePanel = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.userControlBeliever1 = new HyunjisaBeliever.UserControlBeliever();
            this.userControlSMS1 = new HyunjisaBeliever.UserControlSMS();
            this.userControlPray1 = new HyunjisaBeliever.UserControlPray();
            this.userControlCalendar1 = new HyunjisaBeliever.UserControlCalendar();
            this.userControlOffering1 = new HyunjisaBeliever.UserControlOffering();
            this.userControlSalvation1 = new HyunjisaBeliever.UserControlSalvation();
            saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.userControlLog1 = new HyunjisaBeliever.UserControlLog();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Goldenrod;
            this.panel1.Controls.Add(this.labelManagePart);
            this.panel1.Controls.Add(this.labelAdminInfo);
            this.panel1.Controls.Add(this.buttonAdminManage);
            this.panel1.Controls.Add(this.labelTemple);
            this.panel1.Controls.Add(this.labelUser);
            this.panel1.Controls.Add(this.SidePanel);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 749);
            this.panel1.TabIndex = 29;
            // 
            // labelManagePart
            // 
            this.labelManagePart.AutoSize = true;
            this.labelManagePart.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelManagePart.Location = new System.Drawing.Point(32, 233);
            this.labelManagePart.Name = "labelManagePart";
            this.labelManagePart.Size = new System.Drawing.Size(39, 20);
            this.labelManagePart.TabIndex = 37;
            this.labelManagePart.Text = "담당";
            // 
            // labelAdminInfo
            // 
            this.labelAdminInfo.AutoSize = true;
            this.labelAdminInfo.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelAdminInfo.Location = new System.Drawing.Point(32, 262);
            this.labelAdminInfo.Name = "labelAdminInfo";
            this.labelAdminInfo.Size = new System.Drawing.Size(89, 20);
            this.labelAdminInfo.TabIndex = 36;
            this.labelAdminInfo.Text = "접속자 정보";
            this.labelAdminInfo.Click += new System.EventHandler(this.LabelAdminInfo_Click);
            // 
            // buttonAdminManage
            // 
            this.buttonAdminManage.FlatAppearance.BorderSize = 0;
            this.buttonAdminManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdminManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonAdminManage.ForeColor = System.Drawing.Color.Black;
            this.buttonAdminManage.Location = new System.Drawing.Point(0, 686);
            this.buttonAdminManage.Name = "buttonAdminManage";
            this.buttonAdminManage.Size = new System.Drawing.Size(213, 63);
            this.buttonAdminManage.TabIndex = 35;
            this.buttonAdminManage.Text = "관리자";
            this.buttonAdminManage.UseVisualStyleBackColor = true;
            this.buttonAdminManage.Visible = false;
            this.buttonAdminManage.Click += new System.EventHandler(this.ButtonAdminManage_Click);
            // 
            // labelTemple
            // 
            this.labelTemple.AutoSize = true;
            this.labelTemple.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTemple.Location = new System.Drawing.Point(32, 206);
            this.labelTemple.Name = "labelTemple";
            this.labelTemple.Size = new System.Drawing.Size(69, 20);
            this.labelTemple.TabIndex = 34;
            this.labelTemple.Text = "소속분원";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelUser.Location = new System.Drawing.Point(32, 180);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(54, 20);
            this.labelUser.TabIndex = 33;
            this.labelUser.Text = "접속자";
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.Color.Brown;
            this.SidePanel.Location = new System.Drawing.Point(0, 284);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(10, 64);
            this.SidePanel.TabIndex = 31;
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button6.ForeColor = System.Drawing.Color.Maroon;
            this.button6.Location = new System.Drawing.Point(0, 603);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(213, 63);
            this.button6.TabIndex = 32;
            this.button6.Text = "일정 관리";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button5.ForeColor = System.Drawing.Color.Maroon;
            this.button5.Location = new System.Drawing.Point(0, 540);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(213, 63);
            this.button5.TabIndex = 31;
            this.button5.Text = "천도재";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button4.ForeColor = System.Drawing.Color.Maroon;
            this.button4.Location = new System.Drawing.Point(0, 477);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(213, 63);
            this.button4.TabIndex = 31;
            this.button4.Text = "공양";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button3.ForeColor = System.Drawing.Color.Maroon;
            this.button3.Location = new System.Drawing.Point(0, 413);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(213, 63);
            this.button3.TabIndex = 31;
            this.button3.Text = "기도";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button2.ForeColor = System.Drawing.Color.Maroon;
            this.button2.Location = new System.Drawing.Point(0, 349);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(213, 63);
            this.button2.TabIndex = 31;
            this.button2.Text = "문자 보내기";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.ForeColor = System.Drawing.Color.Maroon;
            this.button1.Location = new System.Drawing.Point(0, 285);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(213, 63);
            this.button1.TabIndex = 31;
            this.button1.Text = "신도 관리";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Goldenrod;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(36, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(136, 174);
            this.panel3.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("GungsuhChe", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(20, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "현지사";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Brown;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(213, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1056, 16);
            this.panel2.TabIndex = 30;
            // 
            // userControlBeliever1
            // 
            this.userControlBeliever1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlBeliever1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlBeliever1.Location = new System.Drawing.Point(213, 15);
            this.userControlBeliever1.Name = "userControlBeliever1";
            this.userControlBeliever1.Size = new System.Drawing.Size(1056, 788);
            this.userControlBeliever1.TabIndex = 31;
            // 
            // userControlSMS1
            // 
            this.userControlSMS1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlSMS1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlSMS1.IsLoggedIn = false;
            this.userControlSMS1.Location = new System.Drawing.Point(213, 15);
            this.userControlSMS1.Name = "userControlSMS1";
            this.userControlSMS1.Size = new System.Drawing.Size(1056, 788);
            this.userControlSMS1.TabIndex = 32;
            // 
            // userControlPray1
            // 
            this.userControlPray1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlPray1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlPray1.Location = new System.Drawing.Point(213, 15);
            this.userControlPray1.Name = "userControlPray1";
            this.userControlPray1.Size = new System.Drawing.Size(1056, 788);
            this.userControlPray1.TabIndex = 33;
            // 
            // userControlCalendar1
            // 
            this.userControlCalendar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlCalendar1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlCalendar1.Location = new System.Drawing.Point(213, 15);
            this.userControlCalendar1.Name = "userControlCalendar1";
            this.userControlCalendar1.Size = new System.Drawing.Size(1056, 788);
            this.userControlCalendar1.TabIndex = 34;
            // 
            // userControlOffering1
            // 
            this.userControlOffering1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlOffering1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlOffering1.Location = new System.Drawing.Point(213, 15);
            this.userControlOffering1.Name = "userControlOffering1";
            this.userControlOffering1.Size = new System.Drawing.Size(1056, 788);
            this.userControlOffering1.TabIndex = 35;
            // 
            // userControlSalvation1
            // 
            this.userControlSalvation1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlSalvation1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlSalvation1.Location = new System.Drawing.Point(213, 15);
            this.userControlSalvation1.Name = "userControlSalvation1";
            this.userControlSalvation1.Size = new System.Drawing.Size(1056, 788);
            this.userControlSalvation1.TabIndex = 35;
            // 
            // userControlLog1
            // 
            this.userControlLog1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlLog1.BackColor = System.Drawing.Color.Moccasin;
            this.userControlLog1.Location = new System.Drawing.Point(213, 15);
            this.userControlLog1.Name = "userControlLog1";
            this.userControlLog1.Size = new System.Drawing.Size(1056, 788);
            this.userControlLog1.TabIndex = 36;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.ClientSize = new System.Drawing.Size(1269, 749);
            this.Controls.Add(this.userControlBeliever1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.userControlSMS1);
            this.Controls.Add(this.userControlPray1);
            this.Controls.Add(this.userControlCalendar1);
            this.Controls.Add(this.userControlOffering1);
            this.Controls.Add(this.userControlSalvation1);
            this.Controls.Add(this.userControlLog1);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "영산불교 현지사";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel SidePanel;
        private UserControlBeliever userControlBeliever1;
        private UserControlSMS userControlSMS1;
        private UserControlPray userControlPray1;
        private UserControlCalendar userControlCalendar1;
        private UserControlOffering userControlOffering1;
        private UserControlSalvation userControlSalvation1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label labelTemple;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Button buttonAdminManage;
        private System.Windows.Forms.Label labelAdminInfo;
        private System.Windows.Forms.Label labelManagePart;
        private UserControlLog userControlLog1;
        public static System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

