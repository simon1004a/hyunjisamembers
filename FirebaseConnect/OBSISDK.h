#pragma once

#include <Unknwn.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OBSI_SMS			1
#define OBSI_MMS			2
#define OBSI_VMS_TEXT		3
#define OBSI_VMS_FILE		4
#define OBSI_FMS			5
#define OBSI_CTC			6
#define OBSI_CALL			7

#define OBSI_ERROR_CLIENT_SYSTEM_ERROR		-1
#define OBSI_ERROR_INVALID_MSG_TYPE			-2
#define OBSI_ERROR_ALREADY_LOGIN				-3
#define OBSI_ERROR_LOGIN_FAILED				-4
#define OBSI_ERROR_NOT_LOGIN					-5
#define OBSI_ERROR_SEND_ABORTED				-6
#define OBSI_ERROR_SEND_FAILED				-7
#define OBSI_ERROR_TTS_FAILED				-8
#define OBSI_ERROR_UPLOAD_VOICE_FAILED		-9
#define OBSI_ERROR_TTF_FAILED				-10
#define OBSI_ERROR_EXCEED_MAX_TTF_FILES		-11
#define OBSI_ERROR_EXCEED_MAX_RECEIVER		-12
#define OBSI_ERROR_EXCEED_MAX_MMS_FILES		-13
#define OBSI_ERROR_UPLOAD_MMS_FILE_FAILED	-14
#define OBSI_ERROR_FILE_NOT_FOUND			-15
#define OBSI_ERROR_TOO_LARGE_FILE			-16
#define OBSI_ERROR_INVALID_ARG				-17
#define OBSI_ERROR_TIMEOUT					-18

#define OBSI_MAX_PATH_LENGTH				260
#define OBSI_MAX_SUBJECT_LENGTH			64
#define OBSI_MAX_RECEIVER_NAME_LENGTH	50
#define OBSI_MAX_RECEIVER_PHONE_LENGTH	30
#define OBSI_DATETIME_LENGTH				14
#define OBSI_MAX_CALLBACK_NUMBER_LENGTH	30
#define OBSI_MAX_SMS_MESSAGE_LENGTH		90
#define OBSI_MAX_MMS_MESSAGE_LENGTH		2000
#define OBSI_MAX_FMS_FILE_COUNT			5
#define OBSI_MAX_MMS_IMAGE_FILE_COUNT	3
#define OBSI_MAX_MMS_SOUND_FILE_COUNT	1
#define OBSI_MAX_VMS_MESSAGE_LENGTH		1000
#define OBSI_MAX_SPEAKER_LENGTH			32
#define OBSI_MAX_IMAGE_SIZE				(1024 * 1024)

#define OBSI_FEMALE_SPEAKER	"YUMI"
#define OBSI_MALE_SPEAKER	"JUNWOO"

struct OBSIResult
{
	long	lResult;
	char	szMessage[1024];
	char	szNoticeUrl[1024];
};

struct OBSILoginResult
{
	OBSIResult	Result;
	OBSIResult	ResultOaasys;
	unsigned long	ulMaxReceiverCount;
};

struct OBSISendResult
{
	OBSIResult	Result;
	void*			pContext;
	unsigned long	ulSent;
	unsigned long	ulFailed;
};

struct OBSIReceiver
{
	char	szName[OBSI_MAX_RECEIVER_NAME_LENGTH + 1];
	char	szPhoneNumber[OBSI_MAX_RECEIVER_PHONE_LENGTH + 1];
};

struct OBSIAttachment
{
	char	szFilePath[OBSI_MAX_PATH_LENGTH + 1];
};

struct OBSISMSContent
{
	char	szMessage[OBSI_MAX_SMS_MESSAGE_LENGTH + 1];
};

struct OBSIMMSContent
{
	char	szMessage[OBSI_MAX_MMS_MESSAGE_LENGTH + 1];
	unsigned long		ulImageFileCount;
	OBSIAttachment*	pImageFiles;
	unsigned long		ulSoundFileCount;
	OBSIAttachment*	pSoundFiles;
};

struct OBSIFMSContent
{
	unsigned long		ulFileCount;
	OBSIAttachment*	pFiles;
};

struct OBSIVMSTextContent
{
	char			szMessage[OBSI_MAX_VMS_MESSAGE_LENGTH + 1];
	char			szSpeaker[OBSI_MAX_SPEAKER_LENGTH + 1];
	unsigned long	ulChoiceCount;
};

struct OBSIVMSFileContent
{
	OBSIAttachment	File;
	unsigned long		ulChoiceCount;
};

struct OBSIMessage
{
	long					lMsgType;
	char					szSubject[OBSI_MAX_SUBJECT_LENGTH + 1];
	union OBSIMessageContent
	{
		OBSISMSContent*		pSMSContent;
		OBSIMMSContent*		pMMSContent;
		OBSIFMSContent*		pFMSContent;
		OBSIVMSTextContent*	pVMSTextContent;
		OBSIVMSFileContent*	pVMSFileContent;
	} Content;
	unsigned long			ulReceiverCount;
	OBSIReceiver*			pReceivers;
	char					szCallbackNumber[OBSI_MAX_CALLBACK_NUMBER_LENGTH + 1];
	char					szReserveTime[OBSI_DATETIME_LENGTH + 1];
};

struct OBSIReceiveMessage
{
	long		lMsgType;
	long		lSeqno;
	char		szSendNumber[OBSI_MAX_CALLBACK_NUMBER_LENGTH + 1];
	char		szReceiveNumber[OBSI_MAX_RECEIVER_PHONE_LENGTH + 1];
	char		szCreateTime[OBSI_DATETIME_LENGTH + 1];
};

typedef void (__stdcall *OBSISendResultProc)(const OBSISendResult* pResult);
typedef void (__stdcall *OBSIReceiveMessageProc)(const OBSIReceiveMessage* pMessage);

#ifndef OBSI_SDK_DLL
__declspec(dllimport) void __stdcall OBSISetReceiveCallbackWindow(void* hWnd, unsigned int uiMsg);
__declspec(dllimport) void __stdcall OBSISetReceiveCallbackFunction(OBSIReceiveMessageProc pCallback);
__declspec(dllimport) long __stdcall OBSILogin(const char* szUserID, const char* szPassword, char* szISVAPIKey, OBSILoginResult* pResult);
__declspec(dllimport) void __stdcall OBSILogout();
__declspec(dllimport) void __stdcall OBSISetCallbackWindow(void* hWnd, unsigned int uiMsg);
__declspec(dllimport) void __stdcall OBSISetCallbackFunction(OBSISendResultProc pCallback);
__declspec(dllimport) long __stdcall OBSISend(OBSIMessage* pMsg, void* pContext, OBSISendResult* pResult);
__declspec(dllimport) void __stdcall OBSIOpenUrl(const char* szUrl, IUnknown* pWebBrowser2);
#endif

#ifdef __cplusplus
}
#endif


