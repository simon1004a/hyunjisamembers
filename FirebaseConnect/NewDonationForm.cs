﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class NewDonationForm : Form
    {
        public DateTime DateOfDonation { get { return this.dateTimePickerDonation.Value; } }
        public int NewDonation { get { return Int32.TryParse(this.textBoxNewDonation.Text, out int newDoation) ? newDoation : 0; } }
        public string DonationRemarks { get { return this.textBoxRemarks.Text; } }
        public NewDonationForm()
        {
            InitializeComponent();
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            if( string.IsNullOrWhiteSpace(this.textBoxNewDonation.Text) )
                MessageBox.Show("기도비가 입력되지 않았습니다.");
            else if(NewDonation == 0)
                MessageBox.Show("기도비 항목에 숫자 외에 다른 문자가 포함되어 있습니다. ");
            else
                this.DialogResult = DialogResult.OK;
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TextBoxNewDonation_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }
    }
}
