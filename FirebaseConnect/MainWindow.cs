﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Excel = Microsoft.Office.Interop.Excel;

namespace HyunjisaBeliever
{
    public partial class MainWindow : Form
    {
        //To refence the Believer Table from every UserControl.
        public enum DATA_FIELD { UID = 0, NAME, PROVINCE, ADDRESS, PHONE, GENDER, BIRTH, AREA, REGISTER, MEMBER_NUMBER, GANZHI, DHARMA_NAME, EMAIL, FAMILY_NUMBER, REMARKS }
        public static string[] strColumns = { "신도아이디", "이름", "지역","상세주소", "휴대폰", "성별", "생일", "소속분원", "등록일", "신도번호", "간지", "법명", "이메일", "가족번호", "비고" };
        public static string[] strColumnsInDatabase = { "BelieverID", "Name", "AddressProvince","AddressDetail", "Phone", "GenderName", "DateOfBirth", "TempleName", "DateOfRegister",
            "BelieverNumber", "Ganzhi", "DharmaName", "Email", "Remarks" };

        public static string GetColumn(DATA_FIELD field) { return strColumns[(int)field]; }
        public static string GetColumnInDatabase(DATA_FIELD field) { return strColumnsInDatabase[(int)field]; }

        //육십갑자 - 천간天干과 지지地支
        static int firstYear = 1954;    //광명만덕 태사부님께서 탄생하신 갑오년을 기준으로 육십갑자를 계산합니다!!!
        static string[] heavenlyStems = new string[] { "갑", "을", "병", "정", "무", "기", "경", "신", "임", "계" };
        static string[] earthlyBranches = new string[] { "자", "축", "인", "묘", "진", "사", "오", "미", "신", "유", "술", "해" };
        public static string GetGanzhi(int iYear)
        {
            int yearDiff = iYear - firstYear;
            while (yearDiff < 0)
            {
                yearDiff += 60;
            }

            var stemIndex = yearDiff % 10;
            var branchIndex = (yearDiff % 12) + 6;
            if (branchIndex >= 12) branchIndex %= 12;
            var heavenlyStem = heavenlyStems[stemIndex];
            var earthlyBranch = earthlyBranches[branchIndex];

            return heavenlyStem + earthlyBranch + "생";
        }

        //접속 스님에 따른 권한 관리
        public static Monk accessedMonk;
        public static LocalTemple manageArea;

        //DataGridView 관련 상수
        public static int iDatagridviewRowHeight = 40;
        public static int iDatagridviewFontSize = 11;

        //Top Control을 찾기 위해 static으로 준비해둔 변수들...(static을 많이 쓰니...좋은 디자인은 아닙니다...)
        static List<UserControl> userControls = new List<UserControl>();
        static UserControl GetTopUserControl()
        {
            foreach (var ctrl in userControls)
            {
                if (ctrl.Parent.Controls.GetChildIndex(ctrl) == 0)
                    return ctrl;
            }
            return null;
        }
        public MainWindow()
        {
            InitializeComponent();
            SidePanel.Height = button1.Height;
            SidePanel.Top = button1.Top;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            userControlBeliever1.BringToFront();

            var loginForm = new LoginForm();
            loginForm.EventLoginMonkID += this.LoginSuccess;

            switch (loginForm.ShowDialog())
            {
                case DialogResult.OK:
                    loginForm.Close();
                    break;
                case DialogResult.Cancel:
                    Dispose();
                    break;
            }

        }

        void LoginSuccess(Monk monkData)
        {
            accessedMonk = monkData;
            manageArea = monkData.BelongedTemple;
            string userMonk = monkData.DharmaName + " 스님";
            this.labelUser.Text = "접속자 : " + userMonk;

            string strBelongedTemple = (monkData.BelongedTemple2 == LocalTemple.미지정) ? manageArea.ToString()
                : manageArea.ToString() + "/" + monkData.BelongedTemple2.ToString();
            this.labelTemple.Text = "소속 : " + strBelongedTemple;

            string strManagePart = "";
            if (accessedMonk.LevelOfAccess == AccessLevel.총괄관리자)
                strManagePart = "총괄";
            else if (accessedMonk.LevelOfAccess == AccessLevel.주지스님)
                strManagePart = "주지";
            else if (accessedMonk.LevelOfAccess == AccessLevel.분원스님)
            {
                foreach (ManagePartFlag managePart in Enum.GetValues(typeof(ManagePartFlag)))
                {
                    if (accessedMonk.IsBelongedManagePart(managePart))
                    {
                        string part = managePart.ToString().Replace("담당","");
                        strManagePart = (string.IsNullOrWhiteSpace(strManagePart)) ? part : strManagePart + "/" + part;
                    }
                }
            }
            this.labelManagePart.Text = "담당 : "+ strManagePart;

            MessageBox.Show(userMonk + "이 접속하셨습니다. ");            
            
            userControlBeliever1.RefreshBelieverListInDataGridView(true);
            userControlPray1.RefreshPrayListInDataGridView(true);
            userControlOffering1.RefreshOfferingListInDataGridView(true);
            userControlSalvation1.RefreshSalvationServiceListInDataGridView(true);
                        
            if (accessedMonk.LevelOfAccess == AccessLevel.총괄관리자)
            {
                this.buttonAdminManage.Visible = true;
                userControlCalendar1.buttonAdminInsert.Visible = true;
                userControlBeliever1.buttonImportExcel.Visible = true;
                userControlBeliever1.buttonFamilyList.Visible = true;
                userControlPray1.buttonImportExcel.Visible = true;
            }

            if (userControls.Count == 0)
            {
                userControls.Add(userControlBeliever1);
                userControls.Add(userControlSMS1);
                userControls.Add(userControlPray1);
                userControls.Add(userControlOffering1);
                userControls.Add(userControlSalvation1);
                userControls.Add(userControlSalvation1);
            }           

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button1.Height;
            SidePanel.Top = button1.Top;
            userControlBeliever1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button2.Height;
            SidePanel.Top = button2.Top;
            userControlSMS1.BringToFront();            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button3.Height;
            SidePanel.Top = button3.Top;
            userControlPray1.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button4.Height;
            SidePanel.Top = button4.Top;
            userControlOffering1.BringToFront();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button5.Height;
            SidePanel.Top = button5.Top;
            userControlSalvation1.BringToFront();

        }

        private void Button6_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button6.Height;
            SidePanel.Top = button6.Top;
            userControlCalendar1.BringToFront();
            userControlCalendar1.RefreshCeremonyListInCalendar(DateTime.Now.Date);
        }

        private void ButtonAdminManage_Click(object sender, EventArgs e)
        {
            //if (accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            //    return;

            SidePanel.Height = buttonAdminManage.Height;
            SidePanel.Top = buttonAdminManage.Top;

            if (!userControlLog1.isAwaken)
            {
                userControlLog1.isAwaken = true;
                userControlLog1.RefreshLogListInDataGridView(true);
            }                
            userControlLog1.BringToFront();            

            var adminManageForm = new AdminManageForm();
            adminManageForm.ShowDialog();
        }

        private void LabelAdminInfo_Click(object sender, EventArgs e)
        {
            //혹시 database상에서 정보가 바뀌었을 수도 있으니 새로 받아옵시다...
            int iMonkID = accessedMonk.UID;
            accessedMonk = SqlManager.SelectMonkFromSQL(iMonkID);
            var adminInfo = new AdminInfoForm(accessedMonk);
            if (adminInfo.ShowDialog() == DialogResult.OK)
            {
                accessedMonk = adminInfo.MonkInfo;
                adminInfo.Close();
                MessageBox.Show( accessedMonk.DharmaName + " 스님의 정보가 성공적으로 수정되었습니다. ");
            }

        }

        public static void ExportExcelFile(DataGridView dataGridView,ProgressBar progressBar = null)
        {
            if (dataGridView.Rows.Count <= 1)
            {
                MessageBox.Show("출력할 데이터가 없습니다.");
                return;
            }

            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Title = "Excel File로 저장하기";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            saveFileDialog1.DefaultExt = "xlsx";
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bool isProgressbarCreated = false;
                UserControl topUserControl = GetTopUserControl();
                //if there is no specified progressBar, Let us make it.
                if (progressBar == null && topUserControl != null)
                {
                    progressBar = new ProgressBar();
                    progressBar.Maximum = 100;
                    progressBar.Step = 1;
                    topUserControl.Controls.Add(progressBar);
                    isProgressbarCreated = true;
                }

                progressBar.Visible = true;
                progressBar.Value = 0;

                var excelApp = new Excel.Application();
                excelApp.Application.Workbooks.Add(Type.Missing);
                excelApp.Columns.ColumnWidth = 20;

                for (int i = 1; i < dataGridView.Columns.Count + 1; i++)
                {
                    excelApp.Cells[1, i] = dataGridView.Columns[i - 1].HeaderText;
                }

                //Storing Each Row and Column value to excel sheet
                int iTotalRowCount = dataGridView.Rows.Count - 1;
                for (int j = 0; j < iTotalRowCount; j++)
                {
                    for (int k = 0; k < dataGridView.Columns.Count; k++)
                    {
                        if (dataGridView.Rows[j].Cells[k].Value != null)
                        {
                            if (excelApp.Cells[1, k + 1].Value.ToString() == GetColumn(DATA_FIELD.PHONE))
                                excelApp.Cells[j + 2, k + 1] = "'" + dataGridView.Rows[j].Cells[k].Value;
                            else
                                excelApp.Cells[j + 2, k + 1] = dataGridView.Rows[j].Cells[k].Value;
                        }
                    }
                    progressBar.Value = 100 * (j + 1) / iTotalRowCount;
                }
                progressBar.Visible = false;
                excelApp.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName);
                excelApp.ActiveWorkbook.Saved = true;
                excelApp.ActiveWorkbook.Close();
                MessageBox.Show(saveFileDialog1.FileName + " 파일이 저장되었습니다.", "엑셀 파일 저장", MessageBoxButtons.OK);

                if (isProgressbarCreated)
                    topUserControl.Controls.Remove(progressBar);

                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
            }

        }


    }
}
