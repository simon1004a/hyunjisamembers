﻿namespace HyunjisaBeliever
{
    partial class BelieverSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPanelTitle = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.comboBoxBelongedTemple = new System.Windows.Forms.ComboBox();
            this.labelArea = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxAddressDetail = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDharmaName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxGanzhi = new System.Windows.Forms.TextBox();
            this.comboBoxProvince = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPanelTitle
            // 
            this.labelPanelTitle.AutoSize = true;
            this.labelPanelTitle.Font = new System.Drawing.Font("GulimChe", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelPanelTitle.Location = new System.Drawing.Point(67, 12);
            this.labelPanelTitle.Name = "labelPanelTitle";
            this.labelPanelTitle.Size = new System.Drawing.Size(114, 22);
            this.labelPanelTitle.TabIndex = 129;
            this.labelPanelTitle.Text = "신도 검색";
            this.labelPanelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonSearch.FlatAppearance.BorderSize = 2;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonSearch.ForeColor = System.Drawing.Color.Maroon;
            this.buttonSearch.Location = new System.Drawing.Point(133, 389);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(111, 50);
            this.buttonSearch.TabIndex = 128;
            this.buttonSearch.Text = "검색하기";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Location = new System.Drawing.Point(102, 180);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(142, 20);
            this.comboBoxGender.TabIndex = 127;
            this.comboBoxGender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxGender_KeyPress);
            // 
            // comboBoxBelongedTemple
            // 
            this.comboBoxBelongedTemple.FormattingEnabled = true;
            this.comboBoxBelongedTemple.Location = new System.Drawing.Point(102, 206);
            this.comboBoxBelongedTemple.Name = "comboBoxBelongedTemple";
            this.comboBoxBelongedTemple.Size = new System.Drawing.Size(142, 20);
            this.comboBoxBelongedTemple.TabIndex = 126;
            this.comboBoxBelongedTemple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxBelongedTemple_KeyPress);
            // 
            // labelArea
            // 
            this.labelArea.AutoSize = true;
            this.labelArea.Location = new System.Drawing.Point(12, 208);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(53, 12);
            this.labelArea.TabIndex = 125;
            this.labelArea.Text = "소속분원";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.Location = new System.Drawing.Point(12, 183);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(29, 12);
            this.labelGender.TabIndex = 124;
            this.labelGender.Text = "성별";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(12, 132);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(41, 12);
            this.labelPhone.TabIndex = 123;
            this.labelPhone.Text = "휴대폰";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(12, 261);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(53, 12);
            this.labelAddress.TabIndex = 122;
            this.labelAddress.Text = "상세주소";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(12, 51);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(29, 12);
            this.labelName.TabIndex = 121;
            this.labelName.Text = "이름";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(102, 129);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(142, 21);
            this.textBoxPhoneNumber.TabIndex = 120;
            this.textBoxPhoneNumber.Text = "-는 빼고 입력해주세요";
            // 
            // textBoxAddressDetail
            // 
            this.textBoxAddressDetail.Location = new System.Drawing.Point(102, 258);
            this.textBoxAddressDetail.Name = "textBoxAddressDetail";
            this.textBoxAddressDetail.Size = new System.Drawing.Size(142, 21);
            this.textBoxAddressDetail.TabIndex = 119;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(102, 48);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(142, 21);
            this.textBoxName.TabIndex = 118;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(261, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(969, 371);
            this.dataGridView1.TabIndex = 130;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonSelect.FlatAppearance.BorderSize = 2;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonSelect.ForeColor = System.Drawing.Color.Maroon;
            this.buttonSelect.Location = new System.Drawing.Point(261, 389);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(969, 50);
            this.buttonSelect.TabIndex = 131;
            this.buttonSelect.Text = "확인";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.ButtonSelect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 133;
            this.label1.Text = "법명";
            // 
            // textBoxDharmaName
            // 
            this.textBoxDharmaName.Location = new System.Drawing.Point(102, 75);
            this.textBoxDharmaName.Name = "textBoxDharmaName";
            this.textBoxDharmaName.Size = new System.Drawing.Size(142, 21);
            this.textBoxDharmaName.TabIndex = 132;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 135;
            this.label2.Text = "생년(간지)";
            // 
            // textBoxGanzhi
            // 
            this.textBoxGanzhi.Location = new System.Drawing.Point(102, 102);
            this.textBoxGanzhi.Name = "textBoxGanzhi";
            this.textBoxGanzhi.Size = new System.Drawing.Size(142, 21);
            this.textBoxGanzhi.TabIndex = 134;
            // 
            // comboBoxProvince
            // 
            this.comboBoxProvince.FormattingEnabled = true;
            this.comboBoxProvince.Location = new System.Drawing.Point(102, 232);
            this.comboBoxProvince.Name = "comboBoxProvince";
            this.comboBoxProvince.Size = new System.Drawing.Size(142, 20);
            this.comboBoxProvince.TabIndex = 137;
            this.comboBoxProvince.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBoxProvince_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 136;
            this.label3.Text = "지역";
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonReset.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonReset.FlatAppearance.BorderSize = 2;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonReset.ForeColor = System.Drawing.Color.Maroon;
            this.buttonReset.Location = new System.Drawing.Point(14, 389);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(111, 50);
            this.buttonReset.TabIndex = 138;
            this.buttonReset.Text = "초기화";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // BelieverSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(1242, 451);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.comboBoxProvince);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxGanzhi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDharmaName);
            this.Controls.Add(this.buttonSelect);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.labelPanelTitle);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.comboBoxGender);
            this.Controls.Add(this.comboBoxBelongedTemple);
            this.Controls.Add(this.labelArea);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelPhone);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.textBoxAddressDetail);
            this.Controls.Add(this.textBoxName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "BelieverSearchForm";
            this.Text = "BelieverSearchForm";
            this.Load += new System.EventHandler(this.BelieverSearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPanelTitle;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.ComboBox comboBoxBelongedTemple;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxAddressDetail;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDharmaName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxGanzhi;
        private System.Windows.Forms.ComboBox comboBoxProvince;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonReset;
    }
}