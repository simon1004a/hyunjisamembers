﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class AdminInfoForm : Form
    {
        EditMode editMode;
        private Monk _MonkInfo = new Monk();
        public Monk MonkInfo
        {
            set
            {
                _MonkInfo = value;
                this.textBoxDharmaName.Text = _MonkInfo.DharmaName;
                this.textBoxPassword.Text = _MonkInfo.Password;
                this.textBoxPasswordConfirm.Text = _MonkInfo.Password;
                this.textBoxEmail.Text = _MonkInfo.Email;
                this.textBoxPhone.Text = _MonkInfo.Phone;
                this.comboBoxGender.SelectedItem = _MonkInfo.GenderType;
                this.comboBoxTemple.SelectedItem = _MonkInfo.BelongedTemple;
                this.comboBoxTemple2.SelectedItem = _MonkInfo.BelongedTemple2;
                this.textBoxRemarks.Text = _MonkInfo.Remarks;
            }
            get
            {
                _MonkInfo.DharmaName = this.textBoxDharmaName.Text;
                _MonkInfo.Password = this.textBoxPassword.Text;
                _MonkInfo.Email = this.textBoxEmail.Text;
                _MonkInfo.Phone = this.textBoxPhone.Text;
                _MonkInfo.GenderType = (Gender)comboBoxGender.SelectedItem;
                _MonkInfo.BelongedTemple = (LocalTemple)comboBoxTemple.SelectedItem;
                _MonkInfo.BelongedTemple2 = (LocalTemple)comboBoxTemple2.SelectedItem;
                _MonkInfo.Remarks = this.textBoxRemarks.Text;
                return _MonkInfo;
            }
        }

        public AdminInfoForm(Monk adminInfo = null)
        {
            InitializeComponent();
            this.comboBoxGender.DataSource = Enum.GetValues(typeof(Gender));
            this.comboBoxTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxTemple2.DataSource = Enum.GetValues(typeof(LocalTemple));

            if (adminInfo == null)
                this.editMode = EditMode.INSERT_MODE;
            else
            {
                this.editMode = EditMode.UPDATE_MODE;
                MonkInfo = adminInfo;                
            }            
        }
        private void AdminInfoForm_Load(object sender, EventArgs e)
        {
            if (MonkInfo.BelongedTemple2 != LocalTemple.미지정)
            {
                this.labelLocalTemple2.Visible = true;
                this.comboBoxTemple2.Visible = true;
            }
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxDharmaName.Text))
            {
                MessageBox.Show("불명을 입력해주세요.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(textBoxPassword.Text))
            {
                MessageBox.Show("비밀번호를 입력해주세요.");
                return;
            }
            else if (textBoxPassword.Text != textBoxPasswordConfirm.Text)
            {
                MessageBox.Show("비밀번호가 서로 일치하지 않습니다.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(textBoxEmail.Text))
            {
                MessageBox.Show("이메일을 입력해주세요.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(textBoxPhone.Text))
            {
                MessageBox.Show("핸드폰 번호를 입력해주세요.");
                return;
            }

            if (editMode == EditMode.INSERT_MODE)
            {
                Monk newMonkInfo = MonkInfo;
                newMonkInfo.LevelOfAccess = AccessLevel.미허가자;

                if (!SqlManager.IsMonkExistInSQL(newMonkInfo.DharmaName))
                {
                    //여기에서 새로운 Admin정보를 Insert합시다.
                    if (SqlManager.InsertMonkToSQL(newMonkInfo))
                    {
                        this.DialogResult = DialogResult.OK;
                        MessageBox.Show("ID가 신청되었습니다. 총괄 관리자에게 따로 연락하여 권한을 요청해주십시오.");
                    }
                }
                else
                    MessageBox.Show("같은 법명을 가진 관리자가 데이터베이스에 이미 존재하고 있습니다.");                
            }
            else
            {
                if (SqlManager.UpdateMonkInSQL(MonkInfo))
                    this.DialogResult = DialogResult.OK;                
            }           

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TextBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void TextBoxEmail_Leave(object sender, EventArgs e)
        {
            //Object initialization for Regex
            Regex reg = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$", RegexOptions.IgnoreCase);
            if (!reg.IsMatch(textBoxEmail.Text))
            {
                MessageBox.Show("이메일 주소의 형식이 틀립니다. 다시 입력해주십시오.");
                textBoxEmail.Clear();
            }
        }

        private void ComboBoxTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if (editMode != EditMode.INSERT_MODE)
                {
                    var changedTemple = (LocalTemple)comboBoxTemple.SelectedItem;
                    if (changedTemple != _MonkInfo.BelongedTemple)
                    {
                        if (changedTemple != LocalTemple.미지정)
                            MessageBox.Show("담당 분원은 본인 스스로 변경할 수 없습니다. 총괄관리자에게 문의해주세요.");
                        comboBoxTemple.SelectedItem = _MonkInfo.BelongedTemple;

                    }
                }
            }
        }

        private void ComboBoxTemple2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if (editMode != EditMode.INSERT_MODE)
                {
                    var changedTemple = (LocalTemple)comboBoxTemple2.SelectedItem;
                    if (changedTemple != _MonkInfo.BelongedTemple2)
                    {
                        if (changedTemple != LocalTemple.미지정)
                            MessageBox.Show("담당 분원은 본인 스스로 변경할 수 없습니다. 총괄관리자에게 문의해주세요.");
                        comboBoxTemple2.SelectedItem = _MonkInfo.BelongedTemple2;

                    }
                }
            }

        }

        
    }
}
