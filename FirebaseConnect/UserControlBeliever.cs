﻿using FireSharp.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HyunjisaBeliever.MainWindow;
using Excel = Microsoft.Office.Interop.Excel;

namespace HyunjisaBeliever
{
    public enum EditMode
    {
        VIEW_MODE,
        INSERT_MODE,
        SEARCH_MODE,
        UPDATE_MODE
    }

    public partial class UserControlBeliever : UserControl
    {
        public DataTable mainDataTable = null;        
        private Believer GetUserDataFromSqlByDataGridViewRow(int iRow)
        {
            var userData = new Believer();
            if (iRow >= 0 && iRow < this.dataGridView1.RowCount-1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[iRow];
                if (row.IsNewRow) return userData;

                int believerID = Convert.ToInt32(row.Cells[GetColumn(DATA_FIELD.UID)].Value);
                var selectedBeliever = SqlManager.SelectBelieverFromSQL(believerID);
                if (selectedBeliever != null)
                    userData = selectedBeliever;
            }
            return userData;
        }
        public Believer GetUserDataFromDataGridViewRow(int iRow)
        {
            var userData = new Believer();
            if (iRow >= 0 && iRow < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[iRow];
                if (row.IsNewRow) return userData;

                try
                {
                    //신도아이디와 이름과 번호는 꼭 필요한 정보입니다. 전부 있어야 합니다. UID가 그 유저의 고유 아이디가 됩니다. 
                    //전체 엑셀 파일을 올리면 UID가 있다면 그 해당 UID의 record를 업데이트하고 없으면 Insert합니다.
                    userData.UID = Convert.ToInt32(row.Cells[GetColumn(DATA_FIELD.UID)].Value);
                    userData.Name = row.Cells[GetColumn(DATA_FIELD.NAME)].Value.ToString();
                    string phoneNumber = row.Cells[GetColumn(DATA_FIELD.PHONE)].Value.ToString();
                    if (phoneNumber.StartsWith("1")) phoneNumber = "0" + phoneNumber;
                    if (phoneNumber.Contains("-")) phoneNumber = phoneNumber.Replace("-", "");
                    userData.Phone = phoneNumber;
                    
                    var believerNumber = row.Cells[GetColumn(DATA_FIELD.MEMBER_NUMBER)].Value;
                    userData.BelieverNumber = (believerNumber != null) ? believerNumber.ToString() : "";
                    var familyIdValue = row.Cells[GetColumn(DATA_FIELD.FAMILY_NUMBER)].Value;
                    userData.FamilyID = (familyIdValue != null && Int32.TryParse(familyIdValue.ToString(),out int iFamilyID)) ? iFamilyID:0;
                    var dharmaName = row.Cells[GetColumn(DATA_FIELD.DHARMA_NAME)].Value;
                    userData.DharmaName = (dharmaName!= null) ? dharmaName.ToString():"";
                    
                    var province = row.Cells[GetColumn(DATA_FIELD.PROVINCE)].Value;
                    if (province != null && Enum.TryParse<Province>(province.ToString(), out Province addressProvince))
                        userData.AddressProvince = addressProvince;
                    else
                        userData.AddressProvince = Province.미지정;

                    var addressDetail = row.Cells[GetColumn(DATA_FIELD.ADDRESS)].Value;
                    userData.AddressDetail = (addressDetail != null)? addressDetail.ToString():"";
                    
                    var gender = row.Cells[GetColumn(DATA_FIELD.GENDER)].Value;
                    if (gender != null && Enum.TryParse(gender.ToString(), out Gender userGender))
                        userData.bGender = Convert.ToBoolean((int)userGender);
                    else
                        userData.bGender = false;

                    var temple = row.Cells[GetColumn(DATA_FIELD.AREA)].Value;
                    if (temple != null && Enum.TryParse(temple.ToString(), out LocalTemple userTemple))
                        userData.BelongedTemple = userTemple;
                    else
                        userData.BelongedTemple = LocalTemple.미지정;

                    var ganzhi = row.Cells[GetColumn(DATA_FIELD.GANZHI)].Value;
                    userData.Ganzhi = (ganzhi != null)? ganzhi.ToString(): "";

                    var birthed = row.Cells[GetColumn(DATA_FIELD.BIRTH)].Value;
                    if (birthed != null && DateTime.TryParse(birthed.ToString(), out DateTime userBirthDate))
                        userData.BirthDate = userBirthDate.ToShortDateString();
                    else if(ganzhi != null)
                    {
                        userData.BirthDate = "";
                        string birthGanzhi = userData.Ganzhi;
                        //간지는 있는데 생일 정보가 없는 경우는, 그 해당하는 간지 해를 가까운 과거 연도에서 찾아 1월1일로 부여한다. 
                        //오류의 가능성을 줄이기 위해 10년 전의 간지부터 계산하도록 한다. 
                        for (int diff = 10; diff < 100; diff++)
                        {
                            int iYear = DateTime.Now.Year - diff;
                            if (MainWindow.GetGanzhi(iYear) == birthGanzhi)
                            {
                                userData.BirthDate = new DateTime(iYear, 1, 1).ToShortDateString();
                                break;
                            }
                        }
                    }
                    
                    var registered = row.Cells[GetColumn(DATA_FIELD.REGISTER)].Value;
                    if (registered != null)
                    {
                        if (DateTime.TryParse(registered.ToString(), out DateTime registeredDateTime))
                            userData.RegisteredDate = registeredDateTime.ToShortDateString();
                        else
                        {
                            if (Int32.TryParse(registered.ToString(), out int registerYear))
                            {
                                if (registerYear > 1990 && registerYear <= DateTime.Now.Year)
                                    userData.RegisteredDate = new DateTime(registerYear, 1, 1).ToShortDateString();
                            }
                        }
                    }
                    else
                        userData.RegisteredDate = "";

                    var email = row.Cells[GetColumn(DATA_FIELD.EMAIL)].Value;
                    userData.Email = (email != null)? email.ToString(): "";
                    var remark = row.Cells[GetColumn(DATA_FIELD.REMARKS)].Value;
                    userData.Remarks = (remark != null) ? remark.ToString(): "";
                }
                catch { MessageBox.Show("데이터 그리드뷰에 필요한 값이 전부 다 들어있지 않습니다."); }
            }
            return userData;
        }

        
        void ResetBelieverListSearchCondition()
        {
            if (accessedMonk != null)
                comboBoxBelongedTemple.SelectedItem = MainWindow.manageArea;
            
            comboBoxProvince.ResetText();
            comboBoxGender.ResetText();

            textBoxName.Clear();
            textBoxDharmaname.Clear();
            textBoxGanzhi.Clear();       
            textBoxPhoneNumber.Clear();
        }
        

        public UserControlBeliever()
        {
            InitializeComponent();            
                                    
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            if (mainDataTable == null) mainDataTable = new DataTable();
            else return;

            //ComboBox Enum Type Setting!            
            comboBoxBelongedTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            comboBoxProvince.DataSource = Enum.GetValues(typeof(Province));
            comboBoxGender.DataSource = Enum.GetValues(typeof(Gender));

            RefreshBelieverListInDataGridView(true);
        }

        //Insert Data
        private void buttonInsert_Click(object sender, EventArgs e)
        {
            var userInfoInsertForm = new UserInfoModifyForm { CurrentEditMode = EditMode.INSERT_MODE };            

            if (userInfoInsertForm.ShowDialog() == DialogResult.OK)
            {
                RefreshBelieverListInDataGridView();
                userInfoInsertForm.Close();
            }
        }
        
        
        private void textBoxNumberOnly_KeyPress(object sender, KeyPressEventArgs e)
        {    
            //엔터 입력을 받으면 전체를 refresh하되, 그 외의 경우는 숫자만 입력받게 합시다. 
            if (e.KeyChar == '\r')
            {
                RefreshBelieverListInDataGridView();
            }
            else if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        //DataGridView의 정보 Excel파일로 export하기!!
        private void buttonExportExcel_Click(object sender, EventArgs e)
        {
            MainWindow.ExportExcelFile(this.dataGridView1, this.progressBar1);
        }
        private void buttonImportExcel_Click(object sender, EventArgs e)
        {
            string warningMessage = "엑셀 파일의 데이터로 전체 신도 정보를 Update합니다. " +
                "엑셀 테이블의 신도 아이디와 동일한 신도 아이디가 데이터베이스에 존재한다면 그 신도의 정보(record)가 업데이트되고, " +
                "엑셀 테이블에 신도 아이디가 있는데 데이터베이스에 없다면 새로이 그 신도의 정보(record)가 생성됩니다. " +
                "그러나 엑셀 테이블에 존재하지 않는 신도 아이디가 데이터베이스에 있으면 데이터베이스의 해당 신도 정보(record)는 삭제됩니다. " +
                "해당 신도가 삭제될 때는 그 신도와 연결된 기도 정보나 공양, 천도재 정보가 없어야만 합니다. " +
                "엑셀 파일이 모든 신도의 정보를 담고 있어야만 하오니, 부디 잘 생각하여 진행해주십시오. " +
                "그래도 계속 진행하시겠습니까?";
            var warningResult = MessageBox.Show(warningMessage, "경고 메시지", MessageBoxButtons.YesNoCancel);
            if (warningResult != DialogResult.Yes)
            {
                MessageBox.Show("엑셀 파일 upload가 취소되었습니다.");
                return;
            }
                
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {   
                progressBar1.Visible = true;
                progressBar1.Value = 5;

                string filename = openFileDialog1.FileName;
                txtFilePath.Text = filename;
                txtFilePath.Visible = true;

                var excelApp = new Excel.Application();

                Excel.Workbook excelBook = excelApp.Workbooks.Open(filename, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Excel.Worksheet excelSheet = (Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                Excel.Range excelRange = excelSheet.UsedRange;

                string strCellData = "";
                int rowCnt = 0;
                int colCnt = 0;

                mainDataTable.Rows.Clear();
                mainDataTable.Columns.Clear();
                dataGridView1.DataSource = null;

                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    string strColumnName = (string)(excelRange.Cells[1, colCnt] as Excel.Range).Value2;
                    mainDataTable.Columns.Add(strColumnName, typeof(string));
                }

                int iTotalRowCnt = excelRange.Rows.Count;
                //먼저 excel data를 읽어서 mainDataTable을 채웁니다. 
                //첫행은 컬럼제목이므로 읽을 필요가 없다.
                for (rowCnt = 2; rowCnt <= iTotalRowCnt; rowCnt++)
                {
                    string strRowData = "";                    
                    bool isAllCellsEmpty = true;
                    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strCellData = excelRange.Cells[rowCnt, colCnt].Value.ToString();
                            isAllCellsEmpty = false;
                        }
                        catch
                        {
                            strCellData = "";
                        }
                        strRowData += strCellData + "|";
                    }                    
                    if (isAllCellsEmpty)
                        continue;

                    strRowData = strRowData.Remove(strRowData.Length - 1, 1);
                    mainDataTable.Rows.Add(strRowData.Split('|'));
                    progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt);
                }

                dataGridView1.DataSource = mainDataTable;
                //mainDataTable의 내용을 이제 dataGridView1으로 옮기고, 이제 실제로 database에 업로드합니다!
                var uidListInExcel = new List<int>();
                for (rowCnt = 2; rowCnt <= iTotalRowCnt; rowCnt++)
                {
                    var userData = this.GetUserDataFromDataGridViewRow(rowCnt - 2);
                    if (SqlManager.UpdateOrInsertBelieverInSQL(userData))
                        uidListInExcel.Add(userData.UID);
                    progressBar1.Value = (50 * (rowCnt - 1) / iTotalRowCnt) + 50;
                }

                //Excel 파일에 없는 UID라면 삭제된 것으로 판단하고 데이터베이스에서도 지우도록 합니다. 
                uidListInExcel.Sort();
                var uidListToRemove = new List<int>();
                int uidLast = uidListInExcel.Last();
                for ( int uid = 1 ; uid <= uidLast; uid++ )
                {
                    if (!uidListInExcel.Contains(uid))
                        uidListToRemove.Add(uid);
                }

                string deletedIdList = "";
                foreach (var deleteId in uidListToRemove)
                {                    
                    SqlManager.DeleteBelieverInSQL(deleteId);
                    deletedIdList += deleteId.ToString() + ", ";
                }

                MessageBox.Show("삭제된 아이디들은 다음과 같습니다. : " + deletedIdList);

                progressBar1.Value = 100;
                MessageBox.Show(filename + " 엑셀 파일을 로드하였습니다.");
                MessageBox.Show("엑셀의 전체 데이터를 서버에 업로드하였습니다.");

                RefreshBelieverListInDataGridView(true);
                excelBook.Close(true, null, null);
                Marshal.FinalReleaseComObject(excelBook);
                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
                progressBar1.Visible = false;                
            }
        }
        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            RefreshBelieverListInDataGridView(true);
        }
        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            RefreshBelieverListInDataGridView();
        }

        public void RefreshBelieverListInDataGridView(bool bResetSearchConditions = false)
        {            
            int iSelectedIndex = (dataGridView1.SelectedRows.Count > 0) ? dataGridView1.SelectedRows[0].Index : 0;

            var searchConditions = new List<string>();
            if (bResetSearchConditions)
            {
                ResetBelieverListSearchCondition();                
            }

            if (!string.IsNullOrWhiteSpace(comboBoxBelongedTemple.Text) && comboBoxBelongedTemple.Text != LocalTemple.전체.ToString())
                searchConditions.Add(" B.BelongedTemple = " + Convert.ToString((int)(Enum.Parse(typeof(LocalTemple), comboBoxBelongedTemple.Text))));
              
            if (!string.IsNullOrWhiteSpace(comboBoxProvince.Text))
                searchConditions.Add(" B.AddressProvince = " + Convert.ToString((int)(Enum.Parse(typeof(Province), comboBoxProvince.Text))));

            if (!string.IsNullOrWhiteSpace(comboBoxGender.Text))
                searchConditions.Add(" B.Gender = " + Convert.ToString((int)(Enum.Parse(typeof(Gender), comboBoxGender.Text))));

            if (!string.IsNullOrWhiteSpace(textBoxName.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.NAME) + " LIKE '%" + textBoxName.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxDharmaname.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.DHARMA_NAME) + " LIKE '%" + textBoxDharmaname.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxGanzhi.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.GANZHI) + " LIKE '%" + textBoxGanzhi.Text + "%'");

            if (!string.IsNullOrWhiteSpace(textBoxPhoneNumber.Text))
                searchConditions.Add("B." + GetColumnInDatabase(DATA_FIELD.PHONE) + " LIKE '%" + textBoxPhoneNumber.Text + "%'");

            
            if (searchConditions.Count != 0)
                mainDataTable = SqlManager.SelectBelieverListFromSQL(searchConditions.ToArray());
            else
                mainDataTable = SqlManager.SelectBelieverListFromSQL();
                        
            this.dataGridView1.DataSource = mainDataTable;
            this.labelRowCount.Text = string.Format("총 {0}개의 데이터가 검색되었습니다. ", dataGridView1.Rows.Count-1);
            
            //dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //발원이 길 경우 multiline으로 만들기 위한 코드...
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                string columnName = dataGridView1.Columns[i].Name;
                if (columnName == "비고")
                    dataGridView1.Columns[i].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }

            //현재 selected row로 돌아오자.
            if (iSelectedIndex != 0 && iSelectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = iSelectedIndex;
                dataGridView1.CurrentCell = dataGridView1.Rows[iSelectedIndex].Cells[0];                
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {            
            if (e.RowIndex >= 0 && e.RowIndex < this.dataGridView1.RowCount - 1)
            {                
                var userData = this.GetUserDataFromSqlByDataGridViewRow(e.RowIndex);
                var userInfoModifyForm = new UserInfoModifyForm { BelieverInfo = userData };                
                if (userInfoModifyForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshBelieverListInDataGridView();
                    userInfoModifyForm.Close();
                }
            }
            else if (e.RowIndex == this.dataGridView1.RowCount-1)
            {
                var userInfoInsertForm = new UserInfoModifyForm { CurrentEditMode = EditMode.INSERT_MODE };

                if (userInfoInsertForm.ShowDialog() == DialogResult.OK)
                {
                    RefreshBelieverListInDataGridView();
                    userInfoInsertForm.Close();
                }
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridView1.RowCount - 1)
                {
                    int iSelectedRowCount = dataGridView1.SelectedRows.Count;
                    if (iSelectedRowCount == 0)
                        dataGridView1.Rows[iClickedRow].Selected = true;

                    var selectedBelieversData = new List<Believer>();
                    bool isAllMemberBelongedToMonkTemple = true;
                    for (int a = 0; a < dataGridView1.SelectedRows.Count; a++)
                    {                        
                        int iSelectedRowIndex = dataGridView1.SelectedRows[a].Index;
                        var selectedBeliever = this.GetUserDataFromDataGridViewRow(iSelectedRowIndex);

                        //BelieverID가 없다면 함수가 제대로 실행될 수 없습니다!
                        if (selectedBeliever.UID <= 0)
                            return;

                        isAllMemberBelongedToMonkTemple &= accessedMonk.IsBelongedTemple(selectedBeliever.BelongedTemple);
                        selectedBelieversData.Add(selectedBeliever);
                    }                    
                    
                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    miniMenu.Tag = selectedBelieversData;
                    //if user selected multiple rows, only the deletion is available.
                    if (iSelectedRowCount <= 1)
                    {
                        miniMenu.Items.Add("수정하기").Name = "Modify";
                        miniMenu.Items.Add("문자보내기").Name = "SendSMS";
                        if (selectedBelieversData[0].FamilyID != 0)
                            miniMenu.Items.Add("가족 정보 보기").Name = "FamilyInfo";                        
                    }
                    else
                    {
                        if (IsFamilyUnionPossibleInSelectedRows(dataGridView1.SelectedRows))
                            miniMenu.Items.Add("가족으로 합치기").Name = "FamilyUnion";                                                                   
                    }

                    //Admin 레벨이 아니라면 자기 분원의 신도 정보만 삭제할 수 있습니다.
                    if (accessedMonk.LevelOfAccess == AccessLevel.총괄관리자 || isAllMemberBelongedToMonkTemple)
                    {
                        miniMenu.Items.Add("삭제하기").Name = "Delete";
                    }   
                    miniMenu.Show(dataGridView1, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += MiniMenu_ItemClicked;
                }
            }
        }

        private bool IsFamilyUnionPossibleInSelectedRows(DataGridViewSelectedRowCollection dataGridViewSelectedRow)
        {
            bool isFamilyNumberOnlyOne = false;
            int iFamilyID = 0;
            int emptyRowCount = 0;

            for (int a = 0; a < dataGridViewSelectedRow.Count; a++)
            {
                var familyNum = dataGridViewSelectedRow[a].Cells[GetColumn(DATA_FIELD.FAMILY_NUMBER)].Value;
                if (familyNum != null && !string.IsNullOrWhiteSpace(familyNum.ToString()))
                {
                    int newFamilyNum = Convert.ToInt32(familyNum);
                    if (iFamilyID == 0)
                    {
                        iFamilyID = newFamilyNum;
                        isFamilyNumberOnlyOne = true;
                    }
                    else if (iFamilyID != newFamilyNum)
                        isFamilyNumberOnlyOne = false; //또다른 새로운 가족 ID가 들어오려고 하면, 무효화시킨다...대표 가족 ID는 하나만 가능해야 한다!
                }
                else
                    emptyRowCount++;
            }
            //가족번호가 없거나, 또는 하나만 있는 경우, 그리고 가족 번호가 빈 멤버가 하나 이상 있는 경우는 가족 합치기를 허용한다. 
            return (isFamilyNumberOnlyOne || iFamilyID == 0) && emptyRowCount > 0;
        }

        private void MiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            List<Believer> clieckedBelieversData = (List<Believer>)((ContextMenuStrip)sender).Tag;
            switch (e.ClickedItem.Name)
            {
                case "Modify":
                    this.UserDataModify(clieckedBelieversData[0]);                    
                    break;
                case "SendSMS":
                    this.SendSMSToUser(clieckedBelieversData[0]);
                    break;
                case "FamilyInfo":
                    this.UserFamilyInfo(clieckedBelieversData[0]);
                    break;
                case "FamilyUnion":
                    this.UnifyAsFamily(clieckedBelieversData);
                    break;
                case "Delete":
                    this.DeleteUserData(clieckedBelieversData);                    
                    break;
            }
        }
        private void UserDataModify(Believer believerData)
        {   
            var userInfoModifyForm = new UserInfoModifyForm { BelieverInfo = believerData };            
            if (userInfoModifyForm.ShowDialog() == DialogResult.OK)
            {
                RefreshBelieverListInDataGridView();
                userInfoModifyForm.Close();
            }
        }

        private void SendSMSToUser(Believer believerData)
        {
            if (believerData != null)
            {
                var smsSendForm = new SMSSendingForm(believerData);
                if (smsSendForm.ShowDialog() == DialogResult.OK)
                    smsSendForm.Close();
            }
        }

        private void UserFamilyInfo(Believer believerData)
        {            
            var userFamilyInfo = new UserFamilyForm(believerData.UID);
            var dialogResult = userFamilyInfo.ShowDialog();
            RefreshBelieverListInDataGridView();
        }
        private void UnifyAsFamily(List<Believer> familyBelieverList)
        {
            var familyMembersIdList = new List<int>();
            var membersWithoutFamilyID = new List<Believer>();
            string familyUnionMessage = string.Empty;
            for (int i = 0; i < familyBelieverList.Count; i++)
            {
                int believerUID = familyBelieverList[i].UID;                
                familyMembersIdList.Add(believerUID);
                familyUnionMessage += familyBelieverList[i].Name + " ";

                if (familyBelieverList[i].FamilyID == 0)
                    membersWithoutFamilyID.Add(familyBelieverList[i]);
            }
            familyUnionMessage += "신도 분들을 하나의 같은 가족으로 통합하시겠습니까?";
            if (MessageBox.Show(familyUnionMessage, "가족으로 통합하기", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                //여기에서 familyMembersIdList를 이용해서 선택된 신도들을 하나의 가족으로 통합한다.
                int unionFamilyID = SqlManager.GetFamilyIDAmongBelieversInSQL(familyMembersIdList.ToArray());
                foreach(var believerInfo in membersWithoutFamilyID)
                {                    
                    var familyMemberInfo = new FamilyMember
                    {
                        Name = believerInfo.Name,
                        BirthDate = believerInfo.BirthDate,
                        Ganzhi = believerInfo.Ganzhi,
                        Remarks = "",
                        IsBeliever = true,
                    };
                    SqlManager.InsertFamilyMemberInSQL(unionFamilyID, familyMemberInfo, out int insertedMemberID);
                    SqlManager.SetNewFamilyMemberIDInSQL(believerInfo.UID, unionFamilyID, insertedMemberID);
                }
                var userFamilyInfo = new UserFamilyForm(familyMembersIdList[0]);
                var dialogClose = userFamilyInfo.ShowDialog();
                RefreshBelieverListInDataGridView();
            }
        }

        private void DeleteUserData(List<Believer> deleteBelieverList)
        {
            var deleteIdList = new List<int>();
            for (int i = 0; i < deleteBelieverList.Count; i++)
            {   
                if (accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
                {
                    //Admin 레벨이 아니라면 자기 분원 외의 신도 정보는 지울 수 없습니다!
                    if ( !accessedMonk.IsBelongedTemple(deleteBelieverList[i].BelongedTemple) )
                    {
                        MessageBox.Show("소속 분원이 다른 신도는 삭제할 수 없습니다. 삭제가 취소됩니다.");
                        return;
                    }
                }
                deleteIdList.Add(deleteBelieverList[i].UID);
            }

            deleteIdList.Sort();
            string deleteMessage = (deleteIdList.Count > 1) ? "다음 ID의 신도들의 정보를 삭제하시겠습니까?" : "다음 ID의 신도 정보를 삭제하시겠습니까?";
            string deleteList = string.Empty;
            for (int j = 0; j < deleteIdList.Count; j++)
            {
                string divider = (deleteIdList.Count > 1 && j != 0) ? ", " : "";
                deleteList += (divider + deleteIdList[j].ToString());
            }

            DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 ID : " + deleteList, "ID 삭제", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                for (int k = 0; k < deleteIdList.Count; k++)
                {
                    SqlManager.DeleteBelieverInSQL(deleteIdList[k]);
                }
                RefreshBelieverListInDataGridView();
            }
        }
        

        private void ComboBoxBelongedTemple_KeyUp(object sender, KeyEventArgs e)
        {            
            if (string.IsNullOrWhiteSpace(comboBoxBelongedTemple.Text))
                return;

            MessageBox.Show("리스트 항목 중에서 선택해주십시오.");
            comboBoxBelongedTemple.ResetText();
            comboBoxBelongedTemple.SelectedItem = manageArea;
        }

        private void ComboBoxProvince_KeyUp(object sender, KeyEventArgs e)
        {            
            if (string.IsNullOrWhiteSpace(comboBoxProvince.Text))
                return;

            MessageBox.Show("리스트 항목 중에서 선택해주십시오.");
            comboBoxProvince.ResetText();
        }

        //Adjust the datagridview row height and font size
        private void DataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {   
                row.MinimumHeight = MainWindow.iDatagridviewRowHeight;
            }
            this.dataGridView1.DefaultCellStyle.Font = new Font("Gulim", MainWindow.iDatagridviewFontSize);
        }

        private void TextBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                RefreshBelieverListInDataGridView();            
        }

        private void TextBoxDharmaname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                RefreshBelieverListInDataGridView();
        }

        private void ComboBoxBelongedTemple_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strTemple = this.comboBoxBelongedTemple.Text;
                if (!string.IsNullOrWhiteSpace(strTemple)
                 && !Enum.TryParse<LocalTemple>(strTemple, out LocalTemple selectedTemple))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshBelieverListInDataGridView();
            }
        }

        private void ComboBoxProvince_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                string strProvince = this.comboBoxProvince.Text;
                if (!string.IsNullOrWhiteSpace(strProvince)
                 && !Enum.TryParse<Province>(strProvince, out Province selectedProvince))
                    MessageBox.Show("입력값이 잘못되었습니다. 가급적 리스트 항목 중에서 선택해주십시오.");
                else
                    this.RefreshBelieverListInDataGridView();
            }
                
        }

        private void ComboBoxGender_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                RefreshBelieverListInDataGridView();
            else
                e.Handled = true;
        }

        private void TextBoxGanzhi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                RefreshBelieverListInDataGridView();
        }

        private void ButtonFamilyList_Click(object sender, EventArgs e)
        {
            new TotalFamilyForm().ShowDialog();
        }

    }
}
