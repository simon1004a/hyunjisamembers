﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class UserPrayUpdateForm : Form
    {
        enum PrayerType { 신도본인명의 = 0, 신도가족명의 = 1, 그외 = 2}

        readonly EditMode editMode;
        int savedBelieverUID = 0;
        FamilyMember registeredFamilyMember = null;

        //To Bind the datagridview to the PrayDonationHistory
        DataTable dataTableDonationHistory;
        //To Bind the datagridview to the PrayRequestHistory
        DataTable dataTableRequestHistory;

        public UserPrayUpdateForm(Pray pray = null)
        {            
            InitializeComponent();
            //지역별로 기도 접수 드리는 부처님이 다르기 때문에!!! SetBuddhaComboBoxByArea 함수를 써야 한다. 2020-03-11
            //this.comboBoxMajesty.DataSource = Enum.GetValues(typeof(MajestyBuddha));
            this.comboBoxPrayTemple.DataSource = Enum.GetValues(typeof(LocalTemple));            
            this.comboBoxPrayClass.DataSource = Enum.GetValues(typeof(PrayerType));

            dataTableDonationHistory = new DataTable();
            dataTableRequestHistory = new DataTable();

            if (pray == null)
            {
                Pray.SetBuddhaComboBoxByArea(this.comboBoxMajesty, MainWindow.manageArea);
                editMode = EditMode.INSERT_MODE;
                this.comboBoxPrayClass.SelectedIndex = (int)PrayerType.신도본인명의;
                this.CreateEmptyDonationHistory();
                this.CreateEmptyRequestHistory();
            }   
            else
            {
                Pray.SetBuddhaComboBoxByArea(this.comboBoxMajesty, pray.PrayTemple);
                editMode = EditMode.UPDATE_MODE;
                if (pray.FamilyMemberUID != 0)
                    this.comboBoxPrayClass.SelectedIndex = (int)PrayerType.신도가족명의;
                else
                {
                    if ( pray.believerUID != 0
                      && SqlManager.SelectBelieverFromSQL(pray.believerUID).Name == pray.PrayerName)
                        this.comboBoxPrayClass.SelectedIndex = (int)PrayerType.신도본인명의;
                    else
                        this.comboBoxPrayClass.SelectedIndex = (int)PrayerType.그외;
                }
                this.SetPrayInfo(pray);
                this.RefreshDonationHistoryWithSQL(pray.UID);
                this.RefreshRequestHistoryWithSQL(pray.UID);
            }

        }

        //For the insert_mode case
        private void CreateEmptyDonationHistory()
        {
            this.dataTableDonationHistory.Clear();
            this.dataTableDonationHistory.Rows.Clear();
            this.dataTableDonationHistory.Columns.Clear();
                        
            this.dataTableDonationHistory.Columns.Add(new DataColumn
            {
                DataType = System.Type.GetType("System.DateTime"),
                ColumnName = "날짜"
            });

            this.dataTableDonationHistory.Columns.Add(new DataColumn
            {
                DataType = System.Type.GetType("System.Int32"),
                ColumnName = "금액"
            });

            this.dataTableDonationHistory.Columns.Add(new DataColumn
            {                
                DataType = System.Type.GetType("System.String"),
                ColumnName = "비고"
            });

            this.dataGridViewDonation.DataSource = dataTableDonationHistory;

            //SortMode must be disabled to maintain the index in dataTableDonationHistory - 
            for (int i = 0; i < dataGridViewDonation.ColumnCount; i++)
                dataGridViewDonation.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void CreateEmptyRequestHistory()
        {
            this.dataTableRequestHistory.Clear();
            this.dataTableRequestHistory.Rows.Clear();
            this.dataTableRequestHistory.Columns.Clear();

            this.dataTableRequestHistory.Columns.Add(new DataColumn
            {
                DataType = System.Type.GetType("System.DateTime"),
                ColumnName = "날짜"
            });

            // Create second column.            
            this.dataTableRequestHistory.Columns.Add(new DataColumn
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "발원"
            });

            this.dataTableRequestHistory.Columns.Add(new DataColumn
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "비고"
            });

            this.dataGridViewRequest.DataSource = dataTableRequestHistory;

            dataGridViewRequest.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //SortMode must be disabled to maintain the index in dataTableRequestHistory - 
            for (int i = 0; i < dataGridViewRequest.ColumnCount; i++)
            {
                dataGridViewRequest.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                //발원같은 경우는 multi-line으로 해야 전체 내역을 다 읽을 수 있습니다. 
                if (dataGridViewRequest.Columns[i].Name == "발원")
                    dataGridViewRequest.Columns[i].DefaultCellStyle.WrapMode = DataGridViewTriState.True;                
            }
        }


        //For the update_mode case
        private void RefreshDonationHistoryWithSQL(int prayID)
        {
            string query = "SELECT DonationID, PrayID, DateOfDonation as 날짜, Donation as 금액, Remarks as 비고 " +
                "FROM PrayDonationHistory " +
                "WHERE PrayID = " + prayID.ToString() + " ORDER BY DateOfDonation DESC ";
            dataTableDonationHistory = SqlManager.GetDataTableFromSQL(query);

            DataTable fileteredHistory = dataTableDonationHistory.Copy();
            fileteredHistory.Columns.Remove("PrayID");
            fileteredHistory.Columns.Remove("DonationID");

            this.UpdateTotalDonationMoney();
            this.dataGridViewDonation.DataSource = fileteredHistory;

            dataGridViewDonation.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewDonation.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //SortMode must be disabled to maintain the index in dataTableDonationHistory - 
            foreach (DataGridViewColumn column in this.dataGridViewDonation.Columns)
            {
                if (column != null)
                {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    if(column.Name == "비고")
                        column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
            }                
        }

        private void UpdateTotalDonationMoney()
        {
            int donatedTotal = 0;            
            for (int i = 0; i < this.dataTableDonationHistory.Rows.Count ; i++)
            {
                try
                {
                    string donated = dataTableDonationHistory.Rows[i]["금액"].ToString();                    
                    donatedTotal += Convert.ToInt32(donated);
                }
                catch { continue; }
            }
            this.textBoxDonatedTotal.Text = donatedTotal.ToString();
        }

        void UpdateFullPaymentDay()
        {
            //총 수납금이 들어오면 완납일도 다시 계산하자!
            if (Int32.TryParse(this.textBoxDonatedTotal.Text, out int totalAccumulatedMoney))
            {
                var askAutoCalc = MessageBox.Show("완납일을 자동으로 다시 계산하려 합니다. 괜찮다면 OK를 눌러주세요.", "완납일 재계산", MessageBoxButtons.OKCancel);
                if (askAutoCalc == DialogResult.OK)
                {
                    int iPaidMonths = (int)(totalAccumulatedMoney / 100000);
                    this.dateTimePickerFullPay.Value = this.dateTimePickerStartDay.Value.AddMonths(iPaidMonths).AddDays(-1);
                }
            }
        }

        private void RefreshRequestHistoryWithSQL(int prayID)
        {
            string query = "SELECT RequestID, PrayID, DateOfRequest as 날짜, Request as 발원, Remarks as 비고 " +
                "FROM PrayRequestHistory " +
                "WHERE PrayID = " + prayID.ToString() + " ORDER BY DateOfRequest DESC";
            dataTableRequestHistory = SqlManager.GetDataTableFromSQL(query);

            DataTable fileteredRequestDiary = dataTableRequestHistory.Copy();
            fileteredRequestDiary.Columns.Remove("PrayID");
            fileteredRequestDiary.Columns.Remove("RequestID");

            this.dataGridViewRequest.DataSource = fileteredRequestDiary;

            dataGridViewRequest.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewRequest.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //SortMode must be disabled to maintain the index in dataTableRequestHistory - 
            foreach(DataGridViewColumn column in dataGridViewRequest.Columns)
            {
                if(column != null)
                {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    if (column.Name == "발원" || column.Name == "비고")
                        column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
            }                
        }

        private void UserPrayUpdateForm_Load(object sender, EventArgs e)
        {
            if (editMode == EditMode.INSERT_MODE)
            {
                this.comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;
                SetDefaultFinishDay();
            }
            else if(editMode == EditMode.UPDATE_MODE)
            {
                UpdateCheckBoxPrayFinishText();                
            }                
        }

        void SetDefaultFinishDay()
        {
            //회향일은 특별히 지정하지 않는다면 100일 기도로 합니다. 
            //시작 날로부터 4달을 잡습니다.
            this.dateTimePickerFinishDay.Value = this.dateTimePickerStartDay.Value.AddMonths(4).AddDays(-1);
        }

        public void SelectBelieverID(int believerID)
        {
            this.savedBelieverUID = believerID;
            if (savedBelieverUID == 0)
            {
                this.textBoxBelieverName.ResetText();                                
                this.textBoxFamilyMemberName.ResetText();
                this.textBoxPrayerRelation.ResetText();
            }
            else
            {
                try
                {
                    var believer = SqlManager.SelectBelieverFromSQL(believerID);
                    this.textBoxBelieverName.Text = believer.Name;

                    //입금자는 신도명으로 일단 정하고, 연락처도 일단 그렇게 한다. 
                    if(string.IsNullOrWhiteSpace(textBoxDonator.Text))
                        this.textBoxDonator.Text = believer.Name;
                    if (string.IsNullOrWhiteSpace(textBoxPrayerPhone.Text))
                        this.textBoxPrayerPhone.Text = believer.Phone;

                    if ((PrayerType)comboBoxPrayClass.SelectedItem == PrayerType.신도본인명의)
                    {
                        this.textBoxPrayerName.Text = believer.Name;
                        this.textBoxPrayerBirthYear.Text = believer.Ganzhi;
                        this.textBoxPrayerAddress.Text = believer.AddressProvince.ToString() + " " + believer.AddressDetail;
                    }
                    else if ((PrayerType)comboBoxPrayClass.SelectedItem == PrayerType.신도가족명의)
                    {
                        if (string.IsNullOrWhiteSpace(textBoxPrayerAddress.Text))
                            this.textBoxPrayerAddress.Text = believer.AddressProvince.ToString() + " " + believer.AddressDetail;
                    }                    

                    //신도가 새로 지정되었으니 가족 정보는 초기화해야 한다. 
                    if((PrayerType)comboBoxPrayClass.SelectedItem == PrayerType.신도본인명의)
                    {
                        this.textBoxFamilyMemberName.Text = believer.Name;
                        this.textBoxPrayerRelation.Text = "본인";
                    }
                    else
                    {
                        this.textBoxFamilyMemberName.ResetText();
                        this.textBoxPrayerRelation.ResetText();
                    }

                }
                catch { MessageBox.Show("신도 정보가 올바르지 않습니다. 다시 입력해주세요."); }
            }
        }

        public void SetFamilyMemberInfo(FamilyMember memberInfo)
        {   
            this.registeredFamilyMember = memberInfo;
            if (memberInfo != null)
            {
                this.textBoxFamilyMemberName.Text = memberInfo.Name;
                this.textBoxPrayerRelation.Text = memberInfo.FamilyRelation;

                this.textBoxPrayerName.Text = memberInfo.Name;
                this.textBoxPrayerBirthYear.Text = memberInfo.Ganzhi;                
            }
        }

        private void SetPrayInfo(Pray _pray)
        {
            this.textBoxID.Text = _pray.UID.ToString();
            this.textBoxPrayPaperNumber.Text = _pray.PrayPaperNumber.ToString();
            this.comboBoxPrayTemple.SelectedItem = _pray.PrayTemple;
            this.comboBoxMajesty.SelectedItem = _pray.buddha.ToString();
            this.SelectBelieverID(_pray.believerUID);
            this.SetFamilyMemberInfo(SqlManager.SelectFamilyMemberFromSQL(_pray.FamilyMemberUID));

            this.textBoxPrayerRelation.Text = _pray.PrayerRelation;
            this.textBoxPrayerName.Text = _pray.PrayerName;
            this.textBoxPrayerBirthYear.Text = _pray.PrayerBirthYear;
            this.textBoxPrayerAddress.Text = _pray.PrayerAddress;
            this.textBoxPrayerPhone.Text = _pray.PrayerPhone;
            this.textBoxDonator.Text = _pray.Donator;

            this.dateTimePickerStartDay.Value = _pray.DateOfStart;
            this.dateTimePickerFinishDay.Value = _pray.DateOfFinish;
            this.dateTimePickerFullPay.Value = _pray.DateOfFullPay;
            this.checkBoxFixFinishDay.Checked = _pray.IsFinishConfirmed;
            this.textBoxRemarks.Text = _pray.Remarks;
            this.checkBoxPrayFinish.Checked = _pray.IsPrayFinished;
        }

        public Pray GetPrayInfo()
        {
            var _UID = (int.TryParse(textBoxID.Text, out int prayID)) ? prayID : 0;
            var _PrayPaperNumber = (int.TryParse(textBoxPrayPaperNumber.Text, out int prayPaperId)) ? prayPaperId : 0;
            var _buddha = (MajestyBuddha)Enum.Parse(typeof(MajestyBuddha), comboBoxMajesty.SelectedItem.ToString());
            var _PrayTemple = (LocalTemple)Enum.Parse(typeof(LocalTemple), comboBoxPrayTemple.SelectedItem.ToString());
            var _believerUID = savedBelieverUID;
            var _FamilyMemberUID = (registeredFamilyMember != null) ? registeredFamilyMember.MemberUID : 0;
            var _PrayerName = textBoxPrayerName.Text;
            var _PrayerBirthYear = textBoxPrayerBirthYear.Text;
            var _PrayerPhone = textBoxPrayerPhone.Text;
            var _PrayerAddress = textBoxPrayerAddress.Text;
            var _PrayerRelation = textBoxPrayerRelation.Text;
            var _Donator = textBoxDonator.Text;
            var _DateOfStart = dateTimePickerStartDay.Value;
            var _DateOfFinish = dateTimePickerFinishDay.Value;
            var _DateOfFullPay = dateTimePickerFullPay.Value;
            var _IsFinishConfirmed = checkBoxFixFinishDay.Checked;
            var _Remarks = textBoxRemarks.Text;
            var _IsPrayFinished = checkBoxPrayFinish.Checked;

            var pray = new Pray
            {
                UID = _UID,
                PrayPaperNumber = _PrayPaperNumber,
                buddha = _buddha,
                PrayTemple = _PrayTemple,
                believerUID = _believerUID,
                FamilyMemberUID = _FamilyMemberUID,
                PrayerName = _PrayerName,
                PrayerBirthYear = _PrayerBirthYear,
                PrayerPhone = _PrayerPhone,
                PrayerAddress = _PrayerAddress,
                PrayerRelation = _PrayerRelation,
                Donator = _Donator,
                DateOfStart = _DateOfStart,
                DateOfFinish = _DateOfFinish,
                DateOfFullPay = _DateOfFullPay,
                IsFinishConfirmed = _IsFinishConfirmed,
                Remarks = _Remarks,
                IsPrayFinished = _IsPrayFinished,
            };
            return pray;
        }


        
        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.textBoxPrayerName.Text))
            {
                MessageBox.Show("입재자가 입력되지 않았습니다. ");
                return;
            }
            else if ((LocalTemple)this.comboBoxPrayTemple.SelectedItem == LocalTemple.미지정)
            {   
                MessageBox.Show("접수 분원이 지정되지 않았습니다. ");
                return;
            }
            else if (this.dataTableRequestHistory.Rows.Count <= 0)
            {
                MessageBox.Show("발원이 입력되지 않았습니다. ");
                return;
            }

            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //----------accessedMonk auth check part----------//            
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple((LocalTemple)comboBoxPrayTemple.SelectedItem) )
                {
                    MessageBox.Show("다른 분원의 기도 정보는 입력할 수 없습니다.");
                    return;
                }
            }

            if (this.editMode == EditMode.INSERT_MODE)
            {
                if (SqlManager.InsertPrayToSQL(GetPrayInfo(), out int insertedPrayID))
                {
                    for (int i = 0; i < dataTableDonationHistory.Rows.Count; i++)
                    {
                        DataRow rowDonation = dataTableDonationHistory.Rows[i];
                        try
                        {
                            DateTime newDate = (DateTime)rowDonation[0];
                            int newDonation = (int)rowDonation[1];
                            string newRemarks = (rowDonation[2] != null) ? rowDonation[2].ToString():string.Empty; 

                            string insertQuery = "INSERT INTO PrayDonationHistory (PrayID, DateOfDonation, donation, remarks) " +
                                "VALUES( " + insertedPrayID + ", '" + newDate.ToShortDateString() + "', "
                                + newDonation + ",'" + newRemarks + "')";
                            SqlManager.ExecuteQueryOnSQL(insertQuery);
                        }
                        catch { continue; }
                    }

                    for (int j = 0; j < dataTableRequestHistory.Rows.Count; j++)
                    {
                        DataRow rowRequest = dataTableRequestHistory.Rows[j];
                        try
                        {
                            DateTime newDate = (DateTime)rowRequest[0];
                            string newRequest = rowRequest[1].ToString();
                            string newRemarks = (rowRequest[2] != null) ? rowRequest[2].ToString() : string.Empty;

                            string insertQuery = "INSERT INTO PrayRequestHistory (PrayID, DateOfRequest, Request, Remarks) " +
                                "VALUES( " + insertedPrayID + ", '" + newDate.ToShortDateString() + "', '" 
                                + newRequest + "','" + newRemarks + "')";
                            SqlManager.ExecuteQueryOnSQL(insertQuery);
                        }
                        catch { continue; }
                    }
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if (SqlManager.UpdatePrayInSQL(GetPrayInfo()))
                    this.DialogResult = DialogResult.OK;                
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonBelieverSearch_Click(object sender, EventArgs e)
        {
            var believerSearchForm = new BelieverSearchForm();
            believerSearchForm.EventSelectBelieverID += this.SelectBelieverID;
            if ( believerSearchForm.ShowDialog() == DialogResult.OK )
            {
                believerSearchForm.Close();
            }
        }

        private void DataGridViewDonation_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.dataGridViewDonation.CommitEdit(DataGridViewDataErrorContexts.Commit);
            //To prevent the error of "reentrant call to setCurrentCellAddressCore"
            this.BeginInvoke(new MethodInvoker(() =>
            {
                UpdatePrayDonationHistory(e.RowIndex);
            }));
        }

        private void UpdatePrayDonationHistory(int rowIndex)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //And two values must be cast-able to its original type.
            DateTime editedDate;
            int editedDonation;
            string editedRemarks = string.Empty;
            try
            {
                editedDate = (DateTime)this.dataGridViewDonation.Rows[rowIndex].Cells[0].Value;
                editedDonation = (int)this.dataGridViewDonation.Rows[rowIndex].Cells[1].Value;
                if (this.dataGridViewDonation.Rows[rowIndex].Cells[2].Value != null)
                    editedRemarks = this.dataGridViewDonation.Rows[rowIndex].Cells[2].Value.ToString();
            }
            catch
            {
                Console.WriteLine("Failed to convert the date and donation...");
                return;
            }

            //CellEdit가 마칠 때에 DonationHistory를 업데이트하는 것은 Update 모드에서만 합니다. - INSERT 모드는 ButtonUpdate_Click에서 처리합니다. 
            if (this.editMode == EditMode.UPDATE_MODE)
            {
                if (!int.TryParse(textBoxID.Text, out int prayID))
                {
                    Console.WriteLine("prayID is wrong...");
                    return;
                }

                if (rowIndex < dataTableDonationHistory.Rows.Count)
                {
                    DataRow editedRow = dataTableDonationHistory.Rows[rowIndex];
                    var prayDonationHistoryID = editedRow[0];
                    string updateQuery = "UPDATE PrayDonationHistory SET DateOfDonation = '" + editedDate.ToShortDateString() + "',"
                        + " donation = " + editedDonation.ToString() + "," 
                        + " Remarks = " + "'" +  editedRemarks + "'"
                        + " WHERE DonationID = " + prayDonationHistoryID.ToString();
                    SqlManager.ExecuteQueryOnSQL(updateQuery);
                }
                else
                {
                    string insertQuery = "INSERT INTO PrayDonationHistory (PrayID, DateOfDonation, donation) " +
                        "VALUES( " + prayID + ", '" + editedDate.ToShortDateString()
                        + "', " + editedDonation.ToString()
                        + ",'" + editedRemarks + "')";
                    SqlManager.ExecuteQueryOnSQL(insertQuery);
                }
                this.RefreshDonationHistoryWithSQL(prayID);
            }            
        }

        private void DataGridViewDonation_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridViewDonation.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridViewDonation.RowCount - 1)
                {
                    if (dataGridViewDonation.SelectedRows.Count == 0)
                        dataGridViewDonation.Rows[iClickedRow].Selected = true;

                    DateTime selectedDate;
                    int selectedDonation;
                    try
                    {
                        selectedDate = (DateTime)this.dataGridViewDonation.Rows[iClickedRow].Cells[0].Value;
                        selectedDonation = (int)this.dataGridViewDonation.Rows[iClickedRow].Cells[1].Value;
                    }
                    catch
                    {
                        Console.WriteLine("Failed to convert the date and donation...");
                        return;
                    }

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridViewDonation, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += DonationMiniMenu_ItemClicked;
                }
            }
        }
        private void DonationMiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }
            switch (e.ClickedItem.Name)
            {   
                case "Delete":
                    var selectedRows = new List<DataGridViewRow>();
                    foreach (DataGridViewCell cell in dataGridViewDonation.SelectedCells)
                    {
                        DataGridViewRow cellRow = cell.OwningRow;
                        if (!selectedRows.Contains(cellRow))
                            selectedRows.Add(cell.OwningRow);
                    }

                    if (editMode == EditMode.UPDATE_MODE)
                    {
                        if (!int.TryParse(textBoxID.Text, out int prayID))
                        {
                            Console.WriteLine("prayID is wrong...");
                            return;
                        }

                        var deleteIdList = new List<int>();
                        var deleteDateList = new List<string>();

                        foreach(DataGridViewRow rowSelected in selectedRows)
                        {
                            int iSelectedRowIndex = rowSelected.Index;
                            DataRow editedRow = dataTableDonationHistory.Rows[iSelectedRowIndex];
                            int prayDonationHistoryID = (int)editedRow[0];
                            string prayDonationDate = Convert.ToDateTime(editedRow[2].ToString()).ToShortDateString();
                            deleteIdList.Add(prayDonationHistoryID);
                            deleteDateList.Add(prayDonationDate);
                        }

                        string deleteMessage = "다음 날짜의 납입 기록을 삭제하시겠습니까?";
                        string deleteList = string.Empty;
                        for (int j = 0; j < deleteDateList.Count; j++)
                        {
                            string divider = (deleteDateList.Count > 1 && j != 0) ? ", " : "";
                            deleteList += (divider + deleteDateList[j].ToString());
                        }

                        DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 날짜 : " + deleteList, "납입 기록 삭제", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            for (int k = 0; k < deleteIdList.Count; k++)
                            {
                                string deleteQuery = "DELETE FROM PrayDonationHistory WHERE DonationID = " + deleteIdList[k];
                                SqlManager.ExecuteQueryOnSQL(deleteQuery);
                            }
                            this.RefreshDonationHistoryWithSQL(prayID);
                            this.UpdateFullPaymentDay();
                        }
                    }
                    else
                    {
                        var selectedRowIndexes = new List<int>();
                        foreach (DataGridViewRow row in selectedRows)
                            selectedRowIndexes.Add(row.Index);

                        selectedRowIndexes.Sort();
                        for (int i = selectedRowIndexes.Count - 1; i >= 0 ; i--)
                        {
                            int iSelectedRowIndex = selectedRowIndexes[i];
                            this.dataTableDonationHistory.Rows.RemoveAt(iSelectedRowIndex);
                        }
                        this.dataGridViewDonation.DataSource = this.dataTableDonationHistory;
                    }
                    break;
            }
        }

        private void dataGridViewRequest_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.dataGridViewRequest.CommitEdit(DataGridViewDataErrorContexts.Commit);
            //To prevent the error of "reentrant call to setCurrentCellAddressCore"
            this.BeginInvoke(new MethodInvoker(() =>
            {
                UpdatePrayRequestHistory(e.RowIndex);
            }));

        }

        private void UpdatePrayRequestHistory(int rowIndex)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //And two values must be cast-able to its original type.
            DateTime editedDate;
            string editedRequest = string.Empty;
            string editedRemarks = string.Empty;
            try
            {
                editedDate = (DateTime)this.dataGridViewRequest.Rows[rowIndex].Cells[0].Value;
                if (this.dataGridViewRequest.Rows[rowIndex].Cells[1].Value != null)
                    editedRequest = this.dataGridViewRequest.Rows[rowIndex].Cells[1].Value.ToString();
                if (this.dataGridViewRequest.Rows[rowIndex].Cells[2].Value != null)
                    editedRemarks = this.dataGridViewRequest.Rows[rowIndex].Cells[2].Value.ToString();
            }
            catch
            {
                Console.WriteLine("Failed to get the date and request...");
                return;
            }

            //CellEdit가 마칠 때에 RequestHistory를 업데이트하는 것은 Update 모드에서만 합니다. - INSERT 모드는 ButtonUpdate_Click에서 처리합니다. 
            if (this.editMode == EditMode.UPDATE_MODE)
            {
                if (!int.TryParse(textBoxID.Text, out int prayID))
                {
                    Console.WriteLine("prayID is wrong...");
                    return;
                }

                if (rowIndex < dataTableRequestHistory.Rows.Count)
                {
                    DataRow editedRow = dataTableRequestHistory.Rows[rowIndex];
                    var prayRequestHistoryID = editedRow[0];
                    string updateQuery = "UPDATE PrayRequestHistory SET DateOfRequest = '" + editedDate.ToShortDateString() + "',"
                        + " Request = " + "'" + editedRequest.ToString() + "',"
                        + " Remarks = " + "'" + editedRemarks + "'"
                        + " WHERE RequestID = " + prayRequestHistoryID.ToString();
                    SqlManager.ExecuteQueryOnSQL(updateQuery);
                }
                else
                {
                    string insertQuery = "INSERT INTO PrayRequestHistory (PrayID, DateOfRequest, Request, Remarks) " +
                        "VALUES( " + prayID + ", '" + editedDate.ToShortDateString()
                        + "', '" + editedRequest.ToString()
                        + "', '" + editedRemarks + "')";
                    SqlManager.ExecuteQueryOnSQL(insertQuery);
                }
                this.RefreshRequestHistoryWithSQL(prayID);
            }
        }

        private void DataGridViewRequest_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int iClickedRow = dataGridViewRequest.HitTest(e.X, e.Y).RowIndex;
                if (iClickedRow >= 0 && iClickedRow < this.dataGridViewRequest.RowCount - 1)
                {
                    if (dataGridViewRequest.SelectedRows.Count == 0)
                        dataGridViewRequest.Rows[iClickedRow].Selected = true;

                    DateTime selectedDate;
                    String selectedRequest;
                    try
                    {
                        selectedDate = (DateTime)this.dataGridViewRequest.Rows[iClickedRow].Cells[0].Value;
                        selectedRequest = this.dataGridViewRequest.Rows[iClickedRow].Cells[1].Value.ToString();
                    }
                    catch
                    {
                        Console.WriteLine("Failed to convert the date and donation...");
                        return;
                    }

                    ContextMenuStrip miniMenu = new ContextMenuStrip();
                    miniMenu.Items.Add("삭제하기").Name = "Delete";
                    miniMenu.Show(dataGridViewRequest, new Point(e.X, e.Y));

                    miniMenu.ItemClicked += RequestMiniMenu_ItemClicked;
                }
            }

        }
        private void RequestMiniMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            switch (e.ClickedItem.Name)
            {
                case "Delete":
                    var selectedRows = new List<DataGridViewRow>();
                    foreach (DataGridViewCell cell in dataGridViewRequest.SelectedCells)
                    {
                        DataGridViewRow cellRow = cell.OwningRow;
                        if (!selectedRows.Contains(cellRow))
                            selectedRows.Add(cell.OwningRow);
                    }

                    if (editMode == EditMode.UPDATE_MODE)
                    {
                        if (!int.TryParse(textBoxID.Text, out int prayID))
                        {
                            Console.WriteLine("prayID is wrong...");
                            return;
                        }

                        if (dataGridViewRequest.RowCount - 1 <= 1)
                        {
                            MessageBox.Show("발원이 하나만 있는 경우는 삭제할 수 없습니다.");
                            return;
                        }

                        var deleteIdList = new List<int>();
                        var deleteDateList = new List<string>();
                        foreach (DataGridViewRow rowSelected in selectedRows)
                        {
                            int iSelectedRowIndex = rowSelected.Index;
                            DataRow editedRow = dataTableRequestHistory.Rows[iSelectedRowIndex];
                            int prayRequestHistoryID = (int)editedRow[0];
                            string prayRequestDate = Convert.ToDateTime(editedRow[2].ToString()).ToShortDateString();
                            deleteIdList.Add(prayRequestHistoryID);
                            deleteDateList.Add(prayRequestDate);
                        }
                                                
                        string deleteMessage = "다음 날짜의 발원 기록을 삭제하시겠습니까?";
                        string deleteList = string.Empty;
                        for (int j = 0; j < deleteDateList.Count; j++)
                        {
                            string divider = (deleteDateList.Count > 1 && j != 0) ? ", " : "";
                            deleteList += (divider + deleteDateList[j].ToString());
                        }

                        DialogResult dialogResult = MessageBox.Show(deleteMessage + " 삭제될 날짜 : " + deleteList, "발원 기록 삭제", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            for (int k = 0; k < deleteIdList.Count; k++)
                            {
                                string deleteQuery = "DELETE FROM PrayRequestHistory WHERE RequestID = " + deleteIdList[k];
                                SqlManager.ExecuteQueryOnSQL(deleteQuery);
                            }
                            this.RefreshRequestHistoryWithSQL(prayID);
                        }
                    }
                    else
                    {
                        var selectedRowIndexes = new List<int>();
                        foreach (DataGridViewRow row in selectedRows)
                            selectedRowIndexes.Add(row.Index);

                        selectedRowIndexes.Sort();
                        for (int i = selectedRowIndexes.Count - 1; i >= 0; i--)
                        {
                            int iSelectedRowIndex = selectedRowIndexes[i];
                            this.dataGridViewRequest.Rows.RemoveAt(iSelectedRowIndex);
                        }
                        this.dataGridViewRequest.DataSource = this.dataTableRequestHistory;
                    }
                    break;
            }
        }


        private void DateTimePickerStartDay_ValueChanged(object sender, EventArgs e)
        {            
            SetDefaultFinishDay();
        }

        private void ButtonNewRequest_Click(object sender, EventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }

            //----------accessedMonk auth check part----------//
            var prayTemple = (LocalTemple)comboBoxPrayTemple.SelectedItem;
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if ( !MainWindow.accessedMonk.IsBelongedTemple(prayTemple))
                {
                    MessageBox.Show("다른 분원의 기도 정보는 등록할 수 없습니다.");
                    return;
                }
            }

            string strNewRequest, requestRemarks;
            DateTime dateOfNewRequest;
            var newRequestForm = new NewRequestForm();
            var dialogResult = newRequestForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
                return;
            else
            {
                strNewRequest = newRequestForm.NewRequest;
                dateOfNewRequest = newRequestForm.DateOfRequest;
                requestRemarks = newRequestForm.Remarks;
            }

            //Update 모드라면, 바로 새로운 Request를 PrayRequestHistory에 입력하지만 
            //Insert 모드라면 먼저 dataTableRequestHistory에만 두고 그 다음 '확인' 버튼 클릭시에 입력한다. 
            if (editMode == EditMode.UPDATE_MODE)
            {
                //먼저 현재 기도의 PrayID부터 있어야 합니다. 
                if (!int.TryParse(textBoxID.Text, out int prayID))
                {
                    Console.WriteLine("prayID is wrong...");
                    return;
                }
                string insertQuery = "INSERT INTO PrayRequestHistory (PrayID, DateOfRequest, Request, Remarks) " +
                        "VALUES( " + prayID + ", '" + dateOfNewRequest.ToShortDateString()
                        + "', '" + strNewRequest 
                        + "', '" + requestRemarks + "')";
                SqlManager.ExecuteQueryOnSQL(insertQuery);
                this.RefreshRequestHistoryWithSQL(prayID);
            }
            else if(editMode == EditMode.INSERT_MODE)
            {
                DataRow newRow = dataTableRequestHistory.NewRow();
                newRow["날짜"] = dateOfNewRequest;
                newRow["발원"] = strNewRequest;
                newRow["비고"] = requestRemarks;
                dataTableRequestHistory.Rows.Add(newRow);
                this.dataGridViewRequest.DataSource = dataTableRequestHistory;
            }

        }
        
        private void ButtonNewDonation_Click(object sender, EventArgs e)
        {
            if (!MainWindow.accessedMonk.IsBelongedManagePart(ManagePartFlag.기도담당))
            {
                MessageBox.Show("담당 영역이 아니므로 작업을 수행할 수 없습니다. 총괄 관리자에게 문의해주세요.");
                return;
            }
            //----------accessedMonk auth check part----------//
            var prayTemple = (LocalTemple)comboBoxPrayTemple.SelectedItem;
            if (MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자)
            {
                if (!MainWindow.accessedMonk.IsBelongedTemple(prayTemple))
                {
                    MessageBox.Show("다른 분원의 기도 정보는 수정할 수 없습니다.");
                    return;
                }
            }

            int iNewDonation;
            DateTime dateOfNewDonation;
            string donationRemarks;
            var newDonationForm = new NewDonationForm();
            if (newDonationForm.ShowDialog() != DialogResult.OK)
                return;
            else
            {
                iNewDonation = newDonationForm.NewDonation;
                dateOfNewDonation = newDonationForm.DateOfDonation;
                donationRemarks = newDonationForm.DonationRemarks;
            }

            if (editMode == EditMode.UPDATE_MODE)
            {
                //먼저 현재 기도의 PrayID부터 있어야 합니다. 
                if (!int.TryParse(textBoxID.Text, out int prayID))
                {
                    Console.WriteLine("prayID is wrong...");
                    return;
                }
                string insertQuery = "INSERT INTO PrayDonationHistory (PrayID, DateOfDonation, donation, Remarks) " +
                    "VALUES( " + prayID + ", '" + dateOfNewDonation.ToShortDateString() + "', "
                    + iNewDonation.ToString() + ",'" + donationRemarks + "')";
                SqlManager.ExecuteQueryOnSQL(insertQuery);                
                this.RefreshDonationHistoryWithSQL(prayID);
            }
            else if (editMode == EditMode.INSERT_MODE)
            {
                DataRow newRow = dataTableDonationHistory.NewRow();
                newRow["날짜"] = dateOfNewDonation;
                newRow["금액"] = iNewDonation;
                newRow["비고"] = donationRemarks;
                dataTableDonationHistory.Rows.Add(newRow);
                this.dataGridViewDonation.DataSource = dataTableDonationHistory;
                UpdateTotalDonationMoney();
            }
            this.UpdateFullPaymentDay();
        }

        private void ComboBoxPrayTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nowSelectedTemple = (LocalTemple)comboBoxPrayTemple.SelectedItem;
            if ( MainWindow.accessedMonk != null
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.주지스님
              && MainWindow.accessedMonk.LevelOfAccess != AccessLevel.총괄관리자
              && !MainWindow.accessedMonk.IsBelongedTemple(nowSelectedTemple) )
            {                
                if ((LocalTemple)comboBoxPrayTemple.SelectedItem != LocalTemple.미지정)
                    MessageBox.Show("다른 분원의 기도는 입력할 수 없습니다.");

                comboBoxPrayTemple.SelectedItem = MainWindow.manageArea;
                return;
            }

            this.comboBoxMajesty.ResetText();
            Pray.SetBuddhaComboBoxByArea(this.comboBoxMajesty, nowSelectedTemple);
        }

        private void ButtonFamilySearch_Click(object sender, EventArgs e)
        {
            if (this.savedBelieverUID == 0)
            {
                MessageBox.Show("먼저 신도 가족부터 입력해주십시오.");                
                this.textBoxFamilyMemberName.ResetText();
                this.textBoxPrayerRelation.ResetText();
                return;
            }

            var userFamilyInfo = new UserFamilyForm(savedBelieverUID, bFamilyMemberSelectionMode: true);
            if (userFamilyInfo.ShowDialog() == DialogResult.OK)
            {
                this.SetFamilyMemberInfo(userFamilyInfo.selectedFamilyMember);
                userFamilyInfo.Close();
            }
        }

        private void ComboBoxPrayClass_SelectedIndexChanged(object sender, EventArgs e)
        {            
            switch(comboBoxPrayClass.SelectedItem)
            {
                case PrayerType.신도본인명의:
                    //신도 본인 명의로 입재하는 경우!
                    EnableBelieverInputArea(true);
                    EnableFamilyInputArea(false);
                    this.textBoxPrayerRelation.Text = "본인";
                    this.textBoxPrayerName.ReadOnly = true;
                    this.textBoxPrayerBirthYear.ReadOnly = true;
                    this.textBoxPrayerAddress.ReadOnly = true;
                    break;
                case PrayerType.신도가족명의:
                    //신도 가족 명의로 입재하는 경우!
                    EnableBelieverInputArea(true);
                    EnableFamilyInputArea(true);
                    this.textBoxPrayerName.ReadOnly = true;
                    this.textBoxPrayerBirthYear.ReadOnly = true;
                    this.textBoxPrayerAddress.ReadOnly = false;
                    break;
                case PrayerType.그외:
                    //그외 경우
                    EnableBelieverInputArea(true);  //신도 지인일 경우 신도를 입력해야 하므로 풀어준다.
                    EnableFamilyInputArea(false);
                    this.textBoxPrayerName.ReadOnly = false;
                    this.textBoxPrayerBirthYear.ReadOnly = false;
                    this.textBoxPrayerAddress.ReadOnly = false;
                    break;
            }
        }

        private void EnableBelieverInputArea(bool bActive)
        {
            this.buttonBelieverSearch.Enabled = bActive;                        
            this.textBoxBelieverName.Enabled = bActive;
            if (!bActive)
            {
                this.savedBelieverUID = 0;
                this.textBoxBelieverName.ResetText();
            }
        }

        private void EnableFamilyInputArea(bool bActive)
        {
            this.buttonFamilySearch.Enabled = bActive;
            this.textBoxFamilyMemberName.ResetText();
            this.textBoxFamilyMemberName.Enabled = bActive;
            this.textBoxPrayerRelation.ResetText();            
        }

        private void CheckBoxFixFinishDay_CheckedChanged(object sender, EventArgs e)
        {
            this.dateTimePickerFinishDay.Enabled = !this.checkBoxFixFinishDay.Checked;
        }

        private void CheckBoxPrayFinish_Click(object sender, EventArgs e)
        {
            if (this.checkBoxPrayFinish.Checked)
            {
                var confirmCheck = MessageBox.Show("이 기도를 회향 처리하시겠습니까? ", "회향 상태 바꾸기", MessageBoxButtons.YesNo);
                if (confirmCheck != DialogResult.Yes)
                    this.checkBoxPrayFinish.Checked = false;
            }
            else
            {
                var confirmCheck = MessageBox.Show("이 기도를 다시 진행중으로 바꾸시겠습니까? ", "회향 상태 바꾸기", MessageBoxButtons.YesNo);
                if (confirmCheck != DialogResult.Yes)
                    this.checkBoxPrayFinish.Checked = true;
            }

            UpdateCheckBoxPrayFinishText();
        }

        void UpdateCheckBoxPrayFinishText()
        {
            if (checkBoxPrayFinish.Checked)
                this.checkBoxPrayFinish.Text = "회향된 기도";
            else
                this.checkBoxPrayFinish.Text = "회향 처리하기";
        }

        private void TextBoxPrayPaperNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }
                
    }
}
