﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class CeremonyAdminInsertForm : Form
    {
        public CeremonyAdminInsertForm()
        {
            InitializeComponent();
            this.comboBoxTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
        }



        private void TextBoxDateNumber_Enter(object sender, EventArgs e)
        {
            this.textBoxDayNumber.Text = "";
        }

        private void TextBoxDateNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자만 입력되도록 필터링
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))    //숫자와 백스페이스를 제외한 나머지를 바로 처리
            {
                e.Handled = true;
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            int iDayNumber;
            string serviceDayName = this.textBoxName.Text;
            DateTime StartDate = this.dateTimePickerStart.Value.Date;
            DateTime EndDate = this.dateTimePickerEnd.Value.Date;

            if (string.IsNullOrWhiteSpace(serviceDayName))
            {
                MessageBox.Show("재일 이름을 입력해주세요.");
                return;
            }
            else if (!Int32.TryParse(this.textBoxDayNumber.Text, out iDayNumber))
            {
                MessageBox.Show("매달 몇 일의 재일인지 정확히 입력해주세요.");
                return;
            }
            else if (iDayNumber < 1 || iDayNumber > 30)
            {
                MessageBox.Show("재일날 숫자가 범위를 벗어납니다.");
                return;
            }
            else if (StartDate >= EndDate)
            {
                MessageBox.Show("종료일이 개시일보다 더 미래여야 합니다.");
                return;
            }

            string strConfirmMessage = StartDate.ToShortDateString() + "부터 " + EndDate.ToShortDateString() + " 사이의 기간 동안에 "
                + "음력 매달 " + iDayNumber + "일을 " + serviceDayName+ "으로 지정하시겠습니까?";

            var result = MessageBox.Show(strConfirmMessage,"음력 재일 지정하기", MessageBoxButtons.YesNo);
            int iInsertedDaysCount = 0;
            if (result == DialogResult.Yes)
            {
                for (DateTime date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))
                {
                    CustomCalendar.LunarDateConvertFromSolarDate(date, out int lunarYear, out int lunarMonth, out int lunarDay);
                    if (lunarDay == iDayNumber)
                    {
                        var ceremony = new Ceremony
                        {
                            Name = serviceDayName,
                            AssemblyCategory = AssemblyClass.재일,
                            Date = date.Date,
                            Temple = (LocalTemple)this.comboBoxTemple.SelectedItem,
                            StartTime = "09:00",
                            EndTime = "11:30",
                            Remarks = "",
                            Preacher = ""
                        };
                        if (SqlManager.InsertCeremonyToSQL(ceremony))
                            iInsertedDaysCount++;
                    }
                }

                result = MessageBox.Show("총 " + iInsertedDaysCount + "일의 "+ serviceDayName + "이 추가되었습니다.");
                this.DialogResult = DialogResult.OK;
            }

        }
    }
}
