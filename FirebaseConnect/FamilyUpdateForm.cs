﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class FamilyUpdateForm : Form
    {
        private EditMode currentEditMode = EditMode.INSERT_MODE;
        
        public int rootBelieverID;      //현재 신도의 ID - 가족은 바로 이 신도의 가족이 됨.
        private int chosenBelieverID;   //현재의 form에서 가족으로 추가되는데 신도인 경우, 그 신도의 ID
        private int familyID;           
        
        private bool _isBeliever = false;
        public bool IsBeliever
        {
            get { return _isBeliever; }
            set
            {
                _isBeliever = value;
                this.SetInputBoxActive(!_isBeliever);
                if (!_isBeliever)
                {                    
                    this.textBoxName.ResetText();
                    this.dateTimeBirthDate.ResetText();
                    this.textBoxGanzhi.ResetText();
                }
            }
        }
        private FamilyMember _memberInfo = new FamilyMember();
        private FamilyMember MemberInfo
        {
            set
            {
                _memberInfo = value;
                this.IsBeliever = _memberInfo.IsBeliever;
                this.checkBoxBeliever.CheckedChanged -= this.CheckBoxBeliever_CheckedChanged;
                this.checkBoxBeliever.Checked = this.IsBeliever;
                this.checkBoxBeliever.CheckedChanged += this.CheckBoxBeliever_CheckedChanged;

                this.textBoxName.Text = _memberInfo.Name;
                this.dateTimeBirthDate.Value = Convert.ToDateTime(_memberInfo.BirthDate);
                this.textBoxGanzhi.Text = MainWindow.GetGanzhi(Convert.ToDateTime(_memberInfo.BirthDate).Year);
                this.textBoxRelation.Text = _memberInfo.FamilyRelation;
                this.textBoxRemarks.Text = _memberInfo.Remarks;
            }
            get
            {
                _memberInfo.IsBeliever = this.IsBeliever;
                _memberInfo.Name = this.textBoxName.Text;
                _memberInfo.BirthDate = this.dateTimeBirthDate.Value.ToShortDateString();
                _memberInfo.Ganzhi = MainWindow.GetGanzhi(this.dateTimeBirthDate.Value.Year);
                _memberInfo.FamilyRelation = this.textBoxRelation.Text;
                _memberInfo.Remarks = this.textBoxRemarks.Text;
                _memberInfo.FamilyID = this.familyID;
                return _memberInfo;
            }
        }

        //가족란에서 가족항목을 더블클릭했을 때 불리는 constructor.
        public FamilyUpdateForm(int believerUID, FamilyMember familyMember)
        {
            Init();
            currentEditMode = EditMode.UPDATE_MODE;
            this.rootBelieverID = believerUID;
            this.familyID = SqlManager.GetFamilyIDFromSQL(rootBelieverID);
            this.MemberInfo = familyMember;
        }
        //가족 추가시에 불리는 constructor
        public FamilyUpdateForm(int believerUID)
        {
            Init();
            currentEditMode = EditMode.INSERT_MODE;
            this.rootBelieverID = believerUID;
            this.familyID = SqlManager.GetFamilyIDFromSQL(rootBelieverID);           
        }

        private void Init()
        {
            InitializeComponent();            
            this.dateTimeBirthDate.ResetText();
            this.textBoxGanzhi.ResetText();
        }

        private void CreateNewFamilyID()
        {
            //현재의 max FamilyID에서 1을 더해서 새로 FamilyID를 만듭시다. 
            familyID = SqlManager.GetNewFamilyIDFromSQL();
            var believerInfo = SqlManager.SelectBelieverFromSQL(rootBelieverID);
            var birthDay = Convert.ToDateTime(believerInfo.BirthDate);
            var believerMemberInfo = new FamilyMember
            {
                FamilyRelation = "본인",
                Name = believerInfo.Name,
                BirthDate = birthDay.ToShortDateString(),
                Ganzhi = MainWindow.GetGanzhi(birthDay.Year),
                Remarks = "",
                IsBeliever = true,
            };
            SqlManager.InsertFamilyMemberInSQL(familyID, believerMemberInfo, out int insertedMemberID);
            SqlManager.SetNewFamilyMemberIDInSQL(rootBelieverID, familyID, insertedMemberID);         
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.textBoxName.Text))
            {
                MessageBox.Show("이름이 입력되지 않았습니다.");
                return;
            }
            else if (this.dateTimeBirthDate.Value.ToShortDateString() == DateTime.Now.ToShortDateString())
            {
                MessageBox.Show("생년이 지정되지 않았습니다.");
                return;
            }

            if (this.familyID == 0)
            {
                CreateNewFamilyID();
                Console.WriteLine("New FamilyID has been issued!");
            }
                
            if (this.currentEditMode == EditMode.INSERT_MODE)
            {                
                if (SqlManager.InsertFamilyMemberInSQL(familyID, this.MemberInfo, out int newMemberID))
                {
                    if (IsBeliever)
                        SqlManager.SetNewFamilyMemberIDInSQL(chosenBelieverID, familyID, newMemberID);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else
            {
                if(SqlManager.UpdateFamilyMemberInSQL(this.MemberInfo))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void SelectBelieverID(int believerID)
        {
            IsBeliever = false;
            if (believerID > 0)         
            {
                try
                {
                    this.chosenBelieverID = believerID;
                    var believer = SqlManager.SelectBelieverFromSQL(believerID);                    
                    this.textBoxName.Text = believer.Name;
                    this.dateTimeBirthDate.Value = Convert.ToDateTime(believer.BirthDate);
                    this.textBoxGanzhi.Text = MainWindow.GetGanzhi(dateTimeBirthDate.Value.Year);
                    this.textBoxRelation.Text = "본인";
                    IsBeliever = true;
                }
                catch
                {                    
                    MessageBox.Show("신도 정보가 올바르지 않습니다.");                    
                }
            }
        }

        private void SetInputBoxActive(bool bActive)
        {
            this.textBoxName.Enabled = bActive;
            this.dateTimeBirthDate.Enabled = bActive;
        }

        private void CheckBoxBeliever_CheckedChanged(object sender, EventArgs e)
        {
            IsBeliever = this.checkBoxBeliever.Checked;
            if (IsBeliever)
            {
                var believerSearchForm = new BelieverSearchForm();
                believerSearchForm.EventSelectBelieverID += this.SelectBelieverID;
                var dialogResult = believerSearchForm.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    believerSearchForm.Close();                    
                }
                if(string.IsNullOrWhiteSpace(textBoxName.Text))
                    this.checkBoxBeliever.Checked = (IsBeliever = false);
            }
        }

        private void DateTimeBirthDate_ValueChanged(object sender, EventArgs e)
        {
            DateTime birthDate = this.dateTimeBirthDate.Value;
            this.textBoxGanzhi.Text = MainWindow.GetGanzhi(birthDate.Year);
        }

    }
}
