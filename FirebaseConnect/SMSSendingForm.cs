﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HyunjisaBeliever
{
    public partial class SMSSendingForm : Form
    {
        private Believer _receiver;
        public Believer Receiver
        {
            get { return _receiver; }
            set
            {
                _receiver = value;
                this.textBoxReceiverName.Text = _receiver.Name;
                this.textBoxPhoneNumber.Text = _receiver.Phone;
                this.textBoxGanzhi.Text = _receiver.Ganzhi;
            }
        }

        public SMSSendingForm(Believer receiverInfo)
        {
            InitializeComponent();
            comboBoxSender.DataSource = Enum.GetValues(typeof(LocalTemple));

            this.Receiver = receiverInfo;
        }

        public SMSSendingForm(string phone, string name, string ganzhi)
        {
            InitializeComponent();
            comboBoxSender.DataSource = Enum.GetValues(typeof(LocalTemple));

            this.Receiver = new Believer
            {
                Name = name,
                Phone = phone,
                Ganzhi = ganzhi,
            };
        }

        private void SMSSendingForm_Load(object sender, EventArgs e)
        {
            comboBoxSender.SelectedItem = MainWindow.manageArea;

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if ( string.IsNullOrWhiteSpace(this.textBoxMessage.Text) )
            {
                MessageBox.Show("보낼 문자 내용이 없습니다.");
                return;
            }

            bool bSendResult = UserControlSMS.SendTextMessageByOBSI(this.Receiver.Phone, textBoxMessage.Text,
                out string strSendResult,
                UserControlSMS.GetSenderNumber((LocalTemple)this.comboBoxSender.SelectedItem));

            if (bSendResult)
            {
                MessageBox.Show(this.Receiver.Phone + " : 전송에 성공하였습니다." + strSendResult);
                this.DialogResult = DialogResult.OK;
            }                
            else
                MessageBox.Show(this.Receiver.Phone + " : 전송에 실패하였습니다." + strSendResult);

        }

    }
}
