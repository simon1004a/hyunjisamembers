﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HyunjisaBeliever.MainWindow;

namespace HyunjisaBeliever
{
    public partial class AdminManageForm : Form
    {
        DataTable dataTableAdmins = new DataTable();
        bool IsSelectedMonkInfoChanged = false;
        private Monk _SelectedMonkInfo = new Monk();
        public Monk SelectedMonkInfo
        {
            set
            {
                _SelectedMonkInfo = value;
                this.textBoxDharmaName.Text = _SelectedMonkInfo.DharmaName;
                this.textBoxEmail.Text = _SelectedMonkInfo.Email;
                this.textBoxPhone.Text = _SelectedMonkInfo.Phone;
                this.textBoxGender.Text = _SelectedMonkInfo.GenderType.ToString();
                this.textBoxRemarks.Text = _SelectedMonkInfo.Remarks;

                this.comboBoxTemple.SelectedItem = _SelectedMonkInfo.BelongedTemple;
                this.comboBoxTemple2.SelectedItem = _SelectedMonkInfo.BelongedTemple2;
                this.comboBoxAccessLevel.SelectedItem = _SelectedMonkInfo.LevelOfAccess;
                this.SetCheckedListBox(_SelectedMonkInfo.PartOfManage);
            }
            get { return _SelectedMonkInfo; }
        }
        public AdminManageForm()
        {
            InitializeComponent();
            this.comboBoxTemple.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxTemple2.DataSource = Enum.GetValues(typeof(LocalTemple));
            this.comboBoxAccessLevel.DataSource = Enum.GetValues(typeof(AccessLevel));

            //CheckedListBox
            string[] manageParts = System.Enum.GetNames(typeof(ManagePartFlag));
            this.checkedListBoxManagePart.Items.AddRange(manageParts);
            this.checkedListBoxManagePart.CheckOnClick = true;
        }

        private void AdminManageForm_Load(object sender, EventArgs e)
        {
            RefreshAdminListFromSQL();
            //ResetMonkInfo();
            //SelectedMonkInfoSynced();

            int monkID = Convert.ToInt32(this.dataGridView1.Rows[0].Cells["MonkID"].Value);
            GetMonkInfoFromSql(monkID);
        }

        void RefreshAdminListFromSQL()
        {
            int iSelectedIndex = (dataGridView1.SelectedRows.Count > 0) ? dataGridView1.SelectedRows[0].Index : 0;

            this.dataTableAdmins = SqlManager.SelectMonkListFromSQL();
            this.dataGridView1.DataSource = dataTableAdmins;

            //현재 selected row로 돌아오자.
            if (iSelectedIndex != 0 && iSelectedIndex < dataGridView1.Rows.Count)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex = iSelectedIndex;
                dataGridView1.CurrentCell = dataGridView1.Rows[iSelectedIndex].Cells[0];
            }
        }
        private void ResetMonkInfo()
        {
            this.textBoxDharmaName.ResetText();
            this.textBoxEmail.ResetText();
            this.textBoxPhone.ResetText();
            this.textBoxGender.ResetText();
            this.textBoxRemarks.ResetText();

            this.comboBoxTemple.ResetText();
            this.comboBoxTemple2.ResetText();
            this.comboBoxAccessLevel.ResetText();
        }

        void GetMonkInfoFromSql(int iMonkID)
        {
            Monk selectedMonk = null;
            if(iMonkID > 0)
                selectedMonk = SqlManager.SelectMonkFromSQL(iMonkID);

            if (selectedMonk != null)
                this.SelectedMonkInfo = selectedMonk;
            else
                ResetMonkInfo();

            SelectedMonkInfoSynced();
        }
        private void DataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            int iClickedRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
            if (iClickedRow >= 0 && iClickedRow < this.dataGridView1.RowCount - 1)
            {
                DataGridViewRow row = this.dataGridView1.Rows[iClickedRow];
                int monkID = Convert.ToInt32(row.Cells["MonkID"].Value);
                if ( monkID != SelectedMonkInfo.UID )
                    GetMonkInfoFromSql(monkID);
            }
        }
        
        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (IsSelectedMonkInfoChanged)
            {                
                Monk changedMonkInfo = SelectedMonkInfo;
                changedMonkInfo.BelongedTemple = (LocalTemple)this.comboBoxTemple.SelectedItem;
                changedMonkInfo.BelongedTemple2 = (LocalTemple)this.comboBoxTemple2.SelectedItem;
                changedMonkInfo.LevelOfAccess = (AccessLevel)this.comboBoxAccessLevel.SelectedItem;
                changedMonkInfo.PartOfManage = this.GetManagePartsFromCheckedListBox();

                if (SqlManager.UpdateMonkInSQL(changedMonkInfo))
                {
                    SelectedMonkInfo = changedMonkInfo;
                    RefreshAdminListFromSQL();
                    SelectedMonkInfoSynced();
                }
            }
            else
                this.Close();

        }

        private void SelectedMonkInfoSynced()
        {
            IsSelectedMonkInfoChanged = false;
            this.labelNotice.ResetText();
            this.buttonUpdate.Text = "창닫기";
        }

        private void SelectedMonkInfoChanged()
        {
            IsSelectedMonkInfoChanged = true;
            this.labelNotice.Text = SelectedMonkInfo.DharmaName + " 스님의 정보가 변경되었습니다.";
            this.buttonUpdate.Text = "변경 사항 적용하기";
        }

        private void ComboBoxTemple_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SelectedMonkInfo.DharmaName))
            {
                if ((LocalTemple)comboBoxTemple.SelectedItem != SelectedMonkInfo.BelongedTemple)
                    SelectedMonkInfoChanged();
            }            
        }

        private void ComboBoxTemple2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SelectedMonkInfo.DharmaName))
            {
                if ((LocalTemple)comboBoxTemple2.SelectedItem != SelectedMonkInfo.BelongedTemple2)
                    SelectedMonkInfoChanged();
            }            
        }

        private void ComboBoxAccessLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SelectedMonkInfo.DharmaName))
            {
                if ((AccessLevel)comboBoxAccessLevel.SelectedItem != SelectedMonkInfo.LevelOfAccess)
                    SelectedMonkInfoChanged();
            }            
        }

        //update the checkedlistbox state according to ManageParts value of selectedMonk
        private void SetCheckedListBox(Int16 ManageParts)
        {
            for (int i = 0; i < this.checkedListBoxManagePart.Items.Count; i++)
            {
                string strManagePart = checkedListBoxManagePart.Items[i].ToString();
                int iManagePart = (int)(Enum.Parse(typeof(ManagePartFlag), strManagePart));
                bool isFlagContained = ((int)ManageParts & iManagePart) != 0;
                if ( isFlagContained )
                    checkedListBoxManagePart.SetItemChecked(i, true);
                else
                    checkedListBoxManagePart.SetItemChecked(i, false);
            }
        }

        Int16 GetManagePartsFromCheckedListBox()
        {
            Int16 newManageParts = 0;
            foreach (string checkedPart in this.checkedListBoxManagePart.CheckedItems)
            {
                Int16 iManagePart = (Int16)(int)(Enum.Parse(typeof(ManagePartFlag), checkedPart));
                newManageParts |= iManagePart;
            }
            return newManageParts;
        }

        private void CheckedListBoxManagePart_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SelectedMonkInfo.DharmaName))
            {
                MessageBox.Show("관리자가 선택되지 않았습니다. 테이블에서 관리자를 클릭해주세요.");
                return;
            }
            
            if (SelectedMonkInfo.PartOfManage != GetManagePartsFromCheckedListBox())
                SelectedMonkInfoChanged();            
        }


    }
}
