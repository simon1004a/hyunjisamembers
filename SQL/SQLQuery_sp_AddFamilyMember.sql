/****** Object:  StoredProcedure [dbo].[sp_AddFamilyMember]    Script Date: 2019-07-01 오후 2:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[sp_AddFamilyMember]
(
@chosenBelieverId AS INT
,@memberName AS NVARCHAR(50)
,@memberBirthYear AS NVARCHAR(50)
,@memberRelation AS TINYINT
,@memberRemark AS NVARCHAR(MAX)
,@memberBelieverId AS INT = NULL
)
AS
BEGIN

DECLARE @familyId INT
DECLARE @memberId INT

--we need familyID first. if there is not, we need to make a new one
IF EXISTS (SELECT FamilyID FROM BelieverFamily WHERE BelieverID = @chosenBelieverId)
	BEGIN
		--Get FamilyID and MainBelieverID
		SET @familyId = (SELECT top 1 FamilyID FROM BelieverFamily WHERE BelieverID = @chosenBelieverId)
	END
ELSE
	BEGIN
	--if the new member who is about to be added is a believer, @memberBelieverId is not null so should be searched.
	IF @memberBelieverId IS NOT NULL AND EXISTS (SELECT FamilyID FROM BelieverFamily WHERE BelieverID = @memberBelieverId)
		BEGIN
			SET @familyId = (SELECT top 1 FamilyID FROM BelieverFamily WHERE BelieverID = @memberBelieverId)			
			INSERT INTO BelieverFamily(FamilyID, BelieverId, FamilyRelation) VALUES(@familyId, @chosenBelieverId, 1)
		END
	ELSE
		BEGIN
			SET @familyId = (SELECT MAX(FamilyID) FROM BelieverFamily) + 1			
			INSERT INTO BelieverFamily (FamilyID, BelieverId, FamilyRelation) VALUES(@familyId, @chosenBelieverId, 1) 
		END		
	END


--Add new Family Member!
INSERT INTO FamilyMember (Name, YearOfBirth, Remarks) VALUES(@memberName, @memberBirthYear, @memberRemark)
SET @memberId = (SELECT MAX(MemberID) FROM FamilyMember)

--Add to Believer Family too
IF @memberBelieverId = 0
	BEGIN
		INSERT INTO BelieverFamily (FamilyID, BelieverID, MemberID, FamilyRelation) 
		VALUES(@familyId, NULL, @memberId, @memberRelation)
	END
ELSE
	BEGIN
		INSERT INTO BelieverFamily (FamilyID, BelieverID, MemberID, FamilyRelation) 
		VALUES(@familyId, @memberBelieverId, @memberId, @memberRelation)
	END

return @@ROWCOUNT

END





