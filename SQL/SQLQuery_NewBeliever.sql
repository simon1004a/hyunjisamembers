SELECT BelieverId as 신도아이디, BelieverNumber as 신도번호, FamilyID as 가족번호, Name as 이름, DharmaName as 법명, 
P.ProvinceName as 지역, AddressDetail as 상세주소, Phone as 휴대폰, G.GenderName as 성별, T.TempleName as 소속분원,
Ganzhi as 간지, DateOfBirth as 생일, DateOfRegister as 등록일 , Email as 이메일
FROM Believer AS B 
INNER JOIN Gender AS G ON B.Gender = G.GenderID 
INNER JOIN Temples AS T ON B.BelongedTemple = T.TempleID
INNER JOIN Provinces AS P ON B.AddressProvince = P.ProvinceID 
ORDER BY case when BelongedTemple = 1 then - 1 else BelongedTemple end
, AddressProvince, 가족번호

SELECT * FROM Believer

SELECT * FROM Believer WHERE BelongedTemple = 7 AND DateOfRegister BETWEEN '2019-01-01' AND '2019-12-31'
AND BelieverNumber IS NOT NULL AND BelieverNumber != ''