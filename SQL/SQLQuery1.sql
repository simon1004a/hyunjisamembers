--SELECT BelieverID, Name, Address, Phone, Gender, BelongedTemple, DateOfBirth, DateOfRegister FROM Believer
--WHERE BelieverID = 5;

INSERT INTO Believer (Name, AddressDetail, Phone, Gender, BelongedTemple, DateOfBirth, DateOfRegister) 
VALUES('권희재', '서울시 서초구 서초대로 48길 48 은혜빌라 B02호','01035096513', 1, 7, '1984-07-17',GETDATE());

SELECT count(*) FROM Believer
SELECT count(*) FROM BelieverFamily

SELECT count(*) FROM BelieverBackup_190927_1208
SELECT max(BelieverID) FROM Believer
SELECT IDENT_CURRENT('Believer')




SELECT BelieverId as 신도아이디, BelieverNumber as 신도번호, FamilyID as 가족번호, Name as 이름, DharmaName as 법명, 
P.ProvinceName as 지역, AddressDetail as 상세주소, Phone as 휴대폰, G.GenderName as 성별, T.TempleName as 소속분원, 
Ganzhi as 간지, DateOfBirth as 생일, DateOfRegister as 등록일 , Email as 이메일 
FROM Believer AS B 
INNER JOIN Gender AS G ON B.Gender = G.GenderID 
INNER JOIN Temples AS T ON B.BelongedTemple = T.TempleID 
INNER JOIN Provinces AS P ON B.AddressProvince = P.ProvinceID 


--Query to backup the table!
--SELECT * INTO BelieverBackup_190927_1208 FROM Believer

--Restore from backup table!
--INSERT INTO Believer 
--(Name, AddressDetail, Phone, Gender, BelongedTemple, DateOfBirth, DateOfRegister, Remarks, BelieverNumber, Ganzhi, DharmaName, Email, AddressProvince, FamilyMemberID, FamilyID)
--SELECT 
--Name, AddressDetail, Phone, Gender, BelongedTemple, DateOfBirth, DateOfRegister, Remarks, BelieverNumber, Ganzhi, DharmaName, Email, AddressProvince, FamilyMemberID, FamilyID
--FROM BelieverBackup_190927_1208


SELECT * FROM Believer WHERE Name = '남춘식'
WHERE FamilyID is not null



--Tasks to reset believer table!!!
--DELETE FROM PrayRequestHistory
--DBCC CHECKIDENT ('PrayRequestHistory', RESEED, 0)

--DELETE FROM PrayDonationHistory
--DBCC CHECKIDENT ('PrayDonationHistory', RESEED, 0)

--DELETE FROM Pray
--DBCC CHECKIDENT ('Pray', RESEED, 0)

--DELETE FROM Offering
--DBCC CHECKIDENT ('Offering', RESEED, 0)

--DELETE FROM SalvationService
--DBCC CHECKIDENT ('SalvationService', RESEED, 0)

--UPDATE Believer SET FamilyID = NULL
--UPDATE Believer SET FamilyMemberID = NULL

--DELETE FROM BelieverFamily
--DBCC CHECKIDENT ('BelieverFamily', RESEED, 0)

--DELETE FROM CeremonyAttendance
--DBCC CHECKIDENT ('CeremonyAttendance', RESEED, 0)

--DELETE FROM Believer
--DBCC CHECKIDENT ('Believer', RESEED, 0)
