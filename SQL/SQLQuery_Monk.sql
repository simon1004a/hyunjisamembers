﻿
SELECT * FROM AccessLevel ORDER BY LevelID

SELECT MonkID, DharmaName, Email, Phone, GenderName, TempleName, LevelName
FROM Monk AS M
INNER JOIN Gender AS G ON M.Gender = G.GenderID
INNER JOIN Temples AS T ON M.BelongedTemple = T.TempleID
INNER JOIN AccessLevel AS L ON M.LevelOfAccess = L.LevelID

SELECT * FROM Monk

UPDATE Monk SET DharmaName = @dharmaname, Password = @password, Email = @email, Phone = @phone, 
Gender = @gender, BelongedTemple = @belongedTemple, Remarks = @remarks,
LevelOfAccess = @accesslevel, BelongedTemple2 = @belongedTemple2, PartOfManage = @managepart
WHERE MonkID = @monkID

--INSERT INTO Monk (DharmaName, Password, Email, Phone, Gender, BelongedTemple, LevelOfAccess , Remarks)
--VALUES ('법은', '123456','simon@gmail.com', 1, 7, 3)

SELECT M.MonkID, M.DharmaName, M.Password, M.Email, M.Phone, 
M.Gender, M.BelongedTemple, M.BelongedTemple2, M.LevelOfAccess, M.PartOfManage, M.Remarks 
FROM Monk AS M 
INNER JOIN Gender AS G ON M.Gender = G.GenderID 
INNER JOIN Temples AS T ON M.BelongedTemple = T.TempleID 
WHERE Dharmaname = '은명' AND Password = '123456'