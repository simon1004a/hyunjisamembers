SELECT * FROM SalvationService


INSERT INTO SalvationService (believer, DateOfService, Request, Remarks, ServiceTempleID)
VALUES(1, GETDATE(), '천도재 재개되기를 발원합니다', '부산 대법회날에 진행되었습니다. ' ,1)

SELECT S.ServiceID, T.TempleName as 천도재분원, 
B.FamilyID as 가족번호, B.Name AS 복위자, S.DateOfService AS 천도일, S.Request AS 발원, S.Remarks AS 비고 
FROM SalvationService AS S
INNER JOIN Believer AS B ON S.believer = B.BelieverID
INNER JOIN Temples as T ON S.ServiceTempleID = T.TempleID
ORDER BY DateOfService DESC

SELECT O.OfferingID, T.TempleName as 공양분원, B.buddha as 부처님, 
L.FamilyID as 가족번호, L.Name as 입재자, DateOfOffer as 공양일, Request as 발원 --,C.CeremonyName as 해당일정
FROM Offering AS O 
INNER JOIN Buddhas AS B ON O.buddha = B.BuddhaID 
INNER JOIN Believer AS L ON O.believer = L.BelieverID 
INNER JOIN Temples as T ON O.OfferingTempleID = T.TempleID 
--LEFT OUTER JOIN Ceremony AS C ON C.CeremonyID = O.CeremonyID
ORDER BY DateOfOffer DESC

SELECT * FROM Offering