SELECT * FROM Offering


SELECT OfferingID, buddha, OfferingTempleID, DateOfOffer,
believer, OfferorRelation, FamilyMemberID, 
OfferorName, OfferorBirthYear, OfferorAddress, OfferorPhone, donator, 
Request, Participants, Remarks 
FROM Offering WHERE OfferingID = 3


SELECT O.OfferingID, T.TempleName as 공양분원, B.buddha as 부처님, 
L.FamilyID as 가족번호, L.Name as 신청자, OfferorRelation as 관계,
OfferorName as 복위자, OfferorBirthYear as 생년, OfferorAddress as 주소, OfferorPhone as 연락처, donator as 입금자, 
DateOfOffer as 공양일, Request as 발원 
FROM Offering AS O
INNER JOIN Buddhas AS B ON O.buddha = B.BuddhaID
INNER JOIN Temples as T ON O.OfferingTempleID = T.TempleID
LEFT OUTER JOIN Believer AS L ON O.believer = L.BelieverID
WHERE L.Name like '%영수%'
OR OfferorName like '%영수%'
