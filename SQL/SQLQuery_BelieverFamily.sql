SELECT F.MemberID, 
B.FamilyMemberID , case when B.FamilyMemberID IS NULL then 0 else 1 end as 신도여부,
F.Name as 이름, R.RelationName as 관계, BirthDate as 생년, F.Ganzhi as 간지, F.Remarks as 비고
FROM BelieverFamily as F
LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID
INNER JOIN FamilyRelation as R ON F.FamilyRelation = R.FamilyRelationID 
WHERE F.FamilyID = 
(SELECT FamilyID FROM Believer WHERE BelieverID = 160)
ORDER BY 생년

SELECT * FROM BelieverFamily
SELECT * FROM FamilyRelation


SELECT F.MemberID, F.FamilyID, F.FamilyRelation, F.Name, F.BirthDate, F.Ganzhi, F.Remarks,
cast (case when B.FamilyMemberID IS NULL then 0 else 1 end as bit) as 신도여부 
From BelieverFamily as F 
LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID 
WHERE MemberID = 8395


--BeliverFamily Reset하는 쿼리
UPDATE Believer SET FamilyID = NULL, FamilyMemberID = NULL WHERE FamilyID IS NOT NULL
DELETE FROM BelieverFamily
DBCC CHECKIDENT ('BelieverFamily', RESEED, 0)


SELECT F.MemberID, F.FamilyID as 가족번호, 
cast (case when B.FamilyMemberID IS NULL then 0 else 1 end as bit) as 신도여부,
F.Name as 이름, R.RelationName as 관계, BirthDate as 생년, F.Ganzhi as 간지, F.Remarks as 비고 
FROM BelieverFamily as F
LEFT OUTER JOIN Believer as B ON F.MemberID = B.FamilyMemberID
INNER JOIN FamilyRelation as R ON F.FamilyRelation = R.FamilyRelationID
ORDER BY F.FamilyID, BirthDate



--BelieverFamily 백업하고서, 
--BelieverFamily에 Relation 관계 field 새로 추가하는 로직
SELECT * INTO BelieverFamily_200316_1652 FROM BelieverFamily

UPDATE BF SET BF.Relation = FR.RelationName
FROM BelieverFamily BF
INNER JOIN FamilyRelation FR 
ON BF.FamilyRelation = FR.FamilyRelationID

SELECT * FROM BelieverFamily WHERE Relation is null