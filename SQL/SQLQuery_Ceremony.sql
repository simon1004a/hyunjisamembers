SELECT * FROM Ceremony;

--INSERT INTO Ceremony (CeremonyName, AssemblyID, Date, Temple, Preacher, StartTime, EndTime, Remarks)
--VALUES ('약사 재일', 2, '2019-05-09', 6, NULL, '09:00', '12:00', '약사 재일이 이날로 옮겨졌습니다.')

--UPDATE Ceremony SET CeremonyName = '대구 봉축법요식', AssemblyID = 7, Date = '2019-05-09', 
--Temple = 5, StartTime = '09:30', EndTime = '11:30', Remarks = '대구 분원 봉축법요식', Preacher = '정대 스님'
--WHERE CeremonyID = 10

SELECT C.CeremonyName, A.AssemblyName, C.Date, T.TempleName, Preacher, StartTime, EndTime, C.Remarks
FROM Ceremony AS C
INNER JOIN Assembly AS A ON C.AssemblyID = A.AssemblyID
INNER JOIN Temples AS T ON C.Temple = T.TempleID
WHERE C.Date BETWEEN '2019-05-01' AND '2019-05-31'

SELECT * FROM Assembly

select * from Believer

SELECT O.OfferingID, B.buddha, L.Name, DateOfOffer, Request, donation, C.CeremonyName
FROM Offering AS O
INNER JOIN Buddhas AS B ON O.buddha = B.BuddhaID
INNER JOIN Believer AS L ON O.believer = L.BelieverID
LEFT OUTER JOIN Ceremony AS C ON C.CeremonyID = O.CeremonyID

select * from offering 

SELECT OfferingID, buddha, believer, DateOfOffer, Request, donation, CeremonyID
FROM Offering

--INSERT INTO Offering (buddha, believer, DateOfOffer, Request, donation, CeremonyID) 
--VALUES (7, 1, '2019-05-09', '지중한 죄업장 용서하소서.', 100000, 3 )

SELECT * FROM Ceremony
