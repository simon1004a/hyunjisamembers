
SELECT PrayID, PrayTempleID, buddha, believer, FamilyMemberID, 
PrayerName, PrayerBirthYear, PrayerAddress, PrayerPhone, donator, 
DateOfStart, DateOfFinish, DateOfFullPay, IsFinishConfirmed, Remarks
FROM Pray WHERE PrayID = 1

--INSERT INTO Pray (buddha, believer, DateOfRegister, Request, donation, DateOfStart, DateOfFinish)
--VALUES (1,1, GETDATE(), '현지사 정법이 이 땅에 퍼질 수 있도록 도와주십시오...', 100000, '2019-01-01', '2019-12-31' );

SELECT * FROM Pray

--Delete FROM PrayDonationHistory
--Delete FROM PrayRequestHistory
--Delete FROM Pray

--DBCC CHECKIDENT ('PrayDonationHistory', RESEED, 0)
--DBCC CHECKIDENT ('PrayRequestHistory', RESEED, 0)
--DBCC CHECKIDENT ('Pray', RESEED, 0)

SELECT * FROM TaskLog


--INSERT INTO Pray (buddha, PrayTempleID, believer, FamilyMemberID, 
--PrayerName, PrayerBirthYear, PrayerAddress, PrayerPhone, donator, 
--DateOfStart, DateOfFinish, DateOfFullPay, IsFinishConfirmed, Remarks) 
--OUTPUT INSERTED.PrayID 
--VALUES(@buddha, @prayTemple, @believerId, @familyMemberId, 
--@prayerName, @prayerBirthYear, @prayerAddress, @prayerPhone, @donator, 
--@startDate, @finishDate, @fullpayDate, @isFinishConfirmed, @remarks)

--UPDATE Pray SET buddha = @buddha, PrayTempleID = @prayTemple, believer = @believerId, FamilyMemberID = @familyMemberId, 
--PrayerName = @prayerName, PrayerBirthYear = @prayerBirthYear, PrayerAddress = @prayerAddress, PrayerPhone = @prayerPhone, donator = @donator, 
--DateOfStart = @startDate, DateOfFinish = @finishDate, DateOfFullPay = @fullpayDate,IsFinishConfirmed = @isFinishConfirmed, Remarks = @remarks 
--WHERE PrayID = @prayId

SELECT cast (case when 
(SELECT SUM(donation) FROM PrayDonationHistory WHERE PrayID = 8) >= 
(SELECT 100000 * DATEDIFF(MONTH, DateOfStart, GETDATE()) FROM Pray WHERE PrayID = 8)
then 1 else 0 end as bit)  as 완납여부

SELECT * FROM Pray

SELECT DonationID, PrayID, DateOfDonation as 날짜, Donation as 금액 
FROM PrayDonationHistory WHERE PrayID = 1

SELECT RequestID, PrayID, DateOfRequest as 날짜, Request as 발원 
FROM PrayRequestHistory WHERE PrayID = 1

--INSERT INTO PrayRequestHistory (PrayID, DateOfRequest, Request)
--VALUES (21, '2019-07-23', '제가 인연맺은 외도신들도 언젠가 부처님께 귀의하기를 발원합니다.')

SELECT 100000 * DATEDIFF(MONTH, '2019-08-01', DATEADD(day, 1, '2019-11-30')) 
SELECT DATEADD(day, 1, '2019-11-30')
SELECT 38 / 5 AS Integer, 38 % 5 AS Remainder ;  

SELECT * FROM Pray

SELECT PrayID, T.TempleName as 기도분원, B.buddha as 부처님, A.FamilyID as 가족번호, A.Name as 실제입재자, 
case 
	when P.FamilyMemberID IS NULL AND P.PrayerName = A.Name then '본인' 
	when P.FamilyMemberID IS NULL AND P.PrayerName <> A.Name then '지인'
	when P.FamilyMemberID IS NOT NULL then FR.RelationName 
	else NULL
end as 관계,
P.PrayerBirthYear as 생년, P.PrayerName as 입재자, P.donator as 입금자, P.PrayerPhone as 연락처, 
P.DateOfStart as 입재일, P.DateOfFinish as 회향일, P.DateOfFullPay as 완납일, P.IsFinishConfirmed as 회향확정,
P.Remarks as 비고, P.PrayerAddress as 주소, 
(SELECT TOP 1 Request FROM PrayRequestHistory as R WHERE P.PrayID = R.PrayID ORDER BY R.DateOfRequest DESC) as 발원
FROM Pray as P
LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID
INNER JOIN Buddhas as B ON P.buddha = B.BuddhaID
INNER JOIN Temples as T ON P.PrayTempleID = TempleID
LEFT OUTER JOIN BelieverFamily as F ON P.FamilyMemberID = F.MemberID
LEFT OUTER JOIN FamilyRelation as FR ON F.FamilyRelation = FR.FamilyRelationID 
ORDER BY PrayID, 가족번호


SELECT * FROM Pray

-- 본인이 기도하는 경우, PrayerRelation을 본인으로 설정합니다. 
UPDATE P SET P.PrayerRelation = '본인'
FROM Pray as P
LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID
WHERE P.FamilyMemberID is null 
and A.Name = P.PrayerName

-- 가족 기도로 등록한 경우, BelieverFamily에 등록된 가족 관계로 설정합니다. 
UPDATE P SET P.PrayerRelation = FR.RelationName
FROM Pray as P
LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID
INNER JOIN BelieverFamily as BF ON P.FamilyMemberID = BF.MemberID
INNER JOIN FamilyRelation as FR ON BF.FamilyRelation = FR.FamilyRelationID
WHERE P.FamilyMemberID is not null 




SELECT A.Name, P.PrayerName, P.FamilyMemberID, FR.RelationName
FROM Pray as P
LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID
INNER JOIN BelieverFamily as BF ON P.FamilyMemberID = BF.MemberID
INNER JOIN FamilyRelation as FR ON BF.FamilyRelation = FR.FamilyRelationID
WHERE P.FamilyMemberID is not null 





SELECT A.Name, P.PrayerName, P.FamilyMemberID 
FROM Pray as P
LEFT OUTER JOIN Believer as A ON P.believer = A.BelieverID
WHERE P.FamilyMemberID is not null 


